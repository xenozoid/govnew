<?php

    class PageProducerCallback
    {
    	public static function Show()
    	{
            PageProducer::ShowMenuItem('Статистика', 'dashboard', 'chart-pie-separate', OBJECT_DASHBOARD);
            if($_SESSION['user_priv'] == 22) // municipality
            {
                PageProducer::ShowMenuItem('Текущие вопросы', 'orders&mode=5', 'projects', OBJECT_ORDERS);
                PageProducer::ShowMenuItem('На делегировании', 'orders&mode=22', 'arrow-branch', OBJECT_ORDERS);
                PageProducer::ShowMenuItem('Архив', 'orders&mode=41', 'drawer', OBJECT_ORDERS);
            }
            else
            {
                PageProducer::ShowMenuItem('Текущие вопросы', 'orders&mode=1', 'projects', OBJECT_ORDERS);
                PageProducer::ShowMenuItem('На делегировании', 'orders&mode=2', 'arrow-branch', OBJECT_ORDERS);
                PageProducer::ShowMenuItem('В муниципалитете', 'orders&mode=5', 'bank--arrow', OBJECT_ORDERS);
                PageProducer::ShowMenuItem('На доведении в КЦ', 'orders&mode=3', 'arrow-return-180-left', OBJECT_ORDERS);
                PageProducer::ShowMenuItem('Срок истек', 'orders&mode=6', 'clock', OBJECT_ORDERS);
                PageProducer::ShowMenuItem('Архив', 'orders&mode=4', 'drawer', OBJECT_ORDERS);
            }

            PageProducer::ShowMenuItem('База знаний', 'tree', 'folder-tree', OBJECT_ARTICLES);
            PageProducer::ShowMenuItem('База для редактора', 'articles', 'kb', OBJECT_ARTICLES, ACL::UPDATE);
            PageProducer::ShowMenuItem('Новости', 'news', 'newspaper', OBJECT_NEWS);
    	}

        public static function ShowUser($user, $login, $role)
        {
            echo "<strong>".$user."</strong> (".$role.')&nbsp;';
            echo '&nbsp;|&nbsp;<a class=menulink href=logout.php>Выход</a>&nbsp;';
        }

        public static function customShowMenu()
        {
            ?>
                <style>
                    #menuTable {
                        padding: 1pt;
                        background-color:#eee;
                        vertical-align:bottom;
                        width:100%;
                        position:absolute;
                        top:0;
                        left:0;
                        -moz-border-radius: 0;
                        -webkit-border-radius: 0;
                        border-left:0;
                        border-right:0;
                        border-top:0;
                        border-bottom:1px solid silver;
                    }
                </style>
            <?php
            echo '<div style="height:50px;"><table id="menuTable"><tr><td style="width:100%;" colspan="2">';
            PageProducerCallback::Show();
            $ds = new DataSource("select code from object_types
            where group_id > 1 order by weight");
            while($data = $ds->GetString())
            {
                if(ACL::Check($data['code'], ACL::READ))
                {
                    PageProducer::$showMenuDelimeter = false;
                    PageProducer::ShowMenuItem('Справочники', 'service', 'service');
                    break;
                }
            }
            if(!empty($_REQUEST['text']))
                $_SESSION['global_search'] = $_REQUEST['text'];
            echo '<tr><td>';
            $sql = new SqlObject();
            $data = $sql->getline('select name from roles where id = '.$_SESSION['user_priv']);
            $role_name = $data['name'];
            PageProducerCallback::ShowUser($_SESSION['user_name'], $_SESSION['user_login'], $role_name);
            echo '</td><td style="text-align:right; color:grey;"><form style="display:inline" method="get"><input type="hidden" name="module" value="search">Поиск:&nbsp;<nobr><input name="text" value="'.$_SESSION['global_search'].'" style="width:300px;"><label for="kb_only"><input id="kb_only" name="kb_only" type="checkbox" value="1" checked> только в БЗ</label></form></td></tr>';
            echo '</table></div><div style="padding-left:10px;">';
        }
    }

	class UserSelectList extends SelectList
    {
    	public function __construct()
    	{
    		parent::SelectList('select id, name from users order by name', 'name', 'id');
    	}
    }

    class AuthorSelectList extends SelectList
    {
        public function __construct()
        {
            $sql = new SqlObject();
            $sql->sql("CREATE TEMPORARY TABLE tmp_authors SELECT DISTINCT author_id from orders");
            parent::SelectList('select id, name from users where id in (select author_id from tmp_authors) order by name', 'name', 'id');
        }
    }

    class DelegateSelectList extends SelectList
    {
        public function __construct($department_id)
        {
            parent::SelectList('select id, COALESCE(name, " (", post, ")") name from users where
            id in (select user_id from user_department where department_id = '.$department_id.')
            and id != '.(int)$_SESSION['user_id']."
            and email != '' and email is not null
            order by users.name
            ", 'name', 'id');
        }
    }

    class DeputyMayorSelectList extends SelectList
    {
        public function __construct($department_id)
        {
            parent::SelectList("select id, CONCAT(name, ' (', post, ')') name  from users
                where priv = 26
                and email != '' and email is not null
                ", 'name', 'id');
        }
    }

    class RoleSelectList extends SelectList
    {
    	public function __construct()
    	{
    		parent::SelectList('select id, name from roles', 'name', 'id');
    	}
    }

    class SexTypeList extends ValueList
    {
		protected $_values = array(
                                   1 => 'муж.',
                                   2 => 'жен.'
								   );
    }

    class YesNoList extends ValueList
    {
		protected $_values = array(
                                   1 => 'да',
                                   2 => 'нет'
								   );
    }

    class StatusList extends ValueList
    {
		protected $_values = array(
                                   //1 => 'Принят',
                                   5 => 'На обработке',
                                   7 => 'На повторной обработке',
                                   10 => 'Делегирован',
                                   // 11 => 'Делегирован муниципалитету',
                                   //12 => 'Делегирован специалисту муниципалитета',
                                   13 => 'Делегирование окончено',
                                   15 => 'На доведении в контакт-центре',
                                   20 => 'Доведен',
                                   //25 => 'Закрыт'
								   );
    }

    class AlertTypeList extends ValueList
    {
		protected $_values = array(
                                   1 => 'обычная',
                                   2 => 'умеренная',
                                   3 => 'высокая'
								   );

        public function __construct($minValue = 0)
        {
            if(!ACL::Check(OBJECT_ORDERS, 'alertedit'))
            {
                if(ACL::Check(OBJECT_ORDERS, 'alerteditup'))
                {
                    if($minValue)
                        foreach($this->_values as $key => $value)
                            if($key < $minValue)
                                unset($this->_values[$key]);
                }
                else
                {
                    if($minValue)
                        foreach($this->_values as $key => $value)
                            if($key != $minValue)
                                unset($this->_values[$key]);
                }
            }
        }
    }

    class MunicipalitySelectList extends SelectList
    {
        public function __construct($params = array())
        {
            $query = new Query("select");
            $query->AddField("id");
            $query->AddField("name");
            $query->AddTable("departments");
            $query->Where->Add("municipality = 1");
            $query->Order = 'name';

            if(!$params['archive'])
            {
                if($params['value'])
                    $query->Where->Add("archive = 0 or id = " . (int)$params['value']);
                else
                    $query->Where->Add("archive = 0");
            }

            parent::SelectList($query, 'name', 'id');
        }
    }

    class DepartmentSelectList extends SelectList
    {
        public function __construct($params = array())
        {
            $query = new Query("select");
            $query->AddField("id");
            $query->AddField("name");
            $query->AddTable("departments");
            if($params['project_id'])
            {
                $query->Where->Add("id in (select department_id from project_department where project_id = ".(int)$params['project_id'].")");
            }
            $query->Where->Add("municipality = 0");
            $query->Order = 'name';

            if(!$params['archive'])
            {
                if($params['value'])
                    $query->Where->Add("archive = 0 or id = " . (int)$params['value']);
                else
                    $query->Where->Add("archive = 0");
            }

    		parent::SelectList($query, 'name', 'id');
        }
    }

    class DepartmentShortSelectList extends SelectList
    {
        public function __construct($params = array())
        {
            $query = new Query("select");
            $query->AddField("id");
            $query->AddField("shortname");
            $query->AddTable("departments");
            if($params['user_id'])
            {
                if(!ACL::Check(OBJECT_ORDERS, 'allaccess'))
                    $query->Where->Add("id in (select department_id from user_department where user_id = ".(int)$params['user_id'].")");
            }
            else if($params['project_id'])
            {
                $query->Where->Add("id not in (select department_id from project_department where project_id = ".(int)$params['project_id'].")");
            }
            if(!$params['show_muni'])
                $query->Where->Add("municipality = 0");
            $query->Order = 'shortname';

            if(!$params['archive'])
            {
                if($params['value'])
                    $query->Where->Add("archive = 0 or id = " . (int)$params['value']);
                else
                    $query->Where->Add("archive = 0");
            }

    		parent::SelectList($query, 'shortname', 'id');
        }
    }

    class JobSelectList extends SelectList
    {
        public function __construct()
        {
            $sql = new SqlObject();
            $sql->sql("CREATE TEMPORARY TABLE tmp_jobs SELECT id, name from jobs where name not like '%Другое%' order by name");
            $sql->sql("INSERT INTO tmp_jobs SELECT id, name from jobs where name like '%Другое%' order by name");
            parent::SelectList('select id, name from tmp_jobs', 'name', 'id');
        }
    }

    class TopicSelectList extends SelectList
    {
        public function __construct()
        {
            parent::SelectList('select id, name from topics', 'name', 'id');
        }
    }

    class RegionSelectList extends SelectList
    {
        public function __construct()
        {
            $sql = new SqlObject();
            $sql->sql("CREATE TEMPORARY TABLE tmp_regions SELECT id, name from regions where name not like '%Другое%' order by name");
            $sql->sql("INSERT INTO tmp_regions SELECT id, name from regions where name like '%Другое%' order by name");
            parent::SelectList('select id, name from tmp_regions', 'name', 'id');
        }
    }

    class SourceList extends ValueList
    {
		protected $_values = array(
                                   1 => 'телефон',
                                   2 => 'сайт',
                                   3 => 'почта',
                                   4 => 'SMS'
								   );
    }

    class TypeList extends ValueList
    {
		protected $_values = array(
                                   1 => 'Обработанный звонок',
                                   2 => 'Запрос информации',
                                   3 => 'Критическое замечание'
								   );
    }

    class TypeListNew extends ValueList
    {
        protected $_values = array(
            1 => 'Обработанный звонок',
            2 => 'Запрос информации',
            //3 => 'Критическое замечание'
        );
    }

    class ResultTypeList extends ValueList
    {
		protected $_values = array(
                                   1 => 'Поддержано',
                                   2 => 'Не поддержано',
                                   3 => 'Разъяснено'
								   );
    }

    class RatingSearchList extends ValueList
    {
        protected $_values = array(
            5 => 5,
            4 => 4,
            3 => 3,
            2 => 2,
            1 => 1,
            -1 => 'Отказ'
        );
    }

    class RatingList extends ValueList
    {
        protected $_values = array(
            5 => 5,
            4 => 4,
            3 => 3,
            2 => 2,
            1 => 1,
            -1 => 'Отказ'
        );
    }

    class ModuleList extends ValueList
    {
        protected $_values = array(
           'orders' => 'Вопросы'
        );
    }

    class LogsActionSelectList extends ValueList
    {
        protected $_values = array(
            'orders' => 'Вопросы'
        );
    }


    class ArticleGroupSelectList extends SelectList
    {
        public function __construct()
        {
            parent::SelectList('select id, name from articles where is_group = 1 order by name', 'name', 'id');
        }
    }

    class ProjectSelectList extends SelectList
    {
        public function __construct($active = false)
        {
            $query = new Query("select");
            $query->AddField("id");
            $query->AddField("name");
            $query->AddTable("projects");
            if(!ACL::Check(OBJECT_PROJECTS, 'allaccess'))
                $query->Where->Add("id in (select project_id from project_role where role_id = ".(int)$_SESSION['user_priv'].")");
            if($active)
                $query->Where->Add("active = 1");
            $query->Order = 'name';
    		parent::SelectList($query, 'name', 'id');

        }
    }

    class OrderUtils
    {
        public static function getOpenOrdersCount($department_id)
        {
            $ds = new DataSource("select COUNT(*) from orders where status in (5, 10, 13) and
            department_id = ".$department_id);
            $data = $ds->GetString();
            return $data[0];
        }

        private static function getOpenOrdersWithinInterval($time, $department_id, $start, $finish)
        {
            $result = array('count' => 0, 'content' => '');

            $endOfDay = DT::getUnixTime(DT::getDate($time)) + 24*60*60;

            $ds = new DataSource("select id, name from orders where status in (5, 10, 13) and
            created <= ".($endOfDay - (15-($start+1))*24*60*60)."
            and created >= ".($endOfDay - (15-($finish+1))*24*60*60)."
            and department_id = ".$department_id."
            ");
            if($ds->RowCount)
            {
                $result['count'] = $ds->RowCount;
                while($data = $ds->GetString())
                {
                    $result['content'] .= $data['id'].' - '.$data['name'].'<br>';
                }
            }

            return $result;
        }

        public static function getOpenOrders($time, $department_id)
        {
            $result = array();
            $count = OrderUtils::getOpenOrdersCount($department_id);
            if($count)
            {
                //echo 'DEBUG: department '.$department_id.' count: '.$count.'<br>';

                $result['count'] = $count;

                $sql = new SqlObject();
                $dept = $sql->getline("select name from departments where id = ".$department_id);

                $result['content'] = '<br><i>'.$dept['name'].'</i><br>';
                $result['content'] .= 'Всего запросов в работе: '.$count.'<br>';
                $days72 = OrderUtils::getOpenOrdersWithinInterval($time, $department_id, 7, 2);
                if($days72['count'])
                {
                    $result['content'] .= '<strong>- со сроком на ответ 7 дней и менее - '.$days72['count'].'</strong><br>';
                    $result['content'] .= $days72['content'];
                }
                $days20 = OrderUtils::getOpenOrdersWithinInterval($time, $department_id, 2, 0);
                if($days20['count'])
                {
                    $result['content'] .= '<strong>- со сроком на ответ 2 дня и менее - '.$days20['count'].'</strong><br>';
                    $result['content'] .= $days20['content'];
                }
                $days0100 = OrderUtils::getOpenOrdersWithinInterval($time, $department_id, -1, -100);
                if($days0100['count'])
                {
                    $result['content'] .= '<strong>- с пропущенным сроком на ответ - '.$days0100['count'].'</strong><br>';
                    $result['content'] .= $days0100['content'];
                }
            }

            return $result;
        }

        public static function getOrdersReport($time = 0, $departments = array())
        {
            if(!$time)
                $time = time();

            $result['content'] = 'Отчет о состоянии обработки запросов граждан на Прямую линию Правительства Архангельской области
 по состоянию на '.date('d.m.Y', $time).'г.<br>';
            $result['count'] = 0;

            $query = new Query("select");
            $query->AddField('id');
            $query->AddTable('departments');
            if($departments)
                $query->Where->Add("id in (".implode(',', $departments).")");
            $ds = new DataSource($query);
            while($data = $ds->GetString())
            {
                //echo 'DEBUG: department '.$data['id'].'<br>';
                $res = OrderUtils::getOpenOrders($time, $data['id']);
                if($res['count'])
                {
                    $result['content'] .= $res['content'];
                    $result['count']   += (int)$res['count'];
                }
            }

            return $result;
        }
    }

    class SMS
    {
        public static function send($phone, $message)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://sms.firecall.ru/proxy/main.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 'login=gov29&pass=gov29sms&alias=Gov29&reciever_num='.$phone.'&message='.$message);
            curl_exec($ch);
            curl_close($ch);
        }
    }

    class Email
    {
        public static function send($address, $subject, $message, $charset = 'utf-8')
        {
            if((!Registry::Get('mail_user')) || (!Registry::Get('mail_password')) || (!Registry::Get('mail_host')))
                return;

            $from = Registry::Get('mail_sender_name');

            if($charset != 'utf-8')
            {
                $from = iconv('utf8', $charset, $from);
                $subject = iconv('utf8', $charset, $subject);
                $message = iconv('utf8', $charset, $message);
            }

            $mail = new PHPMailer(true);
            try
            {
                $mail->IsSMTP();
                $mail->Host       = Registry::Get('mail_host');
                $mail->SMTPAuth   = true;
                $mail->SMTPSecure = 'ssl';
                $mail->Port       = Registry::Get('mail_port');
                $mail->Username   = Registry::Get('mail_user');
                $mail->Password   = Registry::Get('mail_password');
                $mail->SetFrom(Registry::Get('mail_sender_address'), $from);
                $mail->AddAddress($address);
                $mail->Subject = $subject;
                $mail->CharSet = $charset;
                $mail->MsgHTML($message);
                $mail->Send();
            }
            catch (phpmailerException $e) {
              //echo $e->errorMessage();
            } catch (Exception $e) {
              //echo $e->getMessage();
            }
        }
    }

	class ExtraTypeSelectList extends SelectList
    {
    	public function __construct()
    	{
    		parent::SelectList('select id, name from extratypes order by name', 'name', 'id');
    	}
    }

    class OutgoingPhoneList extends ValueList
    {
        protected $_values = array(
            1 => 'Телефон',
            2 => 'Мобильный',
            3 => 'Внутренний'
        );
    }

// 123