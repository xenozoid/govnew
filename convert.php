<?php

require_once "framework_loader.php";
require_once "classes.php";

$sql = new SqlObject();


$lines = file('./services.txt');
foreach($lines as $line)
{
    $line = trim($line);
    $line = str_replace("\t", ' ', $line);
    $sql->sql("insert into services (name) values ('" . $line . "')");
}









exit;

////////////////////////////////////////////////////


$sql->sql("delete from userfiles");
$sql->sql("delete from logs");
$sql->sql("delete from blacklist");
$sql->sql("delete from comments");
$sql->sql("delete from orders");

$ds = new DataSource("select * from treatments_may11");
while($data = $ds->GetString())
{
    $query = new Query("insert");
    $query->AddTable("orders");
    $query->AddField("id", $data['id']);
    $query->AddField("created", DT::getUnixTime($data['timestamp']));
    $query->AddField("num", $data['phone']);
    $query->AddField("source", 1);
    $query->AddField("project_id", 1);
    $query->AddField("lastname", $data['surename']);
    $query->AddField("firstname", $data['firstname']);
    $query->AddField("middlename", $data['sndname']);
    $query->AddField("sex", ($data['sex'] == 'm')?1:2);
    $query->AddField("age", $data['age']);
    $query->AddField("job_id", "inline:(select id from jobs where name = '".$data['occupation']."')");
    $query->AddField("city", $data['city']);
    $query->AddField("region_id", "inline:(select id from regions where name = '".$data['district']."')");
    $query->AddField("address", $data['address']);
    $query->AddField("phone", $data['phone']);
    $query->AddField("email", $data['email']);
    $query->AddField("name", $data['zag']);
    $query->AddField("question", $data['qtext']);
    $query->AddField("department_id", "inline:(select id from departments where name = (select fullname from pl_codes where code = ".$data['qtheme']."))");
    //$query->AddField("answer", "inline:(select answer from answers where message_id = ".$data['id']." limit 1)");
    if($data['mtype'] == 'processed')
        $type = 1;
    else if($data['mtype'] == 'critical')
        $type = 3;
    else
        $type = 2;
    $query->AddField("type", $type);
    $query->AddField("garbage", $data['trash']);
    $query->AddField("call_id", $data['call_id']);

    $query->Execute();
    $id = $data['id'];

    $answer = $sql->getline("select timestamp, answer from answers where message_id = ".$data['id']);
    if($answer['answer'])
    {
        $query = new Query("update");
        $query->AddTable("orders");
        $query->AddField("status", 20);
        $query->AddField("result", 1);
        $query->AddField("answer", $answer['answer']);
        $query->AddField("days", (int)ceil((DT::getUnixTime($answer['timestamp']) - DT::getUnixTime($data['timestamp'])) / (24*60*60)));
        $query->Where->Add("id = ".$id);
        $query->Execute();
    }
    else
    {
        $query = new Query("update");
        $query->AddTable("orders");
        $query->AddField("status", 5);
        $query->Where->Add("id = ".$id);
        $query->Execute();
    }

    $comment = $sql->getline("select timestamp, ctext from mcomments where messageid = ".$data['id']);
    if($comment['ctext'])
    {
        $query = new Query("insert");
        $query->AddTable("comments");
        $query->AddField("created", DT::getUnixTime($comment['timestamp']));
        $query->AddField("owner_id", $id);
        $query->AddField("module", 'orders');
        $query->AddField("comment", $comment['ctext']);
        $query->AddField('high', 1);
        $query->Execute();
    }
}
