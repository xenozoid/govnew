<?php
	
	require_once('config.php');

	require_once(FRAMEWORK_PATH.'framework_constants.php');

	function __autoload($class_name)
	{
		if(strpos($class_name, 'FormComponent', 1))
			require_once(PLUGINS_PATH.$class_name.'.php');
		else
			require_once(FRAMEWORK_PATH.$class_name.'.php');
	}