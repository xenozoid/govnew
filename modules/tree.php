<?php 
	class Tree extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_ARTICLES;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "База знаний";
			$this->parseHeaders();
            $this->Page->addScript('/scripts/jquery.jstree.js');
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}
?>
    <script>
        $(document).ready(function(){
            $('#tree').jstree({
                core : {
                    
                },
                plugins : [
                    //'themes', 'json_data'
                        "themes","json_data","ui","crrm","dnd","search","types","contextmenu"
                ],
                json_data : {
                    ajax : {
                        url : 'index.php?module=articles&act=getnode&nohead=1',
                        method : 'post',
                        "data" : function (n) {
                            return {
                                "operation" : "get_children",
                                "id" : n.attr ? n.attr("id").replace("node_","") : 0
                            };
                        }
                    }
                },
                themes : {
                    "theme" : "classic"
                },
                types : {
                    types : {
                        item : {
                            icon : {
                                image : "images/item.png"
                            }
                        }
                    }
                }
            })
            .bind("select_node.jstree", function (event, data) {
	            // `data.rslt.obj` is the jquery extended node that was clicked
	            //alert(data.rslt.obj.attr("rel"));
                if(data.rslt.obj.attr("rel") == 'folder')
                {
                    $('#tree').jstree("toggle_node", '#' + (data.rslt.obj.attr("id")));
                }
                else if(data.rslt.obj.attr("rel") == 'item')
                {
                    //window.open('index.php?module=articles&act=view&id=' + data.rslt.obj.attr("id"));
                    $('#article').dialog({
                        modal: true,
                        autoOpen: true,
                        height: 600,
                        width: 800,
                        open: function() {
                            $("#article").load("index.php?module=articles&act=popupview&id=" + data.rslt.obj.attr("id"));
                        }
                    })
                }
	        })
        })
    </script>

    <div id="article"></div>
    <div id="tree">
        <ul>
            <li>
                <a href="#">First</a>
                <ul>

                </ul>
            </li>
            <li>
                <a href="#">Second</a>
            </li>
        </ul>
    </div>
<?php

		}

        public function ajaxAction()
        {
        }
		

		protected function parseHeaders()
		{
		}
	}
