<?php

    class Service extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->Page = $page;
			$this->Page->Title = "Справочники";
            $_SESSION['__active_menu_item'] = 'service';
		}

		public function indexAction()
		{
            $ds = new DataSource("select * from object_types
            where group_id > 1 order by group_id desc, name asc");
            while($data = $ds->GetString())
            {
                if(ACL::Check($data['code'], ACL::READ))
                {
                    if(!empty($data['icon']))
                        echo '<img src="images/'.$data['icon'].'.png" style="vertical-align:bottom"> ';
                    echo '<a href="index.php?module='.$data['module'].'">'.$data['name'].'</a><br><br>';
                }
            }
		}
    }
