<?php 
	class Extratypes extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_EXTRATYPES;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "Типы событий для экстренных служб";
			$this->parseHeaders();
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}
			
			$this->Table = new ExtratypesListTable($this->ObjectType);
			$this->Table->Show();
		}
		
		public function viewAction()
		{
			$this->editAction();
		}
		
		public function newAction()
		{	
			$form = new Form("Новая запись...", array(
				array('caption' => 'Название', 'type' => 'text', 'name' => 'name', 'required' => '1', 'size' => '50'),
				array('caption' => 'Ответственный', 'type' => 'select', 'name' => 'department_id', 'required' => '1', 'data' => new DepartmentSelectList(), 'empty_option' => '(...)'),
				array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Добавить', 'value' => 'Добавить', 'cancel' => 1)	
			));

			$form->Show();
		}
		
		public function editAction()
		{
			$data = $this->getline("select * from extratypes where id = ".$this->Id);
	
			$form = new Form("Редактирование записи...", array(
				array('caption' => 'Название', 'type' => 'text', 'name' => 'name', 'value' => $data['name'], 'required' => '1', 'size' => '50'),
				array('caption' => 'Ответственный', 'type' => 'select', 'name' => 'department_id', 'value' => $data['department_id'], 'required' => '1', 'data' => new DepartmentSelectList(), 'empty_option' => '(...)'),
				array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'id', 'value' => $this->Id),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'upd'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Сохранить', 'value' => 'Сохранить', 'cancel' => 1)	
			));
			
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                $form->SetElementAttribute('*', 'readonly', 1);
				
			$form->Show();
		}
		
		public function addAction()
		{	
			if(!ACL::Check($this->ObjectType, ACL::CREATE))
			{
				$this->Page->Redirect(START_PAGE);
			}
			
			$query = new Query("insert");
			$query->AddTable("extratypes");
			$query->AddField("name", $this->name);
			$query->AddField("department_id", $this->department_id);

			$query->Execute();
			
			LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

		public function updAction()
		{	
			if(ACL::Check($this->ObjectType, ACL::UPDATE))
			{
				$query = new Query("update");
				$query->AddTable("extratypes");
				$query->AddField("name", $this->name);
				$query->AddField("department_id", $this->department_id);

				$query->Where->Add("id = ".$this->Id);
				$query->Execute();
			}
			
			LocationManager::redirect("index.php?module=".$this->ModuleName);
		}		

		public function delAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::DELETE))
			{
				LocationManager::redirect(START_PAGE);
			}			

            $this->sql("delete from extratypes where id = ".$this->Id);

            LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

		protected function parseHeaders()
		{
			$this->Id = $_REQUEST["id"];
			$this->name = $_REQUEST["name"];
			$this->department_id = $_REQUEST["department_id"];
		}	
	}

	class ExtratypesListTable extends Table
	{	
		public function __construct($objectType)
		{
			$this->ObjectType = $objectType;
			
			$query = new Query("select");
			$query->AddTable("extratypes
			left outer join departments on extratypes.department_id = departments.id
			");
			$query->Fields = array("extratypes.id", "extratypes.name", "departments.name");
	
			$captions = array("", "Название", "Ответственный");
	
			parent::Table("Типы событий для экстренных служб",
						  $query,
						  $captions,
						  $query->Fields);

			$this->ShowNewLinks = ACL::Check($this->ObjectType, ACL::CREATE);
			$this->ShowEditLinks = true;
			$this->ShowDeleteLinks = ACL::Check($this->ObjectType, ACL::DELETE);
		}
	}	
	