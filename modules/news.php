<?php 
	class News extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_NEWS;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "Новости";
            unset($_SESSION['__active_menu_item']);
			$this->parseHeaders();
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}
			
			$this->Table = new NewsListTable($this->ObjectType);
			$this->Table->Show();
		}
		
		public function viewAction()
		{
			$this->editAction();
		}
		
		public function newAction()
		{	
			$form = new Form("Новая запись...", array(
				array('caption' => 'Заголовок', 'type' => 'text', 'name' => 'name', 'required' => '1', 'size' => '50'),
				array('caption' => 'Содержание', 'type' => 'textarea', 'name' => 'comment', 'required' => '1', 'width' => '50', 'height' => '5'),
				array('caption' => 'Приложить файл', 'type' => 'file', 'name' => 'userfile[]'),
                array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Добавить', 'value' => 'Добавить', 'cancel' => 1)	
			));

            require_once "userfiles.php";
            Userfiles::show($this->Id, $this->ModuleName, $form);

			$form->Show();
		}
		
		public function editAction()
		{
			$data = $this->getline("select * from news where id = ".$this->Id);
	
			$form = new Form("Редактирование записи...", array(
				array('caption' => 'Добавлено', 'type' => 'label', 'name' => 'created', 'value' => DT::getDateTime($data['created'])),
				array('caption' => 'кем', 'type' => 'select', 'name' => 'author_id', 'value' => $data['author_id'], 'data' => new UserSelectList(), 'readonly' => 1),
				array('caption' => 'Заголовок', 'type' => 'text', 'name' => 'name', 'value' => $data['name'], 'required' => '1', 'size' => '50'),
				array('caption' => 'Содержание', 'type' => 'textarea', 'name' => 'comment', 'value' => $data['comment'], 'required' => '1', 'width' => '50', 'height' => '5'),
				array('caption' => 'Приложить файл', 'type' => 'file', 'name' => 'userfile[]'),
                array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'id', 'value' => $this->Id),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'upd'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Сохранить', 'value' => 'Сохранить', 'cancel' => 1)	
			));

            require_once "userfiles.php";
            Userfiles::show($this->Id, $this->ModuleName, $form);
			
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                $form->SetElementAttribute('*', 'readonly', 1);
				
			$form->Show();
		}
		
		public function addAction()
		{	
			if(!ACL::Check($this->ObjectType, ACL::CREATE))
			{
				$this->Page->Redirect(START_PAGE);
			}
			
			$query = new Query("insert");
			$query->AddTable("news");
			$query->AddField("created", 'inline:UNIX_TIMESTAMP()');
			$query->AddField("author_id", $_SESSION['user_id']);
			$query->AddField("name", $this->name);
			$query->AddField("comment", $this->comment);

			$query->Execute();
            $this->Id = $query->last_insert_id();

            require_once "userfiles.php";
            Userfiles::process($this->Id, $this->ModuleName);
			
			LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

		public function updAction()
		{	
			if(ACL::Check($this->ObjectType, ACL::UPDATE))
			{
				$query = new Query("update");
				$query->AddTable("news");
				$query->AddField("name", $this->name);
				$query->AddField("comment", $this->comment);

				$query->Where->Add("id = ".$this->Id);
				$query->Execute();

                ini_set('display_errors', 1);
                error_reporting(E_ALL^E_NOTICE);

                require_once "userfiles.php";
                Userfiles::process($this->Id, $this->ModuleName);
			}
			
			LocationManager::redirect("index.php?module=".$this->ModuleName);
		}		

		public function delAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::DELETE))
			{
				LocationManager::redirect(START_PAGE);
			}			

            $this->sql("delete from userfiles where module = 'news' and owner_id = ".$this->Id);
            $this->sql("delete from news where id = ".$this->Id);

            LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

		protected function parseHeaders()
		{
			$this->Id = $_REQUEST["id"];
			$this->name = $_REQUEST["name"];
			$this->comment = $_REQUEST["comment"];
		}	
	}

	class NewsListTable extends Table
	{	
		public function __construct($objectType)
		{
			$this->ObjectType = $objectType;
			
			$query = new Query("select");
			$query->AddTable("news
			join users on news.author_id = users.id");
			$query->Fields = array("news.id", "news.created", "news.name", "news.comment");

			$captions = array("", "Добавлено", "Заголовок", "Содержание");
	
			parent::Table("Новости",
						  $query,
						  $captions,
						  $query->Fields);

			$this->ShowNewLinks = ACL::Check($this->ObjectType, ACL::CREATE);
			$this->ShowEditLinks = true;
			$this->ShowDeleteLinks = ACL::Check($this->ObjectType, ACL::DELETE);
		}

        function ShowCell($data, $cellnum)
        {
            if($this->_is_visible_cell($cellnum))
            {
                if($cellnum == 1)
                {
                    $this->_render_cell($data, DT::getDateTime($data[$cellnum]));
                }
                else
                    parent::ShowCell($data, $cellnum);
            }
        }
	}	
	