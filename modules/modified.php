<?php 
	class Modified extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_ARTICLES;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "База знаний - обновленные статьи";
            $this->Page->IncludeRichEditor = true;
			$this->parseHeaders();
		}

		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}

            if(empty($_SESSION['search_articles_from']))
                $_SESSION['search_articles_from'] = DT::getDate(time() - 3*24*60*60);

            $form = new Form('Обновленные статьи', array(
                                    array('type' => 'date', 'caption' => 'С', 'name' => 'search_articles_from', 'value' => $_SESSION['search_articles_from'], 'nobr' => 1),
                                    array('type' => 'submit', 'name' => 'btnFilter', 'value' => 'Показать', 'nobr' => 1),
                                    array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
                                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1)
            ));

            $form->Show();
            echo '<br>';
			
			$this->Table = new ModifiedArticlesListTable();
			$this->Table->Show();

            LocationManager::add();
		}

		protected function parseHeaders()
		{
            foreach($_REQUEST as $key => $value)
            {
                if(preg_match('/^search_/', $key))
                {
                    $_SESSION['search_from'] = $_REQUEST['search_from'];
                    LocationManager::redirect("index.php?module=modified&tbl_page=1");
                }
            }
		}	
	}

	class ModifiedArticlesListTable extends Table
	{	
		public function __construct()
		{
			$query = new Query("select");
			$query->AddTable("articles
			left outer join departments on articles.department_id = departments.id
			");
			$query->Fields = array("articles.id", "articles.name", "articles.comment", 'articles.is_group', 'departments.shortname');
            $query->Where->Add("is_group = 0");
            $query->Where->Add("modified >= ".DT::getUnixTime($_SESSION['search_articles_from']));
	
			$captions = array("", "Заголовок", "Ответ", '', 'Ответственный');

			$query->Order = "articles.name";
	
			parent::Table("База знаний",
						  $query,
						  $captions,
						  $query->Fields);

			$this->ShowNewLinks = false;
			$this->ShowEditLinks = true;
			$this->ShowDeleteLinks = false;

			$this->_edit_url = '?module=articles&act=edit&id=%id%';
            $this->Paging = false;
            $this->Sortable = false;
		}

        function ShowCell($data, $cellnum)
        {
            if($this->_is_visible_cell($cellnum))
            {
                if($cellnum == 1)
                {
                    echo '<td class="tableCell"><img src=images/item.png>&nbsp;<a href="index.php?module=articles&act=edit&parent='.$this->parent_id.'&id='.$data['id'].'">'.$data[$cellnum].'</a></td>';
                }
                else if($cellnum == 2)
                {
                    $this->_render_cell($data, Utils::ShortString($data[$cellnum], 100));
                }
                else
                    parent::ShowCell($data, $cellnum);
            }
        }

	}	
	