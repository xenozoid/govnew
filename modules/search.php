<?php

class Search extends Document
{

    public function indexAction()
    {
        ?>
            <style>
                .search_link, .search_link:visited, .search_link:hover
                {
                    font-family: Arial, sans-serif;
                    font-size:16px;
                    color:blue;
                    text-decoration:underline;
                }

                .result_block
                {
                    padding-top:5px;
                    padding-left:15px;
                    padding-bottom:10px;
                }

                .result_url
                {
                    color: #0E774A;
                }
            </style>
        <?php


        ini_set('display_errors', 1);
        error_reporting(E_ALL^E_NOTICE);

        require_once('./sphinxapi.php');

        $sphinx = new SphinxClient();
        $sphinx->SetServer('localhost', 9312);
        $sphinx->SetMatchMode(SPH_MATCH_ANY);
        $sphinx->SetSortMode(SPH_SORT_RELEVANCE);
        $sphinx->SetArrayResult(true);
        $results = $sphinx->Query($_REQUEST['text']);
        ?>
            <div style="width:600px;">
        <?php

        $counter = 0;

        $status = new StatusList();


        if(count($results['matches']))
        foreach($results['matches'] as $num => $data)
        {
            if($_REQUEST['kb_only'] && $data['attrs']['tbl_type'] == 1)
                continue;

            echo (++$counter).'. ';

            switch($data['attrs']['tbl_type'])
            {
                case 1:
                    $order = $this->getline("select *
                    from orders
                    where orders.id = ".$data['id']);
                    $url = 'index.php?module=orders&act=edit&id='.$data['id'];
                    echo '<a href="'.$url.'" class="search_link" target="_blank">'.$order['name'].'</a>';
                    echo '<div class="result_block">';
                    echo Utils::ShortString($order['question'], 200).'<br>';
                    echo '<span style="color:grey;">'.$status->Get($order['status']).'</span><br>';
                    echo '<span class="result_url">'.$url.'</span>';
                    echo '</div>';
                break;

                case 2:
                    $article = $this->getline("select *
                    from articles
                    where articles.id = ".$data['id']);
                    $url = 'index.php?module=articles&act=edit&id='.$data['id'];
                    echo '<a href="'.$url.'" class="search_link" target="_blank">'.$article['name'].'</a>';
                    echo '<div class="result_block">';
                    if($article['comment_simple'])
                        echo Utils::ShortString($article['comment_simple'], 200).'<br>';
                    if($article['parent_id'])
                    {
                        $parent = $this->getline("select name from articles where id = ".$article['parent_id']);
                        echo '<span style="color:grey;">'.$parent['name'].'</span><br>';
                    }
                    echo '<span class="result_url">'.$url.'</span>';
                    echo '</div>';
                break;
            }
        }

        if(!$counter)
            echo 'Ничего не найдено.';

        ?>
            </div>
        <?php
    }

}

