<?php

	class Logs extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_LOGS;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "История работы с системой";
			$this->parseHeaders();
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}


			$dt = new DT();
			
			$today = $dt->toDate(DT::Now);
			$today = explode('.', $today);
			$from = '01.'.$today[1].'.'.$today[2];

    		$form = new Form('Фильтр', array(
                    array('type' => 'select', 'caption' => 'Модуль', 'name' => 'search_logs_module', 'data' => new ModuleList(), 'empty_option' => '(Все)', 'value' => $_SESSION['search_logs_module'], 'nobr' => 1, 'inline' => 'style="width:100px;"'),
					array('caption' => 'ID', 'name' => 'search_logs_record', 'value' => $_SESSION['search_logs_record'], 'size' => 5, 'type' => 'text', 'nobr' => 1),
                    array('type' => 'select', 'caption' => 'Пользователь', 'name' => 'search_logs_user_id', 'data' => new UserSelectList(), 'empty_option' => '(Все)', 'value' => $_SESSION['search_logs_user_id'], 'nobr' => 1, 'inline' => 'style="width:120px;"'),
        			//array('type' => 'select', 'caption' => 'Действие', 'name' => 'search_logs_action', 'data' => new LogsActionSelectList(), 'empty_option' => '(Все)', 'value' => $_SESSION['search_logs_action'], 'nobr' => 1, 'inline' => 'style="width:120px;"'),
                    array('type' => 'date', 'caption' => 'С', 'name' => 'search_logs_from', 'value' => empty($_SESSION['search_logs_from'])?($from):($_SESSION['search_logs_from']), 'nobr' => 1),
        			array('type' => 'date', 'caption' => 'По', 'name' => 'search_logs_to', 'value' => empty($_SESSION['search_logs_to'])?($dt->toDate(DT::Now)):($_SESSION['search_logs_to']), 'nobr' => 1),
        			array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
					array('type' => 'submit', 'name' => 'btnFilter', 'value' => 'Выбрать', 'nobr' => 1),
					array('type' => 'submit', 'name' => 'btnClear', 'value' => 'Очистить', 'nobr' => 1)
    		));	
    
			$form->Show();
			
			echo '<br>';
			
			if(empty($this->from))
				$this->from = $from;

			if(empty($this->to))
				$this->to = $dt->toDate(DT::Now);

			$this->Table = new LogsListTable();
			$this->Table->Show();
		}
		
		protected function parseHeaders()
		{
			$this->user_id = $_REQUEST["user_id"];
			$this->object = $_REQUEST["object"];
			$this->module = $_REQUEST["modulename"];
			$this->action = $_REQUEST["action"];
			$this->from = $_REQUEST["from"];
			$this->to = $_REQUEST["to"];

            if(!empty($_REQUEST['btnFilter']))
			{
                $_SESSION['search_logs_module'] = $_REQUEST['search_logs_module'];
				$_SESSION['search_logs_record'] = $_REQUEST['search_logs_record'];
                $_SESSION['search_logs_user_id'] = $_REQUEST['search_logs_user_id'];
                $_SESSION['search_logs_action'] = $_REQUEST['search_logs_action'];
                $_SESSION['search_logs_from'] = $_REQUEST['search_logs_from'];
                $_SESSION['search_logs_to'] = $_REQUEST['search_logs_to'];

                $this->Page->Redirect("index.php?module=".$this->ModuleName."&tbl_page=1");
			}

			if(!empty($_REQUEST['btnClear']))
			{
				unset($_SESSION['search_logs_module']);
                unset($_SESSION['search_logs_record']);
                unset($_SESSION['search_logs_user_id']);
                unset($_SESSION['search_logs_action']);
                unset($_SESSION['search_logs_from']);
                unset($_SESSION['search_logs_to']);

                $this->Page->Redirect("index.php?module=".$this->ModuleName."&tbl_page=1");
			}
		}		
	}
	
	class LogsListTable extends Table
	{	
		public function __construct()
		{		
			$this->ObjectType = OBJECT_LOGS;
			
			$query = new Query("select");
			$query->AddTable("logs
			left outer join users
			on logs.user_id = users.id
			");
			$query->Fields = array(
				"logs.id", "dt", "users.name", "object", "module", "record", "action", "prev", "new");

			if(!empty($_SESSION['search_logs_module']))
				$query->Where->Add("module = '".$_SESSION['search_logs_module'].'"');
            if(!empty($_SESSION['search_logs_record']))
				$query->Where->Add('record = '.$_SESSION['search_logs_record']);
            if(!empty($_SESSION['search_logs_user_id']))
				$query->Where->Add('user_id = '.$_SESSION['search_logs_user_id']);
            if(!empty($_SESSION['search_logs_action']))
				$query->Where->Add("action = '".$_SESSION['search_logs_action']."'");
			if(!empty($_SESSION['search_logs_from']))
				$query->Where->Add("dt >= ".DT::getUnixTime($_SESSION['search_logs_from']));
			if(!empty($_SESSION['search_logs_to']))
				$query->Where->Add("dt <= ".(DT::getUnixTime($_SESSION['search_logs_to']) + 24*60*60));

			if(empty($this->SortedField))
			{
				$this->SortedField = 1;
				$this->SortType = 'desc';
			}
				
			parent::Table("История работы с системой",
						  $query,
						  array('', "Дата/время", "Пользователь", 
						  "", //"Тип объекта", 
						  "Модуль", // "Модуль",
                          "Номер записи", "Действие", "Было", "Стало"),
						  $query->Fields);

			$this->ShowNewLinks = false;
			$this->ShowEditLinks = false;
			$this->ShowDeleteLinks = false;
			
			$this->DefaultURL = '?module=orders&act=view&id=%record%';
		}
		
		function ShowCell($data, $cellnum)
		{
			if($this->_is_visible_cell($cellnum))
			{
				if($cellnum == 1)
				{
                    $mod = new DT();
                    $this->_render_cell($data, $mod->toDateTime($data[$cellnum])); 
				}
                else if($cellnum == 4)
				{
                    $mod = new ModuleList();
                    $this->_render_cell($data, $mod->Get($data[$cellnum]));
				}
				else
					parent::ShowCell($data, $cellnum);
			}
		}		
	}	