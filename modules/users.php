<?php 
	class Users extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_USERS;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "Пользователи";
			$this->parseHeaders();
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}

            $form = new Form('Отбор', array(
                array('caption' => 'Роль', 'type' => 'select', 'name' => 'search_user_role', 'value' => $_SESSION['search_user_role'], 'data' => new RoleSelectList(), 'empty_option' => '(все)', 'nobr' => 1, 'inline' => 'style="width: 150px;"'),
                array('caption' => 'Департамент', 'type' => 'select', 'name' => 'search_user_dept', 'value' => $_SESSION['search_user_dept'], 'data' => new DepartmentShortSelectList(), 'empty_option' => '(все)', 'nobr' => 1, 'inline' => 'style="width: 150px;"'),
                array('caption' => 'Что ищем', 'type' => 'text', 'name' => 'search_user_text', 'value' => $_SESSION['search_user_text'], 'size' => 35, 'nobr' => 1),
                array('type' => 'submit', 'name' => 'btnFilter', 'value' => 'Применить', 'nobr' => 1),
                array('type' => 'submit', 'name' => 'btnClear', 'value' => 'Очистить', 'nobr' => 1),
                array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
                array('type' => 'hidden', 'name' => 'nohead', 'value' => 1)
            ));
            $form->Show();
            echo '<br>';

            $this->Table = new UsersListTable($this->ObjectType);
			$this->Table->Show();

            LocationManager::add();
		}

        public function ajaxAction()
        {
            $ds = new DataSource("select id, name value from users where id in (select user_id from user_department where department_id = ".(int)$_REQUEST['department_id'].") and id != ".(int)$_SESSION['user_id'] . " order by name");
            echo $ds->SerializeJSON();
        }
		
		public function viewAction()
		{
			$this->editAction();
		}

		public function newAction()
		{	
            $form = new Form("Новая запись...", array(
                array('caption' => 'Логин', 'type' => 'text', 'name' => 'userlogin', 'required' => '1', 'size' => '30'),
                array('caption' => 'Пароль', 'type' => 'text', 'name' => 'userpassword', 'required' => '1', 'size' => '30'),
                array('caption' => 'ФИО', 'type' => 'text', 'name' => 'name', 'required' => '1', 'size' => '50'),
                array('caption' => 'Должность', 'type' => 'text', 'name' => 'post', 'size' => '50'),
                array('caption' => 'Email', 'type' => 'text', 'name' => 'email', 'required' => '1', 'size' => '30'),
                array('caption' => 'Сот. телефон', 'type' => 'text', 'name' => 'cellphone', 'size' => '30'),
                array('caption' => 'Телефон', 'type' => 'text', 'name' => 'phone', 'size' => '30'),
                array('caption' => 'Внутренний номер', 'type' => 'text', 'name' => 'internal_num', 'size' => '30'),
                array('label' => 'Принимает звонки', 'type' => 'checkbox', 'name' => 'internal_active', 'value' => 1, 'checked' => $data['internal_active']),
                array('caption' => 'Использовать для исходящих', 'type' => 'radio', 'name' => 'outgoing_phone', 'data' => new OutgoingPhoneList()),
                array('label' => 'Совершает звонки', 'type' => 'checkbox', 'name' => 'outgoing_active', 'value' => 1, 'checked' => 1),
                array('caption' => 'Канал', 'type' => 'text', 'name' => 'channel', 'value' => Registry::Get('ats_channel'), 'size' => '30'),
                array('caption' => 'ICQ', 'type' => 'text', 'name' => 'icq', 'size' => '30'),
                array('caption' => 'Skype', 'type' => 'text', 'name' => 'skype', 'size' => '30'),
                array('caption' => 'Статус', 'type' => 'select', 'name' => 'priv', 'required' => '1', 'data' => new RoleSelectList()),
                array('caption' => 'Примечание', 'type' => 'textarea', 'name' => 'comment', 'width' => '50', 'height' => '5'),
                array('caption' => 'Заблокирован', 'type' => 'checkbox', 'name' => 'disabled', 'value' => 1),
                array('caption' => 'Доступна основная версия', 'type' => 'checkbox', 'name' => 'web_access', 'value' => 1, 'checked' => 1),
                array('caption' => 'Доступна мобильная версия', 'type' => 'checkbox', 'name' => 'mobile_access', 'value' => 1),
                array('caption' => 'Активен с', 'type' => 'datetime', 'name' => 'active_from'),
                array('caption' => 'по', 'type' => 'datetime', 'name' => 'active_to'),
                array('label' => 'Уведомлять о новых вопросах', 'type' => 'checkbox', 'name' => 'email_notify', 'value' => 1, 'checked' => $data['email_notify']),
                array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
                array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
                array('type' => 'submit', 'name' => 'Добавить', 'value' => 'Добавить', 'cancel' => 1)
            ));

            if(!ACL::Check($this->ObjectType, 'allaccess'))
            {
                $form->DeleteElement('userlogin');
                $form->DeleteElement('userpassword');
                $form->DeleteElement('priv');
                //$form->DeleteElement('disabled');
                //$form->DeleteElement('active_from');
                //$form->DeleteElement('active_to');
                $form->DeleteElement('web_access');
                $form->DeleteElement('mobile_access');
                $form->DeleteElement('internal_num');
                $form->DeleteElement('internal_active');
                $form->DeleteElement('outgoing_phone');
                $form->DeleteElement('outgoing_active');
                $form->DeleteElement('channel');
            }

            $form->Show();
		}
		
		public function editAction()
		{
            $data = $this->getline("select * from users where id = ".$this->Id);

            $form = new Form("Редактирование записи...", array(
                array('caption' => 'Создан', 'type' => 'label', 'name' => 'created','value' => DT::getDateTime($data['created'])),
				array('caption' => 'кем', 'type' => 'select', 'name' => 'author_id', 'value' => $data['author_id'], 'data' => new UserSelectList(), 'readonly' => 1),
                array('caption' => 'Последняя активность', 'type' => 'label', 'name' => 'last_activity','value' => DT::getDateTime($data['last_activity'])),
                array('caption' => 'Логин', 'type' => 'text', 'name' => 'userlogin','value' => $data['login'], 'required' => '1', 'size' => '30'),
                array('caption' => 'Пароль', 'type' => 'text', 'name' => 'userpassword','value' => $data['password'], 'required' => '1', 'size' => '30'),
                array('caption' => 'ФИО', 'type' => 'text', 'name' => 'name','value' => $data['name'], 'required' => '1', 'size' => '50'),
                array('caption' => 'Должность', 'type' => 'text', 'name' => 'post','value' => $data['post'], 'size' => '50'),
                array('caption' => 'Email', 'type' => 'text', 'name' => 'email','value' => $data['email'], 'required' => '1', 'size' => '30'),
                array('caption' => 'Сот. телефон', 'type' => 'text', 'name' => 'cellphone','value' => $data['cellphone'], 'size' => '30'),
                array('caption' => 'Телефон', 'type' => 'text', 'name' => 'phone','value' => $data['phone'], 'size' => '30'),
                array('caption' => 'Внутренний номер', 'type' => 'text', 'name' => 'internal_num','value' => $data['internal_num'], 'size' => '30'),
                array('label' => 'Принимает звонки', 'type' => 'checkbox', 'name' => 'internal_active', 'value' => 1, 'checked' => $data['internal_active']),
                array('caption' => 'Использовать для исходящих', 'type' => 'radio', 'name' => 'outgoing_phone', 'data' => new OutgoingPhoneList(), 'value' => $data['outgoing_phone']),
                array('label' => 'Совершает звонки', 'type' => 'checkbox', 'name' => 'outgoing_active', 'value' => 1, 'checked' => $data['outgoing_active']),
                array('caption' => 'Канал', 'type' => 'text', 'name' => 'channel', 'value' => $data['channel'], 'size' => '30'),
                array('caption' => 'ICQ', 'type' => 'text', 'name' => 'icq','value' => $data['icq'], 'size' => '30'),
                array('caption' => 'Skype', 'type' => 'text', 'name' => 'skype','value' => $data['skype'], 'size' => '30'),
                array('caption' => 'Статус', 'type' => 'select', 'name' => 'priv','value' => $data['priv'], 'required' => '1', 'data' => new RoleSelectList()),
                array('caption' => 'Примечание', 'type' => 'textarea', 'name' => 'comment','value' => $data['comment'], 'width' => '50', 'height' => '5'),
                array('caption' => 'Заблокирован', 'type' => 'checkbox', 'name' => 'disabled','value' => '1', 'checked' => $data['disabled']),
                array('caption' => 'Доступна основная версия', 'type' => 'checkbox', 'name' => 'web_access', 'value' => 1, 'checked' => $data['web_access']),
                array('caption' => 'Доступна мобильная версия', 'type' => 'checkbox', 'name' => 'mobile_access', 'value' => 1, 'checked' => $data['mobile_access']),
                array('caption' => 'Активен с', 'type' => 'datetime', 'name' => 'active_from', 'value' => $data['active_from']),
                array('caption' => 'по', 'type' => 'datetime', 'name' => 'active_to', 'value' => $data['active_from']),
                array('label' => 'Уведомлять о новых вопросах', 'type' => 'checkbox', 'name' => 'email_notify', 'value' => 1, 'checked' => $data['email_notify']),
                array('type' => 'hidden', 'name' => 'id', 'value' => $this->Id),
                array('type' => 'hidden', 'name' => 'act', 'value' => 'upd'),
                array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
                array('type' => 'submit', 'name' => 'Сохранить', 'value' => 'Сохранить', 'cancel' => 0, 'nobr' => 1),
                array('type' => 'submit', 'name' => 'btnEmail', 'value' => 'Уведомить по email', 'cancel' => 1, 'nobr' => 1)
            ));

            if(empty($data['author_id']))
                $form->DeleteElement('author_id');

            if(!ACL::Check($this->ObjectType, 'allaccess'))
            {
                $form->DeleteElement('userlogin');
                $form->DeleteElement('userpassword');
                $form->DeleteElement('priv');
                //$form->DeleteElement('disabled');
                $form->DeleteElement('web_access');
                $form->DeleteElement('mobile_access');
                //$form->DeleteElement('active_from');
                //$form->DeleteElement('active_to');
                $form->DeleteElement('internal_num');
                $form->DeleteElement('internal_active');
                $form->DeleteElement('outgoing_phone');
                $form->DeleteElement('outgoing_active');
                $form->DeleteElement('channel');
            }

            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                    $form->SetElementAttribute('*', 'readonly', 1);

            echo '<table style="border:0px;"><tr><td style="vertical-align:top;">';
            if($_REQUEST['notified'])
            {
                $error = new ErrorBox('Уведомление пользователю было выслано!');
                $error->Show();
                echo '<br>';
            }
            $form->Show();
            echo '</td><td style="vertical-align:top; padding-left: 10px;">';
            $depts = new UserDepartmentListTable($this->Id);
            $depts->Show();
            echo '<br><br>';
            $regions = new UserRegionListTable($this->Id);
            $regions->Show();
            echo '</td></tr></table>';

            LocationManager::add();
		}

		public function addAction()
		{	
            if(ACL::Check($this->ObjectType, ACL::CREATE))
            {
                $query = new Query("insert");

                if(!ACL::Check($this->ObjectType, 'allaccess'))
                {
                    $this->login = Utils::Translit(str_replace(array(' ', '.'), '', Utils::getFIO($this->name)));
                    $this->password = '-';
                    $this->priv = Registry::Get('default_user_role', 0);
                    //$this->disabled = true;
                }
                else
                {
                    $query->AddField("web_access", $this->web_access);
                    $query->AddField("mobile_access", $this->mobile_access);

                    $query->AddField("internal_num", $this->internal_num);
                    $query->AddField("internal_active", $this->internal_active);
                    $query->AddField("outgoing_phone", $this->outgoing_phone);
                    $query->AddField("outgoing_active", $this->outgoing_active);
                }

                $query->AddTable("users");
                $query->AddField("login", $this->login);
                $query->AddField("password", $this->password);
                $query->AddField("name", $this->name);
                $query->AddField("post", $this->post);
                $query->AddField("email", $this->email);
                $query->AddField("cellphone", $this->cellphone);
                $query->AddField("phone", $this->phone);
                $query->AddField("created", 'UNIX_TIMESTAMP()');
                $query->AddField("author_id", $_SESSION['user_id']);
                $query->AddField("priv", $this->priv);
                $query->AddField("comment", $this->comment);
                $query->AddField("active_from", DT::getUnixTime($this->active_from));
                $query->AddField("active_to", DT::getUnixTime($this->active_to));
                $query->AddField("disabled", $this->disabled);
                $query->AddField("email_notify", $this->email_notify);
                $query->AddField("channel", $this->channel);

                $query->Execute();
                $this->Id = $query->last_insert_id();

                if(!ACL::Check($this->ObjectType, 'allaccess'))
                {
                    $ds = new DataSource("select department_id from user_department where user_id = ".$_SESSION['user_id']);
                    while($data = $ds->GetString())
                        $this->sql("insert into user_department (user_id, department_id) values (".$this->Id.", ".$data['department_id'].")");
                }
            }
			//Log::Add($this->ObjectType, $this->ModuleName, ACL::CREATE, $this->Id);
			
			LocationManager::last();
		}

		public function updAction()
		{	
			if(ACL::Check($this->ObjectType, ACL::UPDATE))
			{
				$query = new Query("update");
                $query->AddTable("users");
                if(ACL::Check($this->ObjectType, 'allaccess'))
                {
                    $query->AddField("login", $this->login);
                    $query->AddField("password", $this->password);
                    $query->AddField("priv", $this->priv);
                    $query->AddField("web_access", $this->web_access);
                    $query->AddField("mobile_access", $this->mobile_access);
                    $query->AddField("internal_num", $this->internal_num);
                    $query->AddField("internal_active", $this->internal_active);
                    $query->AddField("outgoing_phone", $this->outgoing_phone);
                    $query->AddField("outgoing_active", $this->outgoing_active);
                    $query->AddField("channel", $this->channel);
                }
                $query->AddField("name", $this->name);
                $query->AddField("post", $this->post);
                $query->AddField("email", $this->email);
                $query->AddField("cellphone", $this->cellphone);
                $query->AddField("phone", $this->phone);
                $query->AddField("comment", $this->comment);
                $query->AddField("email_notify", $this->email_notify);
                $query->AddField("active_from", DT::getUnixTime($this->active_from));
                $query->AddField("active_to", DT::getUnixTime($this->active_to));
                $query->AddField("disabled", $this->disabled);

                $query->Where->Add("id = ".$this->Id);
                $query->Execute();
			}
			
			if($_REQUEST['btnEmail'])
                LocationManager::redirect('index.php?module=users&act=email&nohead=1&id='.$this->Id);
            else
                LocationManager::last();
		}

        public function emailAction()
        {
            $data = $this->getline("select * from users where id = ".$this->Id);
            $role = $this->getline("select name from roles where id = ".$data['priv']);
            if($data['email'])
            {
                $content = 'Добрый день,<br><br>Вам был предоставлен доступ в систему обработки запросов граждан на Прямую линию Правительства Архангельской области<br>';
                if((DT::getDate($data['active_from'])) || (DT::getDate($data['active_to'])))
                {
                    $content .= '<strong><u>на период ';
                    if(DT::getDate($data['active_from']))
                        $content .= ' с '.DT::getDate($data['active_from']);
                    if(DT::getDate($data['active_to']))
                        $content .= ' по '.DT::getDate($data['active_to']);
                    $content .= '</u></strong>';
                }
                $content .= 'Адрес: <a href="http://' . Registry::Get('server_domain', 'gov29.firecall.net') . '">http://' . Registry::Get('server_domain', 'gov29.firecall.net') . '</a><br>';
                $content .= 'Имя: '.$data['login'].'<br>';
                $content .= 'Пароль: '.$data['password'].'<br>';
                $content .= 'Роль: '.$role['name'].'<br>';

                $ds = new DataSource("select departments.name
                from user_department join departments
                on user_department.department_id = departments.id
                where user_id = ".$this->Id);
                while($dept = $ds->GetString())
                {
                    $depts[] = $dept['name'];
                }
                if($depts)
                    $content .= 'Доступные разделы: '.implode(', ', $depts).'<br>';

                $content .= '<br><br>--<br>Центр обработки вызовов Прямой линии Правительства Архангельской области';

                Email::send($data['email'], 'Реквизиты доступа с системе обработки запросов граждан на Прямую линию Правительства Архангельской области', $content);
                LocationManager::redirect('index.php?module=users&act=edit&id='.$this->Id.'&notified=1');
            }

            LocationManager::redirect('index.php?module=users&act=edit&id='.$this->Id);
        }

		public function delAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::DELETE))
			{
				$this->Page->Redirect(START_PAGE);
			}			

            $this->sql("delete from user_department where user_id = ".$this->Id);
            $this->sql("delete from users where id = ".$this->Id);

            LocationManager::last();
		}

        public function adddepartmentAction()
        {
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                exit;

            if($_REQUEST['btnAll'])
            {
                $ds = new DataSource("select id from departments");
                while($data = $ds->GetString())
                {
                    $query = new Query("insert");
                    $query->AddTable("user_department");
                    $query->AddField("user_id", $this->Id);
                    $query->AddField("department_id", $data['id']);
                    $query->AddField("fullaccess", (int)$_REQUEST['fullaccess']);
                    $query->Execute();
                }
                LocationManager::last();
            }

            if((int)$_REQUEST['department_id'])
            {
                $query = new Query("insert");
                $query->AddTable("user_department");
                $query->AddField("user_id", $this->Id);
                $query->AddField("department_id", (int)$_REQUEST['department_id']);
                $query->AddField("fullaccess", (int)$_REQUEST['fullaccess']);
                $query->Execute();
            }
            LocationManager::last();
        }

        public function deldepartmentAction()
        {
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                exit;

            $query = new Query("delete");
            $query->AddTable("user_department");
            $query->Where->Add("id = ".$this->Id);
            $query->Execute();

            LocationManager::last();
        }

        public function addregionAction()
        {
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                exit;

            if($_REQUEST['btnAll'])
            {
                $ds = new DataSource("select id from regions");
                while($data = $ds->GetString())
                {
                    $query = new Query("insert");
                    $query->AddTable("user_region");
                    $query->AddField("user_id", $this->Id);
                    $query->AddField("region_id", $data['id']);
                    $query->Execute();
                }
                LocationManager::last();
            }

            if((int)$_REQUEST['region_id'])
            {
                $query = new Query("insert");
                $query->AddTable("user_region");
                $query->AddField("user_id", $this->Id);
                $query->AddField("region_id", (int)$_REQUEST['region_id']);
                $query->Execute();
            }
            LocationManager::last();
        }

        public function delregionAction()
        {
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                exit;

            $query = new Query("delete");
            $query->AddTable("user_region");
            $query->Where->Add("id = ".$this->Id);
            $query->Execute();

            LocationManager::last();
        }
		
		protected function parseHeaders()
		{
			$this->Id = $_REQUEST["id"];
			$this->login = $_REQUEST["userlogin"];
			$this->password = $_REQUEST["userpassword"];
			$this->name = $_REQUEST["name"];
            $this->post = $_REQUEST["post"];
			$this->email = $_REQUEST["email"];
			$this->cellphone = $_REQUEST["cellphone"];
			$this->phone = $_REQUEST["phone"];
			$this->icq = $_REQUEST["icq"];
			$this->skype = $_REQUEST["skype"];
			$this->priv = $_REQUEST["priv"];
			$this->comment = $_REQUEST["comment"];
			$this->last_activity = $_REQUEST["last_activity"];
			$this->disabled = $_REQUEST["disabled"];
            $this->active_from = Form::GetFieldValue("active_from", 'datetime');
            $this->active_to = Form::GetFieldValue("active_to", 'datetime');
            $this->web_access = $_REQUEST["web_access"];
            $this->mobile_access = $_REQUEST["mobile_access"];
            $this->email_notify = $_REQUEST["email_notify"];
            $this->internal_num = $_REQUEST["internal_num"];
            $this->internal_active = $_REQUEST["internal_active"];
            $this->outgoing_phone = $_REQUEST["outgoing_phone"];
            $this->outgoing_active = $_REQUEST["outgoing_active"];
            $this->channel = $_REQUEST["channel"];

            if(!empty($_REQUEST['btnClear']))
            {
                unset($_SESSION['search_user_role']);
                unset($_SESSION['search_user_dept']);
                unset($_SESSION['search_user_text']);

                LocationManager::redirect("index.php?module=".$this->ModuleName."&tbl_page=1");
            }

            foreach($_REQUEST as $key => $value)
            {
                if(preg_match('/^search_/', $key))
                {
                    $_SESSION['search_user_role'] = $_REQUEST['search_user_role'];
                    $_SESSION['search_user_dept'] = $_REQUEST['search_user_dept'];
                    $_SESSION['search_user_text'] = $_REQUEST['search_user_text'];

                    LocationManager::redirect("index.php?module=".$this->ModuleName."&tbl_page=1");
                }
            }
        }
	}

	class UsersListTable extends Table
	{	
		public function __construct($objectType)
		{
			$this->ObjectType = $objectType;
			
			$query = new Query("select");
			$query->AddTable("users
			left outer join roles
			on users.priv = roles.id
			");
			$query->Fields = array(
				"users.id", "login", "users.name", "users.post", "users.phone", "email", "roles.name", "disabled",
                "(select GROUP_CONCAT(departments.shortname SEPARATOR ', ') from user_department join departments on user_department.department_id = departments.id where user_id = users.id) depts"
            );

            if(!ACL::Check($this->ObjectType, 'allaccess'))
            {
                $sql = new SqlObject();
                $sql->sql("CREATE TEMPORARY TABLE tmp_depts SELECT department_id FROM user_department WHERE user_id = ".$_SESSION['user_id']);
                $query->Where->Add("users.id in (select user_id from user_department where department_id in (select * from tmp_depts))");
            }

            if(!empty($_SESSION['search_user_role']))
                $query->Where->Add("users.priv = " . $_SESSION['search_user_role']);

            if(!empty($_SESSION['search_user_dept']))
                $query->Where->Add("users.id in (select user_id from user_department where department_id = " . $_SESSION['search_user_dept'] . ")");

            if(!empty($_SESSION['search_user_text']))
            {
                $where = new Where();
                $where->Policy = 'or';
                $where->Add("users.login like '%".$_SESSION['search_user_text']."%'");
                $where->Add("users.name like '%".$_SESSION['search_user_text']."%'");
                $where->Add("users.email like '%".$_SESSION['search_user_text']."%'");
                $where->Add("users.phone like '%".$_SESSION['search_user_text']."%'");
                $where->Add("users.cellphone like '%".$_SESSION['search_user_text']."%'");
                $where->Add("users.post like '%".$_SESSION['search_user_text']."%'");
                $where->Add("users.comment like '%".$_SESSION['search_user_text']."%'");
                $query->Where->Add($where);
            }

			parent::Table("Пользователи",
						  $query,
						  array("", "Логин", "ФИО", "Должность", "Телефон", "Email", "Роль", "", "Департамент"),
						  $query->Fields);

			$this->ShowNewLinks = ACL::Check($this->ObjectType, ACL::CREATE);
			$this->ShowEditLinks = true;
			$this->ShowDeleteLinks = ACL::Check($this->ObjectType, ACL::DELETE);
		}
		
		function BeginRow($rowdata, $rownum)
		{
			if($rowdata[7] == '5')
				$class = 'red';			
			
			$this->_begin_row($rowdata, $rownum, $class);
		}		
	}


	class UserDepartmentListTable extends Table
	{
		public function __construct($user_id)
		{
            $this->user_id = $user_id;
			$query = new Query("select");
			$query->AddTable("user_department
			join departments on user_department.department_id = departments.id");
			$query->Fields = array("user_department.id", "departments.shortname", "user_department.fullaccess");
            $query->Where->Add("user_id = ".$this->user_id);

			$captions = array("", "Название", "Право отвечать");

			parent::Table("Ответственные подразделения",
						  $query,
						  $captions,
						  $query->Fields);

			$this->ShowNewLinks = false;
			$this->ShowEditLinks = false;
			$this->ShowDeleteLinks = ACL::Check(OBJECT_USERS, ACL::UPDATE);

            $this->_delete_url = 'index.php?module=users&act=deldepartment&id=%id%&nohead=1';
            $this->_edit_url = '#';
            $this->DefaultURL = '#';
            $this->Sortable = false;
            $this->Paging = false;
		}

        function ShowCell($data, $cellnum)
        {
            if($this->_is_visible_cell($cellnum))
            {
                if($cellnum == 2)
                {
                    if($data[$cellnum])
                        $data[$cellnum] = 'Да';
                    else
                        $data[$cellnum] = '';

                    $this->_render_cell($data, $data[$cellnum]);
                }
                else
                    parent::ShowCell($data, $cellnum);
            }
        }

        function End()
        {
            parent::End();

            if(ACL::Check(OBJECT_USERS, ACL::UPDATE))
            {
                echo '<br>';
                $form = new Form('Добавить доступ к подразделению', array(
                    array('caption' => 'Название', 'type' => 'select', 'name' => 'department_id', 'data' => new DepartmentShortSelectList(array('user_id' => $_SESSION['user_id'], 'show_muni' => true)), 'empty_option' => '(...)'),
                    array('caption' => 'Право отвечать', 'type' => 'checkbox', 'name' => 'fullaccess','value' => '1', 'nobr' => 1),
                    array('type' => 'hidden', 'name' => 'id', 'value' => $this->user_id, 'nobr' => 1),
                    array('type' => 'hidden', 'name' => 'act', 'value' => 'adddepartment'),
                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                    array('type' => 'hidden', 'name' => 'module', 'value' => 'users'),
                    array('type' => 'submit', 'name' => 'btnAdd', 'value' => 'Добавить'),
                    array('type' => 'submit', 'name' => 'btnAddAll', 'value' => 'Добавить все подразделения'),
                ));
                $form->Show();
            }
        }
	}


    class UserRegionListTable extends Table
    {
        public function __construct($user_id)
        {
            $this->user_id = $user_id;
            $query = new Query("select");
            $query->AddTable("user_region
                join regions on user_region.region_id = regions.id");
            $query->Fields = array("user_region.id", "regions.name");
            $query->Where->Add("user_id = ".$this->user_id);

            $captions = array("", "Название");

            parent::Table("Фильтр по районам",
                $query,
                $captions,
                $query->Fields);

            $this->ShowNewLinks = false;
            $this->ShowEditLinks = false;
            $this->ShowDeleteLinks = ACL::Check(OBJECT_USERS, ACL::UPDATE);

            $this->_delete_url = 'index.php?module=users&act=delregion&id=%id%&nohead=1';
            $this->_edit_url = '#';
            $this->DefaultURL = '#';
            $this->Sortable = false;
            $this->Paging = false;
        }

        function End()
        {
            parent::End();

            if(ACL::Check(OBJECT_USERS, ACL::UPDATE))
            {
                echo '<br>';
                $form = new Form('Добавить доступ к заявкам по районам', array(
                    array('caption' => 'Название', 'type' => 'select', 'name' => 'region_id', 'data' => new RegionSelectList(), 'empty_option' => '(...)'),
                    array('type' => 'hidden', 'name' => 'id', 'value' => $this->user_id, 'nobr' => 1),
                    array('type' => 'hidden', 'name' => 'act', 'value' => 'addregion'),
                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                    array('type' => 'hidden', 'name' => 'module', 'value' => 'users'),
                    array('type' => 'submit', 'name' => 'btnAdd', 'value' => 'Добавить'),
                    array('type' => 'submit', 'name' => 'btnAddAll', 'value' => 'Добавить все районы'),
                ));
                $form->Show();
                echo '<br><br><div style="color:silver; width: 400px;">Если в списке районы отсутствуют, предоставляется доступ к заявкам по всем районам.
                <br><br>Если в списке районы присутствуют, то предоставляется доступ к заявкам только по этим районам.</div>';
            }
        }
    }
