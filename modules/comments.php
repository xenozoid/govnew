<?php 
	class Comments extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_COMMENTS;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "Комментарии";
			$this->parseHeaders();
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}
			
		}

        public function addAction()
        {
			$query = new Query("insert");
			$query->AddTable("comments");
			$query->AddField("created", 'UNIX_TIMESTAMP()');
			$query->AddField("author_id", $_SESSION['user_id']);
            $query->AddField("owner_id", $this->owner_id);
            $query->AddField("module", $this->owner_module);
			$query->AddField("comment", $this->comment);
			$query->Execute();

            $this->Id = $query->last_insert_id();

            require_once "userfiles.php";
            Userfiles::process($this->Id, $this->ModuleName);

            LocationManager::redirect($_REQUEST['returnto']);
        }

        public function delAction()
        {
            if(ACL::Check(OBJECT_COMMENTS, ACL::DELETE))
            {
                $this->sql("delete from userfiles where module = 'comments' and owner_id = ".$this->Id);
                $this->sql("delete from comments where id = ".$this->Id);
            }
        }

		protected function parseHeaders()
		{
            $this->Id = $_REQUEST["id"];
            $this->owner_id = $_REQUEST["owner_id"];
            $this->owner_module = $_REQUEST["owner_module"];
            $this->comment = $_REQUEST["comment"];
		}

        public static function showComments($owner_id, $owner_module, $returnto, $show_add_dialog = true)
        {
?>
     <script>
         function deleteComment(id)
         {
             if(!confirm('Вы уверены, что хотите удалить этот комментарий?'))
                return;

             $.ajax({
                 type : 'post',
                 url  : 'index.php?module=comments&act=del&nohead=1&id=' + id,
                 success : function(){
                     $('#comment_' + id).hide();
                 }
             })
         }
     </script>
<?php
            echo '<div style="width:500px; padding-top:15px;"><span style="color:#2643b9;font-weight:bold;font-size:16px;">Комментарии</span><br>';
            $query = new Query("select");
            $query->AddTable("comments left outer join users on comments.author_id = users.id");
            $query->Fields = array("comments.id", "users.name contact", "comments.created", "comments.comment", "comments.high");
            $query->Where->Add("owner_id = ".$owner_id." and module = '".$owner_module."'");
            $query->Order = 'comments.created desc';
            $ds = new DataSource($query);
            while($data = $ds->GetString())
            {
                if($data['high'])
                    $data['contact'] = '<span style="color:red;">Комментарий губернатора</span>';
                echo '<div id="comment_'.$data['id'].'">';
                echo '<hr style="height:1px; width:300px;  color:white; background-color:#E0E0E0; border: 0px; margin-left: 0"><br>';
                echo '<strong>'.$data['contact'].'</strong> <span style="color:silver"> | '.DT::getDateTime($data['created']).'</span>';
                if(ACL::Check(OBJECT_COMMENTS, ACL::DELETE))
                    echo '<span style="color:silver"> | </span><a href="javascript:deleteComment('.$data['id'].')" style="color:silver;">удалить</a>';
                echo '<br><br>'.Utils::EscapeString($data['comment']);
                $fileds = new DataSource('select * from userfiles where owner_id = '.$data['id']." and module = 'comments'");
                if($fileds->RowCount)
                    echo '<p>&nbsp;';
                while($filedata = $fileds->GetString())
                {
                    echo '<img src="images/paper-clip.png"> <a href="download.php?id='.$filedata['id'].'">'.$filedata['name_orig'].'</a><br>';
                }
                echo '</div>';
                echo '<p>';
            }
            if(!$ds->RowCount)
                echo '<br><span style="color:silver">Нет комментариев</span><br>';

            if($show_add_dialog)
            {
                echo '<br>';

                $form = new Form('Добавить комментарий', array(
                    array('type' => 'textarea', 'name' => 'comment', 'width' => 50, 'height' => 3, 'required' => 1),
                    array('caption' => 'Приложить файл', 'type' => 'file', 'name' => 'userfile[]'),
                    array('type' => 'hidden', 'name' => 'owner_id', 'value' => $owner_id),
                    array('type' => 'hidden', 'name' => 'owner_module', 'value' => $owner_module),
                    array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                    array('type' => 'hidden', 'name' => 'returnto', 'value' => $returnto),
                    array('type' => 'hidden', 'name' => 'module', 'value' => 'comments'),
                    array('type' => 'submit', 'name' => 'btnAdd', 'value' => 'Добавить')
                ));
                $form->Show();
            }
        }
	}
