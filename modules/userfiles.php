<?php 
	class Userfiles extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_USERFILES;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "Файлы пользователей";
			$this->parseHeaders();
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}
		}

        public static function process($owner_id, $owner_module)
        {
            foreach($_FILES["userfile"]["error"] as $key => $error)
            {
                if($error == UPLOAD_ERR_OK)
                {
                    if(is_uploaded_file($_FILES['userfile']['tmp_name'][$key]))
                    {
                        $dir = 'userfiles/'.date('Y/m/d/');
                        @mkdir($dir, 0777, true);
                        $filename = time().'_'.basename($_FILES['userfile']['name'][$key]);
                        $res = move_uploaded_file($_FILES['userfile']['tmp_name'][$key], $dir.$filename);
                        if($res)
                        {
                            $sql = new SqlObject();
                            $sql->sql("insert into userfiles (owner_id, module, uploaded, uploader_id, name, name_orig) values (".$owner_id.", '".$owner_module."', UNIX_TIMESTAMP(), '".$_SESSION['user_id']."', '".$dir.$filename."', '".mysql_real_escape_string($_FILES['userfile']['name'][$key])."')");
                        }
                    }
                }
            }
        }

        public static function show($owner_id, $owner_module, $form, $showDelete = true, $beforeElement = 'userfile[]')
        {
            $ds = new DataSource('select * from userfiles where owner_id = '.$owner_id." and module = '".$owner_module."'");
            while($data = $ds->GetString())
            {
                $postfix = DT::getDateTime($data['uploaded']);
                if($data['uploader_id'])
                {
                    $sql = new SqlObject();
                    $uploader = $sql->getline("select name from users where id = " . $data['uploader_id']);
                    $postfix = $uploader['name'] . ', ' . $postfix;
                }
                $postfix = '<i>' . $postfix . '</i>';

                $name = 'userfile'.$data['id'];
                $form->InsertBefore($beforeElement,
                    array('type' => 'select_url', 'caption' => 'Файл', 'url' => 'download.php?id='.$data['id'], 'url_caption' => $data['name_orig'], 'name' => $name, 'nobr' => 1, 'postfix' => $postfix )
                );

                if($showDelete)
                {
                    $form->InsertAfter($name,
                        array('type' => 'ajax_url', 'url' => 'unlink.php?id='.$data['id'], 'url_caption' => 'удалить', 'name' => 'del'.$name, 'nobr' => 1, 'checkhide' => $name, 'confirm' => 'Вы уверены, что хотите удалить этот файл?' )
                    );

                    $form->InsertAfter('del'.$name,
                        array()
                    );
                }
            }
        }

		protected function parseHeaders()
		{
		}

	}
