<?php 
	class Cities extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_CITIES;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "Населенные пункты";
			$this->parseHeaders();
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}
			
			$this->Table = new CitiesListTable($this->ObjectType);
			$this->Table->Show();
		}

        public function ajaxAction()
        {
            $this->sql("CREATE TEMPORARY TABLE tmp_cities SELECT id, name from cities where name not like '%Другое%' and region_id = ".(int)$this->region_id." order by name");
            $this->sql("INSERT INTO tmp_cities SELECT id, name from cities where name like '%Другое%' and region_id = ".(int)$this->region_id." order by name");
            $ds = new DataSource("select name id, name value from tmp_cities");
            echo $ds->SerializeJSON();
        }
		
		public function viewAction()
		{
			$this->editAction();
		}
		
		public function newAction()
		{	
			$form = new Form("Новая запись...", array(
				array('caption' => 'Район', 'type' => 'select', 'name' => 'region_id', 'required' => '1', 'data' => new RegionSelectList(), 'empty_option' => '(...)'),
				array('caption' => 'Название', 'type' => 'text', 'name' => 'name', 'required' => '1', 'size' => '50'),
				array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Добавить', 'value' => 'Добавить', 'cancel' => 1)	
			));

			$form->Show();
		}
		
		public function editAction()
		{
			$data = $this->getline("select * from cities where id = ".$this->Id);
	
			$form = new Form("Редактирование записи...", array(
				array('caption' => 'Район', 'type' => 'select', 'name' => 'region_id', 'value' => $data['region_id'], 'required' => '1', 'data' => new RegionSelectList(), 'empty_option' => '(...)'),
				array('caption' => 'Название', 'type' => 'text', 'name' => 'name', 'value' => $data['name'], 'required' => '1', 'size' => '50'),
				array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'id', 'value' => $this->Id),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'upd'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Сохранить', 'value' => 'Сохранить', 'cancel' => 1)	
			));
			
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                $form->SetElementAttribute('*', 'readonly', 1);
				
			$form->Show();
		}
		
		public function addAction()
		{	
			if(!ACL::Check($this->ObjectType, ACL::CREATE))
			{
				$this->Page->Redirect(START_PAGE);
			}
			
			$query = new Query("insert");
			$query->AddTable("cities");
			$query->AddField("region_id", $this->region_id);
			$query->AddField("name", $this->name);

			$query->Execute();
			
			LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

		public function updAction()
		{	
			if(ACL::Check($this->ObjectType, ACL::UPDATE))
			{
				$query = new Query("update");
				$query->AddTable("cities");
				$query->AddField("region_id", $this->region_id);
				$query->AddField("name", $this->name);

				$query->Where->Add("id = ".$this->Id);
				$query->Execute();
			}
			
			LocationManager::redirect("index.php?module=".$this->ModuleName);
		}		

		public function delAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::DELETE))
			{
				LocationManager::redirect(START_PAGE);
			}			

            $this->sql("delete from cities where id = ".$this->Id);

            LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

		protected function parseHeaders()
		{
			$this->Id = $_REQUEST["id"];
			$this->region_id = $_REQUEST["region_id"];
			$this->name = $_REQUEST["name"];
		}	
	}

	class CitiesListTable extends Table
	{	
		public function __construct($objectType)
		{
			$this->ObjectType = $objectType;
			
			$query = new Query("select");
			$query->AddTable("cities
			join regions on cities.region_id = regions.id
			");
			$query->Fields = array("cities.id", "regions.name", "cities.name");
	
			$captions = array("", "Район", "Название");
	
			parent::Table("Населенные пункты",
						  $query,
						  $captions,
						  $query->Fields);

			$this->ShowNewLinks = ACL::Check($this->ObjectType, ACL::CREATE);
			$this->ShowEditLinks = true;
			$this->ShowDeleteLinks = ACL::Check($this->ObjectType, ACL::DELETE);
		}
	}	
	