<?php 
	class Projects extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_PROJECTS;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "Проекты";
			$this->parseHeaders();
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}
			
			$this->Table = new ProjectsListTable($this->ObjectType);
			$this->Table->Show();
		}

        public function checksimpleAction()
        {
            $data = $this->getline("select simple from projects where id = ".$this->Id);
            if($data[0])
                echo 1;
        }
		
		public function viewAction()
		{
			$this->editAction();
		}
		
		public function newAction()
		{	
			$form = new Form("Новая запись...", array(
				array('caption' => 'Название', 'type' => 'text', 'name' => 'name', 'required' => '1', 'size' => '50'),
                array('caption' => 'Упрощенная форма вопроса', 'type' => 'checkbox', 'name' => 'simple', 'value' => 1),
                array('caption' => 'Номер телефона', 'type' => 'text', 'name' => 'phone', 'size' => '50'),
                array('caption' => 'Контекст', 'type' => 'text', 'name' => 'context', 'value' => Registry::Get('ats_context'), 'size' => '50'),
				array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Добавить', 'value' => 'Добавить', 'cancel' => 1)	
			));

			$form->Show();
		}
		
		public function editAction()
		{
			$data = $this->getline("select * from projects where id = ".$this->Id);
	
			$form = new Form("Редактирование записи...", array(
				array('caption' => 'Название', 'type' => 'text', 'name' => 'name', 'value' => $data['name'], 'required' => '1', 'size' => '50'),
				array('caption' => 'Упрощенная форма вопроса', 'type' => 'checkbox', 'name' => 'simple', 'value' => 1, 'checked' => $data['simple']),
                array('caption' => 'Номер телефона', 'type' => 'text', 'name' => 'phone', 'size' => '50', 'value' => $data['phone']),
                array('caption' => 'Контекст', 'type' => 'text', 'name' => 'context', 'value' => $data['context'], 'size' => '50'),
                array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'id', 'value' => $this->Id),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'upd'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Сохранить', 'value' => 'Сохранить', 'cancel' => 1)	
			));
			
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                $form->SetElementAttribute('*', 'readonly', 1);

            //echo '<table style="border:0px; vertical-align:top;"><tr><td>';
            $form->Show();
            //echo '</td><td style="vertical-align:top;">';
            echo '<br>';
            $depts = new ProjectDepartmentListTable($this->Id);
            $depts->Show();
            //echo '</td></tr></table>';

            LocationManager::add();
		}
		
		public function addAction()
		{	
			if(!ACL::Check($this->ObjectType, ACL::CREATE))
			{
				$this->Page->Redirect(START_PAGE);
			}
			
			$query = new Query("insert");
			$query->AddTable("projects");
			$query->AddField("name", $this->name);
            $query->AddField("simple", $this->simple);
            $query->AddField("phone", $this->phone);
            $query->AddField("context", $this->context);
			$query->Execute();
            $this->Id = $query->last_insert_id();

            $query = new Query("insert");
            $query->AddTable("project_role");
            $query->AddField("role_id", $_SESSION['user_priv']);
            $query->AddField("project_id", $this->Id);
            $query->Execute();
			
			LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

		public function updAction()
		{	
			if(ACL::Check($this->ObjectType, ACL::UPDATE))
			{
				$query = new Query("update");
				$query->AddTable("projects");
				$query->AddField("name", $this->name);
                $query->AddField("simple", $this->simple);
                $query->AddField("phone", $this->phone);
                $query->AddField("context", $this->context);
				$query->Where->Add("id = ".$this->Id);
				$query->Execute();
			}
			
			LocationManager::redirect("index.php?module=".$this->ModuleName);
		}		

		public function delAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::DELETE))
			{
				LocationManager::redirect(START_PAGE);
			}			

            $this->sql("delete from projects where id = ".$this->Id);

            LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

        public function adddepartmentAction()
        {
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                exit;

            if($_REQUEST['btnAddAll'])
            {
                $ds = new DataSource("select id from departments where municipality = 0");
                while($data = $ds->GetString())
                {
                    $query = new Query("insert");
                    $query->AddTable("project_department");
                    $query->AddField("project_id", $this->Id);
                    $query->AddField("department_id", $data['id']);
                    $query->Execute();
                }
            }
            else
            {
                if((int)$_REQUEST['department_id'])
                {
                    $query = new Query("insert");
                    $query->AddTable("project_department");
                    $query->AddField("project_id", $this->Id);
                    $query->AddField("department_id", (int)$_REQUEST['department_id']);
                    $query->Execute();
                }
            }

            LocationManager::last();
        }

        public function deldepartmentAction()
        {
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                exit;

            $query = new Query("delete");
            $query->AddTable("project_department");
            $query->Where->Add("id = ".$this->Id);
            $query->Execute();

            LocationManager::last();
        }

		protected function parseHeaders()
		{
			$this->Id = $_REQUEST["id"];
			$this->name = $_REQUEST["name"];
            $this->phone = $_REQUEST["phone"];
            $this->simple = $_REQUEST["simple"];
            $this->context = $_REQUEST["context"];
		}	
	}

	class ProjectsListTable extends Table
	{	
		public function __construct($objectType)
		{
			$this->ObjectType = $objectType;
			
			$query = new Query("select");
			$query->AddTable("projects");
			$query->Fields = array("id", "name", "phone", "context");
	
			$captions = array("", "Название", "Телефон", "Контекст");
	
			parent::Table("Проекты",
						  $query,
						  $captions,
						  $query->Fields);

			$this->ShowNewLinks = ACL::Check($this->ObjectType, ACL::CREATE);
			$this->ShowEditLinks = true;
			$this->ShowDeleteLinks = ACL::Check($this->ObjectType, ACL::DELETE);
		}
	}	



    class ProjectDepartmentListTable extends Table
    {
        public function __construct($project_id)
        {
            $this->project_id = $project_id;
            $query = new Query("select");
            $query->AddTable("project_department
            join departments on project_department.department_id = departments.id");
            $query->Fields = array("project_department.id", "departments.shortname");
            $query->Where->Add("project_id = ".$this->project_id);

            $captions = array("", "Название");

            parent::Table("Ответственные подразделения",
                          $query,
                          $captions,
                          $query->Fields);

            $this->ShowNewLinks = false;
            $this->ShowEditLinks = false;
            $this->ShowDeleteLinks = ACL::Check(OBJECT_PROJECTS, ACL::UPDATE);

            $this->_delete_url = 'index.php?module=projects&act=deldepartment&id=%id%&nohead=1';
            $this->_edit_url = '#';
            $this->DefaultURL = '#';
            $this->Sortable = false;
            $this->Paging = false;
        }

        function Begin()
        {
            if(ACL::Check(OBJECT_PROJECTS, ACL::UPDATE))
            {
                $form = new Form('Добавить доступ к подразделению', array(
                    array('caption' => 'Название', 'type' => 'select', 'name' => 'department_id', 'data' => new DepartmentShortSelectList(array('project_id' => $_SESSION['project_id'])), 'empty_option' => '(...)'),
                    array('type' => 'hidden', 'name' => 'id', 'value' => $this->project_id, 'nobr' => 1),
                    array('type' => 'hidden', 'name' => 'act', 'value' => 'adddepartment'),
                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                    array('type' => 'hidden', 'name' => 'module', 'value' => 'projects'),
                    array('type' => 'submit', 'name' => 'btnAdd', 'value' => 'Добавить', 'nobr' => 1),
                    array('type' => 'submit', 'name' => 'btnAddAll', 'value' => 'Добавить все подразделения', 'nobr' => 1)
                ));
                $form->Show();
                echo '<br>';
            }

            parent::Begin();
        }
    }
