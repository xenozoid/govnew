<?php 
	class Articles extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_ARTICLES;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "База знаний";
            $this->Page->IncludeRichEditor = true;
            unset($_SESSION['__active_menu_item']);
			$this->parseHeaders();
            if($_REQUEST['act'] == 'popupview')
                $this->Page->ShowMenu = false;
		}

        public function getanswerAction()
        {
            require_once('./sphinxapi.php');

            $sphinx = new SphinxClient();
            $sphinx->SetServer('localhost', 9312);
            $sphinx->SetMatchMode(SPH_MATCH_ANY);
            $sphinx->SetSortMode(SPH_SORT_RELEVANCE);
            $sphinx->SetArrayResult(true);

            $this->name = str_replace(array(',', '?', '.'), '', $this->name);

            $results = $sphinx->Query($this->name, 'articles_index');

            $output = array();

            if(count($results['matches']))
            foreach($results['matches'] as $num => $data)
            {
                switch($data['attrs']['tbl_type'])
                {
                    case 2:
                        $article = $this->getline("select *
                        from articles
                        where articles.id = ".$data['id']);
                        $output[] = $article;
                    break;
                }
            }

            echo json_encode($output);
        }

        public function getnodeAction()
        {
            //$s = print_r($_REQUEST, true);
            //Debug::Log($s);

            $result = array();

            $ds = new DataSource("select * from articles where parent_id = ".(int)$this->Id);
            while($data = $ds->GetString())
            {
                $item = array();
                $attr = array();
                $attr['id'] = $data['id'];
                if($data['is_group'])
                {
                    $attr['rel'] = 'folder';
                    $item['state'] = 'closed';
                }
                else
                {
                    $attr['rel'] = 'item';
                }
                $item['attr'] = $attr;
                $item['data'] = $data['name'];
                $result[] = $item;
            }

            echo json_encode($result);
        }
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}

            $form = new Form('Обновленные статьи', array(
                                    array('type' => 'date', 'caption' => 'С', 'name' => 'search_articles_from', 'value' => $_SESSION['search_articles_from'], 'nobr' => 1),
                                    array('type' => 'submit', 'name' => 'btnFilter', 'value' => 'Показать', 'nobr' => 1),
                                    array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
                                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1)
            ));

            $form->Show();
            echo '<br>';
			
			$this->Table = new ArticlesListTable($this->ObjectType, $this->parent, $this->project_id);
			$this->Table->Show();

            LocationManager::add();
		}

        public function popupviewAction()
        {
            $data = $this->getline("select * from articles where id = ".$this->Id);
            if($data['department_id'])
            {
                $dept = $this->getline("select * from departments where id = ".$data['department_id']);
                echo '<small>Ответственный: '.$dept['name'].'</small><br><br>';
            }

?>
                <strong>ВОПРОС:<br>
                <?php echo $data['name']; ?>
                </strong>
                <br><br>
                ОТВЕТ:<br>
                <?php echo $data['comment']; ?>
<?php
        }

		public function viewAction()
		{
			$this->editAction();
		}
		
		public function newAction()
		{	
			$form = new Form("Новая запись...", array(
				array('caption' => 'Заголовок', 'type' => 'text', 'name' => 'name', 'required' => '1', 'size' => '100'),
				array('caption' => 'Ответ', 'type' => 'textarea', 'name' => 'comment', 'width' => '50', 'height' => '5', 'rich' => 1),
				array('caption' => 'Это группа', 'type' => 'checkbox', 'name' => 'is_group', 'value' => 1 ),
                array('caption' => 'Принадлежит группе', 'type' => 'select', 'name' => 'parent_id', 'data' => new ArticleGroupSelectList(), 'value' => $this->parent, 'empty_option' => '(...)', 'inline' => 'style="width:400px;"'),
				//array('caption' => 'Не видно оператору', 'type' => 'checkbox', 'name' => 'restricted', 'value' => 1 ),
                array('caption' => 'Ответственный', 'type' => 'select', 'name' => 'department_id', 'data' => new DepartmentSelectList(), 'empty_option' => '(...)', 'inline' => 'style="width:400px;"'),
                //array('type' => 'hidden', 'name' => 'parent_id', 'value' => $this->parent),
                array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Добавить', 'value' => 'Добавить', 'cancel' => 1)	
			));

			$form->Show();
		}
		
		public function editAction()
		{
			$data = $this->getline("select * from articles where id = ".$this->Id);
	
			$form = new Form("Редактирование записи...", array(
                array('caption' => 'Изменено', 'type' => 'label', 'value' => DT::getDateTime($data['modified'])),
                array('caption' => 'Заголовок', 'type' => 'text', 'name' => 'name', 'value' => $data['name'], 'required' => '1', 'size' => '100'),
				array('caption' => 'Ответ', 'type' => 'textarea', 'name' => 'comment', 'value' => $data['comment'], 'width' => '50', 'height' => '5', 'rich' => 1),
				array('caption' => 'Это группа', 'type' => 'checkbox', 'name' => 'is_group', 'value' => 1, 'checked' => $data['is_group']),
				array('caption' => 'Принадлежит группе', 'type' => 'select', 'name' => 'parent_id', 'data' => new ArticleGroupSelectList(), 'value' => $data['parent_id'], 'empty_option' => '(...)', 'inline' => 'style="width:400px;"'),
                //array('caption' => 'Не видно оператору', 'type' => 'checkbox', 'name' => 'restricted', 'checked' => $data['restricted'], 'value' => 1 ),
                array('caption' => 'Ответственный', 'type' => 'select', 'name' => 'department_id', 'value' => $data['department_id'], 'data' => new DepartmentSelectList(), 'empty_option' => '(...)', 'inline' => 'style="width:400px;"'),
                array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'id', 'value' => $this->Id),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'upd'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Сохранить', 'value' => 'Сохранить', 'cancel' => 1)	
			));
			
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                $form->SetElementAttribute('*', 'readonly', 1);

            if($data['parent_id'])
            {
                $parent = $this->getline("select * from articles where id = ".$data['parent_id']);
                echo '<img src="images/folder.png" style="vertical-align:middle;"> <a href="index.php?module=articles&parent='.$parent['id'].'">'.$parent['name'].'</a> /<br><br>';
            }

			$form->Show();
		}
		
		public function addAction()
		{	
			if(!ACL::Check($this->ObjectType, ACL::CREATE))
			{
				$this->Page->Redirect(START_PAGE);
			}
			
			$query = new Query("insert");
			$query->AddTable("articles");
            $query->AddField("created", 'inline:UNIX_TIMESTAMP()');
            $query->AddField("modified", 'inline:UNIX_TIMESTAMP()');
			$query->AddField("name", $this->name);
			$query->AddField("comment", $this->comment);
            $query->AddField("comment_simple", strip_tags($this->comment));
			$query->AddField("is_group", $this->is_group);
            $query->AddField("parent_id", $this->parent_id);
            $query->AddField("department_id", $this->department_id);

			$query->Execute();
			
			$this->Page->Redirect("index.php?module=".$this->ModuleName.'&parent='.$this->parent_id);
		}

		public function updAction()
		{
			if(ACL::Check($this->ObjectType, ACL::UPDATE))
			{
                $data = $this->getline("select comment_simple from articles where id = ".$this->Id);

				$query = new Query("update");
				$query->AddTable("articles");
                if($data['comment_simple'] != strip_tags($this->comment))
                    $query->AddField("modified", 'inline:UNIX_TIMESTAMP()');
                $query->AddField("name", $this->name);
                $query->AddField("comment", $this->comment);
                $query->AddField("comment_simple", strip_tags($this->comment));
                $query->AddField("is_group", $this->is_group);
                $query->AddField("parent_id", $this->parent_id);
                $query->AddField("department_id", $this->department_id);

				$query->Where->Add("id = ".$this->Id);
				$query->Execute();

                if((int)$this->department_id)
                {
                    $children = $this->_getChildren($this->Id);
                    $this->sql("update articles set department_id = ".(int)$this->department_id." where id in (".implode(',', $children).")");
                }
			}

            LocationManager::last();
			//$this->Page->Redirect("index.php?module=".$this->ModuleName.'&parent='.$this->parent_id);
		}		

		public function delAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::DELETE))
			{
				$this->Page->Redirect(START_PAGE);
			}			

            $this->sql("delete from articles where id = ".$this->Id);

            $this->Page->Redirect("index.php?module=".$this->ModuleName);
		}

        private function _getChildrenRecursive($id)
        {
            $ds = new DataSource('select id, is_group from articles where parent_id = '.$id);
			while($data = $ds->GetString())
			{
                $this->_tmparr[] = $data['id'];
				if($data['is_group'] == 1)
                    $this->_getChildrenRecursive($data['id']);
			}
        }

        private function _getChildren($id)
        {
            $this->_tmparr = array(0);
            $this->_getChildrenRecursive($id);
            return $this->_tmparr;
        }

		protected function parseHeaders()
		{
			$this->Id = $_REQUEST["id"];
			$this->name = $_REQUEST["name"];
			$this->comment = $_REQUEST["comment"];
			$this->is_group = $_REQUEST["is_group"];
            $this->parent_id = $_REQUEST['parent_id'];
            $this->department_id = $_REQUEST['department_id'];

            $this->parent = $_REQUEST['parent'];
            $this->project_id = $_REQUEST['project_id'];

            foreach($_REQUEST as $key => $value)
            {
                if(preg_match('/^search_/', $key))
                {
                    $_SESSION['search_articles_from'] = $_REQUEST['search_articles_from'];
                    LocationManager::redirect("index.php?module=modified&tbl_page=1");
                }
            }
		}	
	}

	class ArticlesListTable extends Table
	{	
		public function __construct($objectType, $parent = 0, $project_id = 0)
		{
			$this->ObjectType = $objectType;
            $this->parent_id = $parent;
            $this->project_id = $project_id;
			
			$query = new Query("select");
			$query->AddTable("articles
			left outer join departments on articles.department_id = departments.id
			");
			$query->Fields = array("articles.id", "articles.name", "articles.comment", 'articles.is_group', 'departments.shortname');
	
			$captions = array("", "Заголовок", "Ответ", '', 'Ответственный');

            $query->Where->Add('parent_id = '.(int)$this->parent_id);
			$query->Order = "articles.is_group desc";

            if(!ACL::Check($this->ObjectType, 'allaccess'))
                $query->Where->Add("articles.department_id in (select department_id from user_department where user_id = ".$_SESSION['user_id'].")");
	
			parent::Table("База знаний",
						  $query,
						  $captions,
						  $query->Fields);

			$this->ShowNewLinks = ACL::Check($this->ObjectType, ACL::CREATE);
			$this->ShowEditLinks = true;
			$this->ShowDeleteLinks = ACL::Check($this->ObjectType, ACL::DELETE);

			$this->_new_url = '?module=articles&act=new&parent='.$parent;
			$this->_edit_url = '?module=articles&act=edit&parent='.$parent.'&id=%id%';
			$this->_delete_url = '?module=articles&act=del&nohead=1&parent='.$parent.'&id=%id%';
            $this->Paging = false;
            $this->Sortable = false;
		}

		protected function Begin()
		{
			parent::Begin();

			if($this->parent_id != 0)
			{
				$ds = new DataSource("select parent_id from articles where id = ".$this->parent_id);
				$item = $ds->GetString();
				$item_id = $item['parent_id'];

				echo '<tr class=silver><td  class="tableCell" colspan='.$this->_colnum.'><a href=index.php?module=articles&parent='.$item_id.'>.. (наверх)</a></td></tr>';
			}
		}

        function ShowCell($data, $cellnum)
        {
            if($this->_is_visible_cell($cellnum))
            {
                if($cellnum == 1)
                {
                    if($data['is_group'] == 1)
                    {
                        echo '<td class="tableCell"><img src=images/folder.png>&nbsp;<a href=index.php?module=articles&parent='.$data['id'].'>'.$data[$cellnum].'</a></td>';
                    }
                    else
                    {
                        echo '<td class="tableCell"><img src=images/item.png>&nbsp;<a href="index.php?module=articles&act=edit&parent='.$this->parent_id.'&id='.$data['id'].'">'.$data[$cellnum].'</a></td>';
                    }
                }
                else if($cellnum == 2)
                {
                    //$this->_render_cell($data, $data[$cellnum], 'index.php?module=articles&parent='.$data['id']);
                    $this->_render_cell($data, Utils::ShortString($data[$cellnum], 100));
                }
                else
                    parent::ShowCell($data, $cellnum);
            }
        }

	}	
	