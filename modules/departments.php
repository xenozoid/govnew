<?php 
	class Departments extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_DEPARTMENTS;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "Ответственные подразделения";
			$this->parseHeaders();
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}
			
			$this->Table = new DepartmentsListTable($this->ObjectType);
			$this->Table->Show();
		}

        public function ajaxAction()
        {
            $ds = new DataSource("select id, name value from departments where id in (select department_id from project_department where project_id = ".(int)$_REQUEST['project_id'].")");
            echo $ds->SerializeJSON();
        }
		
		public function viewAction()
		{
			$this->editAction();
		}
		
		public function newAction()
		{	
			$form = new Form("Новая запись...", array(
				array('caption' => 'Название', 'type' => 'text', 'name' => 'name', 'required' => '1', 'size' => '100'),
                array('caption' => 'Сокращенное название', 'type' => 'text', 'name' => 'shortname', 'required' => '1', 'size' => '30'),
                array('label' => 'муниципальный', 'type' => 'checkbox', 'name' => 'municipality', 'value' => 1),
                array('label' => 'архивный', 'type' => 'checkbox', 'name' => 'archive', 'value' => 1),
                array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Добавить', 'value' => 'Добавить', 'cancel' => 1)	
			));

			$form->Show();
		}
		
		public function editAction()
		{
			$data = $this->getline("select * from departments where id = ".$this->Id);
	
			$form = new Form("Редактирование записи...", array(
				array('caption' => 'Название', 'type' => 'text', 'name' => 'name', 'value' => $data['name'], 'required' => '1', 'size' => '100'),
				array('caption' => 'Сокращенное название', 'type' => 'text', 'name' => 'shortname', 'value' => $data['shortname'], 'required' => '1', 'size' => '30'),
                array('label' => 'муниципальный', 'type' => 'checkbox', 'name' => 'municipality', 'checked' => $data['municipality'], 'value' => 1),
                array('label' => 'архивный', 'type' => 'checkbox', 'name' => 'archive', 'checked' => $data['archive'], 'value' => 1),
                array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'id', 'value' => $this->Id),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'upd'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Сохранить', 'value' => 'Сохранить', 'cancel' => 1)	
			));
			
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                $form->SetElementAttribute('*', 'readonly', 1);
				
			$form->Show();
		}
		
		public function addAction()
		{	
			if(!ACL::Check($this->ObjectType, ACL::CREATE))
			{
				$this->Page->Redirect(START_PAGE);
			}
			
			$query = new Query("insert");
			$query->AddTable("departments");
			$query->AddField("name", $this->name);
            $query->AddField("shortname", $this->shortname);
            $query->AddField("municipality", $this->municipality);
            $query->AddField("archive", $this->archive);

			$query->Execute();
			
			LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

		public function updAction()
		{	
			if(ACL::Check($this->ObjectType, ACL::UPDATE))
			{
				$query = new Query("update");
				$query->AddTable("departments");
				$query->AddField("name", $this->name);
                $query->AddField("shortname", $this->shortname);
                $query->AddField("municipality", $this->municipality);
                $query->AddField("archive", $this->archive);

				$query->Where->Add("id = ".$this->Id);
				$query->Execute();
			}
			
			LocationManager::redirect("index.php?module=".$this->ModuleName);
		}		

		public function delAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::DELETE))
			{
				LocationManager::redirect(START_PAGE);
			}			

            $this->sql("delete from departments where id = ".$this->Id);

            LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

		protected function parseHeaders()
		{
			$this->Id = $_REQUEST["id"];
			$this->name = $_REQUEST["name"];
            $this->shortname = $_REQUEST["shortname"];
            $this->municipality = $_REQUEST["municipality"];
            $this->archive = $_REQUEST["archive"];
		}	
	}

	class DepartmentsListTable extends Table
	{	
		public function __construct($objectType)
		{
			$this->ObjectType = $objectType;
			
			$query = new Query("select");
			$query->AddTable("departments");
			$query->Fields = array("id", "shortname", "name", "IF(municipality=1,'Да','') muni", "IF(archive=1,'Да','') archive");
	
			$captions = array("", "Сокращенное", "Название", 'Муниципальный', 'Архивный');
	
			parent::Table("Ответственные подразделения",
						  $query,
						  $captions,
						  $query->Fields);

			$this->ShowNewLinks = ACL::Check($this->ObjectType, ACL::CREATE);
			$this->ShowEditLinks = true;
			$this->ShowDeleteLinks = ACL::Check($this->ObjectType, ACL::DELETE);
		}
	}	
	