<?php 
	class Blacklist extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_BLACKLIST;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "Черный список";
			$this->parseHeaders();
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}
			
			$this->Table = new BlacklistListTable($this->ObjectType);
			$this->Table->Show();

            LocationManager::add();
		}
		
		public function viewAction()
		{
			$this->editAction();
		}

        public function checkAction()
        {
            if(!ACL::Check($this->ObjectType, ACL::READ))
            {
                exit;
            }

            $num = preg_replace('/\D/', '', $this->num);
            $phone = preg_replace('/\D/', '', $_REQUEST['phone']);
            $phone2 = preg_replace('/\D/', '', $_REQUEST['phone2']);

            $data = $this->getline("select * from blacklist where num = '".$num."' or num = '".$phone."' or num = '" . $phone2 . "'");
            if($data['id'])
            {
                echo '<strong style="color:red; font-size:18px;">Этот номер есть в черном списке:</strong><br><br>';
                echo $data['comment'].'<br><br><br>';
            }
        }
		
		public function newAction()
		{
            if(!$this->num)
                $this->num = $_REQUEST['phone'];

			$form = new Form("Новая запись...", array(
                array('caption' => 'Номер', 'type' => 'text', 'name' => 'num', 'required' => '1', 'size' => '30', 'value' => $this->num),
				array('caption' => 'Примечание', 'type' => 'textarea', 'name' => 'comment', 'width' => '50', 'height' => '3'),
				array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Добавить', 'value' => 'Добавить', 'cancel' => 1)	
			));

			$form->Show();
		}
		
		public function editAction()
		{
			$data = $this->getline("select * from blacklist where id = ".$this->Id);
	
			$form = new Form("Редактирование записи...", array(
                array('caption' => 'Добавлено', 'type' => 'label', 'name' => 'created', 'value' => DT::getDateTime($data['created'])),
                array('caption' => 'кем', 'type' => 'select', 'name' => 'author_id', 'value' => $data['author_id'], 'data' => new UserSelectList(), 'readonly' => 1),
 				array('caption' => 'Номер', 'type' => 'text', 'name' => 'num', 'value' => $data['num'], 'required' => '1', 'size' => '30'),
				array('caption' => 'Примечание', 'type' => 'textarea', 'name' => 'comment', 'value' => $data['comment'], 'width' => '50', 'height' => '3'),
				array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'id', 'value' => $this->Id),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'upd'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Сохранить', 'value' => 'Сохранить', 'cancel' => 1)	
			));
			
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                $form->SetElementAttribute('*', 'readonly', 1);
				
			$form->Show();
		}
		
		public function addAction()
		{	
			if(!ACL::Check($this->ObjectType, ACL::CREATE))
			{
				$this->Page->Redirect(START_PAGE);
			}
			
			$query = new Query("insert");
			$query->AddTable("blacklist");
            $query->AddField("created", 'inline:UNIX_TIMESTAMP()');
            $query->AddField("author_id", $_SESSION['user_id']);
			$query->AddField("num", $this->num);
			$query->AddField("comment", $this->comment);

			$query->Execute();
			
			LocationManager::last();
		}

		public function updAction()
		{	
			if(ACL::Check($this->ObjectType, ACL::UPDATE))
			{
				$query = new Query("update");
				$query->AddTable("blacklist");
				$query->AddField("num", $this->num);
				$query->AddField("comment", $this->comment);

				$query->Where->Add("id = ".$this->Id);
				$query->Execute();
			}
			
			LocationManager::last();
		}		

		public function delAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::DELETE))
			{
				LocationManager::redirect(START_PAGE);
			}			

            $this->sql("delete from blacklist where id = ".$this->Id);

            LocationManager::last();
		}

		protected function parseHeaders()
		{
			$this->Id = $_REQUEST["id"];
			$this->num = $_REQUEST["num"];
			$this->comment = $_REQUEST["comment"];
		}	
	}

	class BlacklistListTable extends Table
	{	
		public function __construct($objectType)
		{
			$this->ObjectType = $objectType;
			
			$query = new Query("select");
			$query->AddTable("blacklist
			left outer join users on blacklist.author_id = users.id
			");
			$query->Fields = array("blacklist.id", "blacklist.num", "blacklist.comment", "users.name", "blacklist.created");
	
			$captions = array("", "Номер", "Примечание", "Кто занес", "Когда");
	
			parent::Table("Черный список",
						  $query,
						  $captions,
						  $query->Fields);

			$this->ShowNewLinks = ACL::Check($this->ObjectType, ACL::CREATE);
			$this->ShowEditLinks = true;
			$this->ShowDeleteLinks = ACL::Check($this->ObjectType, ACL::DELETE);
		}

        function ShowCell($data, $cellnum)
        {
            if($this->_is_visible_cell($cellnum))
            {
                if($cellnum == 4)
                {
                    $this->_render_cell($data, DT::getDateTime($data[$cellnum]));
                }
                else
                    parent::ShowCell($data, $cellnum);
            }
        }
	}	
	