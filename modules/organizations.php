<?php
class Organizations extends Document
{
    public function __construct($moduleName, $page)
    {
        $this->ObjectType = OBJECT_ORGANIZATIONS;
        $this->ModuleName = $moduleName;
        $this->Page = $page;
        $this->Page->Title = "Организации";
        $this->parseHeaders();
    }

    public function indexAction()
    {
        if(!ACL::Check($this->ObjectType, ACL::READ))
        {
            exit;
        }

        $this->Table = new OrganizationsListTable($this->ObjectType);
        $this->Table->Show();
    }

    public function autocompleteAction()
    {
        $result = '';
        $query = "select name
from organizations
where name like '%". urldecode($_REQUEST['q'])."%' order by name limit ".$_REQUEST['limit'];

        $ds = new DataSource($query);
        while($data = $ds->GetString())
            $result .= $data['name']."\n";

        echo $result;
    }

    public function viewAction()
    {
        $this->editAction();
    }

    public function newAction()
    {
        $form = new Form("Новая запись...", array(
            array('caption' => 'Название', 'type' => 'text', 'name' => 'name', 'required' => '1', 'size' => '50'),
            array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
            array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
            array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
            array('type' => 'submit', 'name' => 'Добавить', 'value' => 'Добавить', 'cancel' => 1)
        ));

        $form->Show();
    }

    public function editAction()
    {
        $data = $this->getline("select * from organizations where id = ".$this->Id);

        $form = new Form("Редактирование записи...", array(
            array('caption' => 'Название', 'type' => 'text', 'name' => 'name', 'value' => $data['name'], 'required' => '1', 'size' => '50'),
            array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
            array('type' => 'hidden', 'name' => 'id', 'value' => $this->Id),
            array('type' => 'hidden', 'name' => 'act', 'value' => 'upd'),
            array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
            array('type' => 'submit', 'name' => 'Сохранить', 'value' => 'Сохранить', 'cancel' => 1)
        ));

        if(!ACL::Check($this->ObjectType, ACL::UPDATE))
            $form->SetElementAttribute('*', 'readonly', 1);

        $form->Show();
    }

    public function addAction()
    {
        if(!ACL::Check($this->ObjectType, ACL::CREATE))
        {
            $this->Page->Redirect(START_PAGE);
        }

        $query = new Query("insert");
        $query->AddTable("organizations");
        $query->AddField("name", $this->name);

        $query->Execute();

        LocationManager::redirect("index.php?module=".$this->ModuleName);
    }

    public function updAction()
    {
        if(ACL::Check($this->ObjectType, ACL::UPDATE))
        {
            $query = new Query("update");
            $query->AddTable("organizations");
            $query->AddField("name", $this->name);

            $query->Where->Add("id = ".$this->Id);
            $query->Execute();
        }

        LocationManager::redirect("index.php?module=".$this->ModuleName);
    }

    public function delAction()
    {
        if(!ACL::Check($this->ObjectType, ACL::DELETE))
        {
            LocationManager::redirect(START_PAGE);
        }

        $this->sql("delete from organizations where id = ".$this->Id);

        LocationManager::redirect("index.php?module=".$this->ModuleName);
    }

    protected function parseHeaders()
    {
        $this->Id = $_REQUEST["id"];
        $this->name = $_REQUEST["name"];
    }
}

class OrganizationsListTable extends Table
{
    public function __construct($objectType)
    {
        $this->ObjectType = $objectType;

        $query = new Query("select");
        $query->AddTable("organizations");
        $query->Fields = array("id", "name");

        $captions = array("", "Название");

        parent::Table("Организации",
            $query,
            $captions,
            $query->Fields);

        $this->ShowNewLinks = ACL::Check($this->ObjectType, ACL::CREATE);
        $this->ShowEditLinks = true;
        $this->ShowDeleteLinks = ACL::Check($this->ObjectType, ACL::DELETE);
    }
}	
	