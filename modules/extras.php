<?php 
	class Extras extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_EXTRAS;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "Экстренные службы";
			$this->parseHeaders();
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}
			
			$this->Table = new ExtrasListTable($this->ObjectType);
			$this->Table->Show();
		}

        private function showHelper($id = 0, $data = array())
        {
?>
            <script>

                function checkDoubles()
                {
                    if(($('#num').val() == '') && ($('#phone').val() == ''))
                        return false;

                    $.ajax({
                        type: 'post',
                        url : 'index.php?module=orders&act=checkdoubles&nohead=1&num=' + $('#num').val() + '&id=<?php echo $id; ?>' + '&phone=' + $('#phone').val(),
                        success : function(msg)
                        {
                            $('#doubles').html(msg);
                        }
                    });

                    return true;
                }

                function checkBlacklist()
                {
                    if(($('#num').val() == '') && ($('#phone').val() == ''))
                        return false;

                    $.ajax({
                        type: 'post',
                        url : 'index.php?module=blacklist&act=check&nohead=1&num=' + $('#num').val() + '&phone=' + $('#phone').val(),
                        success : function(msg)
                        {
                            if(msg.length)
                            {
                                $('#btnAddToBlacklisttr').hide();
                                $('#black').html(msg);
                            }
                        }
                    });

                    return true;
                }

                function cancelOperator()
                {
                    $.ajax({
                        type : 'post',
                        url  : 'index.php?module=orders&nohead=1&act=canceloperator&id=<?php echo $id; ?>',
                        success : function(msg){
                                $('#operator_id').html('');
                        }
                    })
                }

                function setOperator()
                {
                    $.ajax({
                        type : 'post',
                        url  : 'index.php?module=orders&nohead=1&act=setoperator&id=<?php echo $id; ?>',
                        success : function(msg){
                            var data = eval('(' + msg + ')');
                            if(data.result == 'ok')
                            {
                                var html = 'На доведении у оператора <strong>' + data.name + '</strong>';
                                html += ' <a href="javascript:cancelOperator()">отменить</a>';
                                $('#operator_id').html(html);
                            }
                            else if(data.result == 'error')
                            {
                                $('#operator_id').html('<span style="color:red">' + data.error + '</span>');
                            }
                        }
                    })
                }

                $(document).ready(function(){
                    $('#phone').bind('change', checkDoubles);

                    checkDoubles();
                    checkBlacklist();

                    $('#btnReturn').click(function(){
                        $("#cover").show();
                        $('#return_dlg').dialog({
                            width:550,
                            autoOpen:false,
                            close: function(event, ui)
                            {
                                $("#cover").hide();
                                //$("#errField").hide();
                                return true;
                            }
                        }).dialog('open');
                        return true;
                    });

                    $('#btnRecall').click(function(){
                        $("#cover").show();
                        $('#recall_dlg').dialog({
                            width:550,
                            autoOpen:false,
                            close: function(event, ui)
                            {
                                $("#cover").hide();
                                //$("#errField").hide();
                                return true;
                            }
                        }).dialog('open');
                        return false;
                    });

                    $('#btnDelegate').click(function(){
                        $("#cover").show();
                        $('#delegate_dlg').dialog({
                            width:550,
                            autoOpen:false,
                            close: function(event, ui)
                            {
                                $("#cover").hide();
                                //$("#errField").hide();
                                return true;
                            }
                        }).dialog('open');
                        return false;
                    });

                    $('#btnReopen').click(function(){
                        $("#cover").show();
                        $('#reopen_dlg').dialog({
                            width:550,
                            autoOpen:false,
                            close: function(event, ui)
                            {
                                $("#cover").hide();
                                //$("#errField").hide();
                                return true;
                            }
                        }).dialog('open');
                        return true;
                    });

                    $('#btnSend').click(function(){
                        if(!$('#answer').val())
                        {
                            alert('Пожалуйста, заполните ответ перед тем, как направить запрос в контакт-центр!');
                            $('#answer')[0].focus();
                            return false;
                        }
                        return true;
                    });

                    $('#region_id').bind('change',
                    {
                        sourceField: 'region_id',
                        targetField: 'city',
                        url : 'index.php?module=cities&act=ajax&region_id=%id%',
                        emptyOption: '(...)',
                        emptyOptionSuccess: '(...)',
                        onSuccess : function(){
                            if($('#city').children().length == 2)
                                $('#city option:nth-child(2)').attr('selected', true);
                        }
                    },
                    changeHandler);

                    $('#order_form input').bind('keydown', function(e){
                        if(e.keyCode == 13)
                        {
                            e.preventDefault();
                            $('input')[($('input').index(this) + 1)].focus();
                        }
                    });
                })
            </script>
<?php
        }

		
		public function viewAction()
		{
			$this->editAction();
		}
		
		public function newAction()
		{
            $this->showHelper();

			$form = new Form("Новая запись...", array(
				array('caption' => 'Тип', 'type' => 'radio', 'name' => 'type', 'data' => new ExtraTypeSelectList(), 'value' => 1),
				array('caption' => 'Подразделение', 'type' => 'text', 'name' => 'department', 'size' => '50'),
				array('caption' => 'Фамилия', 'type' => 'text', 'name' => 'lastname', 'size' => '30'),
				array('caption' => 'Имя', 'type' => 'text', 'name' => 'firstname', 'size' => '30'),
				array('caption' => 'Отчество', 'type' => 'text', 'name' => 'middlename', 'size' => '30'),
				array('caption' => 'Нас. пункт', 'type' => 'text', 'name' => 'city', 'size' => '30'),
				array('caption' => 'Адрес', 'type' => 'text', 'name' => 'address', 'size' => '30'),
				array('caption' => 'Телефон', 'type' => 'text', 'name' => 'phone', 'size' => '30'),
				array('caption' => 'Что случилось', 'type' => 'textarea', 'name' => 'comment', 'width' => '50', 'height' => '5'),
				array('caption' => 'Call ID', 'type' => 'text', 'name' => 'call_id', 'size' => '30'),
				array('caption' => 'Статус', 'type' => 'select', 'name' => 'status', 'data' => new StatusList()),
				array('caption' => 'Результат', 'type' => 'select', 'name' => 'result', 'data' => new ResultTypeList()),
				array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Добавить', 'value' => 'Добавить', 'cancel' => 1)	
			));

			$form->Show();
		}
		
		public function editAction()
		{
			$data = $this->getline("select * from extras where id = ".$this->Id);
	
			$form = new Form("Редактирование записи...", array(
				array('caption' => 'Оператор', 'type' => 'select', 'name' => 'author_id', 'value' => $data['author_id'], 'data' => new UserSelectList()),
				array('caption' => 'Дата/время', 'type' => 'label', 'name' => 'created', 'value' => $data['created']),
				array('caption' => 'Тип', 'type' => 'radio', 'name' => 'type', 'value' => $data['type'], 'data' => new ExtraTypeSelectList()),
				array('caption' => 'Подразделение', 'type' => 'text', 'name' => 'department', 'value' => $data['department'], 'size' => '50'),
				array('caption' => 'Фамилия', 'type' => 'text', 'name' => 'lastname', 'value' => $data['lastname'], 'size' => '30'),
				array('caption' => 'Имя', 'type' => 'text', 'name' => 'firstname', 'value' => $data['firstname'], 'size' => '30'),
				array('caption' => 'Отчество', 'type' => 'text', 'name' => 'middlename', 'value' => $data['middlename'], 'size' => '30'),
				array('caption' => 'Нас. пункт', 'type' => 'text', 'name' => 'city', 'value' => $data['city'], 'size' => '30'),
				array('caption' => 'Адрес', 'type' => 'text', 'name' => 'address', 'value' => $data['address'], 'size' => '30'),
				array('caption' => 'Телефон', 'type' => 'text', 'name' => 'phone', 'value' => $data['phone'], 'size' => '30'),
				array('caption' => 'Что случилось', 'type' => 'textarea', 'name' => 'comment', 'value' => $data['comment'], 'width' => '50', 'height' => '5'),
				array('caption' => 'Статус', 'type' => 'select', 'name' => 'status', 'value' => $data['status'], 'data' => new StatusList()),
				array('caption' => 'Результат', 'type' => 'select', 'name' => 'result', 'value' => $data['result'], 'data' => new ResultTypeList()),
				array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'id', 'value' => $this->Id),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'upd'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'Сохранить', 'value' => 'Сохранить', 'cancel' => 1)	
			));
			
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                $form->SetElementAttribute('*', 'readonly', 1);
				
			$form->Show();
		}
		
		public function addAction()
		{	
			if(!ACL::Check($this->ObjectType, ACL::CREATE))
			{
				$this->Page->Redirect(START_PAGE);
			}
			
			$query = new Query("insert");
			$query->AddTable("extras");
            $query->AddField("author_id", $_SESSION['user_id']);
			$query->AddField("created", 'inline:UNIX_TIMESTAMP()');
            $query->AddField("type", $this->type);
			$query->AddField("department", $this->department);
			$query->AddField("lastname", $this->lastname);
			$query->AddField("firstname", $this->firstname);
			$query->AddField("middlename", $this->middlename);
			$query->AddField("city", $this->city);
            $query->AddField("region_id", $this->region_id);
			$query->AddField("address", $this->address);
			$query->AddField("phone", $this->phone);
			$query->AddField("comment", $this->comment);
			$query->AddField("call_id", $this->call_id);
			$query->AddField("status", $this->status);
			$query->AddField("result", $this->result);

			$query->Execute();
			
			LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

		public function updAction()
		{	
			if(ACL::Check($this->ObjectType, ACL::UPDATE))
			{
				$query = new Query("update");
				$query->AddTable("extras");
				$query->AddField("type", $this->type);
				$query->AddField("department", $this->department);
				$query->AddField("lastname", $this->lastname);
				$query->AddField("firstname", $this->firstname);
				$query->AddField("middlename", $this->middlename);
				$query->AddField("city", $this->city);
                $query->AddField("region_id", $this->region_id);
				$query->AddField("address", $this->address);
				$query->AddField("phone", $this->phone);
				$query->AddField("comment", $this->comment);
				$query->AddField("call_id", $this->call_id);
				$query->AddField("status", $this->status);
				$query->AddField("result", $this->result);

				$query->Where->Add("id = ".$this->Id);
				$query->Execute();
			}
			
			LocationManager::redirect("index.php?module=".$this->ModuleName);
		}		

		public function delAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::DELETE))
			{
				LocationManager::redirect(START_PAGE);
			}			

            $this->sql("delete from extras where id = ".$this->Id);

            LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

		protected function parseHeaders()
		{
			$this->Id = $_REQUEST["id"];
			$this->author_id = $_REQUEST["author_id"];
			$this->created = $_REQUEST["created"];
			$this->type = $_REQUEST["type"];
			$this->department = $_REQUEST["department"];
			$this->lastname = $_REQUEST["lastname"];
			$this->firstname = $_REQUEST["firstname"];
			$this->middlename = $_REQUEST["middlename"];
            $this->city = $_REQUEST["city"];
            if($this->city == '(...)')
                $this->city = '';
            $this->region_id = $_REQUEST["region_id"];
			$this->address = $_REQUEST["address"];
			$this->phone = $_REQUEST["phone"];
			$this->comment = $_REQUEST["comment"];
			$this->call_id = $_REQUEST["call_id"];
			$this->status = $_REQUEST["status"];
			$this->result = $_REQUEST["result"];
		}	
	}

	class ExtrasListTable extends Table
	{	
		public function __construct($objectType)
		{
			$this->ObjectType = $objectType;
			
			$query = new Query("select");
			$query->AddTable("extras");
			$query->Fields = array("id", "author_id", "created", "type", "department", "lastname", "phone", "status", "result");		
	
			$captions = array("", "Оператор", "Дата/время", "Угроза", "Подразделение", "Фамилия", "Телефон", "Статус", "Результат");
	
			parent::Table("Экстренные службы",
						  $query,
						  $captions,
						  $query->Fields);

			$this->ShowNewLinks = ACL::Check($this->ObjectType, ACL::CREATE);
			$this->ShowEditLinks = true;
			$this->ShowDeleteLinks = ACL::Check($this->ObjectType, ACL::DELETE);
		}
	}	
	