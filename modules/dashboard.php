<?php

    class Dashboard extends Document
	{
		public function __construct($moduleName, $page)
		{
            $this->ObjectType = OBJECT_DASHBOARD;
			$this->Page = $page;
			$this->Page->Title = "Статистика";
            unset($_SESSION['__active_menu_item']);

            if($_SESSION['auth_status'] != Auth::AUTH_STATUS_OK)
                LocationManager::redirect("logout.php");

            if(!ACL::Check($this->ObjectType, ACL::READ))
                LocationManager::redirect("index.php?module=orders&mode=2");
		}

        public function acceptAction()
        {
            if($_REQUEST['accepted'] && $_REQUEST['id'])
                $this->sql("insert into user_news (user_id, news_id) values (".$_SESSION['user_id'].", ".$_REQUEST['id'].")");

            LocationManager::redirect('index.php?module=dashboard');
        }

		public function indexAction()
		{
            $depts = array();
            if(!ACL::Check(OBJECT_ORDERS, 'allaccess'))
            {
                $ds = new DataSource("select department_id, name from user_department
                join departments on department_id = departments.id
                where user_id = ".$_SESSION['user_id']);
                while($data = $ds->GetString())
                {
                    $depts[] = $data['department_id'];
                    $names[] = $data['name'];
                }
            }

            $regions = array();
            $uds = new DataSource("select region_id, name from user_region
            join regions on region_id = regions.id
            where user_id = " . $_SESSION['user_id']);
            while($udata = $uds->GetString())
            {
                $regions[] = $udata['region_id'];
                $rnames[]  = $udata['name'];
            }

            $high = array();
            $ids = new DataSource("select owner_id from comments where module = 'orders' and high = 1");
            while($hdata = $ids->GetString())
            {
                $high[] = $hdata['owner_id'];
            }
?>
        <div id="cover"></div>

                <strong>Статистика работы с обращениями граждан, поступающими в Правительство Архангельской области</strong>

                <br><br>

<?php
            if($names)
            {
                foreach($names as $name)
                    echo '<i>'.$name.'</i><br>';

                echo '<br><br>';
            }

            if($rnames)
            {
                echo '<strong>Районы: </strong><br><br>';
                foreach($rnames as $name)
                    echo '<i>'.$name.'</i><br>';

                echo '<br><br>';
            }
?>
            <style>
                .stat_table td
                {
                    padding-top: 3px;
                    padding-bottom: 3px;
                    padding-left: 10px;
                    padding-right: 10px;
                }

                .data
                {
                    text-align: center;
                    font-weight: bold;
                }
            </style>

            <table class="stat_table">
            <tr style="background-color: #4f81bd; font-weight: bold; color: white; text-align: center;">
                <td>
                    Категории
                </td>
                <td>
                    Всего
                </td>
                <td>
                    Обработанных<br>звонков
                </td>
                <td>
                    Информ-<br>запросов
                </td>
                <td>
                    Критических<br>замечаний
                </td>
                <td>
                    С уровнем<br>угрозы
                </td>
            </tr>
            <tr style="background-color: #b8cce4;">
                <td>
                    Сегодня
                </td>
                <td class="data">
<?php
    $query = new Query("select");
    $query->AddTable("orders");
    $query->AddField("COUNT(*)");
    $query->Where->Add("created >= ".DT::getUnixTime(DT::getDate(DT::Now)));
    if($depts)
        $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
    if($regions)
        $query->Where->Add("region_id in (".implode(',', $regions).")");
    $ds = new DataSource($query);
    $data = $ds->GetString();
    if($data[0])
        echo $data[0];
?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".DT::getUnixTime(DT::getDate(DT::Now)));
                        $query->Where->Add("type = 1");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".DT::getUnixTime(DT::getDate(DT::Now)));
                        $query->Where->Add("type = 2");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".DT::getUnixTime(DT::getDate(DT::Now)));
                        $query->Where->Add("type = 3");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".DT::getUnixTime(DT::getDate(DT::Now)));
                        $query->Where->Add("alert in (2,3)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
            </tr>
            <tr style="background-color: #dce6f1;">
                <td>
                    Вчера
                </td>
                <td class="data">
<?php
    $query = new Query("select");
    $query->AddTable("orders");
    $query->AddField("COUNT(*)");
    $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 24*60*60)." and created <= ".DT::getUnixTime(DT::getDate(DT::Now)));
    if($depts)
        $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
    if($regions)
        $query->Where->Add("region_id in (".implode(',', $regions).")");
    $ds = new DataSource($query);
    $data = $ds->GetString();
    if($data[0])
        echo $data[0];
?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 24*60*60)." and created <= ".DT::getUnixTime(DT::getDate(DT::Now)));
                        $query->Where->Add("type = 1");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 24*60*60)." and created <= ".DT::getUnixTime(DT::getDate(DT::Now)));
                        $query->Where->Add("type = 2");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 24*60*60)." and created <= ".DT::getUnixTime(DT::getDate(DT::Now)));
                        $query->Where->Add("type = 3");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 24*60*60)." and created <= ".DT::getUnixTime(DT::getDate(DT::Now)));
                        $query->Where->Add("alert in (2,3)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
            </tr>
            <tr style="background-color: #b8cce4;">
                <td>
                    Неделя
                </td>
                <td class="data">
<?php
    $query = new Query("select");
    $query->AddTable("orders");
    $query->AddField("COUNT(*)");
    $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 6*24*60*60));
    if($depts)
        $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
    if($regions)
        $query->Where->Add("region_id in (".implode(',', $regions).")");
    $ds = new DataSource($query);
    $data = $ds->GetString();
    if($data[0])
        echo $data[0];
?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 6*24*60*60));
                        $query->Where->Add("type = 1");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 6*24*60*60));
                        $query->Where->Add("type = 2");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 6*24*60*60));
                        $query->Where->Add("type = 3");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 6*24*60*60));
                        $query->Where->Add("alert in (2,3)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
            </tr>
            <tr style="background-color: #dce6f1;">
                <td>
                    Месяц
                </td>
                <td class="data">
<?php
    $query = new Query("select");
    $query->AddTable("orders");
    $query->AddField("COUNT(*)");
    $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 30*24*60*60));
    if($depts)
        $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
    if($regions)
        $query->Where->Add("region_id in (".implode(',', $regions).")");
    $ds = new DataSource($query);
    $data = $ds->GetString();
    if($data[0])
        echo $data[0];
?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 30*24*60*60));
                        $query->Where->Add("type = 1");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 30*24*60*60));
                        $query->Where->Add("type = 2");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 30*24*60*60));
                        $query->Where->Add("type = 3");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 30*24*60*60));
                        $query->Where->Add("alert in (2,3)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
            </tr>
            <tr style="background-color: #b8cce4;">
                <td>
                    Год
                </td>
                <td class="data">
<?php
    $query = new Query("select");
    $query->AddTable("orders");
    $query->AddField("COUNT(*)");
    $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 365*24*60*60));
    if($depts)
        $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
    if($regions)
        $query->Where->Add("region_id in (".implode(',', $regions).")");
    $ds = new DataSource($query);
    $data = $ds->GetString();
    if($data[0])
        echo $data[0];
?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 365*24*60*60));
                        $query->Where->Add("type = 1");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 365*24*60*60));
                        $query->Where->Add("type = 2");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 365*24*60*60));
                        $query->Where->Add("type = 3");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created >= ".(DT::getUnixTime(DT::getDate(DT::Now)) - 365*24*60*60));
                        $query->Where->Add("alert in (2,3)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
            </tr>
            <tr style="background-color: #dce6f1;">
                <td>
                    Всего
                </td>
                <td class="data">
<?php
    $query = new Query("select");
    $query->AddTable("orders");
    $query->AddField("COUNT(*)");
    if($depts)
        $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
    if($regions)
        $query->Where->Add("region_id in (".implode(',', $regions).")");
    $ds = new DataSource($query);
    $data = $ds->GetString();
    if($data[0])
        echo $data[0];
?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("type = 1");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("type = 2");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("type = 3");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("alert in (2,3)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="6">

                </td>
            </tr>
            <tr>
                <td colspan="6">

                </td>
            </tr>
            <tr style="background-color: #c0504d; font-weight: bold; color: white; text-align: center;">
                <td>
                    Срок реагирования
                </td>
                <td>
                    Всего
                </td>
                <td>
                    15-7 дней
                </td>
                <td>
                    7-2 дня
                </td>
                <td>
                    Менее 2-х дней
                </td>
                <td>
                    Просрочено
                </td>
            </tr>
            <tr style="background-color: #e6b8b7;">
                <td>
                    В работе
                </td>
                <td class="data">
<?php
    $query = new Query("select");
    $query->AddTable("orders");
    $query->AddField("COUNT(*)");
    $query->Where->Add("status in (5, 7, 10, 13, 15)");
    if($depts)
        $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
    if($regions)
        $query->Where->Add("region_id in (".implode(',', $regions).")");
    $ds = new DataSource($query);
    $data = $ds->GetString();
    if($data[0])
        echo $data[0];
?>
                </td>
                <td class="data">
                    <?php
                        $endOfDay = DT::getUnixTime(DT::getDate(DT::Now)) + 24*60*60;

                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created <= ".($endOfDay - (15-(15+1))*24*60*60));
                        $query->Where->Add("created >= ".($endOfDay - (15-(7+1))*24*60*60));
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created <= ".($endOfDay - (15-(7+1))*24*60*60));
                        $query->Where->Add("created >= ".($endOfDay - (15-(2+1))*24*60*60));
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data" style="color: red;">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created <= ".($endOfDay - (15-(2+1))*24*60*60));
                        $query->Where->Add("created >= ".($endOfDay - (15-(0+1))*24*60*60));
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data" style="color: red;">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created <= ".($endOfDay - (15-(0+1))*24*60*60));
                        $query->Where->Add("created >= ".($endOfDay - (15-(-100+1))*24*60*60));
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
            </tr>
            <tr style="background-color: #f2dcdb;">
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;в т.ч. делегировано
                </td>
                <td class="data">
<?php
    $query = new Query("select");
    $query->AddTable("orders");
    $query->AddField("COUNT(*)");
    $query->Where->Add("status in (10)");
    if($depts)
        $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
    if($regions)
        $query->Where->Add("region_id in (".implode(',', $regions).")");
    $ds = new DataSource($query);
    $data = $ds->GetString();
    if($data[0])
        echo $data[0];
?>
                </td>
                <td class="data">
                    <?php
                        $endOfDay = DT::getUnixTime(DT::getDate(DT::Now)) + 24*60*60;

                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created <= ".($endOfDay - (15-(15+1))*24*60*60));
                        $query->Where->Add("created >= ".($endOfDay - (15-(7+1))*24*60*60));
                        $query->Where->Add("status in (10)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created <= ".($endOfDay - (15-(7+1))*24*60*60));
                        $query->Where->Add("created >= ".($endOfDay - (15-(2+1))*24*60*60));
                        $query->Where->Add("status in (10)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data" style="color: red;">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created <= ".($endOfDay - (15-(2+1))*24*60*60));
                        $query->Where->Add("created >= ".($endOfDay - (15-(0+1))*24*60*60));
                        $query->Where->Add("status in (10)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data" style="color: red;">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("created <= ".($endOfDay - (15-(0+1))*24*60*60));
                        $query->Where->Add("created >= ".($endOfDay - (15-(-100+1))*24*60*60));
                        $query->Where->Add("status in (10)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="6">

                </td>
            </tr>
            <tr>
                <td colspan="6">

                </td>
            </tr>
            <tr style="background-color: #f79646; font-weight: bold; color: white; text-align: center;">
                <td>
                    Категории
                </td>
                <td>
                    Текущих
                </td>
                <td>
                    Обработанных<br>звонков
                </td>
                <td>
                    Информ-<br>запросов
                </td>
                <td>
                    Критических<br>замечаний
                </td>
                <td>
                    В архиве
                </td>
            </tr>
            <tr style="background-color: #fcd5b4;">
                <td>
                    С уровнем угрозы<br>ЖЕЛТЫЙ
                </td>
                <td class="data">
<?php
    $query = new Query("select");
    $query->AddTable("orders");
    $query->AddField("COUNT(*)");
    $query->Where->Add("status in (5, 7, 10, 13, 15)");
    $query->Where->Add("alert in (2)");
    if($depts)
        $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
    if($regions)
        $query->Where->Add("region_id in (".implode(',', $regions).")");
    $ds = new DataSource($query);
    $data = $ds->GetString();
    if($data[0])
        echo $data[0];
?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        $query->Where->Add("alert in (2)");
                        $query->Where->Add("type = 1");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        $query->Where->Add("alert in (2)");
                        $query->Where->Add("type = 2");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        $query->Where->Add("alert in (2)");
                        $query->Where->Add("type = 3");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("status in (20)");
                        $query->Where->Add("alert in (2)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
            </tr>
            <tr style="background-color: #fde9d9;">
                <td>
                    С уровнем угрозы<br>КРАСНЫЙ
                </td>
                <td class="data">
<?php
    $query = new Query("select");
    $query->AddTable("orders");
    $query->AddField("COUNT(*)");
    $query->Where->Add("status in (5, 7, 10, 13, 15)");
    $query->Where->Add("alert in (3)");
    if($depts)
        $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
    if($regions)
        $query->Where->Add("region_id in (".implode(',', $regions).")");
    $ds = new DataSource($query);
    $data = $ds->GetString();
    if($data[0])
        echo $data[0];
?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        $query->Where->Add("alert in (3)");
                        $query->Where->Add("type = 1");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        $query->Where->Add("alert in (3)");
                        $query->Where->Add("type = 2");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        $query->Where->Add("alert in (3)");
                        $query->Where->Add("type = 3");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("status in (20)");
                        $query->Where->Add("alert in (3)");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
            </tr>
            <tr style="background-color: #fcd5b4;">
                <td>
                    С комментарием<br>Губернатора
                </td>
                <td class="data">
<?php
    $query = new Query("select");
    $query->AddTable("orders");
    $query->AddField("COUNT(*)");
    $query->Where->Add("status in (5, 7, 10, 13, 15)");
    $query->Where->Add("id in (".implode(',', $high).")");
    if($depts)
        $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
    if($regions)
        $query->Where->Add("region_id in (".implode(',', $regions).")");
    $ds = new DataSource($query);
    $data = $ds->GetString();
    if($data[0])
        echo $data[0];
?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        $query->Where->Add("id in (".implode(',', $high).")");
                        $query->Where->Add("type = 1");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        $query->Where->Add("id in (".implode(',', $high).")");
                        $query->Where->Add("type = 2");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("status in (5, 7, 10, 13, 15)");
                        $query->Where->Add("id in (".implode(',', $high).")");
                        $query->Where->Add("type = 3");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
                <td class="data">
                    <?php
                        $query = new Query("select");
                        $query->AddTable("orders");
                        $query->AddField("COUNT(*)");
                        $query->Where->Add("status in (20)");
                        $query->Where->Add("id in (".implode(',', $high).")");
                        if($depts)
                            $query->Where->Add("department_id in (".implode(',', $depts).") or municipality_id in (".implode(',', $depts).")");
                        if($regions)
                            $query->Where->Add("region_id in (".implode(',', $regions).")");
                        $ds = new DataSource($query);
                        $data = $ds->GetString();
                        if($data[0])
                            echo $data[0];
                    ?>
                </td>
            </tr>
            </table>


<?php
            $ds = new DataSource("select * from news where author_id != ".$_SESSION['user_id']."
            and id not in (select news_id from user_news where user_id = ".$_SESSION['user_id'].")
            order by created
            limit 1
            ");
            while($data = $ds->GetString())
            {
                $form = new Form('', array(
                    array('type' => 'label', 'value' => '<strong>'.$data['name'].'</strong>'),
                    array('type' => 'label', 'value' => $data['comment']),
                    array('type' => 'label', 'value' => '<br><br><br>', 'name' => 'label_sep'),
                    array('type' => 'checkbox', 'name' => 'accepted', 'label' => 'Я ознакомлен(а) с содержанием новости', 'value' => 1),
                    array('type' => 'submit', 'name' => 'btnSave', 'value' => 'Закрыть'),
                    array('type' => 'hidden', 'name' => 'module', 'value' => 'dashboard'),
                    array('type' => 'hidden', 'name' => 'act', 'value' => 'accept'),
                    array('type' => 'hidden', 'name' => 'id', 'value' => $data['id']),
                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1)
                                     ));
                require_once "userfiles.php";
                Userfiles::show($data['id'], 'news', $form, false, 'label_sep');
                
                $form->id = 'news_'.$data['id'].'_form';
?>
            <style>
                #<?php echo $form->id; ?> table {border:0; background-color: transparent;}
            </style>

            <div id="news_<?php echo $data['id']; ?>_dlg" style="display:none;">
                <?php
                    $form->Show();
                ?>
            </div>

            <script>
                $(document).ready(function(){
                    $("#cover").show();
                    $('#news_<?php echo $data['id']; ?>_dlg').dialog({
                        width:550,
                        autoOpen:true,
                        close: function(event, ui)
                        {
                            $("#cover").hide();
                            $("#errField").hide();
                            return true;
                        }
                    }).dialog('open');

                    $('#<?php echo $form->id; ?>').bind('submit', function(){
                        if(!$('#accepted').is(':checked'))
                        {
                            alert('Пожалуйста, подтвердите ознакомление с новостью!');
                            return false;
                        }
                        return true;
                    })

                });
            </script>
<?php
            }
		}
    }
