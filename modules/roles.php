<?php
//--------------------------------------- Roles ----------------------------------------------

	class Roles extends Document
	{		
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_ROLES;
			$this->ModuleName = $moduleName;
			$this->parseHeaders();
			$this->Page = $page;
            $this->Page->Title = "Роли пользователей";
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				$this->Page->Redirect(START_PAGE);
			}			
			$this->Table = new RolesListTable($this->ObjectType);
			$this->Table->Show();

            LocationManager::add();
		}
		
		public function viewAction()
		{
			$this->editAction();
		}
		
		public function newAction()
		{
				$form = new Form('Новая роль', array(
					array('caption' => 'Название', 'type' => 'text', 'name' => 'name', 'size' => 50, 'value' => $data['name'], 'required' => '1'),
					array('type' => 'select', 'caption' => 'Проект по умолчанию', 'name' => 'project_id', 'data' => new ProjectSelectList(), 'value' => $data['project_id'], 'empty_option' => '(...)'),
                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
													  array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
													  array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
													  array('type' => 'submit', 'name' => 'Добавить', 'value' => 'Добавить', 'cancel' => 1)
													  )
								);
								
                $ds = new DataSource('select id, name from object_groups order by name desc');
                while($group_data = $ds->GetString())
                {
                	$group_id = $group_data['id'];
                	$group_name = $group_data['name'];
                	
                	$form->InsertBefore('act', array());
                	$form->InsertBefore('act', array('type' => 'subsection', 'caption' => $group_name));
                	
                	$obj_ds = new DataSource('select id, name from object_types where group_id = '.$group_id.' order by name desc');
                	while($object_data = $obj_ds->GetString())
                	{
                		$object_id = $object_data['id'];
                		$object_code = $object_data['code'];
                		$object_name = $object_data['name'];
                		
                		$checkcheck = array('acl_'.$object_id.'_'.ACL::READ, 'acl_'.$object_id.'_'.ACL::CREATE, 'acl_'.$object_id.'_'.ACL::UPDATE, 'acl_'.$object_id.'_'.ACL::DELETE);

                		$form->InsertBefore('act', array());
                		$form->InsertBefore('act', array('type' => 'label', 'caption' => $object_name, 'nobr' => '1'));
                		$form->InsertBefore('act', array('type' => 'checkbox', 'label' => 'Все', 'name' => $object_id.'_all', 'value' => '1', 'nobr' => '1'));
                		$form->InsertBefore('act', array('type' => 'checkbox', 'label' => 'Просмотр', 'name' => 'acl_'.$object_id.'_'.ACL::READ, 'value' => '1', 'nobr' => '1', 'uncheckunchecksimple' => $object_id.'_all'));
                		$form->InsertBefore('act', array('type' => 'checkbox', 'label' => 'Создание', 'name' => 'acl_'.$object_id.'_'.ACL::CREATE, 'value' => '1', 'nobr' => '1', 'uncheckunchecksimple' => $object_id.'_all'));
                		$form->InsertBefore('act', array('type' => 'checkbox', 'label' => 'Правка', 'name' => 'acl_'.$object_id.'_'.ACL::UPDATE, 'value' => '1', 'nobr' => '1', 'uncheckunchecksimple' => $object_id.'_all'));
                		$form->InsertBefore('act', array('type' => 'checkbox', 'label' => 'Удаление', 'name' => 'acl_'.$object_id.'_'.ACL::DELETE, 'value' => '1', 'nobr' => '1', 'uncheckunchecksimple' => $object_id.'_all'));
                		
                		$eop_ds = new DataSource("select code, name from eop where ot_code = '".$object_code."'");
                		while($eop_data = $eop_ds->GetString())
                		{
                			$eop_name = $eop_data['code'];
                			$eop_caption = $eop_data['name'];
                			
                			$form->InsertBefore('act', array());
                			$form->InsertBefore('act', array('type' => 'label', 'caption' => '', 'nobr' => '1'));	
                			$form->InsertBefore('act', array('type' => 'checkbox', 'label' => $eop_caption, 'name' => 'acl_'.$object_id.'_'.$eop_name, 'value' => '1', 'uncheckunchecksimple' => $object_id.'_all', 'nobr' => '1'));
                			$form->InsertBefore('act', array());
                			$checkcheck[] = 'acl_'.$object_id.'_'.$eop_name;
                		}

                        ?>
                            <script>
                                $(document).ready(function(){
                                    $('#<?php  echo $object_id.'_all'; ?>').click(function(){
                                        var checked = $('#<?php  echo $object_id.'_all'; ?>').is(':checked')
                                        <?php
                                            foreach($checkcheck as $checkbox)
                                            {
                                                ?>
                                                    $('#<?php  echo $checkbox; ?>').attr('checked', checked)
                                                <?php
                                            }
                                        ?>
                                    })

                                    $('#<?php echo implode(', #', $checkcheck); ?>').click(function(){
                                        if(!$(this).is(':checked'))
                                            $('#<?php echo $object_id.'_all'; ?>').attr('checked', false)
                                    })
                                })
                            </script>
                        <?php
                	}
                }

				$form->Show();			
		}

        public function setpdAction()
        {
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                exit;

            $this->sql("delete from role_pd where role_id = " . $_REQUEST['id'] . " and pd = '" . $_REQUEST['pd'] . "'");
            if($_REQUEST['value'])
                $this->sql("insert into role_pd (role_id, pd) values ( " . $_REQUEST['id'] . ", '" . $_REQUEST['pd'] . "')");
        }
		
		public function editAction()
		{
?>
            <script>
                $(document).ready(function(){
                    $('input[id^="pd_"]').click(function(){
                        var cb = this;
                        var cb_id = cb.id.split('_')[1];
                        $.ajax({
                            type : 'post',
                            url  : 'index.php?module=roles&nohead=1&act=setpd&id=<?php echo $this->Id; ?>&pd=' + cb_id + '&value=' + Number($(cb).is(':checked')),
                            success : function(msg){

                            },
                            error : function(msg){
                                alert('Ошибка связи с сервером!');
                                $(cb).removeAttr('checked');
                            }
                        })
                    })
                })
            </script>
<?php

                $dt = new DT();

				$data = $this->getline("select * from roles where id = ".$this->Id);

				$form = new Form($data['name'], array(
					array('caption' => 'Название', 'type' => 'text', 'name' => 'name', 'size' => 50, 'value' => $data['name'], 'required' => '1'),
                    array('type' => 'select', 'caption' => 'Проект по умолчанию', 'name' => 'project_id', 'data' => new ProjectSelectList(), 'value' => $data['project_id'], 'empty_option' => '(...)'),
                    array('type' => 'hidden', 'name' => 'id', 'value' => $this->Id),
                    array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                    array('type' => 'hidden', 'name' => 'act', 'value' => 'upd'),
                    array('type' => 'submit', 'name' => 'Обновить данные', 'value' => 'Обновить данные', 'cancel' => 1)
													  )
								);

                $ds = new DataSource('select id, name from object_groups order by name desc');
                while($group_data = $ds->GetString())
                {
                	$group_id = $group_data['id'];
                	$group_name = $group_data['name'];
                	
                	$form->InsertBefore('act', array());
                	$form->InsertBefore('act', array('type' => 'subsection', 'caption' => $group_name));
                	
                	$obj_ds = new DataSource('select id, code, name from object_types where group_id = '.$group_id.' order by name');
                	while($object_data = $obj_ds->GetString())
                	{
                		$object_id = $object_data['id'];
                		$object_code = $object_data['code'];
                		$object_name = $object_data['name'];
                		
                		$checkcheck = array('acl_'.$object_id.'_'.ACL::READ, 'acl_'.$object_id.'_'.ACL::CREATE, 'acl_'.$object_id.'_'.ACL::UPDATE, 'acl_'.$object_id.'_'.ACL::DELETE);
                		
                		$checked_view = ACL::Check($object_code, ACL::READ, $this->Id)?'1':'0';
                		$checked_create = ACL::Check($object_code, ACL::CREATE, $this->Id)?'1':'0';
                		$checked_edit = ACL::Check($object_code, ACL::UPDATE, $this->Id)?'1':'0';
                		$checked_del = ACL::Check($object_code, ACL::DELETE, $this->Id)?'1':'0';
                		$checked_all = (($checked_view == '1') && ($checked_create == '1') && ($checked_edit == '1') && ($checked_del == '1'))?'1':'0';

                		$form->InsertBefore('act', array());
                		$form->InsertBefore('act', array('type' => 'label', 'caption' => $object_name, 'nobr' => '1'));
                		$form->InsertBefore('act', array('type' => 'checkbox', 'label' => 'Все', 'name' => $object_id.'_all', 'value' => '1', 'nobr' => '1', 'checkcheck' => $checkcheck, 'checked' => $checked_all));
                		$form->InsertBefore('act', array('type' => 'checkbox', 'label' => 'Просмотр', 'name' => 'acl_'.$object_id.'_'.ACL::READ, 'value' => '1', 'nobr' => '1', 'checked' => $checked_view, 'uncheckunchecksimple' => $object_id.'_all'));
                		$form->InsertBefore('act', array('type' => 'checkbox', 'label' => 'Создание', 'name' => 'acl_'.$object_id.'_'.ACL::CREATE, 'value' => '1', 'nobr' => '1', 'checked' => $checked_create, 'uncheckunchecksimple' => $object_id.'_all'));
                		$form->InsertBefore('act', array('type' => 'checkbox', 'label' => 'Правка', 'name' => 'acl_'.$object_id.'_'.ACL::UPDATE, 'value' => '1', 'nobr' => '1', 'checked' => $checked_edit, 'uncheckunchecksimple' => $object_id.'_all'));
                		$form->InsertBefore('act', array('type' => 'checkbox', 'label' => 'Удаление', 'name' => 'acl_'.$object_id.'_'.ACL::DELETE, 'value' => '1', 'nobr' => '1', 'checked' => $checked_del, 'uncheckunchecksimple' => $object_id.'_all'));
                		
                		$eop_ds = new DataSource("select code, name from eop where ot_code = '".$object_code."'");
                		while($eop_data = $eop_ds->GetString())
                		{
                			$eop_name = $eop_data['code'];
                			$eop_caption = $eop_data['name'];
                			
                			$checked = ACL::Check($object_code, $eop_name, $this->Id)?'1':'0';
                			
                			$form->InsertBefore('act', array());
                			$form->InsertBefore('act', array('type' => 'label', 'caption' => '', 'nobr' => '1'));	
                			$form->InsertBefore('act', array('type' => 'checkbox', 'label' => $eop_caption, 'name' => 'acl_'.$object_id.'_'.$eop_name, 'value' => '1', 'uncheckunchecksimple' => $object_id.'_all', 'nobr' => '1', 'checked' => $checked));
                			$form->InsertBefore('act', array());
                			$checkcheck[] = 'acl_'.$object_id.'_'.$eop_name;
                			$form->SetElementAttribute($object_id.'_all', 'checked', $checked_all & $checked);	
                		}

                        ?>
                            <script>
                                $(document).ready(function(){
                                    $('#<?php  echo $object_id.'_all'; ?>').click(function(){
                                        var checked = $('#<?php  echo $object_id.'_all'; ?>').is(':checked')
                                        <?php
                                            foreach($checkcheck as $checkbox)
                                            {
                                                ?>
                                                    $('#<?php  echo $checkbox; ?>').attr('checked', checked)
                                                <?php
                                            }
                                        ?>
                                    })

                                    $('#<?php echo implode(', #', $checkcheck); ?>').click(function(){
                                        if(!$(this).is(':checked'))
                                            $('#<?php echo $object_id.'_all'; ?>').attr('checked', false)
                                    })
                                })
                            </script>
                        <?php
                	}
                }

                if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                    $form->SetElementAttribute('*', 'readonly', 1);

            echo '<table style="border:0px;"><tr><td>';
            $form->Show();
            echo '</td><td style="vertical-align:top; padding-left: 10px;">';
            $table = new ProjectRoleListTable($this->Id);
            $table->Show();
            echo '<br><br>';
            $pdForm = new Form('Доступ к персональным данным', array(
                array('type' => 'hidden', 'name' => 'sep')
            ));

            $pd = array();
            $ds = new DataSource('select pd from role_pd where role_id = ' . $data['id']);
            while($edata = $ds->GetString())
                $pd[] = $edata[0];

            $ds = new DataSource('select * from pd');
            while($cdata = $ds->GetString())
            {
                $pdForm->InsertBefore('sep', array(
                    'type' => 'checkbox', 'id' => 'pd_' . $cdata['name'], 'name' => 'pd[]', 'label' => $cdata['title'], 'value' => $cdata['name'], 'checked' => in_array($cdata['name'], $pd)
                ));
            }

            $pdForm->Show();
            echo '</td></tr></table>';

            LocationManager::add();
		}	
		
		public function addAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::CREATE))
			{
				$this->Page->Redirect(START_PAGE);
			}			
			
            $dt = new DT();

			$this->sql("insert into roles (
					name,
					project_id
                    ) values
                                              (
					'".$this->name."',
					'".$this->project_id."'
                    )");
			
			$id = $this->last_insert_id();
			$this->_process_roles($id);

			LocationManager::redirect('index.php?module=roles&act=view&id=' . $id);
		}
		
		public function updAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::UPDATE))
			{
				$this->Page->Redirect(START_PAGE);
			}			
			
            $dt = new DT();
			$this->sql("update roles set
					name = '".$this->name."',
					project_id = '".$this->project_id."'

                                             where id ='".$this->Id."'");

			$this->_process_roles($this->Id);
			
			LocationManager::last();
		}
		
		public function delAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::DELETE))
			{
				$this->Page->Redirect(START_PAGE);
			}
						
			$this->sql("delete from roles where id = ".$this->Id);
			$this->sql("delete from acl where role_id = ".$this->Id);
			
			LocationManager::last();
		}

        public function addprojectAction()
        {
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                exit;

            if((int)$_REQUEST['project_id'])
            {
                $query = new Query("insert");
                $query->AddTable("project_role");
                $query->AddField("role_id", $this->Id);
                $query->AddField("project_id", (int)$_REQUEST['project_id']);
                $query->Execute();
            }

            LocationManager::last();
        }

        public function delprojectAction()
        {
            if(!ACL::Check($this->ObjectType, ACL::UPDATE))
                exit;

            $query = new Query("delete");
            $query->AddTable("project_role");
            $query->Where->Add("id = ".$this->Id);
            $query->Execute();

            LocationManager::last();
        }

		protected function parseHeaders()
		{
			$this->Id = $_REQUEST['id'];
			$this->name = $_REQUEST['name'];
            $this->project_id = $_REQUEST['project_id'];
		}

		protected function _process_roles($id)
		{
			ACL::Clear($id);
			
		    foreach($_REQUEST as $key => $value)
            {
            	if(preg_match('/^acl_(\w+)_(\w+)$/', $key, $matches))
            	{
            		ACL::Create($matches[1], $matches[2], $id, $value);
            	}
            }
		}
	}
	
	
	
	class RolesListTable extends Table
	{		
		function __construct($objectType)
		{
			$this->ObjectType = $objectType;
			
			parent::Table("Роли пользователей",
			"select
                    roles.id,
					roles.name,
					projects.name
            from roles
            left outer join projects on roles.project_id = projects.id
				   ",
						  array('', 'Название', 'Проект по умолчанию'), array('', 'roles.name', 'projects.name'));

			$this->ShowNewLinks = ACL::Check($this->ObjectType, ACL::CREATE);
			$this->ShowEditLinks = true;
			$this->ShowDeleteLinks = ACL::Check($this->ObjectType, ACL::DELETE); 

			$this->AddLabelCaption = 'Добавить роль';

		}
	}	


    class ProjectRoleListTable extends Table
    {
        public function __construct($role_id)
        {
            $this->role_id = $role_id;
            $query = new Query("select");
            $query->AddTable("project_role
            join projects on project_role.project_id = projects.id");
            $query->Fields = array("project_role.id", "projects.name");
            $query->Where->Add("role_id = ".$this->role_id);

            $captions = array("", "Название");

            parent::Table("Проекты",
                          $query,
                          $captions,
                          $query->Fields);

            $this->ShowNewLinks = false;
            $this->ShowEditLinks = false;
            $this->ShowDeleteLinks = ACL::Check(OBJECT_ROLES, ACL::UPDATE);

            $this->_delete_url = 'index.php?module=roles&act=delproject&id=%id%&nohead=1';
            $this->_edit_url = '#';
            $this->DefaultURL = '#';
            $this->Sortable = false;
            $this->Paging = false;
        }

        function End()
        {
            parent::End();

            if(ACL::Check(OBJECT_ROLES, ACL::UPDATE))
            {
                echo '<br>';
                $form = new Form('Добавить доступ к проекту', array(
                    array('caption' => 'Название', 'type' => 'select', 'name' => 'project_id', 'data' => new ProjectSelectList(), 'empty_option' => '(...)'),
                    array('type' => 'hidden', 'name' => 'id', 'value' => $this->role_id, 'nobr' => 1),
                    array('type' => 'hidden', 'name' => 'act', 'value' => 'addproject'),
                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                    array('type' => 'hidden', 'name' => 'module', 'value' => 'roles'),
                    array('type' => 'submit', 'name' => 'btnAdd', 'value' => 'Добавить', 'cancel' => 1)
                ));
                $form->Show();
            }
        }
    }