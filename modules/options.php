<?php 
	class Options extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_OPTIONS;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "Настройки системы";
            $this->Page->IncludeRichEditor = true;
			$this->parseHeaders();
		}
		
		public function indexAction()
		{
			if(!ACL::Check(OBJECT_OPTIONS, ACL::READ))
				exit;

            if($this->message)
                echo $this->message;
				
		    $form = new Form('Настройки системы', array(
		            array('type' => 'subsection', 'caption' => 'Email-рассылки'),
		            array('type' => 'text', 'caption' => 'Почтовый сервер', 'name' => 'mail_host', 'size' => 30, 'value' => Registry::Get('mail_host')),
                    array('type' => 'text', 'caption' => 'порт', 'name' => 'mail_port', 'size' => 5, 'value' => Registry::Get('mail_port')),
                    array('type' => 'text', 'caption' => 'Пользователь', 'name' => 'mail_user', 'size' => 30, 'value' => Registry::Get('mail_user')),
                    array('type' => 'text', 'caption' => 'Пароль', 'name' => 'mail_password', 'size' => 30, 'value' => Registry::Get('mail_password')),
                    array('type' => 'text', 'caption' => 'Адрес отправителя', 'name' => 'mail_sender_address', 'size' => 30, 'value' => Registry::Get('mail_sender_address')),
                    array('type' => 'text', 'caption' => 'Имя отправителя', 'name' => 'mail_sender_name', 'size' => 30, 'value' => Registry::Get('mail_sender_name')),
                    array('type' => 'text', 'caption' => 'Домен в URL', 'name' => 'server_domain', 'size' => 30, 'value' => Registry::Get('server_domain', 'gov29.firecall.net')),
                    array('type' => 'subsection', 'caption' => 'Пользователи системы'),
                    array('type' => 'select', 'caption' => 'Роль специалиста', 'name' => 'default_user_role', 'data' => new RoleSelectList(), 'value' => Registry::Get('default_user_role'), 'empty_option' => '(...)'),
                    array('type' => 'subsection', 'caption' => 'АТС'),
                    array('type' => 'text', 'caption' => 'Адрес сервера', 'name' => 'ats_host', 'size' => 30, 'value' => Registry::Get('ats_host')),
                    array('type' => 'text', 'caption' => 'порт', 'name' => 'ats_port', 'size' => 5, 'value' => Registry::Get('ats_port')),
                    array('type' => 'text', 'caption' => 'Логин', 'name' => 'ats_user', 'size' => 30, 'value' => Registry::Get('ats_user')),
                    array('type' => 'text', 'caption' => 'Пароль', 'name' => 'ats_password', 'size' => 30, 'value' => Registry::Get('ats_password')),
                    array('type' => 'text', 'caption' => 'Канал по умолчанию', 'name' => 'ats_channel', 'size' => 30, 'value' => Registry::Get('ats_channel')),
                    array('type' => 'text', 'caption' => 'Контекст по умолчанию', 'name' => 'ats_context', 'size' => 30, 'value' => Registry::Get('ats_context')),
                    array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
		            array('type' => 'submit', 'name' => 'submit_btn', 'value' => 'Обновить настройки')
		    ));
		
		    $form->Show();
		}
		
		protected function parseHeaders()
		{
		    if(($_REQUEST['submit_btn'] != '') && (ACL::Check(OBJECT_OPTIONS, ACL::UPDATE)))
		    {
		        Registry::Set('mail_host', $_REQUEST['mail_host']);
                Registry::Set('mail_port', $_REQUEST['mail_port']);
		        Registry::Set('mail_user', $_REQUEST['mail_user']);
                Registry::Set('mail_password', $_REQUEST['mail_password']);
		        Registry::Set('mail_sender_address', $_REQUEST['mail_sender_address']);
                Registry::Set('mail_sender_name', $_REQUEST['mail_sender_name']);
                Registry::Set('server_domain', $_REQUEST['server_domain']);
                Registry::Set('default_user_role', $_REQUEST['default_user_role']);

                Registry::Set('ats_host', $_REQUEST['ats_host']);
                Registry::Set('ats_port', $_REQUEST['ats_port']);
                Registry::Set('ats_user', $_REQUEST['ats_user']);
                Registry::Set('ats_password', $_REQUEST['ats_password']);
                Registry::Set('ats_channel', $_REQUEST['ats_channel']);
                Registry::Set('ats_context', $_REQUEST['ats_context']);


                $this->message = '<span style="color:green;">Настройки обновлены!</span><br><br>';
		    }
		}		
	}

