<?php 
	class Orders extends Document
	{
		public function __construct($moduleName, $page)
		{
			$this->ObjectType = OBJECT_ORDERS;
			$this->ModuleName = $moduleName;
			$this->Page = $page;
			$this->Page->Title = "Вопросы";
			$this->parseHeaders();
            if($_SESSION['user_data']['disabled'])
                $this->Page->ShowMenu = false;
            if(ACL::Check($this->ObjectType, 'listencalls'))
            {
                $this->Page->addScript('scripts/jplayer/jquery.jplayer.min.js');
                $this->Page->addCSS('styles/jplayer/jplayer.blue.monday.css');
            }
		}
		
		public function indexAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::READ))
			{
				exit;
			}

            $form = new Form('Отбор', array(
                                    array('caption' => 'Номер', 'type' => 'text', 'name' => 'search_id', 'value' => $_SESSION['search_id'], 'size' => 5, 'nobr' => 1),
                                    array('type' => 'date', 'caption' => 'С', 'name' => 'search_from', 'value' => $_SESSION['search_from'], 'nobr' => 1),
					                array('type' => 'date', 'caption' => 'По', 'name' => 'search_to', 'value' => $_SESSION['search_to'], 'nobr' => 1),
                                    array('caption' => 'Ответственный', 'type' => 'select', 'name' => 'search_department', 'value' => $_SESSION['search_department'], 'data' => new DepartmentShortSelectList(array('archive' => true)), 'empty_option' => '(все)', 'nobr' => 1, 'inline' => 'style="width:250px;"'),
                                    array('caption' => 'Статус', 'type' => 'select', 'name' => 'search_status', 'value' => $_SESSION['search_status'], 'data' => new StatusList(), 'empty_option' => '(все)', 'nobr' => 1),
                                    array(),
                                    array('caption' => 'Район', 'type' => 'select', 'name' => 'search_region', 'value' => $_SESSION['search_region'], 'data' => new RegionSelectList(), 'empty_option' => '(все)', 'nobr' => 1),
                                    array('caption' => 'Делегированные в МО', 'type' => 'select', 'name' => 'search_municipality', 'value' => $_SESSION['search_municipality'], 'data' => new MunicipalitySelectList(), 'empty_option' => '(все)', 'nobr' => 1, 'inline' => 'style="width:250px;"'),
                                    //array('caption' => 'Тип', 'type' => 'select', 'name' => 'search_type', 'value' => $_SESSION['search_type'], 'data' => new TypeList(), 'empty_option' => '(все)', 'nobr' => 1),
                                    //array('caption' => 'Угроза', 'type' => 'select', 'name' => 'search_alert', 'value' => $_SESSION['search_alert'], 'data' => new AlertTypeList(), 'empty_option' => '(все)', 'nobr' => 1),
                                    array('caption' => 'Тематика', 'type' => 'select', 'name' => 'search_topic', 'value' => $_SESSION['search_topic'], 'data' => new TopicSelectList(), 'empty_option' => '(все)', 'nobr' => 1, 'inline' => 'style="width:140px;"'),
                                    array('caption' => 'Результат', 'type' => 'select', 'name' => 'search_result', 'value' => $_SESSION['search_result'], 'data' => new ResultTypeList(), 'empty_option' => '(все)', 'nobr' => 1, 'inline' => 'style="width:70px;"'),
                                    array('caption' => 'Комм. губернатора', 'type' => 'select', 'name' => 'search_gubcomment', 'value' => $_SESSION['search_gubcomment'], 'data' => new YesNoList(), 'empty_option' => '(все)', 'nobr' => 1),
                                    //array('caption' => 'Источник', 'type' => 'select', 'name' => 'search_source', 'value' => $_SESSION['search_source'], 'data' => new SourceList(), 'empty_option' => '(все)', 'nobr' => 1),
                                    array('caption' => 'Оценка', 'type' => 'select', 'name' => 'search_rating', 'value' => $_SESSION['search_rating'], 'data' => new RatingSearchList(), 'empty_option' => '(все)', 'nobr' => 1),
                                    array(),
                                    array('caption' => 'Проект', 'type' => 'select', 'name' => 'search_project', 'value' => $_SESSION['search_project'], 'data' => new ProjectSelectList(), 'empty_option' => '(все)', 'nobr' => 1, 'inline' => 'style="width:150px;"'),
                                    array('caption' => 'Оператор', 'type' => 'select', 'name' => 'search_author', 'value' => $_SESSION['search_author'], 'data' => new AuthorSelectList(), 'empty_option' => '(все)', 'nobr' => 1, 'inline' => 'style="width:150px;"'),
                                    array('caption' => 'Что ищем', 'type' => 'text', 'name' => 'search_text', 'value' => $_SESSION['search_text'], 'size' => 20, 'nobr' => 1),
                                    array('type' => 'submit', 'name' => 'btnFilter', 'value' => 'Применить', 'nobr' => 1),
                                    array('type' => 'submit', 'name' => 'btnClear', 'value' => 'Очистить', 'nobr' => 1),
                                    array('type' => 'submit', 'name' => 'btnExport', 'value' => 'Сохранить в Excel', 'nobr' => 1),
                                    array('type' => 'submit', 'name' => 'btnExportMini', 'value' => 'Сохранить в Excel (сокр.)', 'nobr' => 1),
                                    array('type' => 'hidden', 'name' => '_mode', 'value' => $_SESSION['mode']),
                                    array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
                                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1)
            ));

            if(!ACL::Check($this->ObjectType, 'excel'))
                $form->DeleteElement('btnExport');

            if($_SESSION['search_status'] != 15 && $_SESSION['search_status'] != 20)
                $form->DeleteElement('search_rating');

            $form->Show();
            echo '<br>';
			
			$this->Table = new OrdersListTable($this->ObjectType);
			$this->Table->Show();
		}

        public function setoperatorAction()
        {
            $data = array();

            $order = $this->getline("select status, operator_id from orders where id = ".(int)$this->Id);
            if($order['status'] != 15)
            {
                $data['result'] = 'error';
                $data['error'] = 'Вопрос не находится на стадии доведения в КЦ!';
            }
            else if($order['operator_id'])
            {
                $operator = $this->getline("select name from users where id = ".$order['operator_id']);

                $data['result'] = 'error';
                $data['error'] = 'Вопрос уже находится на доведении у оператора '.$operator['name'].'!';
            }
            else
            {
                $this->sql("update orders set operator_id = ".$_SESSION['user_id']." where id = ".(int)$this->Id);
                $data['result'] = 'ok';
                $data['name'] = $_SESSION['user_name'];
            }

            echo json_encode($data);
        }

        public function canceloperatorAction()
        {
            $data = array();

            $order = $this->getline("select status, operator_id from orders where id = ".(int)$this->Id);
            if($order['status'] != 15)
            {
                $data['result'] = 'error';
                $data['error'] = 'Вопрос не находится на стадии доведения в КЦ!';
            }
            else if(($order['operator_id'] != $_SESSION['user_id']) && (!ACL::Check($this->ObjectType, 'requestedit')))
            {
                $operator = $this->getline("select name from users where id = ".$order['operator_id']);

                $data['result'] = 'error';
                $data['error'] = 'Вопрос уже находится на доведении у оператора '.$operator['name'].'!';
            }
            else
            {
                $this->sql("update orders set operator_id = 0 where id = ".(int)$this->Id);
                $data['result'] = 'ok';
                $data['name'] = $_SESSION['user_name'];
            }

            echo json_encode($data);
        }

        public function checkdoublesAction()
        {
            if(!ACL::Check($this->ObjectType, ACL::READ))
            {
                exit;
            }

            if($this->num == 'anonymous')
                $this->num = '';

            if($this->phone == 'anonymous')
                $this->phone = '';

            if(!$this->num && !$this->phone && !$this->phone2)
                exit;

            if(preg_match('/(.{10})$/', $this->num, $matches))
                $this->num = $matches[1];

            $this->phone = preg_replace('/\D/', '', $this->phone);
            if(preg_match('/(.{10})$/', $this->phone, $matches))
                $this->phone = $matches[1];

            $this->phone2 = preg_replace('/\D/', '', $this->phone2);
            if(preg_match('/(.{10})$/', $this->phone2, $matches))
                $this->phone2 = $matches[1];

            $query = new Query("select");
            $query->AddTable("orders");
            $query->AddField('id');
            $query->AddField('name');
            $query->AddField('status');
            $where = new Where();
            $where->Policy = 'or';
            if($this->num)
                $where->Add("num = '".$this->num."' or phone = '".$this->num."'");
            if($this->phone)
                $where->Add("phone = '".$this->phone."' or num = '".$this->phone."'");
            if($this->phone2)
                $where->Add("phone = '".$this->phone2."' or num = '".$this->phone2."'");
            if($this->email)
                $where->Add("email = '".$this->email."'");
            $query->Where->Add($where);
            $query->Where->Add("id != ".(int)$this->Id);
            //$query->Where->Add("num != 'anonymous'");

            //echo $query->Get();

            $ds = new DataSource($query);
            $header = false;
            $status = new StatusList();
            while($data = $ds->GetString())
            {
                if(!$header)
                {
                    echo '<strong style="color:red; font-size:18px;">С этого номера или емейла есть заявки:</strong><br><br>';
                    $header = true;
                }

                echo '<a href="index.php?module=orders&act=view&id='.$data['id'].'" style="font-weight:bold;" target="_blank">'.$data['id'].'</a> <strong>('.$status->Get($data['status']).')</strong>';
                echo ' <button class="submitButton" onclick="copyData('.$data['id'].')">скопировать данные</button>';
                echo '<br><span style="color:grey;">'.$data['name'].'</span><br><br>';
            }
            if($header)
                echo '<br><br>';
        }
		
		public function viewAction()
		{
			$this->editAction();
		}

        public function getdataAction()
        {
            $data = $this->getline("select * from orders where id = " . $_REQUEST['id']);
            $res = array();
            $res['lastname'] = $data['lastname'];
            $res['firstname'] = $data['firstname'];
            $res['middlename'] = $data['middlename'];
            $res['sex'] = $data['sex'];
            $res['age'] = $data['age'];
            $res['job_id'] = $data['job_id'];
            $res['region_id'] = $data['region_id'];
            $res['city'] = $data['city'];
            $res['address'] = $data['address'];
            $res['phone'] = $data['phone'];
            $res['phone2'] = $data['phone2'];
            $res['email'] = $data['email'];
            echo json_encode($res);
        }

        private function showHelper($id = 0, $data = array())
        {
?>
            <script>

                var g_city;

                function copyData(id) {
                    $.ajax({
                        url : 'index.php?module=orders&act=getdata&nohead=1',
                        data : {
                            id : id
                        },
                        dataType : 'json',
                        success : function(data) {
                            $('#lastname').val(data.lastname)
                            $('#firstname').val(data.firstname)
                            $('#middlename').val(data.middlename)
                            $('[name="sex"][value="' + data.sex + '"]').attr('checked', 'checked')
                            $('#age').val(data.age)
                            $('#job_id').val(data.job_id)
                            g_city = data.city
                            $('#region_id')
                                .val(data.region_id)
                                .trigger('change')

                            $('#address').val(data.address)
                            $('#phone').val(data.phone)
                            $('#phone2').val(data.phone2)
                            $('#email').val(data.email)
                        }
                    })
                }

                function checkDoubles()
                {
                    if(($('#num').val() == '') && ($('#phone').val() == ''))
                        return false;
                    
                    $.ajax({
                        type: 'post',
                        url : 'index.php?module=orders&act=checkdoubles&nohead=1&num=' + $('#num').val() + '&id=<?php echo $id; ?>' + '&phone=' + $('#phone').val() + '&phone2=' + $('#phone2').val() + '&email=' + $('#email').val(),
                        success : function(msg)
                        {
                            $('#doubles').html(msg);
                        }
                    });

                    return true;
                }

                function checkBlacklist()
                {
                    if(($('#num').val() == '') && ($('#phone').val() == ''))
                        return false;

                    $.ajax({
                        type: 'post',
                        url : 'index.php?module=blacklist&act=check&nohead=1&num=' + $('#num').val() + '&phone=' + $('#phone').val() + '&phone2=' + $('#phone2').val(),
                        success : function(msg)
                        {
                            if(msg.length)
                            {
                                $('#btnAddToBlacklisttr').hide();
                                $('#black').html(msg);
                            }
                            else
                            {
                                $('#btnAddToBlacklisttr').show();
                                $('#black').html('');
                            }
                        }
                    });

                    return true;
                }

                function cancelOperator()
                {
                    $.ajax({
                        type : 'post',
                        url  : 'index.php?module=orders&nohead=1&act=canceloperator&id=<?php echo $id; ?>',
                        success : function(msg){
                                $('#operator_id').html('');
                        }
                    })
                }

                function setOperator()
                {
                    $.ajax({
                        type : 'post',
                        url  : 'index.php?module=orders&nohead=1&act=setoperator&id=<?php echo $id; ?>',
                        success : function(msg){
                            var data = eval('(' + msg + ')');
                            if(data.result == 'ok')
                            {
                                var html = 'На доведении у оператора <strong>' + data.name + '</strong>';
                                html += ' <a href="javascript:cancelOperator()">отменить</a>';
                                $('#operator_id').html(html);
                            }
                            else if(data.result == 'error')
                            {
                                $('#operator_id').html('<span style="color:red">' + data.error + '</span>');
                            }
                        }
                    })
                }

                $(document).ready(function(){
                    $('#phone').bind('change', function() { checkDoubles(); checkBlacklist(); });
                    $('#phone2').bind('change', function() { checkDoubles(); checkBlacklist(); });
                    $('#email').bind('change', function() { checkDoubles(); });

                    checkDoubles();
                    checkBlacklist();

                    $('#btnReturn').click(function(){
                        $("#cover").show();
                        $('#return_dlg').dialog({
                            width:550,
                            autoOpen:false,
                            close: function(event, ui)
                            {
                                $("#cover").hide();
                                //$("#errField").hide();
                                return true;
                            }
                        }).dialog('open');
                        return true;
                    });

                    $('#btnRecall').click(function(){
                        $("#cover").show();
                        $('#recall_dlg').dialog({
                            width:550,
                            autoOpen:false,
                            close: function(event, ui)
                            {
                                $("#cover").hide();
                                //$("#errField").hide();
                                return true;
                            }
                        }).dialog('open');
                        return false;
                    });

                    $('#btnDelegate').click(function(){
                        $("#cover").show();
                        $('#delegate_dlg').dialog({
                            width:550,
                            autoOpen:false,
                            close: function(event, ui)
                            {
                                $("#cover").hide();
                                //$("#errField").hide();
                                return true;
                            }
                        }).dialog('open');
                        return false;
                    });

                    $('#btnDeputyMayor').click(function(){
                        $("#cover").show();
                        $('#deputymayor_dlg').dialog({
                            width:550,
                            autoOpen:false,
                            close: function(event, ui)
                            {
                                $("#cover").hide();
                                //$("#errField").hide();
                                return true;
                            }
                        }).dialog('open');
                        return false;
                    });

                    $('#btnReopen').click(function(){
                        $("#cover").show();
                        $('#reopen_dlg').dialog({
                            width:550,
                            autoOpen:false,
                            close: function(event, ui)
                            {
                                $("#cover").hide();
                                //$("#errField").hide();
                                return true;
                            }
                        }).dialog('open');
                        return true;
                    });

                    $('#btnSend').click(function(){
                        if(!$('#answer').val())
                        {
                            alert('Пожалуйста, заполните ответ перед тем, как направить запрос в контакт-центр!');
                            $('#answer')[0].focus();
                            return false;
                        }
                        return true;
                    });

//                    $('#project_id').bind('change',
//                    {
//                        sourceField: 'project_id',
//                        targetField: 'department_id',
//                        url : 'index.php?module=departments&act=ajax&project_id=%id%',
//                        emptyOption: '(...)',
//                        emptyOptionSuccess: '(...)'
//                    },
//                    changeHandler);

                    $('#project_id').change(function(){
                        $.ajax({
                            type : 'post',
                            url  : 'index.php',
                            data : {
                                module : 'departments',
                                act    : 'getforproject',
                                project_id : $('#project_id').val(),
                                nohead : 1
                            },
                            dataType : 'json',
                            success : function(data) {
                                if(data){
                                    if(data.departments) {
                                        var department_id = $('#department_id')
                                        department_id.empty()
                                        for(var i in data.departments) {
                                            if(data.default_id && (data.default_id == data.departments[i].id)) {
                                                department_id.append('<option value="' + data.departments[i].id + '" selected>' + data.departments[i].value + '</option>')
                                            } else {
                                                department_id.append('<option value="' + data.departments[i].id + '">' + data.departments[i].value + '</option>')
                                            }
                                        }
                                    }
                                }
                            }
                        })

                        $.ajax({
                            type : 'post',
                            url  : 'index.php',
                            data : {
                                module : 'projects',
                                act    : 'getjob',
                                project_id : $('#project_id').val(),
                                nohead : 1
                            },
                            success : function(data) {
                                if(data){
                                    $('#job_id').val(data)
                                }
                            }
                        })
                    })

                    $('#project_id').bind('change', function(){
                        $.ajax({
                           type : 'post',
                           url  : 'index.php?module=projects&act=checksimple&nohead=1&id=' + $('#project_id').val() ,
                           success : function(msg){
                               if(msg == 1)
                               {
                                   $('#sextr').hide();
                                   $('#emailtr').hide();
                                   $('#answers').hide();
                                   $('#black').hide();
                                   $('#doubles').hide();
                               }
                               else
                               {
                                   $('#sextr').show();
                                   $('#emailtr').show();
                                   $('#answers').show();
                                   $('#black').show();
                                   $('#doubles').show();
                               }
                           }
                        });
                    });

                    $('#project_id').bind('change', function(){
                        if($(this).val() == 9) {
                            $('#organizationtr').show();
                            $('#servicetr').show();
                        } else {
                            $('#organizationtr').hide();
                            $('#servicetr').hide();
                        }
                    });
                    $('#project_id').trigger('change');

                    $('#region_id').bind('change',
                    {
                        sourceField: 'region_id',
                        targetField: 'city',
                        url : 'index.php?module=cities&act=ajax&region_id=%id%',
                        emptyOption: '(...)',
                        emptyOptionSuccess: '(...)',
                        onSuccess : function(){
                            if(g_city) {
                                $('#city').val(g_city)
                            } else {
                                if($('#city').children().length == 2)
                                    $('#city option:nth-child(2)').attr('selected', true);
                            }
                        }
                    },
                    changeHandler);

                    $('#department_id').bind('change',
                    {
                        sourceField: 'department_id',
                        targetField: 'delegate_form #expert_id',
                        url : 'index.php?module=users&act=ajax&department_id=%id%',
                        emptyOption: '(...)',
                        emptyOptionSuccess: '(...)'
                    },
                    changeHandler);

                    $('#order_form input').bind('keydown', function(e){
                        if(e.keyCode == 13)
                        {
                            e.preventDefault();
                            $('input')[($('input').index(this) + 1)].focus();
                        }
                    });

                    <?php
                        if($data['status'] == 15)
                        {
                            if($data['operator_id'])
                            {
                                $operator = $this->getline("select name from users where id = ".$data['operator_id']);
                    ?>
                                var html = 'На доведении у оператора <strong><?php echo $operator['name']; ?></strong>';
                                <?php
                                    if(($data['operator_id'] == $_SESSION['user_id']) || (ACL::Check($this->ObjectType, 'requestedit')))
                                    {
                                ?>
                                html += ' <a href="javascript:cancelOperator()">отменить</a>';
                                <?php
                                    }
                                ?>
                                $('#operator_id').html(html);
                    <?php
                            }
                            else
                            {
                    ?>
                            $('#operator_id').html('<a href="javascript:setOperator()" id="set_operator">ВЗЯТЬ НА ДОВЕДЕНИЕ</a>');
                    <?php
                            }
                        }
                    ?>
                })
            </script>

            <style>
                #return_form table {border:0; background-color: transparent;}
            </style>
            <div id="cover"></div>
            <div id="return_dlg" style="display:none;">
                <span style="font-size:20px; font-weight:bold; color:#58595b;">Вернуть на модерацию</span><br><br>
                <?php
                    $return_form = new Form('', array(
                        array('caption' => 'Комментарий по причине возврата', 'type' => 'textarea', 'name' => 'comment', 'width' => 50, 'height' => 5, 'required' => 1),
                        array('type' => 'submit', 'name' => 'btnOk', 'value' => 'Вернуть'),
                        array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                        array('type' => 'hidden', 'name' => 'id', 'value' => $id),
                        array('type' => 'hidden', 'name' => 'act', 'value' => 'return'),
                        array('type' => 'hidden', 'name' => 'module', 'value' => 'orders')
                    ));
                    $return_form->id = 'return_form';
                    $return_form->Show();
                ?>
            </div>


            <style>
                #recall_form table {border:0; background-color: transparent;}
            </style>
            <div id="recall_dlg" style="display:none;">
                <span style="font-size:20px; font-weight:bold; color:#58595b;">Перезвонить позже</span><br><br>
                <?php
                    $recall_form = new Form('', array(
                        array('caption' => 'Когда', 'type' => 'datetime', 'name' => 'recall', 'required' => 1),
                        array('caption' => 'Комментарий', 'type' => 'textarea', 'name' => 'comment', 'width' => 50, 'height' => 5, 'required' => 1),
                        array('type' => 'submit', 'name' => 'btnOk', 'value' => 'Отправить'),
                        array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                        array('type' => 'hidden', 'name' => 'id', 'value' => $id),
                        array('type' => 'hidden', 'name' => 'act', 'value' => 'recall'),
                        array('type' => 'hidden', 'name' => 'module', 'value' => 'orders')
                    ));
                    $recall_form->id = 'recall_form';
                    $recall_form->Show();
                ?>
            </div>

            <style>
                #delegate_form table {border:0; background-color: transparent;}
            </style>
            <script>
                $(document).ready(function(){
                    $('#delegate_form').bind('submit', function(e){
                        if(!$('#delegate_form #expert_id').val() && !$('#delegate_form #municipality_id').val()){
                            e.preventDefault();
                            alert('Пожалуйста, выберите специалиста или отдел муниципалитета!');
                        } else
                            return true;
                    });
                });
            </script>
            <div id="delegate_dlg" style="display:none;">
                <span style="font-size:20px; font-weight:bold; color:#58595b;">Отправить вопрос специалисту</span><br><br>
                <?php
                    $delegate_form = new Form('', array(
                        array('caption' => 'Кому', 'type' => 'select', 'name' => 'expert_id', 'data' => new DelegateSelectList($data['municipality_id']?$data['municipality_id']:$data['department_id']), 'empty_option' => '(...)', 'inline' => 'style="width:300px;"'),
                        array('caption' => 'или', 'type' => 'select', 'name' => 'municipality_id', 'data' => new MunicipalitySelectList(), 'empty_option' => '(выберите отдел муниципалитета...)', 'inline' => 'style="width:300px;"'),
                        array('caption' => 'Комментарий', 'type' => 'textarea', 'name' => 'comment', 'width' => 50, 'height' => 5),
                        array('type' => 'submit', 'name' => 'btnDelegateOk', 'value' => 'Отправить'),
                        array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                        array('type' => 'hidden', 'name' => 'id', 'value' => $id),
                        array('type' => 'hidden', 'name' => 'act', 'value' => 'delegate'),
                        array('type' => 'hidden', 'name' => 'module', 'value' => 'orders')
                    ));
                if($data['status'] !== 13)
                if($data['municipality_id'])
                {
                    //$delegate_form->DeleteElement('municipality_id');
                    //$delegate_form->SetElementAttribute('expert_id', 'required', 1);
                }
                    $delegate_form->id = 'delegate_form';
                    $delegate_form->Show();
                ?>
            </div>

            <style>
                #deputymayor_form table {border:0; background-color: transparent;}
            </style>
            <script>
                $(document).ready(function(){
                    $('#deputymayor_form').bind('submit', function(e){
                        $('#deputyanswer').val($('#answer').val())
                        if(!$('#deputymayor_form #expert_id').val()){
                            e.preventDefault();
                            alert('Пожалуйста, выберите зам. мэра!');
                        } else
                            return true;
                    });
                });
            </script>
            <div id="deputymayor_dlg" style="display:none;">
                <span style="font-size:20px; font-weight:bold; color:#58595b;">Отправить вопрос зам. мэра</span><br><br>
                <?php
                $deputymayor_form = new Form('', array(
                    array('caption' => 'Кому', 'type' => 'select', 'name' => 'expert_id', 'data' => new DeputyMayorSelectList($data['municipality_id']), 'empty_option' => '(...)', 'inline' => 'style="width:300px;"'),
                    array('caption' => 'Комментарий', 'type' => 'textarea', 'name' => 'comment', 'width' => 50, 'height' => 5),
                    array('type' => 'hidden', 'name' => 'deputyanswer', 'value' => ''),
                    array('type' => 'submit', 'name' => 'btnDeputyMayorOk', 'value' => 'Отправить'),
                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                    array('type' => 'hidden', 'name' => 'id', 'value' => $id),
                    array('type' => 'hidden', 'name' => 'act', 'value' => 'deputymayor'),
                    array('type' => 'hidden', 'name' => 'module', 'value' => 'orders')
                ));
                $deputymayor_form->id = 'deputymayor_form';
                $deputymayor_form->Show();
                ?>
            </div>

            <style>
                #reopen_form table {border:0; background-color: transparent;}
            </style>
            <div id="cover"></div>
            <div id="reopen_dlg" style="display:none;">
                <span style="font-size:20px; font-weight:bold; color:#58595b;">Отправить на повторное рассмотрение</span><br><br>
                <?php
                    $reopen_form = new Form('', array(
                        array('caption' => 'Комментарий', 'type' => 'textarea', 'name' => 'comment', 'width' => 50, 'height' => 5, 'required' => 1),
                        array('type' => 'submit', 'name' => 'btnOk', 'value' => 'Отправить'),
                        array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                        array('type' => 'hidden', 'name' => 'id', 'value' => $id),
                        array('type' => 'hidden', 'name' => 'act', 'value' => 'reopen'),
                        array('type' => 'hidden', 'name' => 'module', 'value' => 'orders')
                    ));
                    $reopen_form->id = 'reopen_form';
                    $reopen_form->Show();
                ?>
            </div>
<?php
        }

        public function showAnswerHelper()
        {
            ?>
                <script>

                    function toggleAnswer(id)
                    {
                        var height = $('#answer_' + id).css('height');

                        if(height == '45px')
                        {
                            $('#answer_' + id).css('height', '100%');
                            $('#toggle_' + id).text('свернуть');
                        }
                        else
                        {
                            $('#answer_' + id).css('height', '45px');
                            $('#toggle_' + id).text('развернуть');
                        }
                    }

                    function filterText(text)
                    {
                        text = text.replace(/&nbsp;/g, ' ');
                        text = text.replace(/&lt;/g, '<');
                        text = text.replace(/&gt;/g, '>');

                        return text;
                    }

                    function selectAnswer(id, department_id)
                    {
                        $('[id^="answer_"]').css('backgroundColor', 'transparent');
                        $('#answer_' + id).css('backgroundColor', 'yellow');
                        $('#article_id').val(id);
                        $('#question').val(filterText($('#answer_' + id + '_name').html()));
                        $('#answer').val(filterText($('#answer_' + id).html()));
                        $('#department_id').val(department_id);
                        $('[name="type"]')[0].checked = true;
                    }

                    $(document).ready(function(){
                        $('#name').change(function(){
                            $.ajax({
                                type: 'post',
                                url : 'index.php?module=articles&act=getanswer&nohead=1&name=' + encodeURIComponent($('#name').val()),
                                success : function(msg)
                                {
                                    $('#answers').empty();
                                    var data = eval('(' + msg + ')');
                                    for(var i = 0; i < data.length; i++)
                                    {
                                        var answer = data[i];
                                        if(answer.comment_simple)
                                            $('#answers').append('<div style="width:400px;"><strong id="answer_' + answer.id + '_name">' + answer.name +  '</strong><br><div id="answer_' + answer.id + '" style="width:400px; height:45px; overflow:  hidden;">' + answer.comment_simple + '</div><div><a id="toggle_' + answer.id + '" href="javascript:toggleAnswer(' + answer.id + ')">развернуть</a> - <a href="javascript:selectAnswer(' + answer.id + ', ' + answer.department_id + ')">выбрать</a></div></div><br>')
                                    }
                                }
                            });
                        })
                    })
                </script>
            <?php
        }

		public function newAction()
		{
            $this->showHelper();
            $this->showAnswerHelper();

            ?>
            <script>
                function setRequired(field, value) {
                    if(value == 1) {
                        $('#' + field).attr('data-required', 1);
                        $('span[data-for="' + field + '"]').html(' *')
                    } else {
                        $('#' + field).attr('data-required', 0);
                        $('span[data-for="' + field + '"]').html('')
                    }
                }

                function checkRequired() {
                    if($('#project_id').val() == 1) {
                        setRequired('age', 1)
                        setRequired('region_id', 1)
                        setRequired('city', 1)
                        setRequired('address', 1)
                    } else {
                        setRequired('age', 0)
                        setRequired('region_id', 0)
                        setRequired('city', 0)
                        setRequired('address', 0)
                    }
                }

                $(document).ready(function(){
                    $('#project_id').change(function(){
                        checkRequired()
                    })

                    checkRequired()
                })
            </script>
            <?php

            if($_REQUEST['parent'])
            {
                $data = $this->getline("select * from orders where id = ".(int)$_REQUEST['parent']);
                $this->num = $data['num'];
            }

            if(preg_match('/(.{10})$/', $this->num, $matches))
                $this->num = $matches[1];

            $project = $this->getline("select project_id from roles where id = ".$_SESSION['user_priv']);
            $project = $project['project_id'] ?: 1;

			$form = new Form("Новая запись...", array(

				array('caption' => 'Номер абонента', 'type' => 'label', 'name' => 'num_label', 'value' => '<h1>'.$this->num.'</h1>', 'nobr' => 1),
                array('caption' => 'Источник', 'type' => 'hidden', 'name' => 'source', 'data' => new SourceList(), 'value' => 1, 'nobr' => 1),
                array('caption' => 'Проект', 'type' => 'select', 'name' => 'project_id', 'data' => new ProjectSelectList(true), 'value' => $project, 'nobr' => 1, 'required' => 1, 'empty_option' => '(...)'),
                array(),
                array('caption' => 'Фамилия', 'type' => 'text', 'name' => 'lastname', 'size' => '15', 'nobr' => 1, 'value' => $data['lastname']),
				array('caption' => 'Имя', 'type' => 'text', 'name' => 'firstname', 'size' => '10', 'nobr' => 1, 'value' => $data['firstname']),
				array('caption' => 'Отч.', 'type' => 'text', 'name' => 'middlename', 'size' => '18', 'nobr' => 1, 'value' => $data['middlename']),
                array(),
                array('caption' => 'Организация', 'type' => 'text', 'name' => 'organization', 'size' => '64', 'value' => $data['organization'], 'autocomplete' => 'index.php?module=organizations&act=autocomplete&nohead=1'),
                array('caption' => 'Услуга', 'type' => 'text', 'name' => 'service', 'size' => '64', 'value' => $data['service'], 'autocomplete' => 'index.php?module=services&act=autocomplete&nohead=1'),
                array(),
				array('caption' => 'Пол', 'type' => 'radio', 'name' => 'sex', 'data' => new SexTypeList(), 'value' => 1, 'nobr' => 1, 'value' => $data['sex']),
				array('caption' => 'Возраст', 'type' => 'text', 'name' => 'age', 'inline' => 'style="width:30px;"', 'nobr' => 1, 'mask' => Form::Digits, 'value' => $data['age']),
				array('caption' => 'Род занятий', 'type' => 'select', 'name' => 'job_id', 'data' => new JobSelectList(), 'empty_option' => '(...)', 'nobr' => 1, 'value' => $data['job_id']),
                array(),
                array('caption' => 'Район', 'type' => 'select', 'name' => 'region_id', 'data' => new RegionSelectList(), 'empty_option' => '(...)', 'nobr' => 1, 'required' => 1, 'value' => $data['region_id']),
				array('caption' => 'Нас. пункт', 'type' => 'select', 'name' => 'city', 'nobr' => 1, 'empty_option' => '(...)', 'required' => 1),
                array(),
				array('caption' => 'Адрес', 'type' => 'text', 'name' => 'address', 'size' => '52', 'nobr' => 1, 'value' => $data['address'], 'required' => 1),
                array(),
				array('caption' => 'Телефон', 'type' => 'text', 'name' => 'phone', 'size' => '15', 'nobr' => 1, 'required' => 1, 'value' => $data['phone']?$data['phone']:$this->num),
                array('caption' => 'Доп. телефон', 'type' => 'text', 'name' => 'phone2', 'size' => '15', 'nobr' => 1, 'value' => $data['phone2']?$data['phone2']:$this->phone2),
                array(),
				array('caption' => 'Email', 'type' => 'text', 'name' => 'email', 'size' => '64', 'nobr' => 1, 'mask' => Form::Email, 'value' => $data['email']),
                array(),
                array('caption' => 'Тематика', 'type' => 'select', 'name' => 'topic_id', 'data' => new TopicSelectList(), 'empty_option' => '(...)', 'value' => $data['topic_id'], 'required' => 1),
				array('caption' => 'Заголовок', 'type' => 'text', 'name' => 'name', 'size' => '64', 'required' => 1),
				array('caption' => 'Вопрос', 'type' => 'textarea', 'name' => 'question', 'width' => '62', 'height' => '3', 'required' => 1),
                array('caption' => 'Ответ', 'type' => 'textarea', 'name' => 'answer', 'width' => '62', 'height' => '5'),
				array('caption' => 'Приложить файл', 'type' => 'file', 'name' => 'userfile[]'),
                array('caption' => 'Комментарий<br>к ответу', 'type' => 'textarea', 'name' => 'comment', 'width' => '62', 'height' => '3'),
				//array('caption' => 'Модератор', 'type' => 'select', 'name' => 'user_id', 'data' => new UserSelectList()),
				//array('caption' => 'Эксперт', 'type' => 'select', 'name' => 'expert_id', 'data' => new UserSelectList()),
				//array('caption' => 'Ответственный', 'type' => 'select', 'name' => 'department_id', 'data' => new DepartmentSelectList(array('project_id' => $project)), 'empty_option' => '(выберите ответственного...)', 'inline' => 'style="width:400px;"', 'required' => 1),
				array('caption' => 'Тип', 'type' => 'radio', 'name' => 'type', 'data' => new TypeListNew(), 'value' => 2),
				//array('caption' => 'Угроза', 'type' => 'radio', 'name' => 'alert', 'data' => new AlertTypeList(), 'value' => 1, 'nobr' => 1),
                //array('label' => '<strong>Низкий приоритет</strong>', 'type' => 'checkbox', 'name' => 'garbage', 'value' => '1', 'nobr' => 1),
				array('caption' => 'Call ID', 'type' => 'hidden', 'name' => 'call_id', 'size' => '30', 'value' => ($_REQUEST['call_id']?$_REQUEST['call_id']:$data['call_id'])),
				//array('caption' => 'Статус', 'type' => 'radio', 'name' => 'status', 'data' => new StatusList(), 'value' => 1),
				array('caption' => 'Результат', 'type' => 'radio', 'name' => 'result', 'data' => new ResultTypeList(), 'value' => 1),
                array('type' => 'hidden', 'name' => 'num', 'value' => $this->num),
                array('type' => 'hidden', 'name' => 'article_id'),
				array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'add'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'btnAdd', 'value' => 'Отправить', 'nobr' => 1),
                ////array('type' => 'submit', 'name' => 'btnFinish', 'value' => 'Ответить абоненту и закрыть', 'nobr' => 1),
			));

            if(empty($this->num))
                $form->DeleteElement('num_label');

//            if($project != 1)
//            {
//                $form->SetElementAttribute('sex', 'required', 0);
//                $form->SetElementAttribute('age', 'required', 0);
//                $form->SetElementAttribute('region_id', 'required', 0);
//            }

			echo '<table style="border:0px;"><tr><td style="vertical-align:top;">';
            $form->id = 'order_form';
            $form->Show();
            echo '</td><td style="vertical-align:top;">';
            echo '<table style="width:400px;"><tr><td id="answers"></td></tr></table>';
            echo '<div id="black"></div>';
            echo '<div id="doubles"></div>';
            echo '</td></tr></table>';
		}
		
		public function editAction()
		{
			$data = $this->getline("select * from orders where id = ".$this->Id);

            if(SYNC_WITH_ASTERISK == 1)
            if(!$data['m_id'] && $data['call_id'])
            {
                $host = 'localhost';
                $user = 'play';
                $password = 'jkjkj42!';
                $dbname_cc = 'call_center';

                $db_cc = mysql_connect($host, $user, $password);
                mysql_select_db($dbname_cc, $db_cc);

                $res = mysql_query("select * from call_entry_record where call_id = " . $data['call_id']);
                $cdata = mysql_fetch_array($res);
                $data['m_id'] = $cdata['m_id'];
                $sql = new SqlObject();
                $sql->sql("update orders set m_id = '" . $data['m_id'] . "' where id = " . $this->Id);
            }

            if(!Orders::hasAccess($this->Id))
                exit;

            $this->showHelper($this->Id, $data);

            ?>
            <script>
                $(document).ready(function(){
                    $('#btnAddArticle').click(function(){
                        location = 'index.php?module=articles&act=new&order_id=' + $('#id').val()
                    })
                })
            </script>
            <?php

            if($_SESSION['user_data']['outgoing_active'])
            {
                $call = '<img class="callbtn" data-num="' . $data['num'] . '" src="images/telephone.png" style="cursor: pointer;" title="позвонить">';
                $call2 = '<img class="callbtn" data-num="' . $data['phone'] . '" src="images/telephone.png" style="cursor: pointer; vertical-align:bottom;" title="позвонить">';
                $call3 = '<img class="callbtn" data-num="' . $data['phone2'] . '" src="images/telephone.png" style="cursor: pointer; vertical-align:bottom;" title="позвонить">';
                $sql = new SqlObject();
                $project = $sql->getline("select context from projects where id = " . $data['project_id']);

                ?>
                <script>
                    $(document).ready(function(){
                        $('.callbtn').click(function(){
                            var num = $(this).attr('data-num')
                            $.ajax({
                                type : 'post',
                                url  : 'request.php',
                                data : {
                                    act : 'dialout',
                                    bnumber : num,
                                    context : '<?php echo $project['context']; ?>'
                                },
                                success : function(data) {
                                    //TODO: who knows...
                                }
                            })
                        })
                    })
                </script>
                <?php
            }

			$form = new Form("Редактирование записи...", array(
                array('caption' => 'ID вопроса', 'type' => 'label', 'value' => '<span style="font-size:26px; font-weight:bold;">'.$data['id'].'</span>&nbsp;&nbsp;&nbsp;<span id="operator_id"></span>'),
                array('caption' => 'Номер абонента', 'type' => 'label', 'name' => 'num_label', 'value' => '<span style="font-size:26px; font-weight:bold;">'.$data['num'].'</span>&nbsp;&nbsp;&nbsp;' . $call, 'nobr' => 1),
                array('type' => 'submit', 'name' => 'btnAddToBlacklist', 'value' => 'Занести в черный список...', 'nobr' => 1),
                    array(),
                array('caption' => 'Источник', 'type' => 'radio', 'name' => 'source', 'data' => new SourceList(), 'value' => $data['source'], 'readonly' => 1, 'nobr' => 1),
                array('caption' => 'Проект', 'type' => 'select', 'name' => 'project_id', 'data' => new ProjectSelectList(), 'value' => $data['project_id'], 'readonly' => 1, 'nobr' => 1),
                    array(),
				//array('caption' => 'Оператор', 'type' => 'select', 'name' => 'author_id', 'value' => $data['author_id'], 'data' => new UserSelectList(), 'readonly' => 1, 'nobr' => 1),
				array('caption' => 'Дата/время', 'type' => 'label', 'name' => 'created', 'value' => DT::getDateTime($data['created']), 'nobr' => 1),
                    array(),

				array('caption' => 'Фамилия', 'type' => 'label', 'name' => 'lastname', 'value' => '<span style="color:blue;">'.$data['lastname'].'</span>', 'size' => '30', 'readonly' => 1, 'nobr' => 1),
				array('caption' => 'Имя', 'type' => 'label', 'name' => 'firstname', 'value' => '<span style="color:blue;">'.$data['firstname'].'</span>', 'size' => '30', 'readonly' => 1, 'nobr' => 1),
				array('caption' => 'Отчество', 'type' => 'label', 'name' => 'middlename', 'value' => '<span style="color:blue;">'.$data['middlename'].'</span>', 'size' => '30', 'readonly' => 1, 'nobr' => 1),
                    array(),
                array('caption' => 'Организация', 'type' => 'text', 'name' => 'organization', 'size' => '64', 'value' => $data['organization'], 'autocomplete' => 'index.php?module=organizations&act=autocomplete&nohead=1'),
                array('caption' => 'Услуга', 'type' => 'text', 'name' => 'service', 'size' => '64', 'value' => $data['service'], 'autocomplete' => 'index.php?module=services&act=autocomplete&nohead=1'),
                    array(),
                array('caption' => 'Пол', 'type' => 'radio', 'name' => 'sex', 'data' => new SexTypeList(), 'value' => $data['sex'], 'readonly' => 1, 'nobr' => 1),
				array('caption' => 'Возраст', 'type' => 'text', 'name' => 'age', 'value' => $data['age'], 'size' => '5', 'readonly' => 1, 'nobr' => 1, 'mask' => Form::Digits),
				array('caption' => 'Род занятий', 'type' => 'select', 'name' => 'job_id', 'value' => $data['job_id'], 'data' => new JobSelectList(), 'empty_option' => '(...)', 'readonly' => 1, 'nobr' => 1),
                    array(),
                array('caption' => 'Район', 'type' => 'select', 'name' => 'region_id', 'value' => $data['region_id'], 'data' => new RegionSelectList(), 'empty_option' => '(...)', 'readonly' => 1, 'nobr' => 1),
				array('caption' => 'Нас. пункт', 'type' => 'text', 'name' => 'city', 'value' => $data['city'], 'size' => '30', 'readonly' => 1, 'nobr' => 1),
				array('caption' => 'Адрес', 'type' => 'text', 'name' => 'address', 'value' => $data['address'], 'size' => '30', 'readonly' => 1, 'nobr' => 1),
                    array(),
				array('caption' => 'Телефон', 'type' => 'label', 'name' => 'phone_label', 'value' => $data['phone'], 'postfix' => '&nbsp;&nbsp;&nbsp;' . $call2, 'size' => '30', 'readonly' => 1, 'nobr' => 1),
				array('caption' => 'Доп. телефон', 'type' => 'label', 'name' => 'phone2_label', 'value' => $data['phone2'], 'postfix' => '&nbsp;&nbsp;&nbsp;' . $call3, 'size' => '30', 'readonly' => 1, 'nobr' => 1),
                array('caption' => ' Email', 'type' => 'text', 'name' => 'email', 'value' => $data['email'], 'size' => '30', 'readonly' => 1, 'nobr' => 1, 'mask' => Form::Email),

                array('caption' => 'Тематика', 'type' => 'select', 'name' => 'topic_id', 'data' => new TopicSelectList(), 'empty_option' => '(...)', 'value' => $data['topic_id'], 'required' => 1),
				array('caption' => 'Заголовок вопроса', 'type' => 'text', 'name' => 'name', 'value' => $data['name'], 'size' => '53', 'readonly' => 1, 'required' => 1),
				array('caption' => 'Вопрос', 'type' => 'textarea', 'name' => 'question', 'value' => $data['question'], 'width' => '50', 'height' => '5', 'readonly' => 1, 'required' => 1),
				array('caption' => 'Ответ', 'type' => 'textarea', 'name' => 'answer', 'value' => $data['answer'], 'width' => '50', 'height' => '5'),
                array('type' => 'button', 'name' => 'btnAddArticle', 'value' => 'Добавить в БЗ'),
                array('caption' => 'Ответ подготовил', 'type' => 'label', 'name' => 'expert'),
                array('caption' => 'Приложить файл', 'type' => 'file', 'name' => 'userfile[]'),

                array('caption' => 'Комментарий к ответу', 'type' => 'textarea', 'name' => 'comment', 'value' => $data['comment'], 'width' => '50', 'height' => '5'),
                array('caption' => 'Комментарий губернатора', 'type' => 'textarea', 'name' => 'gubcomment', 'value' => $data['gubcomment'], 'width' => '50', 'height' => '5', 'readonly' => 1),
				//array('caption' => 'Модератор', 'type' => 'select', 'name' => 'user_id', 'value' => $data['user_id'], 'data' => new UserSelectList()),
				//array('caption' => 'Эксперт', 'type' => 'select', 'name' => 'expert_id', 'value' => $data['expert_id'], 'data' => new UserSelectList()),
				array('caption' => 'Ответственный', 'type' => 'select', 'name' => 'department_id', 'value' => $data['department_id'], 'data' => new DepartmentSelectList(array('value' => $data['department_id'])), 'empty_option' => '(нет)', 'inline' => 'style="width:400px;"'),
                array('caption' => 'Муниципалитет', 'type' => 'select', 'name' => 'municipality_id', 'value' => $data['municipality_id'], 'data' => new MunicipalitySelectList(array('value' => $data['municipality_id'])), 'empty_option' => '', 'inline' => 'style="width:400px;"'),
				array('caption' => 'Тип', 'type' => 'select', 'name' => 'type', 'value' => $data['type'], 'data' => new TypeList(), 'readonly' => 1),
				//array('caption' => 'Угроза', 'type' => 'radio', 'name' => 'alert', 'value' => $data['alert'], 'data' => new AlertTypeList($data['alert'])),
                //array('caption' => 'Низкий приоритет', 'type' => 'checkbox', 'name' => 'garbage', 'value' => '1', 'checked' => $data['garbage']),
				//array('caption' => 'Call ID', 'type' => 'text', 'name' => 'call_id', 'value' => $data['call_id'], 'size' => '30'),
				array('caption' => 'Статус', 'type' => 'select', 'name' => 'status', 'value' => $data['status'], 'data' => new StatusList(), 'readonly' => 1),
				array('caption' => 'Результат', 'type' => 'select', 'name' => 'result', 'value' => $data['result'], 'data' => new ResultTypeList(), 'empty_option' => '(...)'),
                array('caption' => 'Оценка', 'type' => 'select', 'name' => 'rating', 'value' => $data['rating'], 'data' => new RatingList(), 'empty_option' => '(...)'),
                //array('caption' => 'Комментарий оценки', 'type' => 'textarea', 'name' => 'rating_comment', 'value' => $data['rating_comment'], 'width' => '50', 'height' => '5'),
                array('type' => 'hidden', 'name' => 'num', 'value' => $data['num']),
                array('type' => 'hidden', 'name' => 'phone', 'value' => $data['phone']),
                array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
				array('type' => 'hidden', 'name' => 'id', 'value' => $this->Id),
				array('type' => 'hidden', 'name' => 'act', 'value' => 'upd'),
				array('type' => 'hidden', 'name' => 'module', 'value' => $this->ModuleName),
				array('type' => 'submit', 'name' => 'btnFinish', 'value' => 'Ответить абоненту и закрыть', 'xnobr' => 1),
                array('type' => 'button', 'name' => 'btnReopen', 'value' => 'Отправить на повторное рассмотрение', 'xnobr' => 1),
                array('type' => 'submit', 'name' => 'btnRecall', 'value' => 'Перезвонить позже', 'xnobr' => 1),
                array('type' => 'submit', 'name' => 'btnSend', 'value' => 'Передать в контакт-центр', 'nobr' => 1),
                array('type' => 'button', 'name' => 'btnDelegate', 'value' => 'Переслать специалисту', 'nobr' => 1),
                array('type' => 'button', 'name' => 'btnDeputyMayor', 'value' => 'Переслать зам. мэра', 'nobr' => 1),
                    array(),
                array('type' => 'submit', 'name' => 'btnPrint', 'value' => 'Печать', 'nobr' => 1),
                array('type' => 'submit', 'name' => 'btnSave', 'value' => 'Сохранить', 'nobr' => 1),
                array('type' => 'submit', 'name' => 'btnClose', 'value' => 'Закрыть', 'nobr' => 1),
                array('type' => 'button', 'name' => 'btnReturn', 'value' => 'Вернуть на модерацию', 'nobr' => 1),
                array('type' => 'submit', 'name' => 'btnFinishProcessing', 'value' => 'Закончить обработку', 'nobr' => 1),
			));

            if($data['project_id'] != 9)
            {
                $form->DeleteElement('organization');
                $form->DeleteElement('service');
            }

            if($_SESSION['user_priv'] != 17)
            {
                $form->DeleteElement('btnDeputyMayor');
            }

            if($data['status'] == 10)
            {
                if($data['municipality_id'])
                {
                    $form->DeleteElement('btnFinish');
                    $form->DeleteElement('btnReopen');
                    $form->DeleteElement('btnRecall');
                    $form->DeleteElement('btnSend');
                    $form->DeleteElement('btnReturn');
                }
                else
                {
                    $form->DeleteElement('btnFinishProcessing');
                }
            }
            else
            {
                $form->DeleteElement('btnFinishProcessing');
            }

            if($_SESSION['user_priv'] == 22 && $data['status'] != 10)
                $form->SetElementAttribute('*', 'readonly', 1);

            if(empty($data['municipality_id']))
                $form->DeleteElement('municipality_id');

            if(empty($data['phone2']))
                $form->DeleteElement('phone2_label');

            if(empty($data['email']))
                $form->DeleteElement('email');

            if($_SESSION['user_data']['disabled'])
            {
                $form->DeleteElement('garbage');
                $form->DeleteElement('btnFinish');
                $form->DeleteElement('btnReopen');
                $form->DeleteElement('btnRecall');
                $form->DeleteElement('btnSend');
                $form->DeleteElement('btnDelegate');
                $form->DeleteElement('btnPrint');
                //$form->DeleteElement('btnClose');
                $form->DeleteElement('btnReturn');

                $form->SetElementAttribute('department_id', 'readonly', 1);
            }

            if($data['type'] == 2 || $data['type'] == 3)
            {
                if(empty($data['answer']))
                    $form->SetElementAttribute('expert', 'caption', 'На делегировании у');

                if($data['expert_id'])
                {
                    $expert = $this->getline("select users.name uname, phone, post, roles.name rname
                    from users join roles on users.priv = roles.id
                    where users.id = ".$data['expert_id']);
                    $form->SetElementAttribute('expert', 'value', $expert['uname'].', '.$expert['post'].', тел. '.$expert['phone']);
                }
                else if($data['user_id'])
                {
                    $expert = $this->getline("select users.name uname, phone, roles.name rname
                    from users join roles on users.priv = roles.id
                    where users.id = ".$data['user_id']);
                    $form->SetElementAttribute('expert', 'value', $expert['uname'].', '.$expert['post'].', тел. '.$expert['phone']);
                }
                else
                {
                    $form->DeleteElement('expert');
                }
            }

            if(!$data['gubcomment'])
                $form->DeleteElement('gubcomment');

            if(!ACL::Check($this->ObjectType, 'print'))
                $form->DeleteElement('btnPrint');

            if(!$data['department_id'])
                $form->DeleteElement('btnReturn');
            if(!$data['num'])
            {
                //$form->DeleteElement('btnAddToBlacklist');
                $form->DeleteElement('num_label');
            }

            if($data['status'] == 15 || $data['status'] == 20)
            {
                $form->SetElementAttribute('*', 'readonly', 1);
                $form->SetElementAttribute('comment', 'readonly', 0);
            }

            if($data['status'] == 15)
            {
                $form->DeleteElement('garbage');
                $form->DeleteElement('btnSend');
                $form->DeleteElement('btnReturn');
                $form->DeleteElement('btnDelegate');
                $form->SetElementAttribute('comment', 'readonly', 1);
                $form->SetElementAttribute('department_id', 'readonly', 0);
            }
            else
            {
                $form->DeleteElement('btnReopen');

                $user = $this->getline("select department_id from user_department where user_id = ".$_SESSION['user_id']);
                if(!$user['department_id'])
                if(!ACL::Check($this->ObjectType, 'requestedit'))
                {
                    $form->DeleteElement('btnSend');
                    //$form->DeleteElement('btnSave');
                    $form->DeleteElement('btnReturn');
                    $form->DeleteElement('btnDelegate');
                    $form->DeleteElement('btnFinish');
                    $form->DeleteElement('btnRecall');
                    $form->SetElementAttribute('*', 'readonly', 1);
                }
            }

            if(ACL::Check($this->ObjectType, 'requestedit'))
            {
                $form->SetElementAttribute('topic_id', 'readonly', 0);
                $form->SetElementAttribute('name', 'readonly', 0);
                $form->SetElementAttribute('question', 'readonly', 0);
            }

            if($data['status'] == 20)
            {
                $form->DeleteElement('btnFinish');
                $form->DeleteElement('btnSend');

                $form->DeleteElement('btnReturn');
                $form->DeleteElement('btnDelegate');
                $form->DeleteElement('btnRecall');
                $form->SetElementAttribute('*', 'readonly', 1);

                if(ACL::Check($this->ObjectType, 'restore'))
                {
                    $form->SetElementAttribute('status', 'readonly', 0);
                }
                else
                {} //$form->DeleteElement('btnSave');
            }
            else
            {
                $form->DeleteElement('btnAddArticle');
            }

            if((!ACL::Check($this->ObjectType, ACL::UPDATE)) || (!Orders::hasAccess($this->Id, ACL::UPDATE)))
                $form->SetElementAttribute('*', 'readonly', 1);

            if(!ACL::Check($this->ObjectType, 'allaccess'))
                $form->SetElementAttribute('department_id', 'readonly', 1);

            $form->SetElementAttribute('btnAddToBlacklist', 'readonly', 0);

            if (in_array($_SESSION['user_priv'], array(1, 14)) && $data['status'] == 3)
            {
                $form->SetElementAttribute('result', 'required', 1);
                $form->SetElementAttribute('result', 'readonly', 0);

                $form->SetElementAttribute('rating', 'required', 1);
                $form->SetElementAttribute('rating', 'readonly', 0);
            } else
            {
                if ($data['rating'])
                    $form->SetElementAttribute('rating', 'readonly', 1);
                else
                    $form->DeleteElement('rating');

                if ($data['result'])
                    $form->SetElementAttribute('result', 'readonly', 1);
                else
                    $form->DeleteElement('result');
            }

//            if(!ACL::Check($this->ObjectType, 'ratingedit'))
//            {
//                if(!ACL::Check($this->ObjectType, 'ratingview'))
//                {
//                    $form->DeleteElement('rating');
//                    $form->DeleteElement('rating_comment');
//                }
//                else
//                {
//                    $form->SetElementAttribute('rating', 'readonly', 1);
//                    $form->SetElementAttribute('rating_comment', 'readonly', 1);
//
//                    if(!$data['rating'])
//                        $form->DeleteElement('rating');
//
//                    if(!$data['rating_comment'])
//                        $form->DeleteElement('rating_comment');
//                }
//            }

            if($_SESSION['user_priv'] == 26)
            {
                $form->SetElementAttribute('answer', 'readonly', 0);
                $val = $form->GetElementAttribute('btnDelegate', 'value');
                if(!$val)
                {
                    $form->InsertBefore('btnSave',
                        array('type' => 'button', 'name' => 'btnDelegate', 'value' => 'Переслать специалисту', 'nobr' => 1)
                    );
                }
            }

            $pd = array();
            $pds = new DataSource("select pd from role_pd where role_id = " . $_SESSION['user_priv']);
            while($pdata = $pds->GetString())
                $pd[] = $pdata['pd'];

            if(in_array('lastname', $pd) === false)
            {
                $form->DeleteElement('lastname');
            }
            if(in_array('firstname', $pd) === false)
            {
                $form->DeleteElement('firstname');
            }
            if(in_array('middlename', $pd) === false)
            {
                $form->DeleteElement('middlename');
            }
            if(in_array('sex', $pd) === false)
            {
                $form->DeleteElement('sex');
            }
            if(in_array('age', $pd) === false)
            {
                $form->DeleteElement('age');
            }
            if(in_array('occupation', $pd) === false)
            {
                $form->DeleteElement('job_id');
            }
            if(in_array('region', $pd) === false)
            {
                $form->DeleteElement('region_id');
            }
            if(in_array('city', $pd) === false)
            {
                $form->DeleteElement('city');
            }
            if(in_array('address', $pd) === false)
            {
                $form->DeleteElement('address');
            }
            if(in_array('phone', $pd) === false)
            {
                $form->DeleteElement('phone_label');
                $form->DeleteElement('num_label');
                $form->DeleteElement('btnAddToBlacklist');
            }
            if(in_array('phone2', $pd) === false)
            {
                $form->DeleteElement('phone2_label');
            }
            if(in_array('email', $pd) === false)
            {
                $form->DeleteElement('email');
            }

            $form->SetElementAttribute('userfile[]', 'readonly', 0);
            require_once "userfiles.php";
            Userfiles::show($this->Id, $this->ModuleName, $form, ACL::Check($this->ObjectType, 'allaccess'));

			echo '<table style="border:0px;"><tr><td style="vertical-align:top;">';
            $form->Show();
            echo '</td><td style="vertical-align:top; padding-left: 15px;">';
            $project = $this->getline("select simple from projects where id = ".$data['project_id']);
            if(!$project['simple'])
            {
                echo '<div id="black"></div>';
                echo '<div id="doubles"></div>';
            }
            require_once "comments.php";
            $show_add_dialog = !($data['status'] == 20);
            Comments::showComments($this->Id, $this->ModuleName, 'index.php?module='.$this->ModuleName.'&act=edit&id='.$this->Id, $show_add_dialog);
            echo '<br><br>';
            $history = new OrderHistoryTable($this->Id);
            $history->Show();
            echo '<br><br>';
            if($data['m_id'])
            if(ACL::Check($this->ObjectType, 'listencalls'))
            {
?>
                <div id="call_<?php echo md5($data['m_id']); ?>"></div>
                <div class="jp-audio">
                    <div class="jp-type-single">
                      <div id="jp_interface_1" class="jp-interface">
                        <ul class="jp-controls">
                          <li><a href="#" class="jp-play" tabindex="1">play</a></li>
                          <li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
                          <li><a href="#" class="jp-stop" tabindex="1">stop</a></li>
                          <li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
                          <li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
                        </ul>
                        <div class="jp-progress">
                          <div class="jp-seek-bar">
                            <div class="jp-play-bar"></div>
                          </div>
                        </div>
                        <div class="jp-volume-bar">
                          <div class="jp-volume-bar-value"></div>
                        </div>
                        <div class="jp-current-time"></div>
                        <div class="jp-duration"></div>
                      </div>
                      <div id="jp_playlist_1" class="jp-playlist">
                        <ul>
                          <li>прослушать звонок</li>
                        </ul>
                      </div>
                    </div>
                </div>
                <script>
                    $(document).ready(function(){
                        $('#call_<?php echo md5($data['m_id']); ?>').jPlayer({
                            swfPath: "/scripts/jplayer",
                            preload: 'auto',
                            ready : function()
                            {
                                $(this).jPlayer("setMedia", {mp3: 'http://play.firecall.net/?m_id=<?php echo (date('Nwz') * date('W')) . $data['m_id']; ?>'});
                            }
                        })

                    })
                </script>
<?php
            }
            if($data['m_id'])
            if(ACL::Check($this->ObjectType, 'downloadcalls'))
            {
                echo '<br><img src="images/cassette.png" style="border:0; vertical-align:middle"> <a href="http://play.firecall.net/?m_id='. (date('Nwz') * date('W')) . $data['m_id'].'">скачать файл звонка</a>';
            }
            echo '</td></tr></table>';

            LocationManager::add();
		}

        public static function hasAccess($order_id, $access = ACL::READ)
        {
            if(ACL::Check(OBJECT_ORDERS, 'allaccess'))
                return true;

            $ds = new DataSource("select * from orders where id = " . $order_id);
            $data = $ds->GetString();

            $uds = new DataSource("select * from user_region where user_id = " . $_SESSION['user_id']);
            if($uds->RowCount)
            {
                $regionHit = false;
                while($udata = $uds->GetString())
                    if($udata['region_id'] == $data['region_id'])
                        $regionHit = true;

                if(!$regionHit)
                    return false;
            }

            $sql = new SqlObject();
            $order = $sql->getline("select department_id, municipality_id from orders where id = ".$order_id);
            $user = $sql->getline("select id from user_department where user_id = ".$_SESSION['user_id']." and (department_id = ".$order['department_id']." or department_id = ".$order['municipality_id'].")".(($access == ACL::UPDATE)?" and fullaccess = 1":""));
            if($user['id'])
                return true;

            if($data['expert_id'] == $_SESSION['user_id'])
                return true;

            return false;
        }

        private function _getHtml($id)
        {
            $data = $this->getline("select * from orders where id = ".$this->Id);
            $job = $this->getline("select * from jobs where id = ".$data['job_id']);
            $region = $this->getline("select * from regions where id = ".$data['region_id']);
            $department = $this->getline("select * from departments where id = ".$data['department_id']);
            $project = $this->getline("select * from projects where id = ".$data['project_id']);
            $topic = $this->getline("select * from topics where id = ".(int)$data['topic_id']);

            $sex = new SexTypeList();
            $source = new SourceList();
            $type = new TypeList();
            $alert = new AlertTypeList();
            $status = new StatusList();
            $result = new ResultTypeList();

            $ret = '<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">
            <head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:View>Print</w:View>
 </w:WordDocument>
</xml><![endif]-->
</head>
            <body><table>
                <tr>
                    <td style="font-weight:bold;">ID</td>
                    <td>'.$data['id'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Проект</td>
                    <td>'.$project['name'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Дата/время</td>
                    <td>'.DT::getDateTime($data['created']).'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">ФИО</td>
                    <td>'.$data['lastname'].' '.$data['firstname'].' '.$data['middlename'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Пол</td>
                    <td>'.$sex->Get($data['sex']).'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Возраст</td>
                    <td>'.$data['age'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Род занятий</td>
                    <td>'.$job['name'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Район</td>
                    <td>'.$region['name'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Нас. пункт</td>
                    <td>'.$data['city'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Адрес</td>
                    <td>'.$data['address'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Телефон</td>
                    <td>'.$data['phone'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Email</td>
                    <td>'.$data['email'].'</td>
                </tr>';
            if($data['topic_id'])
            {
                $ret .=
                    '<tr>
                    <td style="font-weight:bold;">Тематика</td>
                    <td>'.$topic['name'].'</td>
                </tr>';
            }
            $ret .=
                '<tr>
                    <td style="font-weight:bold;">Заголовок вопроса</td>
                    <td>'.$data['name'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Вопрос</td>
                    <td>'.$data['question'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Ответственный</td>
                    <td>'.$department['name'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Ответ</td>
                    <td>'.$data['answer'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Комментарий</td>
                    <td>'.$data['comment'].'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Источник</td>
                    <td>'.$source->Get($data['source']).'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Тип</td>
                    <td>'.$type->Get($data['type']).'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Угроза</td>
                    <td>'.$alert->Get($data['alert']).'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Статус</td>
                    <td>'.$status->Get($data['status']).'</td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Результат</td>
                    <td>'.$result->Get($data['result']).'</td>
                </tr>';
             if($data['rating'])
             {
                 $ret .= '
                 <tr>
                    <td style="font-weight:bold;">Оценка</td>
                    <td>'.$data['comment'].'</td>
                </tr>';
                 if($data['rating_comment'])
                 {
                     $ret .=
                         '<tr>
                            <td style="font-weight:bold;">Комментарий к оценке</td>
                            <td>'.$data['rating_comment'].'</td>
                          </tr>';
                 }
             }
            $ret .= '
                </table>
                <br><br>';

            return $ret;
        }

        public function printAction()
        {

            header('Content-Disposition: attachment; filename="Вопрос №'.$this->Id.'.doc"');
            header('Content-Type: application/msword');

            echo $this->_getHtml($this->Id);

            $query = new Query("select");
            $query->AddTable("comments left outer join users on comments.author_id = users.id");
            $query->Fields = array("comments.id", "users.name contact", "comments.created", "comments.comment");
            $query->Where->Add("owner_id = ".$this->Id." and module = 'orders'");
            $query->Order = 'comments.created asc';
            $ds = new DataSource($query);
            if($ds->RowCount)
                echo '<div style="width:500px; padding-top:15px;"><span style="font-weight:bold;font-size:16px;"><i>Комментарии</i></span><br><br>';
            while($data = $ds->GetString())
            {
                echo '<strong>'.$data['contact'].'</strong> <span style="color:silver"> | '.DT::getDateTime($data['created']).'</span><br>'.Utils::EscapeString($data['comment']);
                $fileds = new DataSource('select * from userfiles where owner_id = '.$data['id']." and module = 'comments'");
                while($filedata = $fileds->GetString())
                {
                    echo 'Приложен файл <i>'.$filedata['name_orig'].'</i><br>';
                }
                echo '<br><br>';
            }
            echo '<br><br>';

            echo '<table border="1"><tr><td colspan="5" align="center"><small><strong>История вопроса</strong></td></tr>
            <tr bgcolor="#eeeeee"><td><small>Дата/время</td><td><small>Пользователь</td><td><small>Действие</td><td><small>Было</td><td><small>Стало</td></tr>
            ';

            $query = new Query("select");
            $query->AddTable("logs
            left outer join users
            on logs.user_id = users.id
            ");
            $query->Fields = array(
                "logs.id", "dt", "users.name", "object", "module", "record", "action", "prev", "new");
            $query->Where->Add("record = ".$this->Id);
            $query->Where->Add("module = 'orders'");
            $query->Order = 'dt asc';

            $ds = new DataSource($query);
            while($data = $ds->GetString())
            {
                echo '<tr><td><small>'.DT::getDateTime($data['dt']).'</td><td><small>'.$data['name'].'</td><td><small>'.$data['action'].'</td><td><small>'.$data['prev'].'</td><td><small>'.$data['new'].'</td></tr>';
            }
            
            echo '</table></body></html>';
        }

        public function reopenAction()
        {
            $data = $this->getline("select * from orders where id = ".$this->Id);
            $old_item = $this->getline("select name from departments where id = ".(int)$data['department_id']);
            $old_item = $old_item['name'];

            $query = new Query("update");
            $query->AddTable("orders");
            $query->AddField("status", 7);
            //$query->AddField("days", '0');
            $query->Where->Add("id = ".$this->Id);
            $query->Execute();

            $caption = 'На обработке';
            if($this->comment)
                $caption .= ': '.$this->comment;
            Log::Add($this->ObjectType, $this->ModuleName, $caption, $this->Id, $old_item, $caption);

            $ds = new DataSource("select distinct user_id id, priv, email
            from user_department
            join users on user_department.user_id = users.id
            where (email != '' and email is not null) and
            department_id = ".$data['department_id']."
            and disabled = 0
            ");
            while($user = $ds->GetString())
            {
                $emails[] = $user['email'];
            }

            $emails = array_unique($emails);
            if($emails)
            {
                $title = 'Вопрос №'.$this->Id.' отправлен на повторное рассмотрение';
                $content = $this->comment;
                foreach($emails as $email)
                    Email::send($email, $title, $content);
            }

            LocationManager::last();
        }

        public function returnAction()
        {
            $data = $this->getline("select * from orders where id = ".$this->Id);
            $old_item = $this->getline("select name from departments where id = ".(int)$data['department_id']);
            $old_item = $old_item['name'];

            $query = new Query("update");
            $query->AddTable("orders");
            $query->AddField("department_id", "inline:(select id from departments where name = 'Другое')");
            $query->Where->Add("id = ".$this->Id);
            $query->Execute();

            $caption = 'Возврат на модерацию';
            if($this->comment)
                $caption .= ': '.$this->comment;
            Log::Add($this->ObjectType, $this->ModuleName, $caption, $this->Id, $old_item);

            $ds = new DataSource("select email, priv from users where
            priv in (select role_id from acl where object_type = 'OBJECT_ORDERS' and action_type = 'allaccess')
            and (email != '' and email is not null)");
            while($user = $ds->GetString())
            {
                if(ACL::Check(OBJECT_ORDERS, 'reportreturn', $user['priv']))
                    $emails[] = $user['email'];
            }

            $emails = array_unique($emails);
            if($emails)
            {
                $title = 'Вопрос №'.$this->Id.' возвращен на модерацию';
                $content = $this->comment;
                foreach($emails as $email)
                {
                    Email::send($email, $title, $content);
                }
            }

            LocationManager::last();
        }

        public function recallAction()
        {
            $query = new Query("update");
            $query->AddTable("orders");
            $query->AddField("status", 15);
            $query->AddField("recall", DT::getUnixTime($this->recall));
            $query->Where->Add("id = ".$this->Id);
            $query->Execute();

//            $query = new Query("insert");
//            $query->AddTable("comments");
//            $query->AddField("created", 'UNIX_TIMESTAMP()');
//            $query->AddField("author_id", $_SESSION['user_id']);
//            $query->AddField("comment", $this->comment);
//            $query->AddField("owner_id", $this->Id);
//            $query->AddField("module", 'orders');
//            $query->Execute();

            Log::Add($this->ObjectType, $this->ModuleName, 'Попытка доведения до абонента: '.$this->comment.' <br>Перезвонить '.DT::getDateTime($this->recall), $this->Id);

            LocationManager::last();
        }

        public function delegateAction()
        {
            $data = $this->getline("select * from orders where id = ".$this->Id);

            if($this->expert_id)
            {
                $region = $this->getline("select * from regions where id = ".$data['region_id']);

                $code = rand(1000000, 9999999);

                $query = new Query("update");
                $query->AddTable("orders");
                $query->AddField("status", 10);
                $query->AddField("expert_id", $this->expert_id);
                $query->AddField("code", $code);
                $query->Where->Add("id = ".$this->Id);
                $query->Execute();

                $caption = 'Делегирован';
                if($this->comment)
                    $caption .= ': '.$this->comment;
                $expert = $this->getline("select name from users where id = " . $this->expert_id);
                Log::Add($this->ObjectType, $this->ModuleName, $caption, $this->Id, '', $expert['name']);

                $user = $this->getline("select email from users where id = ".$this->expert_id);
                $text = '<strong>Дата и время поступления вопроса:</strong> ' . DT::getDateTime($data['created']) . '<br>' .
                    '<strong>Фамилия Имя Отчество:</strong> ' . $data['lastname'] . ' ' . $data['firstname'] . ' ' . $data['middlename'] . '<br>' .
                    '<strong>Район проживания:</strong> ' . $region['name'] . '<br>' .
                    '<strong>Тема вопроса:</strong> ' . $data['name'] . '<br>' .
                    '<strong>Вопрос:</strong> ' . $data['question'] . '<br>' .
                    '<p>Вы можете ответить на него, перейдя по ссылке: <a href="http://' . Registry::Get('server_domain', 'gov29.firecall.net') . '/expert.php?code='.$code.'">http://' . Registry::Get('server_domain', 'gov29.firecall.net') . '/expert.php?code='.$code.'</a>';
                Email::send($user['email'], 'Вам делегирован вопрос: '.$data['name'], $text);
            }

            if((int)$_REQUEST['municipality_id'])
            {
                $query = new Query("update");
                $query->AddTable("orders");
                $query->AddField("status", 10);
                $query->AddField("municipality_id", (int)$_REQUEST['municipality_id']);
                $query->Where->Add("id = ".$this->Id);
                $query->Execute();

                $caption = 'Делегирован муниципалитету';
                if($this->comment)
                    $caption .= ': '.$this->comment;
                $municipality = $this->getline("select name from departments where id = " . (int)$_REQUEST['municipality_id']);
                Log::Add($this->ObjectType, $this->ModuleName, $caption, $this->Id, '', $municipality['name']);

                if($data['municipality_id'] != $_REQUEST['municipality_id'])
                {
                    $emails = array();
                    $ds = new DataSource("select email from users where id in (select user_id from user_department where department_id = ".(int)$_REQUEST['municipality_id'].") and disabled = 0 and email_notify = 1");
                    while($user = $ds->GetString())
                    {
                        if($user['email'])
                            $emails[] = $user['email'];
                    }

                    $emails = array_unique($emails);
                    if($emails)
                    {
                        $content = $this->_getHtml($this->Id);
                        foreach($emails as $email)
                            Email::send($email, 'Делегирован запрос', $content);
                    }
                }
            }

            LocationManager::last();
        }

        public function deputymayorAction()
        {
            if($this->expert_id)
            {
                if($_REQUEST['deputyanswer'])
                {
                    $this->sql("update orders set answer = '" . $_REQUEST['deputyanswer'] . "' where id = " . $this->Id);
                }

                $data = $this->getline("select * from orders where id = ".$this->Id);
                $region = $this->getline("select * from regions where id = ".$data['region_id']);

                $code = rand(1000000, 9999999);

                $query = new Query("update");
                $query->AddTable("orders");
                $query->AddField("status", 10);
                $query->AddField("expert_id", $this->expert_id);
                $query->AddField("code", $code);
                $query->Where->Add("id = ".$this->Id);
                $query->Execute();

                $caption = 'Делегирован зам. мэра';
                if($this->comment)
                    $caption .= ': '.$this->comment;
                $expert = $this->getline("select name from users where id = " . $this->expert_id);
                Log::Add($this->ObjectType, $this->ModuleName, $caption, $this->Id, '', $expert['name']);

                $user = $this->getline("select email from users where id = ".$this->expert_id);
                $text = '<strong>Дата и время поступления вопроса:</strong> ' . DT::getDateTime($data['created']) . '<br>' .
                    '<strong>Фамилия Имя Отчество:</strong> ' . $data['lastname'] .  $data['firstname'] . $data['middlename'] . '<br>' .
                    '<strong>Район проживания:</strong> ' . $region['name'] . '<br>' .
                    '<strong>Тема вопроса:</strong> ' . $data['name'] . '<br>' .
                    '<strong>Вопрос:</strong> ' . $data['question'] . '<br>' .
                    '<p>Вы можете ответить на него, перейдя по ссылке: <a href="http://' . Registry::Get('server_domain', 'gov29.firecall.net') . '/expert.php?code='.$code.'">http://' . Registry::Get('server_domain', 'gov29.firecall.net') . '/expert.php?code='.$code.'</a>';
                Email::send($user['email'], 'Вам делегирован вопрос: '.$data['name'], $text);
            }
            else if((int)$_REQUEST['municipality_id'])
            {
                $query = new Query("update");
                $query->AddTable("orders");
                $query->AddField("status", 10);
                $query->AddField("municipality_id", (int)$_REQUEST['municipality_id']);
                $query->Where->Add("id = ".$this->Id);
                $query->Execute();

                $caption = 'Делегирован муниципалитету';
                if($this->comment)
                    $caption .= ': '.$this->comment;
                Log::Add($this->ObjectType, $this->ModuleName, $caption, $this->Id);
            }

            LocationManager::last();
        }
		
		public function addAction()
		{	
			if(!ACL::Check($this->ObjectType, ACL::CREATE))
			{
				$this->Page->Redirect(START_PAGE);
			}

			$sql = new SqlObject();
			$department = $sql->getline("select id from departments where name = 'Другое'");

			$query = new Query("insert");
			$query->AddTable("orders");
			$query->AddField("author_id", $_SESSION['user_id']);
			$query->AddField("created", 'inline:UNIX_TIMESTAMP()');
            $query->AddField("num", $this->num);
            $query->AddField("source", $this->source);
            $query->AddField("project_id", $this->project_id);
			$query->AddField("alert", $this->alert);
			$query->AddField("lastname", $this->lastname);
			$query->AddField("firstname", $this->firstname);
			$query->AddField("middlename", $this->middlename);
			$query->AddField("sex", $this->sex);
			$query->AddField("age", $this->age);
			$query->AddField("job_id", $this->job_id);
			$query->AddField("city", $this->city);
			$query->AddField("region_id", $this->region_id);
			$query->AddField("address", $this->address);
            $query->AddField("organization", $this->organization);
            $this->phone = preg_replace('/\D/', '', $this->phone);
            if(preg_match('/(.{10})$/', $this->phone, $matches))
                $this->phone = $matches[1];
			$query->AddField("phone", $this->phone);
            $query->AddField("phone2", $this->phone2);
			$query->AddField("email", $this->email);
            $query->AddField("topic_id", $this->topic_id);
			$query->AddField("name", $this->name);
			$query->AddField("question", $this->question);
            $query->AddField("answer", $this->answer);
			$query->AddField("comment", $this->comment);
            $query->AddField("article_id", $this->article_id);
			//$query->AddField("expert_id", $this->expert_id);

			$query->AddField("type", $this->type);
			$query->AddField("garbage", $this->garbage);
			$query->AddField("call_id", $this->call_id);
            //if($_REQUEST['btnFinish'])
            if($this->type == 1)
            {
			    $query->AddField("status", 20);
                $query->AddField("result", $this->result);
                $query->AddField("user_id", $this->user_id);
            }
            else
                $query->AddField("status", 5);

            $query->AddField("department_id", $department);

			$query->Execute();
            $this->Id = $query->last_insert_id();

            require_once "userfiles.php";
            Userfiles::process($this->Id, $this->ModuleName);

            Log::Add($this->ObjectType, $this->ModuleName, 'Создание вопроса', $this->Id);
            $new_item = $this->getline("select name from departments where id = ".(int)$this->department_id);
            $new_item = $new_item['name'];
            Log::Add($this->ObjectType, $this->ModuleName, 'Назначение ответственного', $this->Id, '', $new_item);
            //if($_REQUEST['btnFinish'])
            if($this->type == 1)
                Log::Add($this->ObjectType, $this->ModuleName, 'Ответ доведен', $this->Id);
            else
                Log::Add($this->ObjectType, $this->ModuleName, 'На обработке', $this->Id);

            if($this->alert > 1)
            {
                $ds = new DataSource("select id, cellphone, email, priv from users where priv in (select role_id from acl where object_type = 'OBJECT_ORDERS' and action_type = 'allaccess') and disabled = 0");
                while($user = $ds->GetString())
                {
                    if(($user['email']) && (ACL::Check(OBJECT_ORDERS, 'alertemail', $user['priv'])))
                        $emails[] = $user['email'];

                    if(($user['cellphone']) && (ACL::Check(OBJECT_ORDERS, 'alertsms', $user['priv'])))
                        $cellphones[] = $user['cellphone'];
                }

                if($this->department_id)
                {
                    $ds = new DataSource("select email from users where id in (select user_id from user_department where department_id = ".$this->department_id.") and disabled = 0");
                    while($user = $ds->GetString())
                    {
                        if($user['email'])
                            $emails[] = $user['email'];
                    }
                }

                $emails = array_unique($emails);
                if($emails)
                {
                    if($this->alert == 2)
                        $title = 'Запрос '.$this->Id.' - средний уровень угрозы';
                    else if($this->alert == 3)
                        $title = 'Запрос '.$this->Id.' - высокий уровень угрозы';

                    $content = $this->_getHtml($this->Id);
                    foreach($emails as $email)
                        Email::send($email, $title, $content);
                }

                if(($this->alert == 3) && ($cellphones))
                {
                    foreach($cellphones as $cellphone)
                        SMS::send($cellphone, 'Запрос '.$this->Id.' - высокий уровень угрозы: '.$this->name);
                }
            }
            else if ($this->department_id)
            {
                if(($this->result == 1 || $this->result == 2) && ($this->type == 2 || $this->type == 3))
                {
                    $ds = new DataSource("select email from users where id in (select user_id from user_department where department_id = ".$this->department_id.") and disabled = 0 and email_notify = 1");
                    while($user = $ds->GetString())
                    {
                        if($user['email'])
                            $emails[] = $user['email'];
                    }

                    $emails = array_unique($emails);
                    if($emails)
                    {
                        $content = $this->_getHtml($this->Id);
                        foreach($emails as $email)
                            Email::send($email, 'Новый запрос', $content);
                    }
                }
            }

            $region = $this->getline("select name from regions where id = " . $this->region_id);

            Email::send('sam@felix24.ru', 'новый запрос на прямую линию правительства Архангельской области',
                "
<b>ID:</b> " . $this->Id . "<br>
<b>Дата:</b> " . DT::getDate(DT::Now) . "<br>
<b>Населенный пункт, район:</b> " . $region['name'] . ", " . $this->city . "<br>
<b>Заголовок:</b> " . $this->name . "<br>
<b>Текст:</b> " . $this->question . "<br>
<b>Оператор:</b> " . $_SESSION['user_name']
            );
			
			if($this->num)
                LocationManager::redirect("index.php?module=".$this->ModuleName."&act=new&parent=".$this->Id);
            else
                LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

		public function updAction()
		{
            require_once "userfiles.php";
            Userfiles::process($this->Id, $this->ModuleName);

            if($_REQUEST['btnClose'] && $_SESSION['user_data']['disabled'])
                LocationManager::redirect("expert.php?act=close");

            if($_REQUEST['btnClose'])
			    LocationManager::redirect("index.php?module=".$this->ModuleName);

            $data = $this->getline("select * from orders where id = ".$this->Id);

			if((ACL::Check($this->ObjectType, ACL::UPDATE)) && (Orders::hasAccess($this->Id, ACL::UPDATE)))
			{
				$query = new Query("update");
				$query->AddTable("orders");
                if($this->alert)
                {
				    $query->AddField("alert", $this->alert);
                    if($data['alert'] != $this->alert)
                    {
                        $alert = new AlertTypeList();
                        Log::Add($this->ObjectType, $this->ModuleName, 'Уровень угрозы', $this->Id, $alert->Get($data['alert']), $alert->Get($this->alert));
                    }
                }

//                if(ACL::Check($this->ObjectType, 'ratingedit'))
//                {
//                    $query->AddField("rating", $this->rating);
//                    $query->AddField("rating_comment", $this->rating_comment);
//
//                    if((int)$data['rating'] != (int)$this->rating)
//                    {
//                        Log::Add($this->ObjectType, $this->ModuleName, 'Выставлена оценка', $this->Id, ($data['rating'] != 0)?$data['rating']:'', $this->rating);
//                        $ratingChanged = true;
//                    }
//                }

//				$query->AddField("lastname", $this->lastname);
//				$query->AddField("firstname", $this->firstname);
//				$query->AddField("middlename", $this->middlename);
//				$query->AddField("sex", $this->sex);
//				$query->AddField("age", $this->age);
//				$query->AddField("job_id", $this->job_id);
//				$query->AddField("city", $this->city);
//				$query->AddField("region_id", $this->region_id);
//				$query->AddField("address", $this->address);
//				$query->AddField("phone", $this->phone);
//				$query->AddField("email", $this->email);

                if(ACL::Check($this->ObjectType, 'requestedit'))
                {
                    $query->AddField("topic_id", $this->topic_id);
                    $query->AddField("name", $this->name);
    				$query->AddField("question", $this->question);
                }

                if(ACL::Check($this->ObjectType, 'allaccess'))
                {
                    if($this->department_id)
                    {
                        $query->AddField("department_id", $this->department_id);

                        if($data['department_id'] != $this->department_id)
                        {
                            $old_item = $this->getline("select name from departments where id = ".(int)$data['department_id']);
                            $old_item = $old_item['name'];
                            $new_item = $this->getline("select name from departments where id = ".(int)$this->department_id);
                            $new_item = $new_item['name'];
                            Log::Add($this->ObjectType, $this->ModuleName, 'Назначение ответственного', $this->Id, $old_item, $new_item);
                        }
                    }

                    if($this->municipality_id)
                    {
                        $query->AddField("municipality_id", $this->municipality_id);

                        if($data['municipality_id'] != $this->municipality_id)
                        {
                            $old_item = $this->getline("select name from departments where id = ".(int)$data['municipality_id']);
                            $old_item = $old_item['name'];
                            $new_item = $this->getline("select name from departments where id = ".(int)$this->municipality_id);
                            $new_item = $new_item['name'];
                            Log::Add($this->ObjectType, $this->ModuleName, 'Назначение муниципалитета', $this->Id, $old_item, $new_item);
                        }
                    }
                }

                if($data['status'] == 5 || $data['status'] == 7 || $data['status'] == 10 || $data['status'] == 13)
                {
                    if($this->answer)
                    {
                        $query->AddField("answer", $this->answer);

                        if($data['status'] == 10)
                        {
                            if($_REQUEST['btnReturn'] || $_REQUEST['btnFinishProcessing'])
                            {
                                $query->AddField("status", 13);
                                Log::Add($this->ObjectType, $this->ModuleName, 'Делегирование окончено', $this->Id);
                            }
                        }
                    }
                    else
                    {
                        if($_REQUEST['btnFinishProcessing'])
                        {
                            $query->AddField("status", 13);
                            Log::Add($this->ObjectType, $this->ModuleName, 'Возврат заявки из муниципалитета', $this->Id);
                        }
                    }
                }

                if($this->rating)
                {
                    $query->AddField("rating", $this->rating);
                }

                if($data['status'] == 20)
                {
                    if(ACL::Check($this->ObjectType, 'restore'))
                    {
                        if($data['status'] != $this->status)
                        {
                            $query->AddField("status", $this->status);
                            $status = new StatusList();
                            $old_item = $status->Get($data['status']);
                            $new_item = $status->Get($this->status);
                            Log::Add($this->ObjectType, $this->ModuleName, $new_item, $this->Id, $old_item);
                        }
                    }
                }
                else
                {
                    $query->AddField("comment", $this->comment);
                }

				//$query->AddField("user_id", $this->user_id);
				//$query->AddField("expert_id", $this->expert_id);

				//$query->AddField("type", $this->type);
				//$query->AddField("garbage", $this->garbage);
				//$query->AddField("call_id", $this->call_id);
				//$query->AddField("status", $this->status);
				//$query->AddField("result", $this->result);

                if($_REQUEST['btnFinish'])
                {
                    $query->AddField("status", 20);
                    $query->AddField("result", $this->result);

                    if((int)$data['days'] == 0)
                        $query->AddField("days", 15 - (int)floor((DT::getUnixTime(DT::Now) - DT::getUnixTime($data['created'])) / (24*60*60)));

                    if($data['status'] != 15 && $data['was_archived'] != 1)
                    {
                        $query->AddField("was_archived", 1);
                    }
                    Log::Add($this->ObjectType, $this->ModuleName, 'Ответ доведен', $this->Id);
                }
                else if($_REQUEST['btnSend'])
                {
                    if((int)$data['days'] == 0)
                        $query->AddField("days", 15 - (int)floor((DT::getUnixTime(DT::Now) - DT::getUnixTime($data['created'])) / (24*60*60)));

                    if($data['external_id'] && $data['external_email'])
                    {
                        $query->AddField("status", 20);

                        $title = 'RE:[TID#'.$data['external_id'].'] gkh.dvinaland.ru: Ответ на обращение';
                        $content = $this->answer?$this->answer:$data['answer'];
                        if($content && ($data['email_sent'] == 0))
                        {
                            $query->AddField('email_sent', 1);
                            Email::send($data['external_email'], $title, $content, 'windows-1251');
                        }
                        Log::Add($this->ObjectType, $this->ModuleName, 'Ответ доведен через сайт', $this->Id);
                    }
                    else
                    {
                        $query->AddField("status", 15);
                        Log::Add($this->ObjectType, $this->ModuleName, 'На доведении в контакт-центре', $this->Id);
                    }
                }

				$query->Where->Add("id = ".$this->Id);
				$query->Execute();

//                if($ratingChanged)
//                {
//                    if($data['department_id'])
//                    {
//                        $ds = new DataSource("select priv, email from users where id in (select user_id from user_department where department_id = ".$data['department_id'].") and disabled = 0");
//                        while($user = $ds->GetString())
//                        {
//                            if($user['email'])
//                                if(ACL::Check(OBJECT_ORDERS, 'ratingreceive', $user['priv']))
//                                    $emails[] = $user['email'];
//                        }
//                    }
//
//                    $emails = array_unique($emails);
//                    if($emails)
//                    {
//                        $content = $this->_getHtml($this->Id);
//                        foreach($emails as $email)
//                            Email::send($email, 'Ответ на запрос получил оценку', $content);
//                    }
//                }
            }

            if($_SESSION['expert_id'])
                LocationManager::redirect("expert.php?act=thanks");
            else if($_REQUEST['btnSave'])
                LocationManager::last();
            else if($_REQUEST['btnPrint'])
                LocationManager::redirect("index.php?module=".$this->ModuleName."&act=print&nohead=1&id=".$this->Id);
            else if($_REQUEST['btnAddToBlacklist'])
                LocationManager::redirect("index.php?module=blacklist&act=new&num=".$data['num'].'&phone='.$data['phone']);
            else
                LocationManager::redirect("index.php?module=".$this->ModuleName);
		}		

		public function delAction()
		{
			if(!ACL::Check($this->ObjectType, ACL::DELETE))
			{
				LocationManager::redirect(START_PAGE);
			}			

            $this->sql("delete from logs where module = 'orders' and record = ".$this->Id);
            $this->sql("delete from comments where module = 'orders' and owner_id = ".$this->Id);
            $this->sql("delete from orders where id = ".$this->Id);

            LocationManager::redirect("index.php?module=".$this->ModuleName);
		}

        public function exportAction()
        {
            ini_set('display_errors', 1);
            error_reporting(E_ALL^E_NOTICE);

            if(!ACL::Check($this->ObjectType, 'excel'))
                exit;

            $query = new Query("select");
            $query->AddTable("orders
            left outer join users authors on orders.author_id = authors.id
            left outer join departments on orders.department_id = departments.id
            left outer join regions on orders.region_id = regions.id
            left outer join jobs on orders.job_id = jobs.id
            left outer join projects on orders.project_id = projects.id
            ");
			$query->Fields = array("orders.id", "orders.id", "projects.name", "regions.name", "orders.city", "orders.address",  "orders.name", "orders.question", "departments.shortname",
                                   "orders.source", "orders.type",
                                   "orders.lastname",
                                   "orders.firstname",
                "orders.middlename",
                "orders.sex", "orders.age",
                "jobs.name",
                "orders.phone", "orders.phone2", "orders.email",
                                   "orders.answer", "orders.comment",
                                   "(select comment from comments where owner_id = orders.id and high = 1 limit 1) gubcomment",
                                   "orders.alert",
                                   "orders.created", "orders.status", "result",
                "DATEDIFF(CURDATE(), DATE(FROM_UNIXTIME(orders.created))) diff"
            );

			$captions = array("", "ID", "Проект", "Район", "Нас. пункт", "Адрес", "Тема вопроса", "Вопрос", "Ответственный",
                              "Источник", "Тип",
                              "Фамилия", "Имя", "Отчество", "Пол", "Возраст", "Род занятий",
                              "Телефон", "Доп. телефон", "Email",
                              "Ответ", "Комментарий", "Комментарий губернатора", "Угроза",
                              "Создан", "Статус", "Результат", '');

            if(!ACL::Check($this->ObjectType, 'allaccess'))
            {
                if(ACL::Check($this->ObjectType, 'allaccessdept'))
                    $query->Where->Add("orders.department_id in (select department_id from user_department where user_id = ".$_SESSION['user_id'].")");
                else
                    $query->Where->Add("orders.expert_id = ".$_SESSION['user_id']);
            }

            if(!empty($_SESSION['search_id']))
                $query->Where->Add("orders.id = ".$_SESSION['search_id']);

            if(!empty($_SESSION['search_from']))
                $query->Where->Add("orders.created >= '".DT::getUnixTime($_SESSION['search_from'])."'");

            if(!empty($_SESSION['search_to']))
                $query->Where->Add("orders.created <= '".(DT::getUnixTime($_SESSION['search_to']) + 24*60*60)."'");

            if(!empty($_SESSION['search_department']))
                $query->Where->Add("orders.department_id = ".$_SESSION['search_department']);

            if(!empty($_SESSION['search_status']))
            {
                if($_SESSION['search_status'] == 10)
                {
                    $query->Where->Add("orders.status = 10 and municipality_id = 0");
                }
                if($_SESSION['search_status'] == 11)
                {
                    $query->Where->Add("orders.status = 10 and municipality_id != 0");
                }
                else if($_SESSION['search_status'] == 21)
                {
                    $query->Where->Add("orders.status != 10 and municipality_id != 0");
                }
                else if($_SESSION['search_status'] == 22)
                {
                    $query->Where->Add("orders.status = 10 and municipality_id != 0 and expert_id != 0");
                }
                else if($_SESSION['search_status'] == 6)
                {
                    $query->Where->Add("orders.status != 20");
                    $query->Where->Add("days <= 0 or days is NULL");
                    $query->Having = 'diff >= 15';
                }
                else
                    $query->Where->Add("orders.status = ".$_SESSION['search_status']);
            }
            else
                $query->Where->Add("orders.status in (5, 10, 13)");

            if(!empty($_SESSION['search_source']))
                $query->Where->Add("orders.source = ".$_SESSION['search_source']);

            if(!empty($_SESSION['search_region']))
                $query->Where->Add("orders.region_id = ".$_SESSION['search_region']);

            if(!empty($_SESSION['search_municipality']))
                $query->Where->Add("orders.municipality_id = ".$_SESSION['search_municipality']);

            if(!empty($_SESSION['search_topic']))
                $query->Where->Add("orders.topic_id = ".$_SESSION['search_topic']);

            if(!empty($_SESSION['search_alert']))
                $query->Where->Add("orders.alert = ".$_SESSION['search_alert']);

            if(!empty($_SESSION['search_type']))
                $query->Where->Add("orders.type = ".$_SESSION['search_type']);

            if(!empty($_SESSION['search_result']))
                $query->Where->Add("orders.result = ".$_SESSION['search_result']);

            if(!empty($_SESSION['search_project']))
                $query->Where->Add("orders.project_id = ".$_SESSION['search_project']);

            if(!empty($_SESSION['search_author']))
                $query->Where->Add("orders.author_id = ".$_SESSION['search_author']);

            if(!empty($_SESSION['search_gubcomment']))
            {
                if($_SESSION['search_gubcomment'] == 1)
                    $query->Where->Add("orders.id in (select owner_id from comments where high = 1)");
                else
                    $query->Where->Add("orders.id not in (select owner_id from comments where high = 1)");
            }

            if(!empty($_SESSION['search_rating']))
            {
                $query->Where->Add("orders.rating = " . (int)$_SESSION['search_rating']);
            }

            if(!empty($_SESSION['search_text']))
            {
                $where = new Where();
                $where->Policy = 'or';
                $where->Add("orders.name like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.question like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.answer like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.comment like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.lastname like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.phone like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.email like '%".$_SESSION['search_text']."%'");
                $query->Where->Add($where);
            }

            $query->Order = 'orders.id desc';

            include './Classes/PHPExcel.php';
            include './Classes/PHPExcel/Writer/Excel2007.php';

            spl_autoload_register(__autoload);

            $excel = new PHPExcel();
            $excel->setActiveSheetIndex(0);

            $row = 1;

            for($i = 0; $i < count($captions); $i++)
            {
                if(empty($captions[$i]))
                    continue;

                $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $captions[$i]);
                $styleArray = array('font' => array('bold' => true));
                $excel->getActiveSheet()->getStyleByColumnAndRow($counter, $row)->applyFromArray($styleArray);
                $excel->getActiveSheet()->getStyleByColumnAndRow($counter, $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFCCCCCC');
                $counter++;
            }

            $source = new SourceList();
            $type = new TypeList();
            $alert = new AlertTypeList();
            $status = new StatusList();
            $result = new ResultTypeList();
            $sex = new SexTypeList();

            $ds = new DataSource($query);
            while($data = $ds->GetString())
            {
                $counter = 0;
                $row++;

                for($i = 0; $i < count($data); $i++)
                {
                    if(!empty($captions[$i]))
                    {
                        if($i == 9)
                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $source->Get($data[$i]));
                        else if($i == 10)
                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $type->Get($data[$i]));
                        else if($i == 14)
                            //$excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, ($data[$i]?'Да':''));
                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $sex->Get($data[$i]));
                        else if($i == 23)
                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $alert->Get($data[$i]));
                        else if($i == 24)
                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, DT::getDate($data[$i]));
                        else if($i == 25)
                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $status->Get($data[$i]));
                        else if($i == 26)
                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $result->Get($data[$i]));
                        else
                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $data[$i]);

                        $counter++;
                    }
                }
            }

            for($i = 0; $i < $counter; $i++)
                $excel->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);

            $xls = new PHPExcel_Writer_Excel2007($excel);

            header('Content-Disposition: attachment; filename="Вопросы.xlsx"');
            header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            $xls->save('php://output');
        }

        public function exportminiAction()
        {
//            ini_set('display_errors', 1);
//            error_reporting(E_ALL^E_NOTICE);

            if(!ACL::Check($this->ObjectType, 'excel'))
                exit;

            $query = new Query("select");
            $query->AddTable("orders
            left outer join users authors on orders.author_id = authors.id
            left outer join departments on orders.department_id = departments.id
            left outer join regions on orders.region_id = regions.id
            left outer join jobs on orders.job_id = jobs.id
            left outer join projects on orders.project_id = projects.id
            ");
            $query->Fields = array("orders.id", "orders.id", "CONCAT_WS(' ', orders.city, orders.address) address",
                "orders.question", "departments.shortname",
                "CONCAT_WS(' ', orders.lastname, orders.firstname, orders.middlename) fio",
                "orders.answer",
                "DATEDIFF(CURDATE(), DATE(FROM_UNIXTIME(orders.created))) diff"
            );

            $captions = array("", "ID", "Адрес", "Вопрос", "Ответственный",
                "ФИО", "Ответ", ''
            );

            if(!ACL::Check($this->ObjectType, 'allaccess'))
            {
                if(ACL::Check($this->ObjectType, 'allaccessdept'))
                    $query->Where->Add("orders.department_id in (select department_id from user_department where user_id = ".$_SESSION['user_id'].")");
                else
                    $query->Where->Add("orders.expert_id = ".$_SESSION['user_id']);
            }

            if(!empty($_SESSION['search_id']))
                $query->Where->Add("orders.id = ".$_SESSION['search_id']);

            if(!empty($_SESSION['search_from']))
                $query->Where->Add("orders.created >= '".DT::getUnixTime($_SESSION['search_from'])."'");

            if(!empty($_SESSION['search_to']))
                $query->Where->Add("orders.created <= '".(DT::getUnixTime($_SESSION['search_to']) + 24*60*60)."'");

            if(!empty($_SESSION['search_department']))
                $query->Where->Add("orders.department_id = ".$_SESSION['search_department']);

            if(!empty($_SESSION['search_status']))
            {
                if($_SESSION['search_status'] == 10)
                {
                    $query->Where->Add("orders.status = 10 and municipality_id = 0");
                }
                if($_SESSION['search_status'] == 11)
                {
                    $query->Where->Add("orders.status = 10 and municipality_id != 0");
                }
                else if($_SESSION['search_status'] == 21)
                {
                    $query->Where->Add("orders.status != 10 and municipality_id != 0");
                }
                else if($_SESSION['search_status'] == 22)
                {
                    $query->Where->Add("orders.status = 10 and municipality_id != 0 and expert_id != 0");
                }
                else if($_SESSION['search_status'] == 6)
                {
                    $query->Where->Add("orders.status != 20");
                    $query->Where->Add("days <= 0 or days is NULL");
                    $query->Having = 'diff >= 15';
                }
                else
                    $query->Where->Add("orders.status = ".$_SESSION['search_status']);
            }
            else
                $query->Where->Add("orders.status in (5, 10, 13)");

            if(!empty($_SESSION['search_source']))
                $query->Where->Add("orders.source = ".$_SESSION['search_source']);

            if(!empty($_SESSION['search_region']))
                $query->Where->Add("orders.region_id = ".$_SESSION['search_region']);

            if(!empty($_SESSION['search_municipality']))
                $query->Where->Add("orders.municipality_id = ".$_SESSION['search_municipality']);

            if(!empty($_SESSION['search_topic']))
                $query->Where->Add("orders.topic_id = ".$_SESSION['search_topic']);

            if(!empty($_SESSION['search_alert']))
                $query->Where->Add("orders.alert = ".$_SESSION['search_alert']);

            if(!empty($_SESSION['search_type']))
                $query->Where->Add("orders.type = ".$_SESSION['search_type']);

            if(!empty($_SESSION['search_result']))
                $query->Where->Add("orders.result = ".$_SESSION['search_result']);

            if(!empty($_SESSION['search_project']))
                $query->Where->Add("orders.project_id = ".$_SESSION['search_project']);

            if(!empty($_SESSION['search_author']))
                $query->Where->Add("orders.author_id = ".$_SESSION['search_author']);

            if(!empty($_SESSION['search_gubcomment']))
            {
                if($_SESSION['search_gubcomment'] == 1)
                    $query->Where->Add("orders.id in (select owner_id from comments where high = 1)");
                else
                    $query->Where->Add("orders.id not in (select owner_id from comments where high = 1)");
            }

            if(!empty($_SESSION['search_rating']))
            {
                $query->Where->Add("orders.rating = " . (int)$_SESSION['search_rating']);
            }

            if(!empty($_SESSION['search_text']))
            {
                $where = new Where();
                $where->Policy = 'or';
                $where->Add("orders.name like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.question like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.answer like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.comment like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.lastname like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.phone like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.email like '%".$_SESSION['search_text']."%'");
                $query->Where->Add($where);
            }

            $query->Order = 'orders.id desc';

            include './Classes/PHPExcel.php';
            include './Classes/PHPExcel/Writer/Excel2007.php';

            spl_autoload_register(__autoload);

            $excel = new PHPExcel();
            $excel->setActiveSheetIndex(0);

            $row = 1;

            for($i = 0; $i < count($captions); $i++)
            {
                if(empty($captions[$i]))
                    continue;

                $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $captions[$i]);
                $styleArray = array('font' => array('bold' => true));
                $excel->getActiveSheet()->getStyleByColumnAndRow($counter, $row)->applyFromArray($styleArray);
                $excel->getActiveSheet()->getStyleByColumnAndRow($counter, $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFCCCCCC');
                $counter++;
            }

            $source = new SourceList();
            $type = new TypeList();
            $alert = new AlertTypeList();
            $status = new StatusList();
            $result = new ResultTypeList();
            $sex = new SexTypeList();

            $ds = new DataSource($query);
            while($data = $ds->GetString())
            {
                $counter = 0;
                $row++;

                for($i = 0; $i < count($data); $i++)
                {
                    if(!empty($captions[$i]))
                    {
//                        if($i == 9)
//                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $source->Get($data[$i]));
//                        else if($i == 10)
//                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $type->Get($data[$i]));
//                        else if($i == 14)
//                            //$excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, ($data[$i]?'Да':''));
//                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $sex->Get($data[$i]));
//                        else if($i == 23)
//                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $alert->Get($data[$i]));
//                        else if($i == 24)
//                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, DT::getDate($data[$i]));
//                        else if($i == 25)
//                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $status->Get($data[$i]));
//                        else if($i == 26)
//                            $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $result->Get($data[$i]));
//                        else

                        switch($counter)
                        {
                            case 0:
                                $excel->getActiveSheet()->getColumnDimensionByColumn($counter)->setWidth(7);
                                break;
                            case 2:
                                $excel->getActiveSheet()->getColumnDimensionByColumn($counter)->setWidth(40);
                                break;
                            case 5:
                                $excel->getActiveSheet()->getColumnDimensionByColumn($counter)->setWidth(80);
                                break;
                            default:
                                $excel->getActiveSheet()->getColumnDimensionByColumn($counter)->setWidth(15);
                                break;
                        }

                        $excel->getActiveSheet()->setCellValueByColumnAndRow($counter, $row, $data[$i]);
                        $excel->getActiveSheet()->getRowDimension($row)->setRowHeight(155);
                        $excel->getActiveSheet()->getStyleByColumnAndRow($counter, $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)->setWrapText(true);

                        $counter++;
                    }
                }
            }

            //for($i = 0; $i < $counter; $i++)


            $xls = new PHPExcel_Writer_Excel2007($excel);

            header('Content-Disposition: attachment; filename="Вопросы.xlsx"');
            header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            $xls->save('php://output');
        }

        private function redirectByMode($mode, $act = null, $nohead = null)
        {
            if($mode == 1)
            {
                unset($_SESSION['search_status']);
                $_SESSION['__active_menu_item'] = 'orders&mode=1';
                LocationManager::redirect("index.php?module=".$this->ModuleName . "&act=" . $act . "&tbl_page=1&nohead=" . $nohead);
            }
            else if($mode == 2)
            {
                $_SESSION['search_status'] = 10;
                $_SESSION['__active_menu_item'] = 'orders&mode=2';
                LocationManager::redirect("index.php?module=".$this->ModuleName . "&act=" . $act . "&tbl_page=1&nohead=" . $nohead);
            }
            else if($mode == 22)
            {
                $_SESSION['search_status'] = 22;
                $_SESSION['__active_menu_item'] = 'orders&mode=22';
                LocationManager::redirect("index.php?module=".$this->ModuleName . "&act=" . $act . "&tbl_page=1&nohead=" . $nohead);
            }
            else if($mode == 3)
            {
                $_SESSION['search_status'] = 15;
                $_SESSION['__active_menu_item'] = 'orders&mode=3';
                LocationManager::redirect("index.php?module=".$this->ModuleName . "&act=" . $act . "&tbl_page=1&nohead=" . $nohead);
            }
            else if($mode == 4)
            {
                $_SESSION['search_status'] = 20;
                $_SESSION['__active_menu_item'] = 'orders&mode=4';
                LocationManager::redirect("index.php?module=".$this->ModuleName . "&act=" . $act . "&tbl_page=1&nohead=" . $nohead);
            }
            else if($mode == 41)
            {
                $_SESSION['search_status'] = 21;
                $_SESSION['__active_menu_item'] = 'orders&mode=41';
                LocationManager::redirect("index.php?module=".$this->ModuleName . "&act=" . $act . "&tbl_page=1&nohead=" . $nohead);
            }
            else if($mode == 5)
            {
                $_SESSION['search_status'] = 11;
                $_SESSION['__active_menu_item'] = 'orders&mode=5';
                LocationManager::redirect("index.php?module=".$this->ModuleName . "&act=" . $act . "&tbl_page=1&nohead=" . $nohead);
            }
            else if($mode == 6)
            {
                $_SESSION['search_status'] = 6;
                $_SESSION['__active_menu_item'] = 'orders&mode=6';
                LocationManager::redirect("index.php?module=".$this->ModuleName . "&act=" . $act . "&tbl_page=1&nohead=" . $nohead);
            }
        }

		protected function parseHeaders()
		{
			$this->Id = $_REQUEST["id"];
            $this->num = $_REQUEST["num"];
            $this->source = $_REQUEST["source"];
            $this->project_id = $_REQUEST["project_id"];
			$this->alert = $_REQUEST["alert"];
			$this->lastname = $_REQUEST["lastname"];
			$this->firstname = $_REQUEST["firstname"];
			$this->middlename = $_REQUEST["middlename"];
			$this->sex = $_REQUEST["sex"];
			$this->age = $_REQUEST["age"];
			$this->job_id = $_REQUEST["job_id"];
			$this->city = $_REQUEST["city"];
            if($this->city == '(...)')
                $this->city = '';
			$this->region_id = $_REQUEST["region_id"];
			$this->address = $_REQUEST["address"];
            $this->organization = $_REQUEST["organization"];
			$this->phone = $_REQUEST["phone"];
            $this->phone2 = $_REQUEST["phone2"];
			$this->email = $_REQUEST["email"];
            $this->topic_id = $_REQUEST["topic_id"];
			$this->name = $_REQUEST["name"];
			$this->question = $_REQUEST["question"];
            $this->answer = $_REQUEST["answer"];
            $this->article_id = $_REQUEST["article_id"];
			$this->comment = $_REQUEST["comment"];
			$this->user_id = $_REQUEST["user_id"];
			$this->expert_id = $_REQUEST["expert_id"];
			$this->department_id = $_REQUEST["department_id"];
            $this->municipality_id = $_REQUEST["municipality_id"];
			$this->type = $_REQUEST["type"];
			$this->garbage = $_REQUEST["garbage"];
			$this->call_id = $_REQUEST["call_id"];
			$this->status = $_REQUEST["status"];
			$this->result = $_REQUEST["result"];
            $this->rating = $_REQUEST["rating"];
            $this->rating_comment = $_REQUEST["rating_comment"];
            $this->recall = Form::GetFieldValue("recall", 'datetime');

            if(!empty($_REQUEST['btnClear']))
            {
                unset($_SESSION['search_id']);
                unset($_SESSION['search_from']);
                unset($_SESSION['search_to']);
                unset($_SESSION['search_department']);
                unset($_SESSION['search_status']);
                unset($_SESSION['search_gubcomment']);

                unset($_SESSION['search_type']);
                unset($_SESSION['search_alert']);
                unset($_SESSION['search_region']);
                unset($_SESSION['search_municipality']);
                unset($_SESSION['search_topic']);
                unset($_SESSION['search_result']);
                unset($_SESSION['search_source']);
                unset($_SESSION['search_project']);
                unset($_SESSION['search_author']);
                unset($_SESSION['search_text']);
                unset($_SESSION['search_rating']);

                $this->redirectByMode($_SESSION['mode']);
            }

            if ($_REQUEST['_mode'])
                $_SESSION['mode'] = $_REQUEST['_mode'];
            else if($_REQUEST['mode'])
            {
                $_SESSION['mode'] = $_REQUEST['mode'];
                $this->redirectByMode($_REQUEST['mode']);
            }

            foreach($_REQUEST as $key => $value)
            {
                if(preg_match('/^search_/', $key))
                {
                    $_SESSION['search_id'] = $_REQUEST['search_id'];
                    $_SESSION['search_from'] = $_REQUEST['search_from'];
                    $_SESSION['search_to'] = $_REQUEST['search_to'];
                    $_SESSION['search_department'] = $_REQUEST['search_department'];
                    $_SESSION['search_status'] = $_REQUEST['search_status'];
                    $_SESSION['search_gubcomment'] = $_REQUEST['search_gubcomment'];

                    $_SESSION['search_type'] = $_REQUEST['search_type'];
                    $_SESSION['search_alert'] = $_REQUEST['search_alert'];
                    $_SESSION['search_region'] = $_REQUEST['search_region'];
                    $_SESSION['search_municipality'] = $_REQUEST['search_municipality'];
                    $_SESSION['search_topic'] = $_REQUEST['search_topic'];
                    $_SESSION['search_result'] = $_REQUEST['search_result'];
                    $_SESSION['search_source'] = $_REQUEST['search_source'];
                    $_SESSION['search_project'] = $_REQUEST['search_project'];
                    $_SESSION['search_author'] = $_REQUEST['search_author'];
                    $_SESSION['search_text'] = $_REQUEST['search_text'];
                    $_SESSION['search_rating'] = $_REQUEST['search_rating'];

                    if(!empty($_REQUEST['btnExport']))
                        $this->redirectByMode($_SESSION['mode'], 'export', 1);

                    if(!empty($_REQUEST['btnExportMini']))
                        $this->redirectByMode($_SESSION['mode'], 'exportmini', 1);
                    
                    $this->redirectByMode($_SESSION['mode']);
                }
            }
		}
	}

	class OrdersListTable extends Table
	{	
		public function __construct($objectType)
		{
			$this->ObjectType = $objectType;
			
			$query = new Query("select");
			$query->AddTable("orders
			left outer join users authors on orders.author_id = authors.id
			left outer join departments on orders.department_id = departments.id
			left outer join regions on orders.region_id = regions.id
			left outer join users operators on orders.operator_id = operators.id
			left outer join sources on orders.source = sources.id
			");
			$query->Fields = array("orders.id", "orders.id", "orders.created", "regions.name", "orders.name", "orders.lastname", "departments.shortname",
                                   "sources.name source", "orders.type", "orders.answer", "orders.comment", "orders.alert",
                                   "orders.created", "orders.status", "result", "(select COUNT(*) from comments where module = 'orders' and owner_id = orders.id and high = 1) gubcomment",
                                   "(select COUNT(*) from comments where module = 'orders' and owner_id = orders.id) comment_count",
                                   "days", "operator_id", "operators.name", "recall", "rating", "sources.icon",
                "DATEDIFF(CURDATE(), DATE(FROM_UNIXTIME(orders.created))) diff"
            );
	
			$captions = array("", "ID", "Принято", "Район", "Тема вопроса", 'Фамилия', "Ответственный",
                              '' /* "Ист" */, '' /* "Тип" */, "Отв", "Комм", '' /* "Угр" */,
                              "Срок", "Статус", "Рез", '', '', '', '', '', '', 'Оц', '');

            $pds = new DataSource("select id from role_pd where role_id = " . $_SESSION['user_priv'] . " and pd = 'lastname'");
            $pd = $pds->GetString();
            if(!$pd['id'])
                $captions[5] = '';

            $caption = "Текущие вопросы";

            if(!ACL::Check($this->ObjectType, 'allaccess'))
            {
                if(ACL::Check($this->ObjectType, 'allaccessdept'))
                    $query->Where->Add("orders.department_id in (select department_id from user_department where user_id = ".$_SESSION['user_id'].")
                    or orders.municipality_id in (select department_id from user_department where user_id = ".$_SESSION['user_id'].")
                    ");
                else
                    $query->Where->Add("orders.expert_id = ".$_SESSION['user_id']);
            }

            if(!ACL::Check(OBJECT_PROJECTS, 'allaccess'))
            {
                $query->Where->Add("orders.project_id in (select project_id from project_role where role_id = ".(int)$_SESSION['user_priv'].")");
            }

            //if(!ACL::Check($this->ObjectType, 'ratingview') || ($_SESSION['search_status'] != 15 && $_SESSION['search_status'] != 20))
            if($_SESSION['search_status'] != 15 && $_SESSION['search_status'] != 20)
            {
                $captions[21] = '';
            }

            if(!empty($_SESSION['search_id']))
                $query->Where->Add("orders.id = ".$_SESSION['search_id']);

            if(!empty($_SESSION['search_from']))
                $query->Where->Add("orders.created >= '".DT::getUnixTime($_SESSION['search_from'])."'");

            if(!empty($_SESSION['search_to']))
                $query->Where->Add("orders.created <= '".(DT::getUnixTime($_SESSION['search_to']) + 24*60*60)."'");

            if(!empty($_SESSION['search_department']))
                $query->Where->Add("orders.department_id = ".$_SESSION['search_department']);

            if(!empty($_SESSION['search_status']))
            {
                if($_SESSION['search_status'] == 10)
                {
                    $query->Where->Add("orders.status = 10 and municipality_id = 0");
                }
                if($_SESSION['search_status'] == 11)
                {
                    $query->Where->Add("orders.status = 10 and municipality_id != 0");
                }
                else if($_SESSION['search_status'] == 21)
                {
                    $query->Where->Add("orders.status != 10 and municipality_id != 0");
                }
                else if($_SESSION['search_status'] == 22)
                {
                    $query->Where->Add("orders.status = 10 and municipality_id != 0 and expert_id != 0");
                }
                else if($_SESSION['search_status'] == 6)
                {
                    $query->Where->Add("orders.status != 20");
                    $query->Where->Add("days <= 0 or days is NULL");
                    $query->Having = 'diff >= 15';
                }
                else
                    $query->Where->Add("orders.status = ".$_SESSION['search_status']);

                if($_SESSION['search_status'] == 15)
                    $caption = 'На доведении в КЦ';
                else if($_SESSION['search_status'] == 20)
                    $caption = 'Архив';
                else if($_SESSION['search_status'] == 21)
                    $caption = 'Архив';
                else if($_SESSION['search_status'] == 10)
                    $caption = 'На делегировании';
                else if($_SESSION['search_status'] == 11)
                    $caption = 'На делегировании в муниципалитете';
                else if($_SESSION['search_status'] == 6)
                    $caption = 'Срок истек';
            }
            else
            {
                if($_SESSION['user_priv'] == 26)
                {
                    $query->Where->Add("orders.status in (5, 7, 13) or (orders.status = 10 and municipality_id != 0)");
                }
                else
                    $query->Where->Add("orders.status in (5, 7, 13)");
            }


            if(!empty($_SESSION['search_source']))
                $query->Where->Add("orders.source = ".$_SESSION['search_source']);

            if(!empty($_SESSION['search_region']))
                $query->Where->Add("orders.region_id = ".$_SESSION['search_region']);

            if(!empty($_SESSION['search_municipality']))
                $query->Where->Add("orders.municipality_id = ".$_SESSION['search_municipality']);

            if(!empty($_SESSION['search_topic']))
                $query->Where->Add("orders.topic_id = ".$_SESSION['search_topic']);

            if(!empty($_SESSION['search_alert']))
                $query->Where->Add("orders.alert = ".$_SESSION['search_alert']);

            if(!empty($_SESSION['search_type']))
                $query->Where->Add("orders.type = ".$_SESSION['search_type']);

            if(!empty($_SESSION['search_result']))
                $query->Where->Add("orders.result = ".$_SESSION['search_result']);

            if(!empty($_SESSION['search_project']))
                $query->Where->Add("orders.project_id = ".$_SESSION['search_project']);

            if(!empty($_SESSION['search_author']))
                $query->Where->Add("orders.author_id = ".$_SESSION['search_author']);

            if(!empty($_SESSION['search_gubcomment']))
            {
                if($_SESSION['search_gubcomment'] == 1)
                    $query->Where->Add("orders.id in (select owner_id from comments where high = 1)");
                else
                    $query->Where->Add("orders.id not in (select owner_id from comments where high = 1)");
            }

            if(!empty($_SESSION['search_rating']))
            {
                $query->Where->Add("orders.rating = " . (int)$_SESSION['search_rating']);
            }

            if(!empty($_SESSION['search_text']))
            {
                $where = new Where();
                $where->Policy = 'or';
                $where->Add("orders.name like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.question like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.answer like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.comment like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.lastname like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.phone like '%".$_SESSION['search_text']."%'");
                $where->Add("orders.email like '%".$_SESSION['search_text']."%'");
                $query->Where->Add($where);
            }

            $uds = new DataSource("select * from user_region where user_id = " . $_SESSION['user_id']);
            if($uds->RowCount)
            {
                $regions = array();
                while($udata = $uds->GetString())
                    $regions[] = $udata['region_id'];
                $query->Where->Add("orders.region_id in (" . implode(',', $regions) . ")");
            }

            if(empty($this->SortedField))
            {
                $this->SortedField = 1;
                $this->SortType = 'desc';
            }

            $caption = $caption . " (всего: %rows%)";

            //echo $query->Get(); exit;

			parent::Table($caption,
						  $query,
						  $captions,
						  $query->Fields);

			$this->ShowNewLinks = ACL::Check($this->ObjectType, ACL::CREATE);
			$this->ShowEditLinks = true;
			$this->ShowDeleteLinks = ACL::Check($this->ObjectType, ACL::DELETE);
		}

        function BeginRow($data, $rownum)
		{
            if($data[17])
                $diff = $data[17];
            else
                $diff = DT::getDayDiff(DT::Now, $data[12] + 15*24*60*60);

            if ($diff <= 7)
                $class = 'red';

			$this->_begin_row($data, $rownum, $class);
		}

        function ShowCell($data, $cellnum)
		{
			if($this->_is_visible_cell($cellnum))
			{
                if($cellnum == 2)
                {
                    $this->_render_cell($data, '<nobr>' . DT::getHumanDateTime($data[$cellnum]) . '</nobr>');
                }
                else if($cellnum == 7)
                {
                    $cell = '<div style="width:100%; text-align:center;"><img style="border:0;" src="images/' . $data[22] . '.png" alt="'.$data[$cellnum].'" title="'.$data[$cellnum].'">';
                    $this->_render_cell($data, $cell);
                }
                else if($cellnum == 8)
                {
                    $type = new TypeList();
                    $type = $type->Get($data[$cellnum]);
                    switch($data[$cellnum])
                    {
                        case 1: $image = 'processed.png'; break;
                        case 2: $image = 'inforequest.png'; break;
                        case 3: $image = 'critical.png'; break;
                    }
                    $cell = '<div style="width:100%; text-align:center;"><img style="border:0;" src="images/'.$image.'" alt="'.$type.'" title="'.$type.'">';
                    $this->_render_cell($data, $cell);
                }
                else if($cellnum == 9)
                {
                    if($data[$cellnum])
                        $cell = '<div style="width:100%; text-align:center;"><img style="border:0;" src="images/balloon-left.png" alt="Есть ответ" title="Есть ответ">';
                    $this->_render_cell($data, $cell);
                }
                else if($cellnum == 10)
                {
                    if($data[14])
                        $cell = '<div style="width:100%; text-align:center;"><img style="border:0;" src="images/balloons-red.png" alt="Есть комментарий губернатора" title="Есть комментарий губернатора">';
                    else if(($data[$cellnum]) || ($data[15]))
                        $cell = '<div style="width:100%; text-align:center;"><img style="border:0;" src="images/balloons.png" alt="Есть комментарий" title="Есть комментарий">';
                    $this->_render_cell($data, $cell);
                }
                else if($cellnum == 11)
                {
                    if($data[$cellnum] == 2)
                        $cell = '<div style="width:100%; text-align:center;"><img style="border:0;" src="images/exclamation-white.png" alt="Средняя угроза" title="Средняя угроза">';
                    else if($data[$cellnum] == 3)
                        $cell = '<div style="width:100%; text-align:center;"><img style="border:0;" src="images/exclamation-red.png" alt="Высокая угроза" title="Высокая угроза">';
                    $this->_render_cell($data, $cell);
                }
                else if($cellnum == 12)
                {
/*

 			$query->Fields = array("orders.id", "orders.id", "orders.created", "regions.name", "orders.name", "orders.lastname", "departments.shortname",
                                   "sources.name source", "orders.type", "orders.answer", "orders.comment", "orders.alert",
                                 12  "orders.created",
"orders.status", "result", "(select COUNT(*) from comments where module = 'orders' and owner_id = orders.id and high = 1) gubcomment",
                                   "(select COUNT(*) from comments where module = 'orders' and owner_id = orders.id) comment_count",
                                 17  "days",
 "operator_id", "operators.name", "recall", "rating", "sources.icon"
            );
 */

                    if($data[17])
                        $diff = $data[17];
                    else
                        $diff = DT::getDayDiff(DT::Now, $data[$cellnum] + 15*24*60*60);
                    if($diff >= 7)
                        $cell = '<div style="color:green; font-size: 16px; font-weight:bold; width:100%; text-align:center;">'.$diff.'</div>';
                    else if ($diff >= 3)
                        $cell = '<div style="color:blue; font-size: 16px; font-weight:bold; width:100%; text-align:center;">'.$diff.'</div>';
                    else
                        $cell = '<div style="color:red; font-size: 16px; font-weight:bold; width:100%; text-align:center;">'.$diff.'</div>';
                    $this->_render_cell($data, $cell);
                }
                else if($cellnum == 13)
                {
                    switch($data[$cellnum])
                    {
                        case 5: $status_img = 'arrow.png'; break;
                        case 7: $status_img = 'arrow-retweet.png'; break;
                        case 10: $status_img = 'arrow-branch.png'; break;
                        case 13: $status_img = 'blue-document-share.png'; break;
                        case 15:
                            if((!$data[18]) || ($data[18] == $_SESSION['user_id']))
                            {
                                $data[20] = DT::getUnixTime(DT::getDateTime($data[20]));
                                if((DT::getDateTime($data[20])) && ($data[20] > time()))
                                    $status_img = 'arrow-return-180-left.png';
                                else
                                    $status_img = 'arrow-return-180-left-red.png';
                            }
                            else
                                $status_img = 'arrow-return-180-left-inactive.png';
                        break;
                        case 20: $status_img = 'tick.png'; break;
                    }
                    if($status_img)
                    {
                        if(($data[18]) && ($data[18] != $_SESSION['user_id']) && ($data[$cellnum] == 15))
                            $status = 'На доведении у оператора '.$data[19];
                        else if(DT::getDateTime($data[20]))
                            $status = 'Перезвонить '.DT::getDateTime($data[20]);
                        else
                        {
                            $status = new StatusList();
                            $status = $status->Get($data[$cellnum]);
                        }
                        $cell = '<div style="width:100%; text-align:center;"><img style="border:0;" src="images/'.$status_img.'" alt="'.$status.'" title="'.$status.'">';
                    }

                    $this->_render_cell($data, $cell);
                }
                else if($cellnum == 14)
                {
                    $mod = new ResultTypeList();
                    if($data[$cellnum] == 1)
                        $cell = '<div style="width:100%; text-align:center;"><img style="border:0;" src="images/smiley.png" alt="'.$mod->Get($data[$cellnum]).'" title="'.$mod->Get($data[$cellnum]).'">';
                    else if($data[$cellnum] == 2)
                        $cell = '<div style="width:100%; text-align:center;"><img style="border:0;" src="images/smiley-sad.png" alt="'.$mod->Get($data[$cellnum]).'" title="'.$mod->Get($data[$cellnum]).'">';
                    $this->_render_cell($data, $cell);
                }
                else if($cellnum == 21)
                {
                    switch($data[$cellnum])
                    {
                        case 5:
                            $cell = '<span style="color: green; font-weight: bold;">5</span>';
                            break;
                        case 4:
                            $cell = '<span style="color: #808000; font-weight: bold;">4</span>';
                            break;
                        case 3:
                            $cell = '<span style="color: #008080; font-weight: bold;">3</span>';
                            break;
                        case 2:
                            $cell = '<span style="color: orangered; font-weight: bold;">2</span>';
                            break;
                        case 1:
                            $cell = '<span style="color: red; font-weight: bold;">1</span>';
                            break;
                    }
                    $this->_render_cell($data, $cell);
                }
				else
					parent::ShowCell($data, $cellnum);
			}
		}
	}	


	class OrderHistoryTable extends Table
	{
		public function __construct($order_id)
		{
			$this->ObjectType = OBJECT_LOGS;

			$query = new Query("select");
			$query->AddTable("logs
			left outer join users
			on logs.user_id = users.id
			");
			$query->Fields = array(
				"logs.id", "dt", "users.name", "object", "module", "record", "action", "prev", "new");
            $query->Where->Add("record = ".$order_id);

            $this->Sortable = false;
            $this->Paging = false;
            $this->TableId = 'history'.$order_id;

			parent::Table("История заявки",
						  $query,
						  array('', "Дата/время", "Пользователь",
						  "", //"Тип объекта",
						  "", // "Модуль",
                          "", // "Номер записи",
                          "Действие", "Было", "Стало"),
						  $query->Fields);

			$this->ShowNewLinks = false;
			$this->ShowEditLinks = false;
			$this->ShowDeleteLinks = false;

			$this->DefaultURL = '#';
		}

		function ShowCell($data, $cellnum)
		{
			if($this->_is_visible_cell($cellnum))
			{
				if($cellnum == 1)
				{
                    $this->_render_cell($data, DT::getDateTime($data[$cellnum]));
				}
				else
					parent::ShowCell($data, $cellnum);
			}
		}
	}
