<?php
/*
ini_set('display_errors', 1);
error_reporting(E_ALL^E_NOTICE);
*/
	require_once('framework_loader.php');
	require_once('classes.php');

	$page = new PageProducer(CRM_CAPTION, false);

    ini_set("session.gc_maxlifetime", "18000");   
	
	$page->Auth = new Auth();
	$page->Auth->CheckCredentials();
	
	if($page->Auth)
	{
		if( (!($page->Auth->IsEnabled())) && empty($_REQUEST['noauth']) )
		{
			$page->Begin();
			$loginbox = new LoginBox('index.php', true);
			$loginbox->Show();
	
			$page->End();
			exit;	
		}
	}

	$module = $_REQUEST['module'];

	if(empty($module))
	{
		LocationManager::redirect(START_PAGE);
	}
	
	$action = $_REQUEST['act'];
	if(empty($action))
	{
		$action = 'index';
	}
	
	$module = str_replace(array('.', '/', "\\"), '', $module);

	if(empty($custom))
	{
		if(file_exists('./modules/'.$module.'.php'))
		{
			include('./modules/'.$module.'.php');
			
			$classname = ucfirst($module); 
			
			if(class_exists($classname))
			{
				$object = new $classname($module, $page);
				if(($action != 'ajax') && (empty($_REQUEST['nohead'])))
					$page->Begin();     
				$methodname = $action.'Action';
				if(method_exists($classname, $methodname))
				{
					call_user_func(array($object, $methodname));
				}
			}
		}
	}

	if(($action != 'ajax') && (empty($_REQUEST['nohead'])))	
		$page->End();
		