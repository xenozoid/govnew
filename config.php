<?php

	define('DEBUG_MODE', '0');
    define('LOG_FILE', '/var/www/vhosts/gov29/debug_log.txt');
    define('STAY_LOGGED_IN', '1');

    define('FRAMEWORK_PATH', './framework/');
    define('PLUGINS_PATH', 'plugins/');
    define('START_PAGE', 'index.php?module=dashboard');

	define('SQL_HOST', 'localhost');
	define('SQL_USER', 'gov29');
    define('SQL_PASSWORD', 'pass29');

	define('SQL_DB', 'gov29');

	define('TRACK_ACTIVITY', '1');
    define('SYNC_WITH_ASTERISK', '1');
	
	define('LOCALE', 'ru');
	define('DATE_FORMAT', '%d.%m.%Y');
	define('DATE_FORMAT_INT', '%m/%d/%Y');
	define('TIME_FORMAT', '%H:%M');
	define('DATETIME_FORMAT', '%d.%m.%Y %H:%M');

	define('CRM_CAPTION', 'Правительство Архангельской области');
	define('MAIL_FROM', 'Info29 <no-reply@firecall.net>');
	
