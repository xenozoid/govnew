<?php

if($_REQUEST['pass'] != 'foobar12345callthefire')
    exit;

define('AUTHOR_ID', 0);

require_once "framework_loader.php";
require_once "classes.php";

switch($_REQUEST['act'])
{
    case 'add':
        $query = new Query("insert");
        $query->AddTable('comments');
        $query->AddField("created", 'UNIX_TIMESTAMP()');
        $query->AddField("author_id", AUTHOR_ID);
        $query->AddField("owner_id", (int)$_REQUEST['id']);
        $query->AddField("module", 'orders');
        $query->AddField("comment", $_REQUEST['comment']);
        $query->AddField("high", 1);
        $query->Execute();
        $id = $query->last_insert_id();

        $sql = new SqlObject();
        $order = $sql->getline("select department_id from orders where id = ".(int)$_REQUEST['id']);

        $emails = array();
        $ds = new DataSource("select id, email, priv from users where priv in (select role_id from acl where object_type = 'OBJECT_ORDERS' and action_type = 'allaccess')");
        while($user = $ds->GetString())
        {
            if(($user['email']) && (ACL::Check(OBJECT_ORDERS, 'alert_email', $user['priv'])))
                $emails[] = $user['email'];
        }

        if($order['department_id'])
        {
            $ds = new DataSource("select email from users where id in (select user_id from user_department where department_id = ".$order['department_id'].")");
            while($user = $ds->GetString())
            {
                if($user['email'])
                    $emails[] = $user['email'];
            }
        }

        $emails = array_unique($emails);
        if($emails)
        {
            $title = 'Комментарий губернатора к вопросу №'.(int)$_REQUEST['id'];
            $content = $_REQUEST['comment'];
            foreach($emails as $email)
                Email::send($email, $title, $content);
        }

        echo $id;
    break;

    case 'upd':
        $query = new Query("update");
        $query->AddTable('comments');
        $query->AddField("comment", $_REQUEST['comment']);
        $query->Where->Add("id", (int)$_REQUEST['id']);
        $query->Where->Add("module", 'orders');
        $query->Where->Add("high", 1);
        $query->Execute();
        echo 'ok';
    break;

    case 'del':
        $sql = new SqlObject();
        $sql->sql("delete from comments where id = ".(int)$_REQUEST['id']." and high = 1");
        echo 'ok';
    break;
}

