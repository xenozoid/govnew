<?php
	require("framework_loader.php");
	require("classes.php");

	$auth = new Auth();
	$auth->Logout();
	$page = new PageProducer();
	$page->Redirect("index.php");
