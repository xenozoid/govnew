<?php

	class Where
	{
		public $Policy = 'and';
		protected $Conditions;
		//public $Conditions;
		
		public function __construct($params = null, $policy = null)
		{
			if(is_string($params))
				$this->Conditions[] = $params;
			else if(is_array($params))
				$this->Conditions = $params;
				
			if(!empty($policy))
			{
				$this->Policy = $policy;
			}
		}
		
		public function Add($condition)
		{
			if(is_object($condition))
				if($condition->IsEmpty())
					return;
					
			$this->Conditions[] = $condition;
		}
		
		public function Get()
		{
			$this->Policy = strtolower($this->Policy);

			$result = '';
			
				foreach($this->Conditions as $condition)
				{
					$result .= ' ( ';
					if(is_object($condition))
					{
						$where_result = $condition->Get();
						$result .= $where_result;
					}
					else
					{
						$result .= $condition;
					}
					$result .= ' ) ';
					$result .= $this->Policy;
				}
				$result = substr($result, 0, strlen($result) - strlen($this->Policy) - 1);
			
			return $result;
		}
		
		public function IsEmpty()
		{
			return !(count($this->Conditions) > 0);
		}
	}
	