<?php

	class DataSource extends SqlObject
	{
		public $QueryString;
		
		private $_result;
		
		public $ColCount = 0;
		public $RowCount = 0;
        public $resultType = MYSQL_BOTH;
		
		function DataSource($data)
		{
				$this->Query($data);
		}
		
		function Query($string)
		{
				$this->_result = $this->sql($string);
                if($this->_result)
                {
				    $this->ColCount = @mysql_num_fields($this->_result);
				    $this->RowCount = @mysql_num_rows($this->_result);
                }
                else
                {
                    $this->ColCount = 0;
                    $this->RowCount = 0;
                }
		}

		function GetString()
		{
            if(!$this->_result)
                return false;

            $result_array = mysql_fetch_array($this->_result, $this->resultType);

            if($result_array !== FALSE)
            {
                return $result_array;
            }
            else
            {
                return FALSE;
            }
		}
		
		function Serialize()
		{
			$result = AJAX_COL_SEP.AJAX_ROW_SEP;
			
			while(($str = $this->GetString()) !== false)
			{
				$string = '';
				for($i = 0; $i < count($str); $i++)
				{
					$string .= $str[$i].AJAX_COL_SEP;
				}
				$result .= $string.AJAX_ROW_SEP;
			}
			
			return $result;
		}

        function SerializeJSON()
        {
            $results = array();

			while(($data = $this->GetString()) !== false)
				$results[] = $data;

            return json_encode($results);
        }
	}