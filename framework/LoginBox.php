<?php

	class LoginBox
	{
		private $action;
		private $showRecoveryLink;
		private $recoveryUrl;
		private $showRegisterLink;
		private $registerUrl;

        public $helloMessage = CRM_CAPTION;
        public $loginBoxCaption = 'Вход в систему';
        public $userNameCaption = 'Пользователь';
        public $passwordCaption = 'Пароль';
        public $loginButtonCaption = 'Войти';
        public $recoveryLinkCaption = 'забыли пароль?';
        public $registerLinkCaption = 'регистрация';
		
		function LoginBox($action = '', $recover = false, $recoveryUrl = 'recover.php', $register = false, $registerUrl = 'register.php')
		{
			$this->action = $action;
			$this->showRecoveryLink = $recover;
			$this->recoveryUrl = $recoveryUrl;
			$this->showRegisterLink = $register;
			$this->registerUrl = $registerUrl;
		}

		function show()
		{
			echo '<table class="loginTable"><tr><td>';
			if(defined('CRM_CAPTION'))
			{
				echo '<div class="crmCaption">'.$this->helloMessage.'</div>';
				echo '</td></tr><tr><td>';
			}
			
			$form = new Form($this->loginBoxCaption, array(
				array('caption' => $this->userNameCaption, 'type' => 'text', 'name' => 'login', 'size' => 25),
				array('caption' => $this->passwordCaption, 'type' => 'password', 'name' => 'password', 'size' => 25),
                array('type' => 'hidden', 'name' => 'loggedin', 'value' => 1),
				array('type' => 'submit', 'name' => 'submit', 'value' => $this->loginButtonCaption)),
				$this->action);
				
			if($this->showRecoveryLink)
			{
				$form->InsertAfter('password', array(
					'type' => 'select_url', 'name' => 'forgot', 'url_caption' => $this->recoveryLinkCaption, 'url' => $this->recoveryUrl
				));
			}

			if($this->showRegisterLink)
			{
				if($form->HasElement('forgot'))
				{
					$form->SetElementAttribute('forgot', 'nobr', 1);
					$after = 'forgot';
				}
				else
					$after = 'password';
				
				$form->InsertAfter($after, array(
					'type' => 'select_url', 'url_caption' => $this->registerLinkCaption, 'url' => $this->registerUrl, 'nobr' => 1, 'name' => 'register'
				));
				
				if($form->HasElement('forgot'))
					$form->SetElementAttribute('register', 'public_value', '|&nbsp;&nbsp;');
			}
						
			$form->Show();
			echo '</td></tr></table>';
		}
	}
