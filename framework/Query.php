<?php

	class Query
	{
		protected $query_types = array('select', 'insert', 'update', 'delete');
		protected $QueryString;
		protected $sql;
        protected $unionAllFlag = false;
		
		public $Type;
		public $QueryMod;
		public $Tables = array();
		public $Fields;
		public $Values;
		public $Where;
		public $Policy;
		public $Group;
        public $Having;
		public $Order;
		public $Limit;

		protected $Union = array();
		
		public $CalcRows = true;
		
		public function __construct($params = null)
		{
			if(is_string($params))
			{
				if(!in_array(strtolower($params), $this->query_types))
				{
					$this->QueryString = $params;
				}
				else
					$this->Type = strtolower($params); 
			}
			else if(is_array($params))
			{
				if(!empty($params['type']))
					$this->Type = $params['type'];
					
				if(!empty($params['tables']))
				{
					if(is_array($params['tables']))
						$this->Tables = $params['tables'];
					else
						//$this->Tables[] = $params['tables'];
						$this->AddTable($params['tables']);
				}

				if(!empty($params['fields']))
				{
					if(is_array($params['fields']))
						$this->Fields = $params['fields'];
					else
						$this->Fields[] = $params['fields'];
				}

				if(!empty($params['values']))
				{
					if(is_array($params['values']))
						$this->Values = $params['values'];
					else
						$this->Values[] = $params['values'];
				}

				if(!empty($params['order']))
					$this->Order = $params['order'];

				if(!empty($params['limit']))
					$this->Limit = $params['limit'];
			}
			
//			$this->Tables = array();
			$this->Where = new Where();
		}
		
		public function AddTable($table)
		{
			array_push($this->Tables, $table);
		}
		
		public function Union($query)
		{
			array_push($this->Union, $query);
		}

        public function UnionAll($query)
		{
			array_push($this->Union, $query);
            $this->unionAllFlag = true;
		}
		
		public function AddField($field, $value = null)
		{
			if(!is_array($this->Fields))
			{
				if(!empty($this->Fields))
				{
					$backup = $this->Fields;
					$this->Fields = array();
					$this->Fields[] = $backup;
				}
				else
				{
					$this->Fields = array();
				}
			}
			
			$this->Fields[] = $field;

			if(!is_array($this->Values))
			{
				if(!empty($this->Values))
				{
					$backup = $this->Values;
					$this->Values = array();
					$this->Values[] = $backup;
				}
				else
				{
					$this->Values = array();
				}
			}			
			
			//if($value !== null)
			$this->Values[] = $value;
		}
		
		public function Get()
		{			
			if(!empty($this->QueryString))
				return $this->QueryString;
			
			$this->Type = strtolower($this->Type);
			
			$result = $this->Type.' ';
			
			if(!empty($this->QueryMod))
				$result .= $this->QueryMod.' ';
						
			if(is_array($this->Tables))
			{
				foreach($this->Tables as $table)
				{
					$table_result .= $table.', ';
				}
				$table_result = substr($table_result, 0, strlen($table_result)-2);
			}
			else
			{
				$table_result = $this->Tables;
			}
			
			switch($this->Type)
			{
				case 'select':
					
					if(is_array($this->Fields))
					{
						foreach($this->Fields as $field)
						{
							$field_result .= $field.', ';
						}
						$field_result = substr($field_result, 0, strlen($field_result)-2);
					}
					else
					{
						$field_result = $this->Fields;
					}
					
					if($this->CalcRows)
						$result .= 'SQL_CALC_FOUND_ROWS ';
					$result .= $field_result;
					if(!empty($table_result))
						$result .= ' from '.$table_result;
					
					//$this->Where = new Where($this->Where);
					
				break;
				
				case 'delete':
					$result .= ' from '.$table_result;
				break;
				
				case 'insert':
					$result .= ' into ';
					$result .= $table_result.' (';
					
					if(is_array($this->Fields))
					{
						foreach($this->Fields as $field)
						{
							$field_result .= $field.', ';
						}
						$field_result = substr($field_result, 0, strlen($field_result)-2);
					}
					else
					{
						$field_result = $this->Fields;
					}
					
					$result .= $field_result.' ) values ( ';
					
					if(is_array($this->Values))
					{
						for($i = 0; $i < count($this->Values); $i++)
						{
							if(
								(strpos($this->Values[$i], 'CURDATE(') === false) &&
                                (strpos($this->Values[$i], 'NOW()') === false) &&
								(strpos($this->Values[$i], 'UNIX_TIMESTAMP()') === false) &&
								(strpos($this->Values[$i], 'INTERVAL ') === false) &&
								(strpos($this->Values[$i], 'inline:') === false)
							)
								$result .= "'".$this->Values[$i]."', ";
							else
							{
								$this->Values[$i] = preg_replace('/^inline:(.*)/', '\1', $this->Values[$i]);
								$result .= $this->Values[$i].", ";
							}						
						}
						
						$result = substr($result, 0, strlen($result)-2);
					}
					
					$result .= ' )';
					
				break;
				
				case 'update':
					$result .= $table_result.' set ';
					
					if(is_array($this->Fields))
					{
						for($i = 0; $i < count($this->Fields); $i++)
						{
							if(
                                (strpos($this->Values[$i], 'CURDATE') === false) &&
								(strpos($this->Values[$i], 'NOW()') === false) &&
								(strpos($this->Values[$i], 'UNIX_TIMESTAMP()') === false) &&
								(strpos($this->Values[$i], 'INTERVAL ') === false) &&
								(strpos($this->Values[$i], 'inline:') === false)
							)
							{
								$result .= $this->Fields[$i]." = '".$this->Values[$i]."', ";
							}
							else
							{
								$this->Values[$i] = preg_replace('/^inline:(.*)/', '\1', $this->Values[$i]);
								$result .= $this->Fields[$i]." = ".$this->Values[$i].", ";
							}
						}
						
						$result = substr($result, 0, strlen($result)-2);
					}
				break;
			}
			
			if(!($this->Where->IsEmpty()))
			{
				if($this->Policy)
					$this->Where->Policy = $this->Policy;
				$where_result = $this->Where->Get();
				$result .= ' where '.$where_result;
			}
					
			if(!empty($this->Group))
				$result .= ' group by '.$this->Group;

            if(!empty($this->Having))
				$result .= ' having '.$this->Having;
				
			if(!empty($this->Order))
				$result .= ' order by '.$this->Order;
				
			if(!empty($this->Limit))
				$result .= ' limit '.$this->Limit;

            $first_union = true;

			foreach($this->Union as $union)
			{
                if(($first_union) && empty($field_result) && empty($table_result))
                    $result = $union->Get();
                else
                {
                    if($this->unionAllFlag)
                        $result .= ' UNION ALL '.($union->Get());
                    else
                        $result .= ' union '.($union->Get());
                }

                $first_union = false;
			}
			
			return $result;
		}
		
		public function Execute()
		{			
			$this->sql = new SqlObject();
			$this->sql->sql($this->Get());
		}
		
		public function last_insert_id()
		{
			return $this->sql->last_insert_id();
		}
	}