﻿<?php

	class ErrorBox
	{
		public $Message;
		
		public function ErrorBox($message = 'Ошибка!')
		{
			$this->Message = $message;
		}
		
		public function Show()
		{
			echo '<p><span style="background-color:white; color:red; font-weight:bold; padding: 5px; border: 1px solid red;">'.$this->Message.'</span></p>';
		}
	}