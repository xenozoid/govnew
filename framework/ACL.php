<?php

	class ACL
	{
		const ACCEPT = 1;
		const REJECT = 0;
		const UNKNOWN = -1;
		
		const CREATE = 'create';
		const READ   = 'read';
		const UPDATE = 'update';
		const DELETE = 'delete';
		
		protected static $permissions = array(); 
		
		static function Check($object, $action, $role = null)
		{
			if($role == null)
				$role = $_SESSION['user_priv'];
				
			if(class_exists('ACLCallback', false))
				if(method_exists('ACLCallback', 'Check'))
				{
					$ret = ACLCallback::Check($object, $action, $role);
					if($ret == ACL::ACCEPT)
						return true;
					else if($ret == ACL::REJECT)
						return false;
				}

			if(empty(ACL::$permissions[$role]))
			{
				$ds = new DataSource("select object_type, action_type, permission from acl where role_id = ".$role);
				while($data = $ds->GetString())
				{
					ACL::$permissions[$role][$data['object_type']][$data['action_type']] = $data['permission'];
				}
			}

			$permission = ACL::$permissions[$role][$object][$action];
			
			if($permission == '1')
				return true;

			return false;
		}
		
		static function Create($object, $action, $role, $permission)
		{
			$sql = new SqlObject();
			$object = $sql->getline("select code from object_types where id = ".$object);
			$object = $object['code'];

			$sql->sql("delete from acl where
								   object_type = '".$object."' and 
								   action_type = '".$action."' and
								   role_id = ".$role);
			
			$sql->sql("insert into acl (object_type, action_type, role_id, permission, author_id, created)
					   values
					   (
					   '".$object."',
					   '".$action."',
					   '".$role."',
					   '".$permission."',
					   ".$_SESSION['user_id'].",
					   UNIX_TIMESTAMP()
					   )");
		}
		
		static function Clear($role)
		{
			$sql = new SqlObject();
			$sql->sql('delete from acl where role_id = '.$role);
		}
	}