<?php
	class Utils
	{
		private static $_bad = array('<', '>', "\n", "'", '   ', '  ', '"');
		private static $_good = array('&lt;', '&gt;', "<br>", '&#39;', '&nbsp;&nbsp;&nbsp;', '&nbsp;&nbsp;', '&quot;');

		//private static $_eng = array('a', 'b', 'v', 'g', 'd', 'e', 'j', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'i', 'e', 'yu', 'ya');
		private static $_rus = array('а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ь',  'ы', 'ъ', 'э', 'ю', 'я', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ь', 'Ы', 'Ъ', 'Э', 'Ю', 'Я');		
		private static $_rtf = array('e0', 'e1', 'e2', 'e3', 'e4', 'e5', 'e6', 'e7', 'e8', 'e9', 'ea', 'eb', 'ec', 'ed', 'ee', 'ef', 'f0', 'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'fc', 'fb', 'fa', 'fd', 'fe', 'ff', 'c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'ca', 'cb', 'cc', 'cd', 'ce', 'cf', 'd0', 'd1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd7', 'd8', 'd9', 'dc', 'db', 'da', 'dd', 'de', 'df');
		
		public static function EscapeString($str)
		{
			mb_internal_encoding("UTF-8");
			mb_regex_encoding("UTF-8");
			
			//return str_replace(Utils::$_bad, Utils::$_good, $str);
			
//			for($i = 0; $i < count(Utils::$_bad); $i++)
//			{
//				$str = mb_ereg_replace(Utils::$_bad[$i], Utils::$_good[$i], $str); 
//			}
//			
//			return $str;

//			echo 'ENCODING: '.$str;

			//$tmp = iconv('utf8', 'windows-1251', $str);
			
			$tmp = mb_convert_encoding($str, 'windows-1251');
			
//			echo 'ENCODING: '.$tmp;
			
			$tmp = str_replace(Utils::$_bad, Utils::$_good, $tmp);
			//$str = iconv('windows-1251', 'UTF-8', $tmp);
			$str = mb_convert_encoding($tmp, 'UTF-8', 'windows-1251');
//			echo 'ENCODING: '.$str;
			
			return $str;
		}
		
		public static function UnescapeString($str)
		{
			mb_internal_encoding("UTF-8");
			
			return str_replace(Utils::$_good, Utils::$_bad, $str);
		}	
		
		public static function ShortString($str, $count = 50)
		{
			mb_internal_encoding("UTF-8");

			if(mb_strlen($str) > $count)
				return mb_substr($str, 0, $count).'...';
			else
				return $str;
		}
		
		public static function MakeURL($str, $proto = 'http://')
		{
			if(strpos($str, $proto) !== 0)
				return $proto.$str;
				
			return $str;
		}

        public static $russianMonthNames = array(
            'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь','Декабрь'
        );

		public static function GetRuMonth($i)
		{
			switch($i)
			{
				case 1: return 'Январь';
				case 2: return 'Февраль';
				case 3: return 'Март';
				case 4: return 'Апрель';
				case 5: return 'Май';
				case 6: return 'Июнь';
				case 7: return 'Июль';
				case 8: return 'Август';
				case 9: return 'Сентябрь';
				case 10: return 'Октябрь';
				case 11: return 'Ноябрь';
				case 12: return 'Декабрь';
				default: return '';
			}
		}

		public static function GetRuMonthShort($i)
		{
			switch($i)
			{
				case 1: return 'Янв';
				case 2: return 'Фев';
				case 3: return 'Мар';
				case 4: return 'Апр';
				case 5: return 'Май';
				case 6: return 'Июн';
				case 7: return 'Июл';
				case 8: return 'Авг';
				case 9: return 'Сен';
				case 10: return 'Окт';
				case 11: return 'Ноя';
				case 12: return 'Дек';
				default: return '';
			}
		}

		public static function GetRuMonthEx($i)
		{
			switch($i)
			{
				case 1: return 'января';
				case 2: return 'февраля';
				case 3: return 'марта';
				case 4: return 'апреля';
				case 5: return 'мая';
				case 6: return 'июня';
				case 7: return 'июля';
				case 8: return 'августа';
				case 9: return 'сентября';
				case 10: return 'октября';
				case 11: return 'ноября';
				case 12: return 'декабря';
				default: return '';
			}
		}

        public static function normalizePhoneNumber($num)
        {
            $num = preg_replace('/\D/', '', $num);

            if(empty($num))
                return $num;

            if(($num == '495')||($num == '498')||($num == '499'))
                return '';

            if(($num[0] == '8') && (strlen($num) > 10))
                $num = substr($num, 1);

            if(strlen($num) == 7)
                $num = '495'.$num;

            $num = $num[0].$num[1].$num[2].'-'.$num[3].$num[4].$num[5].'-'.$num[6].$num[7].'-'.$num[8].$num[9];

            return $num;
        }

        public static function normalizePhoneNumberArray($num)
        {
            $num = preg_replace('/\D/', '', $num);

            if(empty($num))
                return array($num);

            if(($num[0] == '8') && (strlen($num) > 10))
                $num = substr($num, 1);

            if(strlen($num) == 7)
                $num = '495'.$num;

            $num = array($num[0].$num[1].$num[2], $num[3].$num[4].$num[5], $num[6].$num[7], $num[8].$num[9]);

            return $num;
        }
		
		public static function Translit($st)
		{
			mb_internal_encoding("UTF-8");
			mb_regex_encoding("UTF-8");
			
    		$replacement = array( 
    		
    		"(а|е|и|о|у|ы|ь|ъ)ё" => '\1ey',
    		'^ё' => 'ey',
    		
    		"ья" => 'ia',
    		"ия" => 'ia',
    		'ий\b' => 'iy',
			'ый\b' => 'iy',
    		
    		"ЬЯ" => 'IA',
    		"ИЯ" => 'IA',    		
    		"/ИЙ$/" => 'IY',
			"/ЫЙ$/" => 'IY',
    		"/(А|Е|И|О|У|Ы|Ь|Ъ)Ё/" => 'EY',
    		"/^Ё/" => 'EY',
    		
    		"а"=>"a",
    		"б"=>"b",
    		"в"=>"v",
    		"г"=>"g",
    		"д"=>"d",
    		"е"=>"e",
    		"ё"=>"e",
    		"ж"=>"zh",
    		"з"=>"z",
    		"и"=>"i",
    		"й"=>"y",
    		"к"=>"k",
    		"л"=>"l",
    		"м"=>"m",
    		"н"=>"n",
    		"о"=>"o",
    		"п"=>"p",
    		"р"=>"r",
    		"с"=>"s",
    		"т"=>"t",
    		"у"=>"u",
    		"ф"=>"f",
    		"х"=>"kh",
        	"ц"=>"ts",
    		"ч"=>"ch", 
        	"ш"=>"sh",
        	"щ"=>"shch",
    		"ь"=>"",
    		"ы"=>"y",
        	"ъ"=>"", 
        	"э"=>"e",
    		"ю"=>"yu", 
        	"я"=>"ya", 
         
    		"А"=>"A",
    		"Б"=>"B",
    		"В"=>"V",
    		"Г"=>"G",
    		"Д"=>"D",
    		"Е"=>"E",
    		"Ё"=>"E",
    		"Ж"=>"ZH",
    		"З"=>"Z",
    		"И"=>"I",
    		"Й"=>"Y",
    		"К"=>"K",
    		"Л"=>"L",
    		"М"=>"M",
    		"Н"=>"N",
    		"О"=>"O",
    		"П"=>"P",
    		"Р"=>"R",
    		"С"=>"S",
    		"Т"=>"T",
    		"У"=>"U",
    		"Ф"=>"F",
    		"Х"=>"KH",
        	"Ц"=>"TS",
    		"Ч"=>"CH", 
        	"Ш"=>"SH",
        	"Щ"=>"SHCH",
    		"Ь"=>"",
    		"Ы"=>"Y",
        	"Ъ"=>"",
    		"Э"=>"E", 
        	"Ю"=>"YU", 
        	"Я"=>"YA", 
    		); 
    
    		foreach($replacement as $i=>$u) 
    		{ 
        		$st = mb_ereg_replace($i,$u,$st); 
    		} 
    		
    		return $st; 
		}
		
		public static function ToRTF($string)
		{
			mb_internal_encoding("UTF-8");
			mb_regex_encoding("UTF-8");
			
			for($i = 0; $i < count(Utils::$_rus); $i++)
			{
				$string = mb_eregi_replace(Utils::$_rus[$i], "\'".Utils::$_rtf[$i], $string);
			}
			
			return $string;
		}
		
		public static function capitalizeFirst($string)
		{ 
            mb_internal_encoding("UTF-8");

            $string = mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
            return $string;
	    }		
		
	    public static function mb_str_pad($input, $pad_length, $pad_string=' ', $pad_type=STR_PAD_RIGHT) {
                $diff = strlen($input) - mb_strlen($input, 'UTF-8');
                return str_pad($input, $pad_length+$diff, $pad_string, $pad_type);
        }

		public static function NumToStr($inn, $stripkop = 1) 
		{
            mb_internal_encoding("UTF-8");

            $num2str = new Num2Str();
            return Utils::capitalizeFirst($num2str->get($inn, $stripkop));
		}
		
		public static function pluralForm($n, $f1, $f2, $f5) 
		{
			$n = abs($n) % 100;
			$n1 = $n % 10;
			if ($n > 10 && $n < 20) return $f5;
			if ($n1 > 1 && $n1 < 5) return $f2;
			if ($n1 == 1) return $f1;
			return $f5;
		}
				
		public static function GenerateLogin($str, $length = 10)
		{
			mb_internal_encoding("UTF-8");
			mb_regex_encoding("UTF-8");
			
			$str = mb_eregi_replace('[^(а-яА-Яa-zA-Z )]', '', $str);
			
			$tmp = explode(' ', $str);
			$tmp = $tmp[0];
			
			$result = Utils::Translit($tmp);
			
			$result = mb_convert_encoding($result, 'windows-1251');

			$rlength = strlen($result);
			if($rlength >= ($length-3))
			{
				$result = substr($result, 0, $length-3);
				$result .= rand(0, 9);
				$result .= rand(0, 9);
				$result .= rand(0, 9);
			}
			else
			{
				for($j = 0; $j < ($length - $rlength); $j++)
					$result .= rand(0, 9);
			}

			$tmp = mb_convert_encoding($result, 'UTF-8', 'windows-1251');
			
			return $tmp;
		}

		public static function GeneratePassword($length = 10)
		{
			for($i = 0; $i < $length; $i++)
				$password .= rand(0, 9);
				
			return $password;
		}

		public static function Colorize($value, $prefix = '', $postfix = '')
		{
			if($value < 0)
			{
				$value = '<font color=red><strong>'.$prefix.$value.$postfix.'</strong></font>';
			}
			else if($value > 0)
			{
				$value = '<font color=green><strong>'.$prefix.$value.$postfix.'</strong></font>';
			}
			else
				$value = '<strong>'.$prefix.$value.$postfix.'</strong>';
			
			return $value;
		}

        public static function getFIO($fullname)
        {
            mb_internal_encoding("UTF-8");

            $result = '';
            $name = explode(' ', $fullname);
            if(!empty($name[0]))
            {
                $result = $name[0];
                if(!empty($name[1]))
                {
                    $result .= ' '.mb_substr($name[1], 0, 1).'.';
                    if(!empty($name[2]))
                    {
                        $result .= mb_substr($name[2], 0, 1).'.';

                    }
                }
            }
            return $result;
        }
	}