<?php

	class SqlObject
	{
		protected $ObjectType = 'UnknownObject';
		
		protected static $_sql_connected = false;
        protected static $_dbh;
				
		protected function _ConnectSQL()
		{
            $dbh = mysql_connect(SQL_HOST, SQL_USER, SQL_PASSWORD);
			if($dbh !== FALSE)
			{
				SqlObject::$_sql_connected = true;
                SqlObject::$_dbh = $dbh;

				if(!mysql_select_db(SQL_DB, SqlObject::$_dbh))
				{
					$error = new ErrorBox('Ошибка выбора базы данных!');
					$error->Show();
					exit;
				}
                mysql_query("SET NAMES utf8", SqlObject::$_dbh);
                //mysql_query("SET time_zone = '+03:00'");

				if((TRACK_ACTIVITY == '1') && (!empty($_SESSION['user_id'])))
				{
					$this->sql("update users set last_activity = UNIX_TIMESTAMP() where id = ".$_SESSION['user_id']);
				}                
			}
			else
			{
				$error = new ErrorBox('Ошибка соединения с сервером данных!');
				$error->Show();
				exit;
			}
		}
	
		function sql($cmd)
		{
			if(!SqlObject::$_sql_connected)
			{
				$this->_ConnectSQL();
			}
			
			if(is_object($cmd))
				$cmd = $cmd->Get();

			Debug::Log($cmd);

			return mysql_query($cmd, SqlObject::$_dbh);
		}
	
		function getline($cmd)
		{
			$res = $this->sql($cmd);
			return mysql_fetch_array($res);
		}
		
		function last_insert_id()
		{
			return mysql_insert_id(SqlObject::$_dbh);
		}
		
		function found_rows_total()
		{
			$data = $this->getline('select FOUND_ROWS() as rows');
			return $data['rows'];
		}
	}