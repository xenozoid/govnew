<?php

class LocationManager
{
    const STACK = '__locationManagerStack';

    protected static function init()
    {
        if(!is_array($_SESSION[LocationManager::STACK]))
            $_SESSION[LocationManager::STACK] = array();
    }

    public static function redirect($url = '')
    {        
        header('Location: '.$url);
        exit;
    }

    public static function add($url = '')
    {
        LocationManager::init();
        if(empty($url))
            $url = $_SERVER['REQUEST_URI'];
        if($_SESSION[LocationManager::STACK][count($_SESSION[LocationManager::STACK])-2] != $url)
            $_SESSION[LocationManager::STACK][] = $url;
    }

    public static function last()
    {
        LocationManager::init();
        LocationManager::redirect($_SESSION[LocationManager::STACK][count($_SESSION[LocationManager::STACK])-1]);
    }

    public static function back()
    {
        LocationManager::init();
        unset($_SESSION[LocationManager::STACK][count($_SESSION[LocationManager::STACK])-1]);
        LocationManager::last();
    }
}