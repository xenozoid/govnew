<?php

	class ValueList
	{
		protected $_values = array();

        public $emptyOption;
		public $name;

		public function Get($index)
		{
			return $this->_values[$index];			
		}

		public function GetList()
		{
			return $this->_values;			
		}
		
		public function GetKeys()
		{
			$result = array();
		
			foreach($this->_values as $value => $key)
				$result[] = $key;
				
			return $result;	
		}
		
		public function GetValues()
		{
			$result = array();
		
			foreach($this->_values as $value => $key)
				$result[] = $value;
				
			return $result;	
		}
		
		public function GetIndex($str)
		{
			foreach($this->_values as $key => $value)
				if($str == $value)
					return $key;
					
			return false;
		}

        public function GetSelectCode()
		{
            $select_name = $this->name;

			$res = '<select id="'.$select_name.'" name="'.$select_name.'" '.$this->inline.'>';
			if(!empty($this->emptyOption))
				$res .= '<option>'.$this->emptyOption.'</option>';
			$i = 0;
			if(count($this->_values))
			{
				foreach($this->_values as $key => $value)
				{
                    if($value == $this->value)
                        $res .= '<option selected value="'.$value.'">'.$this->_keys[$i].'</option>';
                    else
					    $res .= '<option value="'.$key.'">'.$value.'</option>';
					$i++;
				}
			}
			$res .= '</select>';
			return $res;
		}
	}
