<?php

	class Auth extends SqlObject
	{
        const AUTH_DEFAULT = -1;
        const AUTH_TRUE = 1;
        const AUTH_FALSE = 0;

        const AUTH_STATUS_OK = 1;
        const AUTH_STATUS_NOSUCHUSER = 2;
        const AUTH_STATUS_INVALIDPASSWORD = 3;
        const AUTH_STATUS_DISABLED = 4;
        
		private $_enabled = false;
	
		function Auth()
		{
			if(defined('STAY_LOGGED_IN'))
				session_set_cookie_params(PHP_INT_MAX);
			else
				session_set_cookie_params(0);

			session_start();		
		}
	
		function CheckCredentials()
		{
			if(!empty($_REQUEST['login']) && !empty($_REQUEST['password']))
			{
				$_SESSION['login'] = $_REQUEST['login'];
                if(defined('HASHED_PASSWORDS') && defined('PASSWORD_SALT'))
                    $_SESSION['password'] = md5($_REQUEST['password'].PASSWORD_SALT);
                else if(defined('HASHED_PASSWORDS'))
                    $_SESSION['password'] = md5($_REQUEST['password']);
                else
				    $_SESSION['password'] = $_REQUEST['password'];

                if(class_exists('AuthCallback', false))
                    if(method_exists('AuthCallback', 'onLoggedIn'))
                        AuthCallback::onLoggedIn();

                if($_REQUEST['loggedin'])
                    $mode = 'web';
                else
                    $mode = 'mobile';
			}
		
			if(!empty($_SESSION['login']) && !empty($_SESSION['password']))
			{
				$data = $this->getline("select * from users where login = '".$_SESSION['login']."'");

                if(!empty($data['id']))
                {
                    if($data['password'] == $_SESSION['password'])
                    {
                        $_SESSION['user_id'] = $data['id'];
                        $_SESSION['user_priv'] = $data['priv'];
                        $_SESSION['user_name'] = $data['name'];
                        $_SESSION['user_data'] = $data;
                        $this->_enabled = true;

                        if(empty($data['disabled']))
                        {
                            $_SESSION['auth_status'] = Auth::AUTH_STATUS_OK;
                        }
                        else
                        {
                            $_SESSION['auth_status'] = Auth::AUTH_STATUS_DISABLED;
                        }

                        if(($mode == 'web' && (!$data['web_access']))
                            || ($mode == 'mobile' && (!$data['mobile_access'])))
                        {
                            $this->_enabled = false;
                            $_SESSION['auth_status'] = Auth::AUTH_STATUS_DISABLED;
                        }

                        if(DT::getDateTime($data['active_from']))
                            if($data['active_from'] > time())
                            {
                                $this->_enabled = false;
                                $_SESSION['auth_status'] = Auth::AUTH_STATUS_DISABLED;
                            }

                        if(DT::getDateTime($data['active_to']))
                            if($data['active_to'] < time())
                            {
                                $this->_enabled = false;
                                $_SESSION['auth_status'] = Auth::AUTH_STATUS_DISABLED;
                            }

                        if(class_exists('AuthCallback', false))
                            if(method_exists('AuthCallback', 'onLoggedIn'))
                                AuthCallback::onLoggedIn();
                    }
                    else
                    {
                        if(!empty($_SESSION['password']))
                            $_SESSION['auth_status'] = Auth::AUTH_STATUS_INVALIDPASSWORD;
                    }
                }
                else
                {
                    $_SESSION['auth_status'] = Auth::AUTH_STATUS_NOSUCHUSER;
                }
			}
		}
		
		function IsEnabled()
		{
            $callback = Auth::AUTH_DEFAULT;

            if(class_exists('AuthCallback', false))
                if(method_exists('AuthCallback', 'isEnabled'))
                    $callback = AuthCallback::isEnabled();

            switch($callback)
            {
                case Auth::AUTH_TRUE: return true;
                case Auth::AUTH_FALSE: return false;
                case Auth::AUTH_DEFAULT: return $this->_enabled;
            }
            
			return $this->_enabled;
		}
		
		function Logout()
		{
            if(class_exists('AuthCallback', false))
                if(method_exists('AuthCallback', 'onLoggedOut'))
                    AuthCallback::onLoggedOut();

			session_destroy();
		}
	}
