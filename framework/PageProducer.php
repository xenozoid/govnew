<?php

	class PageProducer
	{
		public $Title;
		public $Auth;
		protected $_scripts = array();
        protected $_css = array();
		public $ShowMenu = true;
		public $IncludeRichEditor = false;
		public $IncludeUI = false;
        public $IncludeFancyBox = false;
        public static $showMenuDelimeter = true;
	
		function PageProducer($title = '', $use_auth = true)
		{
			$this->Title = $title;
			
			if($use_auth)
			{	
				$this->Auth = new Auth();
				$this->Auth->CheckCredentials();
				if(!$this->Auth->IsEnabled())
				{
					$this->Redirect("index.php");
				}
			}
		}

        function addScript($url)
        {
            $this->_scripts[] = $url;
        }

        function addCSS($url)
		{
			$this->_css[] = $url;
		}
		
		function Begin()
		{
			echo '<!DOCTYPE html>
			<html>
			<head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>'.$this->Title.'</title>
			<link type="text/css" rel="stylesheet" href="styles/css.css" media="all" />
			<link type="text/css" rel="stylesheet" href="styles/jquery.autocomplete.css" media="all" />
            <link type="text/css" rel="stylesheet" href="styles/jquery-ui.css" media="all" />

			<script type="text/javascript" src="scripts/highlight/highlight-active-input.js"></script>
			<script type="text/javascript" src="scripts/jquery.js"></script>
			<script type="text/javascript" src="scripts/jqueryui.js"></script>
			<script type="text/javascript" src="scripts/jquery.ui.datepicker-ru.js"></script>
            <script type="text/javascript" src="scripts/jquery.scrollTo-min.js"></script>
            <script type="text/javascript" src="scripts/utils.js"></script>
                ';

            if($this->IncludeRichEditor)
            {
                echo '<script type="text/javascript" src="scripts/ckeditor/ckeditor.js"></script>';
            }

            for($i = 0; $i < count($this->_scripts); $i++)
            {
                echo '<script type="text/javascript" src="'.$this->_scripts[$i].'"></script>';
            }

            for($i = 0; $i < count($this->_css); $i++)
            {
                echo '<link type="text/css" rel="stylesheet" href="'.$this->_css[$i].'" media="all" />';
            }

            if($this->IncludeFancyBox)
            {
                echo '<script type="text/javascript" src="scripts/fancybox/jquery.fancybox-1.3.1.js"></script>';
                echo '<link type="text/css" rel="stylesheet" href="scripts/fancybox/jquery.fancybox-1.3.1.css" media="all" />';
            }

            if(class_exists('PageProducerCallback', false))
                if(method_exists('PageProducerCallback', 'customHeadSection'))
                    PageProducerCallback::customHeadSection();

            echo '
			</head>
			<body>
			';
			
			if($this->Auth)
			{
				if($this->Auth->IsEnabled())
				{
                    if($this->ShowLogo)
                    {
                        echo '<table style="border:0px;padding:0px;margin:0px;"><tr><td><img src="images/logo.png"></td><td>';
                    }

					if(($this->ShowMenu) && (!method_exists('PageProducerCallback', 'customShowMenu')))
					{
                        echo
                '<table style="padding: 5pt; background-color:#eee; vertical-align:bottom; width:1200px"><tr><td style="width:100%;">';

                        if(class_exists('PageProducerCallback', false))
                            PageProducerCallback::Show();
                        
                        $ds = new DataSource("select code from object_types
                        where group_id > 1 order by weight");
                        while($data = $ds->GetString())
                        {
                            if(ACL::Check($data['code'], ACL::READ))
                            {
                                $this->ShowMenuItem('Справочники', 'service', 'service');
                                break;
                            }
                        }

                        $sql = new SqlObject();
                        $data = $sql->getline('select name from roles where id = '.$_SESSION['user_priv']);
                        $role_name = $data['name'];
                        $user_shown = false;
                        if(class_exists('PageProducerCallback', false))
                        {
                            if(method_exists('PageProducerCallback', 'ShowUser'))
                            {
                                PageProducerCallback::ShowUser($_SESSION['user_name'], $_SESSION['user_login'], $role_name);
                                $user_shown = true;
                            }
                        }
                        if(!$user_shown)
                            echo "<strong><a href=\"profile.php\">".$_SESSION['user_name']."</a></strong> (<strong>".$_SESSION['login']."</strong>, ".$role_name.") ";
                        if(class_exists('PageProducerCallback', false))
                            if(method_exists('PageProducerCallback', 'ShowVersion'))
                                PageProducerCallback::ShowVersion();
                        echo "<td align=right><a class=menulink href=logout.php>Выход</a>";

                echo '</table>';


					}
                    
                    if(($this->ShowMenu) && (method_exists('PageProducerCallback', 'customShowMenu')))
                    {
                        PageProducerCallback::customShowMenu();
                    }

                    if($this->ShowLogo)
                    {
                        echo '</td></tr></table>';
                    }

                    ?>
                    <script type="text/javascript" src="/scripts/jquery.timers-1.2.js"></script>
                    <?php

                    if($_SESSION['user_data']['internal_active'])
                    {
                        ?>
                        <script type="text/javascript" src="/scripts/wopen.js"></script>
                        <input type="hidden" id="exten" value="<?php echo $_SESSION['user_data']['internal_num']; ?>">
                        <input type="hidden" id="pstate" value="Idle">
                        <div id="state" style="position: fixed; bottom: 0; left: 0; background-color: thistle; width: 300px; height: 20px;"></div>
                    <?php
                    }

				}
			}
			echo '
			<br>
			';			
		}
		
		function ShowMenuItem($caption, $module, $icon = '', $object = '', $action = ACL::READ)
		{
			if(!empty($object))
			{
				if(!ACL::Check($object, $action))
				{
					return;
				}
			}
            
            echo '<nobr>';

			if(!empty($icon))
				echo '<img src="images/'.$icon.'.png" style="vertical-align:middle; margin-bottom:5px; padding:0px;">';
				
			if(strpos($module, '.') !== false)
				$url = $module;
			else
				$url = 'index.php?module='.$module;

            if(($module == $_REQUEST['module']) &&
                ((empty($_REQUEST['act'])) || ($_REQUEST['act'] == 'index')))
            {
                    echo '&nbsp;<strong>'.$caption.'</strong>&nbsp;';
                    unset($_SESSION['__active_menu_item']);
            }
            else if($_SESSION['__active_menu_item'] == $module)
                echo '&nbsp;<strong>'.$caption.'</strong>&nbsp;';
            else
			    echo '&nbsp;<a class="menulink" href="'.$url.'">'.$caption.'</a>&nbsp;';

            echo '</nobr>';

            if(PageProducer::$showMenuDelimeter)
                echo '|&nbsp;';
		}
		
		function End()
		{
			if(class_exists('PageProducerCallback', false))
				if(method_exists('PageProducerCallback', 'ShowFooter'))
					PageProducerCallback::ShowFooter();

            if(defined('HIGHLIGHT'))
            {
                echo '<script type="text/javascript">
                    $(document).ready(initInputHighlightScript());
                  </script>';
            }

			echo '</body></html>';
		}
		
		function Redirect($location = 'index.php')
		{
			header("Location: ".$location);
			exit;
		}
		
	}
