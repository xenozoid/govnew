<?php

	class Table
	{
		public $TableId;
		public $Caption;
		protected $_data;
		protected $_data_source;
		
		protected $_colnum = 0;
		
		protected $_rownum = 0;
		
		protected $_headers = array();
		protected $_fields = array();
		
		public $IndexField = 'id';
		
		protected $ModuleName;
		
		protected $_new_url;
		protected $_edit_url;
		protected $_delete_url;
		
		public $_sort_additional_params;
		
		public $ShowNewLinks = false;
		public $ShowEditLinks = false;
		public $ShowDeleteLinks = false;
		
		public $ShowTopNewLink = true;
		public $ShowBottomNewLink = true;
		
		public $AddLabelCaption = 'Создать';
		
		protected $SortType;
		protected $SortedField;
		
		protected $_page = 0;
		protected $_pages = 0;
		
		public $Paging = true;
		public $RowLimit = 50;
        public $showRowLimit = false;
		
		public $Width;
		public $ShowCaption = true;
		public $Sortable = true;

        public $TotalRows = 0;
        
        public $Checkboxes = false;
        public $NumerateCheckboxes = false;
        protected $_ids = array();
        public $ChecklistElements = array();
        public $FormAction = '?';
        
        protected $_additional_buttons = array();
        
        public $DefaultURL;
	
		function Table($caption, 
					   $data, 
					   $headers = array(), 
					   $fields = array(), 
					   $sort_additional_params = '' 
					  )
		{
			$this->ModuleName = $_REQUEST['module'];
			
			if(empty($this->TableId)) 
				$this->TableId = $this->ModuleName.'_'.get_class();
			
			$this->Caption = $caption;
			$this->_data = $data;
			$this->_headers = $headers;

            if(count($fields))
			foreach($fields as $key => $field)
			{
				if(preg_match('/^\`.*\`$/', $field) == 0)
				{
					$field = explode(' ', $field);
					$field = $field[count($field)-1];
					$fields[$key] = $field;
				}
			}
			
			$this->_fields = $fields;
			
			//print_r($fields);
			
			$session_page = $this->TableId.'_PAGE';
			
            if(!empty($_SESSION[$session_page]))
            	$this->_page = $_SESSION[$session_page];

			if($_REQUEST['tbl_page'] > 0)
			{
				$this->_page = $_REQUEST['tbl_page'] - 1;
				$_SESSION[$session_page] = $this->_page;
			}
		}
		
		protected function PrepareData()
		{
			$data = $this->_data;
			
			if(is_object($data))
			{
				$data = $data->Get();
			}

            if(is_string($data))
            {
            	$session_st = $this->TableId.'_ST';
            	$session_sf = $this->TableId.'_SF';
            	
            	
            	if(!empty($_SESSION[$session_st]))
            		$this->SortType = $_SESSION[$session_st];
            		
            	if(!empty($_SESSION[$session_sf]))
            		$this->SortedField = $_SESSION[$session_sf];
            	
				if(!empty($_REQUEST['st']))
				{
					$this->SortType = $_REQUEST['st'];
					$_SESSION[$session_st] = $this->SortType; 
				}
				else if(empty($this->SortType))
					$this->SortType = 'asc';	
					
				if(strstr($this->SortType, ',') !== false)
				{
					$st = str_replace(' ', '', $this->SortType);
					$this->SortType = explode(',', $st);
				}

				if((!empty($_REQUEST['sf'])) || ($_REQUEST['sf'] === 0)) 
				{
					$this->SortedField = $_REQUEST['sf'];
					$_SESSION[$session_sf] = $this->SortedField;
				}
					
				if((!empty($this->SortedField)) && ($this->Sortable))
				{
					if(strstr($this->SortedField, ',') !== false)
					{
						$sf = str_replace(' ', '', $this->SortedField);
						$sf = explode(',', $sf);
						$data .= ' order by ';
						for($i = 0; $i < count($sf); $i++)
						{
							$data .= $this->_fields[$sf[$i]];
							if(is_array($this->SortType))
								$data .= ' '.$this->SortType[$i];
							else
								$data .= ' '.$this->SortType;
							$data .= ', ';
						}
						$data = substr($data, 0, strlen($data)-2);
					}
					else
					{
						//print_r($this->_fields);
						//echo 'SF: '.$this->SortedField;
						$data .= ' order by '.$this->_fields[$this->SortedField].' '.$this->SortType;
					}
				}

				if($this->Paging)
                {
                    if(!empty($_SESSION['tbl_rowlimit_'.$this->TableId]))
                        $this->RowLimit = $_SESSION['tbl_rowlimit_'.$this->TableId];

                    $data .= ' limit '.$this->_page*$this->RowLimit.', '.$this->RowLimit;
                }

			
            	$this->_data_source = new DataSource($data);
				$this->_colnum = $this->_data_source->ColCount;
            	//$this->TotalRows = $this->_data_source->RowCount;
            	$this->TotalRows = $this->_data_source->found_rows_total();
			
            }
            else if(is_array($data))
            {
                $this->_data_source = $data;
                $this->_colnum = count($this->_data_source[0]);
                //$this->TotalRows = count($this->_data_source);
                $this->TotalRows = $this->_data_source->found_rows_total();
            } 
				
			if($sort_additional_params != '')
            {
                if($sort_additional_params{0} != '&')
                    $sort_additional_params = '&'.$sort_additional_params;
                $this->_sort_additional_params = $sort_additional_params;
            }


		}
		
		protected function ShowNewLink()
		{
			if(empty($this->_new_url))
				$this->_new_url = '?module='.$this->ModuleName.'&act=new';

			echo '<br><img src="images/new.png">&nbsp;<a href="'.$this->_new_url.'" class="table_link_new">'.$this->AddLabelCaption.'</a><br><br>';
		}

		protected function Begin()
		{
			
			if(($this->ShowNewLinks) && ($this->ShowTopNewLink))
				$this->ShowNewLink();
				
			$this->showPages();
			
//			$colspan = $this->_colnum;
//			foreach($this->_headers as $key => $val)
//				if(strlen($val) == 0)
//					$colspan--;
//
//                    echo $colspan;
//
//            if($colspan <= 0)
//            {
                $colspan = count($this->_headers);
       			foreach($this->_headers as $key => $val)
    				if(strlen($val) == 0)
        				$colspan--;
            //}

            //echo $colspan;
					
			if($this->ShowEditLinks)
				$colspan++;

			if($this->ShowDeleteLinks)
				$colspan++;
				
		    if($this->Checkboxes)
            {
                $colspan++;
                echo '<form method="post" action="'.$this->FormAction.'">';
            }

            $colspan += count($this->_additional_buttons);

			$this->Caption = str_replace('%rows%', $this->TotalRows, $this->Caption);
			
			if($this->Width != '')
				$width = 'width="'.$this->Width.'"';
			
			echo '<table '.$width.' class="commonTable">';
		
			if($this->ShowCaption)
				echo '<tr><td colspan='.$colspan.' class="tableHeader">'.$this->Caption.'</td></tr>';

                echo '<tr><td><table class="innerTable">';

			if(count($this->_headers) > 0)
			{
				echo '<tr class="tableMenu">';
				$field_num = 0;

                if($this->Checkboxes)
					echo '<td class="tableMenuItem">&nbsp;';
				
				foreach($this->_headers as $key => $val)
				{
					$sort_img = '';
					
					if(strlen($val) > 0)
					{
						if($field_num == $this->SortedField)
						{
							if($this->SortType == 'asc')
							{
								$sort_img = '&nbsp;<img src="images/up.png">';
								$st = 'desc';
							}
							else
							{
								$sort_img = '&nbsp;<img src="images/down.png">';
								$st = 'asc';
							}
						}
						
						if(empty($st))
							$st = 'desc';
						
						if($this->Sortable)
							echo '<td class="tableMenuItem"><a href="?module='.$this->ModuleName.'&sf='.$field_num.'&st='.$st.$this->_sort_additional_params.'">'.$val.'</a>'.$sort_img.'</td>';
						else
							echo '<td class="tableMenuItem">'.$val.'</td>';
					}

					$field_num++;
				}
				
                foreach($this->_additional_buttons as $key => $value)
                    echo '<td class="tableMenuItemButton">&nbsp;';

				if($this->ShowEditLinks)
					echo '<td class="tableMenuItemButton">&nbsp;';
                    
				if($this->ShowDeleteLinks)
					echo '<td class="tableMenuItemButton">&nbsp;';
				
				echo "</tr>";
			}
		}
		
		protected function _is_visible_cell($cellnum)
		{
			if(!empty($this->_headers[$cellnum]))
			{
				return true;
			}
			
			return false;
		}
		
		protected function _render_cell($data, $celldata, $url = null)
		{
			if(empty($url))
			{
				if(empty($this->DefaultURL))
					$this->DefaultURL = '?module='.$this->ModuleName.'&act=view&id=%id%';
				
				$url = str_replace('%id%', $data[$this->IndexField], $this->DefaultURL);
            	if(preg_match_all('/%(.+)%/', $url, $matches) !== false)
            	{
                	for($i = 0; $i < count($matches[1]); $i++)
                    	$url = str_replace($matches[0][$i], $data[$matches[1][$i]], $url);
            	}				
			}
			
			if(preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $celldata, $matches))
			{
				$dt = new DT();
				$celldata = $dt->toDate($celldata);
			}
            else if(preg_match('/^(\d{4})\-(\d{2})\-(\d{2}) \d{2}\:\d{2}\:\d{2}$/', $celldata, $matches))
			{
				$dt = new DT();
				$celldata = $dt->toDateTime($celldata);
			}

			echo '<td class="tableCell">';
			if(empty($this->wasRowHref))
			{
				$this->wasRowHref = 1;
				echo '<a name="row'.$data[$this->IndexField].'" />';
			}
			echo '<a class="cellLink" href="'.$url.'">'.$celldata.'</a></td>';
		}
		
		protected function ShowCell($data, $cellnum)
		{
			if($this->_is_visible_cell($cellnum))
			{
				$celldata = $data[$cellnum];
				$this->_render_cell($data, $celldata);
			}
		}

        protected function _render_tr($rowdata, $rownum, $class)
        {
            echo '<tr class="'.$class.'" id="'.$this->TableId.'_'.$rowdata[$this->IndexField].'">';
        }

		protected function _begin_row($rowdata, $rownum, $class)
		{
			$this->wasRowHref = 0;
			
			if(strpos($class, 'style=') !== false)
			{
				echo '<tr '.$class.' id="'.$this->TableId.'_'.$rowdata[$this->IndexField].'">';
			}
			else
			{			
				if($class == '')
				{
					if($rownum%2 != 0) $class = 'oddRow'; else $class = 'evenRow';
				}
			
				$this->_render_tr($rowdata, $rownum, $class);
			}
			
		    if($this->Checkboxes)
            {
                echo '<td class="tableCell"><input type="checkbox" id="check_'.$rowdata[$this->IndexField].'" name="check_'.$rowdata[$this->IndexField].'" value="1"';
                if($this->isChecked($rowdata, $rownum))
                    echo ' checked';
                echo '>';
                if($this->NumerateCheckboxes)
                	echo '&nbsp;'.++$this->_checkbox_number;
                echo '</td>';
                $this->_ids[] = $rowdata[$this->IndexField];
            }
		}

        protected function isChecked($rowdata, $rownum)
        {
            return false;
        }
			
		protected function BeginRow($rowdata, $rownum)
		{
			//print_r($rowdata);
			$this->_begin_row($rowdata, $rownum, $class);
		}
			
		protected function ShowString($data)
		{
			$cellnum = 0;
		
			$this->BeginRow($data, $this->_rownum++);

			for($i = 0; $i < $this->_colnum; $i++)
			{
				$this->ShowCell($data, $cellnum++);
			}
			$this->EndRow($data, $this->_rownum);
		}

		protected function _show_edit_link($rowdata, $rownum)
		{
			$edit_url = $this->_edit_url;
			$edit_url = str_replace('%id%', $rowdata[$this->IndexField], $edit_url);
            if(preg_match_all('/%(.+)%/', $edit_url, $matches) !== false)
            {
                for($i = 0; $i < count($matches[1]); $i++)
                    $edit_url = str_replace($matches[0][$i], $rowdata[$matches[1][$i]], $edit_url);
            }			
			echo '<td class="tableCellButton"><a href="'.$edit_url.'"><img src="images/edit.png" alt="править" title="править" border=no></a></td>';
		}
		
		protected function ShowEditLink($rowdata, $rownum)
		{			
			if($this->_edit_url == '')
				$this->_edit_url = '?module='.$this->ModuleName.'&act=edit&id=%id%';
				
			$this->_show_edit_link($rowdata, $rownum);
		}

		protected function _show_delete_link($rowdata, $rownum)
		{
			$delete_url = $this->_delete_url;
			$delete_url = str_replace('%id%', $rowdata[$this->IndexField], $delete_url);
            if(preg_match_all('/%(.+)%/', $delete_url, $matches) !== false)
            {
                for($i = 0; $i < count($matches[1]); $i++)
                    $delete_url = str_replace($matches[0][$i], $rowdata[$matches[1][$i]], $delete_url);
            }			            
			echo '<td class="tableCellButton"><a href="'.$delete_url.'" onclick="return confirm(\'Удалить?\')"><img src="images/del.png" alt="удалить" title="удалить" border=no></a></td>';
		}
		
		protected function ShowDeleteLink($rowdata, $rownum)
		{
			if($this->_delete_url == '')
				$this->_delete_url = '?module='.$this->ModuleName.'&act=del&nohead=1&id=%id%';

			$this->_show_delete_link($rowdata, $rownum);
		}

        protected function _show_additional_button($rowdata, $rownum, $url, $image, $alt, $target, $inline = '')
        {
            if(preg_match_all('/%(.+)%/', $url, $matches) !== false)
            {
                for($i = 0; $i < count($matches[1]); $i++)
                    $url = str_replace($matches[0][$i], $rowdata[$matches[1][$i]], $url);
            }

            if($target != '')
                $target = 'target='.$target;

            $image_name = explode('.', $image);
            $image_name = $image_name[0];

            echo '<td class="tableCellButton">';
            if(!empty($url))
                echo '<a '.$target.' href="'.$url.'">';
            echo '<img id="img_add_'.$rownum.'_'.$image_name.'" src="images/'.$image.'" alt="'.$alt.'" title="'.$alt.'" border=no '.$inline.'>';
            if(!empty($url))
                echo '</a>';
            echo '</td>';
        }

		protected function EndRow($rowdata, $rownum)
		{
            foreach($this->_additional_buttons as $key => $value)
                $this->_show_additional_button($rowdata, $rownum, $value['url'], $value['image'], $value['alt'], $value['target']);

			if($this->ShowEditLinks)
				$this->ShowEditLink($rowdata, $rownum);

			if($this->ShowDeleteLinks)
				$this->ShowDeleteLink($rowdata, $rownum);

			echo '</tr>';
		}

		protected function _showPages()
		{
			$this->_pages = ceil($this->TotalRows / $this->RowLimit);
			
			if(($this->Paging) && ($this->_pages > 1))
			{
				for($i = 0; $i < $this->_pages; $i++)
				{		
					if($i == $this->_page)
						echo '<strong style="font-size:14px;">'.($i+1).'</strong> ';
					else
						echo '<a style="font-size:14px;" href="?tbl_page='.($i+1).'&module='.$this->ModuleName.'">'.($i+1).'</a> ';
				}
			
				echo '<br>';
			}			
		}		
		
		protected function showPages()
		{
			$this->_pages = ceil($this->TotalRows / $this->RowLimit);

            if($this->Paging)
            if($this->showRowLimit)
            {
                echo '<table style="border:0px; border-collapse:collapse;"><tr><td>';
                $form = new Form('', array(
                    array('type' => 'select', 'caption' => 'Показать', 'name' => 'tbl_rowlimit', 'data' => new RowLimitList(), 'nobr' => 1, 'value' => $_SESSION['tbl_rowlimit_'.$this->TableId]),
                    array('type' => 'hidden', 'name' => 'module', 'value' => $this->moduleName),
                    array('type' => 'hidden', 'name' => 'nohead', 'value' => 1),
                    array('type' => 'hidden', 'name' => 'tbl_id', 'value' => $this->TableId),
                    array('type' => 'submit', 'name' => 'btnChangeRowLimit', 'value' => 'OK', 'nobr' => 1)
                 ));
                $form->style = 'display:inline;border:0px;';
                $form->Show();

                echo '<td>';
            }
			
			if(($this->Paging) && ($this->_pages > 1))
			{

				for($i = 0; $i < $this->_pages; $i++)
				{
					if($i == $this->_page)
						echo '<table style="margin:3px; border: 1px solid silver; float:left; font-size:14px; color:silver; font-weight:bold; background-color:#99c2ff;"><tr><td style="font-size:14px; color:black;">'.($i+1).'</td></tr></table>';
					else
						echo '<table style="margin:3px; border: 1px solid silver; float:left; color:silver; font-weight:bold; cursor:pointer;" onclick="document.location.href=\'?tbl_page='.($i+1).'&module='.$this->ModuleName.$this->pagesParams.'\'" onmouseover="this.style.backgroundColor=\'#cccccc\'" onmouseout="this.style.backgroundColor=\'#fff\'"><tr><td><a style="font-size:14px; color:#999999; text-decoration:none" href="?tbl_page='.($i+1).'&module='.$this->ModuleName.$this->pagesParams.'">'.($i+1).'</td></tr></table> ';
				}

//                if($this->showRowLimit)
//                {
//                    echo '</td></tr></table>';
//                }
//                else
//                {
//                    echo '<br><br>';
//                }
			}

            if($this->Paging)
            if($this->showRowLimit)
            {
                echo '</td></tr></table>';
            }
            else
            if(($this->Paging) && ($this->_pages > 1))
            {
                echo '<br><br>';
            }

		}

		protected function End()
		{
            echo '</td></tr></table>';

			echo "</table>";
			
		    if($this->Checkboxes)
            {
                echo '<br>';
                echo '<input type=button onclick="';
                for($i = 0; $i < count($this->_ids); $i++)
                    echo 'document.getElementById(\'check_'.$this->_ids[$i].'\').checked = true;';
                echo '" value="Выбрать все"> ';

                echo '<input type=button onclick="';
                for($i = 0; $i < count($this->_ids); $i++)
                    echo 'document.getElementById(\'check_'.$this->_ids[$i].'\').checked = false;';
                echo '" value="Отменить выбор"> ';

                foreach($this->ChecklistElements as $element)
                {
                	echo $element.'&nbsp;';
                }
                
                echo '</form>';
            }
			
			$this->showPages();
			
			if($this->ShowNewLinks)
				if($this->ShowBottomNewLink)
					$this->ShowNewLink();
		}
		
		public function Show()
		{
			$this->PrepareData();
			
			$this->Begin();

            if(is_object($this->_data_source))
            {
                $rowCount = $this->_data_source->RowCount;
                //while(($str = $this->_data_source->GetString()) !== FALSE)
                for($i = 0; $i < $rowCount; $i++)
                {
                    $str = $this->_data_source->GetString();
                    $this->ShowString($str);
                }
            }

            if(is_array($this->_data_source))
            {
                for($i = 0; $i < count($this->_data_source); $i++)
                {
//    				if($this->Paging)
//        			{
//            			if($rows_to_skip > 0)
//                		{
//                    		$rows_to_skip--;
//                        	continue;
//                        }
//                    }

                    $str = $this->_data_source[$i];
                    $this->ShowString($str);

//                    if($this->Paging)
//                    {
//                        $rows_shown++;
//                        if($rows_shown == $this->RowLimit)
//                            break;
//                    }

                }
            }
		
			$this->End();
		}
		
	}
