<?php

	class SelectList
	{
		protected $_keys;
		protected $_values;
		protected $_value_field;
        protected $value;
		public $emptyOption;
		public $name;
        public $inline;
		
		public function SelectList($sql, $key_field, $value_field)
		{
			$this->_value_field = $value_field;
			
			$ds = new DataSource($sql);
			while(($data = $ds->GetString()) !== false)
			{
				$this->_keys[] = $data[$key_field];
				$this->_values[] = $data[$value_field]; 
			}
		}
		
		public function GetKeys()
		{
			return $this->_keys;
		}

		public function GetValues()
		{
			return $this->_values;
		}
		
		public function Key($i)
		{
			return $this->_keys[$i];
		}
		
		public function Value($i)
		{
			return $this->_values[$i];
		}

        public function setValue($i)
        {
            $this->value = $i;
        }
		
		public function GetSelectCode()
		{
			if(empty($this->name))
				$select_name = $this->_value_field;
			else
				$select_name = $this->name;
				
			$res = '<select id="'.$select_name.'" name="'.$select_name.'" '.$this->inline.'>';
			if(!empty($this->emptyOption))
				$res .= '<option>'.$this->emptyOption.'</option>';
			$i = 0;
			if(count($this->_values))
			{
				foreach($this->_values as $key => $value)
				{
                    if($value == $this->value)
                        $res .= '<option selected value="'.$value.'">'.$this->_keys[$i].'</option>';
                    else
					    $res .= '<option value="'.$value.'">'.$this->_keys[$i].'</option>';
					$i++;
				}
			}
			$res .= '</select>';
			return $res;
		}
	}
