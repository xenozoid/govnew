<?php

	class DT
	{
		const Now = 'Now';
		const Yesterday = -1;
		const Tomorrow = 1;
		
		protected $_undefined = false;
	
		public $_year;
		public $_month;
		public $_day;
		
		public $_hour;
		public $_min;
		public $_sec;

        protected static $year;
        protected static $month;
        protected static $day;

        protected static $hour;
        protected static $min;
        protected static $sec;

		
		protected $_month_abbr = array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
        private static $_month_ru = array('янв', 'фев', 'мар', 'апр', 'мая', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек');
		
		protected function _init($dt)
		{
			$this->_clear();
		
			if(($dt == -1) || ($dt == ''))
			{
				$this->_undefined = true;
				return;
			}
			
			if(($m = $this->_contains_month($dt)) !== false)
			{		
				$this->_day = 1;
				$this->_month = $m;
				$this->_year = intval(preg_replace('/\D/', '', $dt));
				
				$this->_undefined = false;

				return;
			}
		
			if(preg_match("/^(\d{1,2})$/", $dt, $matches))
			{
				if($matches[1] <= 24)
				{
					$this->_hour = $matches[1];
				}		
				
				return;
			}

			if(preg_match("/^(\d{1,2})[\.\:\-]?(\d{2})$/", $dt, $matches))
			{
				if(($matches[1] <= 24) && ($matches[2] <= 60))
				{
					$this->_hour = $matches[1];
					$this->_min = $matches[2];
				}		
				
				return;
			}
		
			if(preg_match("/^\d+$/", $dt, $matches))
			{
				$timestamp = $dt;
				$timestamp_defined = true;
			}
			else
				$timestamp = time();
		
			if(($dt == DT::Now) || ($timestamp_defined == true) || ($dt == 'NOW') || ($dt == 'ASAP'))
			{
				$values = explode(';', date('Y;m;d;H;i:s', $timestamp));
				
				$this->_year = $values[0];
				$this->_month = $values[1];
				$this->_day = $values[2];
				$this->_hour = $values[3];
				$this->_min = $values[4];
				$this->_sec = $values[5];
				
				$this->_undefined = false;

                return;
			}
			else if(preg_match("/^(\d?\d)[\.\,](\d?\d)[\.\,]?\'?(\d?\d?\d\d)$/", $dt, $matches))
			{
				$this->_undefined = false;
			
				$this->_year = $matches[3];
					if(($this->_year > 50) && ($this->_year < 100))
						$this->_year += 1900;
					else if ($this->_year < 50)
						$this->_year += 2000;
				$this->_month = $matches[2];
				$this->_day = $matches[1];

                return;
			}
            else if(preg_match("/^(\d\d)\.(\d\d)\.(\d\d\d\d) (\d?\d)[\.\:](\d\d)$/", $dt, $matches))
			{
				$this->_undefined = false;

				$this->_year = $matches[3];
				$this->_month = $matches[2];
				$this->_day = $matches[1];
                
				$this->_hour = $matches[4];
				$this->_min = $matches[5];
                $this->_sec = '00';

                return;
			}
			else if(preg_match("/(\d\d\d\d)\-(\d\d)\-(\d\d) (\d?\d)[\.\:](\d\d)[\.\:](\d\d)/", $dt, $matches))
			{
				$this->_undefined = false;

				$this->_year = $matches[1];
				$this->_month = $matches[2];
				$this->_day = $matches[3];

				$this->_hour = $matches[4];
				$this->_min = $matches[5];
                $this->_sec = $matches[6];

                return;
			}
			else if(preg_match("/(\d\d\d\d)\-(\d\d)\-(\d\d)/", $dt, $matches))
			{
				$this->_undefined = false;
			
				$this->_year = $matches[1];
				$this->_month = $matches[2];
				$this->_day = $matches[3];

                return;
			}
			else if(preg_match("/(\d+)\/(\d+)\/(\d\d\d\d)/", $dt, $matches))
			{
				$this->_undefined = false;
			
				$this->_year = $matches[3];
				$this->_month = $matches[1];
				$this->_day = $matches[2];

                return;
			}			
			else if(preg_match("/(\d\d)\:(\d\d)\:(\d\d)/", $dt, $matches))
			{
				$this->_hour = $matches[1];
				$this->_min = $matches[2];
				$this->_sec = $matches[3];

                return;
			}
			else if(preg_match("/(\d\d)\:(\d\d)/", $dt, $matches))
			{
				$this->_hour = $matches[1];
				$this->_min = $matches[2];

                return;
			}	
		}
		
		function DT($dt = -1)
		{
			$this->_init($dt);
				
			setlocale(LC_TIME, LOCALE);			
		}
		
		protected function _contains_month($str)
		{
			$str = strtoupper($str);
			$contains = false;
			$result = '';
			
			foreach($this->_month_abbr as $key => $value)
			{
				if(strstr($str, $value) !== false)
					$result = $key + 1;		
			}
	
			if($result != '')
				return $result;
			else
				return false;
		}
		
		protected function _clear()
		{
			$this->_year = '0000';
			$this->_month = '00';
			$this->_day = '00';
		
			$this->_hour = '00';
			$this->_min = '00';
			$this->_sec = '00';
		}

		protected function _is_empty()
		{
			if($this->_undefined == true)
				return true;
		
			if(
			($this->_year == 0) &&
			($this->_month == 0) &&
			($this->_day == 0) &&
			($this->_hour == 0) &&
			($this->_min == 0) &&
			($this->_sec == 0)
			)
			return true;
			
			return false;
		}

		protected function _is_date_empty()
		{
			if(
			($this->_year == 0) &&
			($this->_month == 0) &&
			($this->_day == 0)
			)
			return true;
			
			return false;
		}


/*		
		public function Add($time)
		{
			$parts = explode(':', $time);
			$seconds = $parts[0]*60*60 + $parts[1]*60;
			$this->_dt += $seconds;
		}

		public function Substract($time)
		{
			$parts = explode(':', $time);
			$seconds = $parts[0]*60*60 + $parts[1]*60;
			$this->_dt -= $seconds;
		}
		
*/		
		public function toDate($param = null)
		{
			$this->_init($param);
			
			if($this->_is_empty())
				return '';
					
			return strftime(DATE_FORMAT, $this->toUnix());
		}
		
		public function toDateInt($param = null)
		{
			$this->_init($param);
			
			if($this->_is_empty())
				return '';
					
			return strftime(DATE_FORMAT_INT, $this->toUnix());
		}

		public function toTime($param = null)
		{
			if(($param == null) || ($param == ''))
				return '';
		
			//if(!empty($param))
				$this->_init($param);
				
//			if($this->_is_empty())
//				return '';
//			else

			//if($this->_is_date_empty())
				


				return strftime(TIME_FORMAT, $this->toUnix());
		}
		
		public function toDateTime($param = null)
		{
			//if(!empty($param))
				$this->_init($param);

            if($this->_undefined)
                return '';

            if($this->_is_empty())
                return '';
			
			return strftime(DATETIME_FORMAT, $this->toUnix());				
		}				

		public function toSQLDate($param = null)
		{
			if($param != null)
			{
				$this->_init($param);
			}
			else
				return '';

			if($this->_undefined)
			{
				return '';
			}
				
			return $this->_year.'-'.$this->_month.'-'.$this->_day;
		}
		
		public function toSQLTime($param = null)
		{
			if(($param == null) || ($param == ''))
				return '';
		
			if(!empty($param))
				$this->_init($param);

			return $this->_hour.':'.$this->_min.':'.$this->_sec;
		}

		public function toSQLDateTime($param = null)
		{
			//if($param != null)
			//{
				$this->_init($param);
			//}
			//else
            if($this->_undefined)
			{
				return '';
			}

			return $this->_year.'-'.$this->_month.'-'.$this->_day.' '.$this->_hour.':'.$this->_min.':'.$this->_sec;
		}
		
		protected function toUnix()
		{
			$dt = mktime($this->_hour, $this->_min, $this->_sec, $this->_month, $this->_day, $this->_year);
			return $dt;
		}
		
		public function toUnixTime($param = '')
		{
			if(!empty($param))
				$this->_init($param);
			else
				return 0;

			return $this->toUnix();
		}
		
		
		private function _date2Days($_timestamp) 
		{
  			$_year=(int) date('Y',$_timestamp);
  			$_month=(int) date('m',$_timestamp);
  			$_day=(int) date('j',$_timestamp);
  			$_century = substr($_year,0,2);
  			$_year = substr($_year,2,2);
  			if($_month>2) 
			  $_month -= 3;
  			else 
			{
        		$_month += 9;
        		if($_year) 
					$_year--;
        		else 
				{
         			$_year=99;
         			$_century --;
        		}
  			}
  			return (floor((146097*$_century)/4)+floor((1461*$_year)/4)+floor((153*$_month+2)/5)+$_day+1721119);
 		}

		public function dayDiff($date1, $date2)
		{
			$date1 = $this->toUnixTime($date1);
			$date2 = $this->toUnixTime($date2);
		
			//echo $date1.'--'.$date2.'*****';
		
			//return $this->_date2Days($date1) - $this->_date2Days($date2);
			
			$diff = $date2 - $date1;
			
			return abs(floor($diff / (24 * 60 * 60)));
		}
		
		public function StartOfWeek()
		{
			$dow = date('N') - 1;
			$start = mktime(0, 0, 0, date('m'), date('j') - $dow, date('Y'));
			
			return $start;
		}

		public function EndOfWeek()
		{
			$dow = 7 - date('N');
			$end = mktime(23, 59, 59, date('m'), date('j') + $dow, date('Y'));
			
			return $end;
		}
        
        protected static function parse($dt)
        {
            if(defined('TIMEZONE'))
                date_default_timezone_set(TIMEZONE);

            if(($dt == DT::Now) || ($dt == 'NOW') || ($dt == 'ASAP'))
            {
                $values = explode(';', date('Y;m;d;H;i;s'));
				
                $year = $values[0];
                $month = $values[1];
                $day = $values[2];
                $hour = $values[3];
                $min = $values[4];
                $sec = $values[5];
            }
            else if(preg_match("/^\d{10}$/", $dt, $matches))
            {
                $values = explode(';', date('Y;m;d;H;i;s', $matches[0]));

                $year = $values[0];
                $month = $values[1];
                $day = $values[2];
                $hour = $values[3];
                $min = $values[4];
                $sec = $values[5];                
            }
            else if(preg_match("/^\d{5}$/", $dt, $matches))
            {
                $date = date("d.m.Y", mktime(0, 0, 0, 1, $matches[0] - 1, 1900));
                $values = explode('.', $date);

                $year = $values[2];
                $month = $values[1];
                $day = $values[0];
            }
            else if(preg_match("/^(\d?\d)[\.\,](\d?\d)[\.\,]?\'?(\d?\d?\d\d)$/", $dt, $matches))
            {
                $year = $matches[3];
                    if(($year > 50) && ($year < 100))
                        $year += 1900;
                    else if ($year < 50)
                        $year += 2000;
                $month = $matches[2];
                $day = $matches[1];
            }
            else if(preg_match("/^(\d\d)\.(\d\d)\.(\d\d\d\d) (\d?\d)[\.\:](\d\d)$/", $dt, $matches))
            {
                $year = $matches[3];
                $month = $matches[2];
                $day = $matches[1];
                
                $hour = $matches[4];
                $min = $matches[5];
                $sec = '00';
            }
            else if(preg_match("/(\d\d\d\d)\-(\d\d)\-(\d\d) (\d?\d)[\.\:](\d\d)[\.\:](\d\d)/", $dt, $matches))
            {
                $year = $matches[1];
                $month = $matches[2];
                $day = $matches[3];

                $hour = $matches[4];
                $min = $matches[5];
                $sec = $matches[6];
            }
            else if(preg_match("/(\d\d\d\d)\-(\d\d)\-(\d\d)/", $dt, $matches))
            {
                $year = $matches[1];
                $month = $matches[2];
                $day = $matches[3];
            }
            else if(preg_match("/(\d?\d)\-(\d\d)\-(\d\d\d\d)/", $dt, $matches))
            {
                $year = $matches[3];
                $month = $matches[2];
                $day = $matches[1];
            }
            else if(preg_match("/(\d+)\/(\d+)\/(\d\d\d\d)/", $dt, $matches))
            {
                $year = $matches[3];
                $month = $matches[1];
                $day = $matches[2];
            }

            DT::$year = $year;
            DT::$month = $month;
            DT::$day = $day;
            DT::$hour = $hour;
            DT::$min = $min;
            DT::$sec = $sec;

            $dt = mktime($hour, $min, $sec, $month, $day, $year);
            return $dt;
        }

        protected static function isEmpty()
        {
            if((DT::$year == 0) && (DT::$month == 0) && (DT::$day == 0) && (DT::$hour == 0) && (DT::$min == 0) && (DT::$sec == 0))
                return true;

            return false;
        }

        public static function getUnixTime($value)
        {
            return DT::parse($value);
        }

        public static function getDate($value)
        {
            $dt = DT::parse($value);

            if(DT::isEmpty())
                return '';

            return strftime(DATE_FORMAT, $dt);
        }

        public static function getDateTime($value)
        {
            $dt = DT::parse($value);

            if(DT::isEmpty())
                return '';

            return strftime(DATETIME_FORMAT, $dt);
        }

        public static function getSQLDate($value)
        {
            DT::parse($value);
            return DT::$year.'-'.DT::$month.'-'.DT::$day;
        }

        public static function getSQLDateTime($value)
        {
            DT::parse($value);
            return DT::$year.'-'.DT::$month.'-'.DT::$day.' '.DT::$hour.':'.DT::$min.':'.DT::$sec;
        }

        public static function getHumanDateTime($value)
        {
            DT::parse($value);

            if(DT::isEmpty())
                return '';

            $today = DT::getDate(DT::Now);
            if(DT::getDate($value) == $today)
            {
                if(!isset(DT::$hour))
                    return 'сегодня';
                else
                    return 'сегодня, '.DT::$hour.':'.DT::$min;
            }

            $beforeYesterday = DT::getDate(DT::getUnixTime(DT::getDate(DT::Now)) - 2*24*60*60);
            if(DT::getDate($value) == $beforeYesterday)
            {
                if(!isset(DT::$hour))
                    return 'позавчера';
                else
                    return 'позавчера, '.DT::$hour.':'.DT::$min;
            }

            $yesterday = DT::getDate(DT::getUnixTime(DT::Now) - 24*60*60);
            if(DT::getDate($value) == $yesterday)
            {
                if(!isset(DT::$hour))
                    return 'вчера';
                else
                    return 'вчера, '.DT::$hour.':'.DT::$min;
            }

            $tomorrow = DT::getDate(DT::getUnixTime(DT::Now) + 24*60*60);
            if(DT::getDate($value) == $tomorrow)
            {
                if(!isset(DT::$hour))
                    return 'завтра';
                else
                    return 'завтра, '.DT::$hour.':'.DT::$min;
            }

            $afterTomorrow = DT::getDate(DT::getUnixTime(DT::Now) + 2*24*60*60);
            if(DT::getDate($value) == $afterTomorrow)
            {
                if(!isset(DT::$hour))
                    return 'послезавтра';
                else
                    return 'послезавтра, '.DT::$hour.':'.DT::$min;
            }

            DT::parse($value);
            if(DT::$day[0] == '0')
                DT::$day = DT::$day[1];
            $dt = DT::$day . ' ' . DT::$_month_ru[DT::$month - 1];
            if(DT::$year != date('Y'))
                $dt .= ' ' . DT::$year;
            if(DT::$hour || DT::$min)
                $dt .= ' ' . DT::$hour . ':' . DT::$min;

            return $dt;
        }


		public static function getDayDiff($date1, $date2)
		{
			$date1 = DT::getUnixTime($date1);
			$date2 = DT::getUnixTime($date2);

			$diff = $date2 - $date1;

			return floor($diff / (24 * 60 * 60));
		}

         public static function getYearDiff($date)
         {
             list($d, $m, $Y) = explode ('.', $date);
             $years = date ('Y') - $Y;

             if(date("md")< $m.$d)
             {
                 $years--;
             }
             
             return $years ;
         }


		/*
		public function IsPast()
		{
			$now = time();
			
			if($now <= $this->_dt)
				return false;
				
			return true;
		}
		
		public function IsPastDate()
		{
			$now = time();
			
			if($now < $this->_dt)
				return false;
			
			if(($now - $this->_dt) > 24*60*60)
				return true;
			else
				return false;
		}
		
		public function getDateStart()
		{
			return mktime(0,0,0,date("m", $this->_dt), date("d", $this->_dt), date("Y", $this->_dt));
		}
		
		public function getDateFinish()
		{
			return mktime(0,0,0,date("m", $this->_dt), date("d", $this->_dt)+1, date("Y", $this->_dt)) - 1;
		}
		
		public function getYesterdayStart()
		{
			return mktime(0,0,0,date("m"), date("d")-1, date("Y"));
		}
		
		public function getYesterdayFinish()
		{
			return mktime(0,0,0,date("m"), date("d"), date("Y"));
		}	
		
		public function getTomorrowStart()
		{
			return mktime(0,0,0,date("m"), date("d")+1, date("Y"));
		}
		*/
	}
