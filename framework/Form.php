<?php
	class Form
	{
		public $Caption = 'Form';
        public $width;
        public $height;
        public $style;
        public $cancelButtonCaption = 'Отмена';
		protected $_fields = array();
		private $_action;
		public $id;
        public $validate = true;
		protected $_required_fields = array();
		protected $_required_fields_names = array();
		protected $_required_fields_compnames = array();

        public $includeDateScript = true;

		protected $_validations = array();
		
		const Digits = '__CONST_DIGITS';
		const Float  = '__CONST_FLOAT';
		const Email  = '__CONST_EMAIL';

		const Deleted = '__DELETED';		
		protected static $_standard_elements = array(
											  'section', 
											  'subsection',
                                              'line',
											  'file',
											  'image',	 
											  'text',
											  'password', 
											  'textarea', 
											  'date', 
											  'datetime', 
											  'dateex', 
											  'checkbox', 
											  'select', 
											  'radio', 
											  'label', 
											  'submit',
                                              'button',
											  'select_url', 
											  'ajax_url',
											  'color',
											  'hidden',
											  'null',
                                              'inline',
											  Form::Deleted
												);
												
		
		protected function _is_standard_element($element)
		{
			return in_array($element, Form::$_standard_elements);
		}
	
		public function Form($caption, $fields, $action = '?')
		{
			$this->Caption = $caption;
			$this->_fields = $fields;
			$this->_action = $action;
			$this->id = 'form'.rand();
		}
		
		public static function GetFieldValue($name, $type)
		{
			if(Form::_is_standard_element($type))
			{
				switch($type)
				{
					case 'datetime':
						$retval = $_REQUEST[$name];
						if($_REQUEST[$name.'_hrs'])
						{
							$retval .= ' '.$_REQUEST[$name.'_hrs'];
							if($_REQUEST[$name.'_min'])
								$retval .= ':'.$_REQUEST[$name.'_min'];
							else
								$retval .= ':00';
						}
						return $retval;
					break;
				}
				
				return $_REQUEST['name']; 
			}
			else
			{
				return call_user_func(array($type.'FormComponent', 'GetFieldValue'), $name);				
			}
		}
		
		public function InsertBefore($before_what, $element)
		{
			$new_fileds = array();
			$_inserted = false;
			
			foreach($this->_fields as $key => $value)
			{
				if(($value['name'] == $before_what) && (!$_inserted))
				{
					$new_fileds[] = $element;
					$_inserted = true;
				}
				$new_fileds[] = $value;
			}
			
			$this->_fields = $new_fileds;
		}
		
		public function InsertAfter($after_what, $element)
		{
			$new_fileds = array();
			$_inserted = false;
			
			foreach($this->_fields as $key => $value)
			{
				$new_fileds[] = $value;
				if(($value['name'] == $after_what) && (!$_inserted))
				{
					$new_fileds[] = $element;
					$_inserted = true;
				}
			}
			
			$this->_fields = $new_fileds;
		}
		
		public function HasElement($name)
		{
			foreach($this->_fields as $key => $element)
			{
				if($element['name'] == $name)
					return true;
			}
			
			return false;
		}

		public function GetElementAttribute($name, $attribute)
		{
			$i = 0;
		
			foreach($this->_fields as $key => $element)
			{
				if($element['name'] == $name)
					return $this->_fields[$i][$attribute];
					
				$i++;
			}
		}
		
		public function SetElementAttribute($name, $attribute, $value)
		{
			$i = 0;
		
			foreach($this->_fields as $key => $element)
			{
				if(($name == '*') || ($element['name'] == $name))
					$this->_fields[$i][$attribute] = $value;
					
				$i++;
			}
		}
		
		public function DeleteElement($name)
		{
			$this->SetElementAttribute($name, 'type', Form::Deleted);
            $this->SetElementAttribute($name, 'name', $name.'_DELETED');
		}

        public function deleteByValue($attribute, $value)
        {
            $i = 0;

            foreach($this->_fields as $key => $element)
            {
                if($this->_fields[$i][$attribute] == $value)
                {
                    $this->_fields[$i]['type'] = Form::Deleted;
                }

                $i++;
            }
        }

        public function deleteAttribute($name, $attribute)
        {
            $i = 0;

            foreach($this->_fields as $key => $element)
            {
                if(($name == '*') || ($element['name'] == $name))
                {
                    unset($this->_fields[$i][$attribute]);

                    $i++;
                }
            }
        }

		function AddValidation($code)
		{
			$this->_validations[] = $code;
		}
		
		public function Show()
		{			
			foreach($this->_fields as $key => $value)
			{
				$new_fileds[] = $value;
				if($value['type'] == 'file')
				{
					$enctype = ' enctype="multipart/form-data"';
                    if($value['maxsize'] > 0)
                        $maxsize = $value['maxsize'];
				}
			}

            echo "<form method=POST id=".$this->id." action=".$this->_action." ";
            if($this->validate)
                echo "onsubmit='return validateForm".$this->id."()' ";
            if(!empty($this->inline))
                    echo $this->inline;
            echo $enctype.">";
			echo '<table id="'.$this->id.'_tbl" class="commonTable"';
            if((!empty($this->width)) || (!empty($this->height)))
            {
                echo ' style="';
                if(!empty($this->width))
                    echo 'width:'.$this->width.'px;';
                if(!empty($this->height))
                    echo 'height:'.$this->height.'px;';
                echo '"';
            }
            else if(!empty($this->style))
            {
                echo ' style="'.$this->style.'"';
            }
            echo '>';
            if(!empty($this->Caption))
                    echo '<tr><td colspan=2 class="tableHeader">'.$this->Caption.'</td></tr>';
			if(!empty($maxsize))
                echo '<input type="hidden" name="MAX_FILE_SIZE" value="'.$maxsize.'000" />';

			$previous_nobr = 0;
			
			$date_script_included = false;
			
			$was_label = false;

			foreach($this->_fields as $field => $params)
			{
				$type = '';
				$id = '';
				$size = '';
				$style = '';
				$name = '';
				$value = '';
				$readonly = '';
				$disabled = '';
				$checked_modifier = '';
				$select_data = null;
				$invisible_modifier = '';
				$onclick = '';
				$onselect = '';
				$required = '';
				
				if($params['type'] == Form::Deleted)
					continue;				
				
				if($params['nobr'] == '1')
				{
					$nobr = 1;
				}
				else
				{
					$nobr = 0;
				}
	
				if(($params['type'] == '') || ($params['type'] == 'null'))
				{
					$previous_nobr = 0;
					$was_label = false;
					continue;
				}
	
				if($params['visible'] == '0')
					$params['type'] = 'hidden';
					
				if($params['invisible'] == '1')
					$invisible_modifier = ' style="display:none" ';
				
				if($params['type'] != 'hidden')
				{						
					if($params['readonly'] == '1')
						$readonly = ' readonly';

					if($params['disabled'] == '1')
						$disabled = ' disabled="disabled"';
				
					if($params['type'] != '')
						$type = 'type='.$params['type'];
						
					if($params['id'] != '')
						$id = 'id='.$params['id'];
					else if($params['name'] != '')
					{
						//$params['id'] = $params['name'].rand();
						$params['id'] = $params['name'];
						$id = 'id='.$params['id'];
					}
					
					if(($params['required'] == '1') && ($params['invisible'] != '1'))
					{
						$required = '1';
						if($params['type'] == 'dateex')
						{
							$this->_required_fields[] = $params['id'].'_day';
							$this->_required_fields[] = $params['id'].'_month';
							$this->_required_fields[] = $params['id'].'_year';
						
							$this->_required_fields_names[] = $params['caption'];
							$this->_required_fields_names[] = $params['caption'];
							$this->_required_fields_names[] = $params['caption'];
							
							$this->_required_fields_compnames[] = $params['type'];
							$this->_required_fields_compnames[] = $params['type'];
							$this->_required_fields_compnames[] = $params['type'];
						}
						else
						{
							$this->_required_fields[] = $params['id'];
							$this->_required_fields_names[] = $params['caption'];
							$this->_required_fields_compnames[] = $params['type'];
						}
					}
					
					if($params['size'] != '')
						$size = 'size='.$params['size'];

					if($params['style'] != '')
						$style = ' style="'.$params['style'].'"';						
						
					if($params['name'] != '')
						$name = 'name="'.$params['name'].'"';

					if($params['value'] != '')
						$value = 'value="'.$params['value'].'"';
						
					if(is_object($params['data']))
					{
						$select_data = $params['data'];
						$params['keys'] = $select_data->GetKeys();
						$params['values'] = $select_data->GetValues();
					}
                    else if(is_array($params['data']))
                    {
                        $params['keys'] = array_values($params['data']);
                        $params['values'] = array_keys($params['data']);
                    }
					
					if(($params['type'] == 'section') || ($params['type'] == 'subsection') || ($params['type'] == 'line') || ($params['type'] == 'inline'))
					{
                        if(empty($params['name']))
                            $params['name'] = rand(1000, 9999);
						echo '<tr id="'.$params['name'].'tr"';
                        if($params['invisible'] == 1)
                            echo ' style="display:none"';
                        echo '><td colspan=2>';
					}
					else if (!$this->_is_standard_element($params['type']))
					{
						if(method_exists($params['type'].'FormComponent', 'Begin'))
							call_user_func(array($params['type'].'FormComponent', 'Begin'), $params);
					}
					else
					{
						if($nobr == 0)
							echo '<tr '.$id.'tr'.' '.$invisible_modifier.'><td class="formField">';
						else if(($nobr == 1) && ($previous_nobr == 0))
						{
							echo '<tr '.$id.'tr'.' '.$invisible_modifier.'><td class="formField">';
						}
					}	

					if (!$this->_is_standard_element($params['type']))
					{
						if(method_exists($params['type'].'FormComponent', 'Show'))						
							call_user_func(array($params['type'].'FormComponent', 'Show'), $params);
					}
					else
                    if($params['type'] == 'inline')
                    {
                        echo $params['value'];
                        //$previous_nobr = 0;
                        //$was_label = false;
                    }
					else if($params['type'] == 'textarea')
					{
						//if(($params['readonly'] == '1') || ($params['disabled'] == '1'))
                        echo '<span class="formFieldCaption">'.$params['caption'].'</span>';

						if($required == '1')
							echo '<span style="color:red"> *</span>';                        

                        if($nobr == 0)
                            echo '</td><td class="formField">';
                        else if(($nobr == 1) && ($previous_nobr == 0))
                            echo '</td><td class="formField">';
                        else if(!$was_label)
                            echo '&nbsp;&nbsp;&nbsp;';


						if($params['readonly'] == '1')
						{
							echo '<div><span class="formFieldValue">'.$params['value'].'</span></div>';
							echo '<input type=hidden name='.$params['name'].' value="'.$params['value'].'">';
						}
						else
						{
							echo '<textarea id="'.$params['id'].'" '.$name.' rows='.$params['height'].' cols='.$params['width'].$readonly.$disabled.$style.'>'.Utils::UnEscapeString($params['value']).'</textarea>';
							if($params['rich'] == '1')
							{
								echo "
				<script type=\"text/javascript\">
				//<![CDATA[
					CKEDITOR.replace('".$params['name']."', {
                        filebrowserImageUploadUrl : 'upload.php'
                    });
				//]]>
				</script>								
								";
							}
						}
					}
					else if($params['type'] == 'radio')
					{
						echo '<span class="formFieldCaption">'.$params['caption'].'</span>';
						
						if($required == '1')
							echo '<span style="color:red"> *</span>';						
						
						if($nobr == 0)
							echo '</td><td  class="formField" style="vertical-align:bottom;">';
						else if(($nobr == 1) && ($previous_nobr == 0))
							echo '</td><td  class="formField" style="vertical-align:bottom;">';
						else
							echo '&nbsp;&nbsp;&nbsp;';                        
							
						for($i = 0; $i < count($params['keys']); $i++)
						{
							if($params['value'] == $params['values'][$i])
							{
								$selected_modifier = ' checked';
								
								if($params['readonly'] == '1')
								{
									echo '<span class="formFieldValue">'.$params['keys'][$i].'</span>';
									echo '<input type=hidden name='.$params['name'].' value="'.$params['values'][$i].'">';
								}			
							}
							else
								$selected_modifier = '';
								
							if($params['readonly'] != '1')
								echo '<input type=radio '.$name.$id.$i.' value="'.$params['values'][$i].'"'.$selected_modifier.' '.$disabled.' '.$params['inline'].'>'.$params['keys'][$i].'&nbsp;&nbsp;&nbsp;';
						}
						
					}
					else if($params['type'] == 'select')
					{
						echo '<span class="formFieldCaption">'.$params['caption'].'</span>';
						
						if($required == '1')
							echo '<span style="color:red"> *</span>';

						if($nobr == 0)
							echo '</td><td class="formField">';
						else if(($nobr == 1) && ($previous_nobr == 0))
							echo '</td><td class="formField">';
						else if(!$was_label)
							echo '&nbsp;&nbsp;&nbsp;';
										
						if(($params['disabled'] == '1') || ($params['readonly'] == '1'))
						{
							if(!empty($params['value']))
							{						
								for($i = 0; $i < count($params['keys']); $i++)
								{
									if($params['value'] == $params['values'][$i])
									{
										echo '<span class="formFieldValue">'.$params['prefix'].$params['keys'][$i].$params['postfix'].'</span>';
										echo '<input type=hidden name='.$params['name'].' value="'.$params['values'][$i].'">';
										break;
									}
								}
							}
						}
						else
						{	
							echo $params['prefix'].'<select '.$id.' '.$name.$readonly.$disabled.$onselect.' '.$params['inline'].'>';
							$has_one_selected = '';
							if($params['value'] == '')
								$selected_modifier = 'selected';
							else
								$selected_modifier = '';
							if($params['empty_option'] != '')
								echo '<option value="" '.$selected_modifier.'>'.$params['empty_option'].'</option>';
							for($i = 0; $i < count($params['keys']); $i++)
							{
								if($params['value'] == $params['values'][$i])
								{
									$selected_modifier = ' selected';
									$has_one_selected = 'yes';
								}
								else
                                {
									$selected_modifier = '';
                                }
									
								if($params['maxsize'])
									$params['keys'][$i] = Utils::ShortString($params['keys'][$i]);
									
								echo '<option value="'.$params['values'][$i].'"'.$selected_modifier.'>'.$params['keys'][$i].'</option>';
							}
							//if($has_one_selected == '')
							//	echo '<option value="-1" selected></option>';
							echo '</select>';
							if(!empty($params['postfix']))
								echo ' '.$params['postfix'];
						}
						//echo '</td>';
					}
					else if($params['type'] == 'checkbox')
					{
						if((int)($params['checked']) == 1)
							$checked_modifier = ' checked';

						//echo '<span class="formFieldCaption">'.$params['caption'].'</span></td><td><input '.$type.' '.$name.' '.$id.' '.$size.' '.$value.$checked_modifier.$readonly.$disabled.$onclick.'></td>';
						echo '<span class="formFieldCaption">'.$params['caption'].'</span>';

						if($nobr == 0)
							echo '</td><td class="formField">';
						else if(($nobr == 1) && ($previous_nobr == 0))
							echo '</td><td class="formField">';
						else
							echo '&nbsp;&nbsp;&nbsp;';
						
						echo $params['prefix'].'<input '.$type.' '.$name.' '.$id.' '.$size.' '.$value.$checked_modifier.$readonly.$disabled.$onclick.' '.$params['inline'].'> <label style="cursor: pointer;" for="' . ($params['id']?$params['id']:$params['name']) . '">' .$params['label'] . '</label>';
					}
					else if ($params['type'] == 'date')
					{						
						if($params['value'] == 'tomorrow')
						{
							$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"));
							$dt = new DT($tomorrow);
							$value = 'value="'.$dt->toDate().'"'; 
						}

						$date_dt = new DT();
						$params['value'] = $date_dt->toDate($params['value']);
					
						if(($params['disabled'] == '1') || ($params['readonly'] == '1'))
						{
							echo '<span class="formFieldCaption">'.$params['caption'].'</span>';
							
							if($nobr == 0)
								echo '</td><td class="formField">';
							else
								echo '&nbsp;&nbsp;&nbsp;';
							
							echo '<span class="formFieldValue">'.$params['value'].'</span>';
							echo '<input type=hidden name='.$params['name'].' value="'.$params['value'].'">';
						}
						else
						{
							echo '<span class="formFieldCaption">'.$params['caption'].'</span>';
							if($required == '1')
								echo '<font color=red> *</font>';							
							if($nobr == 0)
								echo '</td><td class="formField">';
							else if(($nobr == 1) && ($previous_nobr == 0))
								echo '</td><td class="formField">';
							else
								echo '&nbsp;&nbsp;&nbsp;';
								
							echo "
							<script type=\"text/javascript\">
                                $(document).ready(function()
                                {
                                    $.datepicker.setDefaults($.datepicker.regional['ru']);
									
									$('#".$params['name']."').datepicker({";

										if($params['mindate'])
										{
											echo "minDate: ".$params['mindate'].",";
										}

										if($params['maxdate'])
										{
											echo "maxDate: ".$params['maxdate'].",";
										}

										if($params['yearrange'])
										{
										    echo "yearRange: '".$params['yearrange']."',";
										}

                            echo "
										changeMonth: true,
										changeYear: true,
										showOn: 'button', 
										buttonImage: 'images/cal.png'
									});
									$('#".$params['name']."').datepicker('option', $.datepicker.regional['ru']);
                                });
							</script>
							";

                            if(!empty($params['tooltip']))
                            {
                                $tooltip = ' title="'.$params['tooltip'].'" ';
                            }
                            else
                                $tooltip = '';

							echo '<input class="textInput" '.$name.' '.$id.' size="10" value="'.$params['value'].'" '.$readonly.$disabled.' '.$params['inline'].' >'.$params['postfix'];

                            if(!empty($params['tooltip']))
                            {
                                echo '&nbsp;<a id="'.$params['id'].'_tooltip" href="#" '.$tooltip.' style="text-decoration:none;"><img src="images/question.png" style="border:0px;"></a>';

                                echo '<script>
                                $(document).ready(function(){
                                    $("#'.$params['id'].'_tooltip").tooltip({
                                        track: true,
                                        delay: 0,
                                        showURL: false,
                                        fade: 200
                                    });
                                });
                                </script>';
                            }
						}						
					}
					else if ($params['type'] == 'datetime')
					{
						if($params['value'] == 'tomorrow')
						{
							$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"));
							$dt = new DT($tomorrow);
							$value = 'value="'.$dt->toDate().'"';
						}

						if($params['readonly'] == '1')
						{
							echo '<span class="formFieldCaption">'.$params['caption'].'</span>';

							if($nobr == 0)
								echo '</td><td class="formField">';
							else
								echo '&nbsp;&nbsp;&nbsp;';

							echo '<span class="formFieldValue">'.$params['value'].'</span>';
							echo '<input type=hidden name='.$params['name'].' value="'.$params['value'].'">';
						}
						else
						{

                            if(!$date_script_included)
							{
								$date_script_included = true;
//								//echo '<script type="text/javascript" src="scripts/calendar/dhtmlgoodies_calendar.js"></script>';
//
//								echo '<script type="text/javascript" src="scripts/jqueryui.js"></script>';
//								//echo '<script type="text/javascript" src="scripts/ui.datetimepicker.js"></script>';
							}
							echo '<span class="formFieldCaption">'.$params['caption'].'</span>';
							if($required == '1')
								echo '<font color=red> *</font>';
							if($nobr == 0)
								echo '</td><td class="formField">';
							else if(($nobr == 1) && ($previous_nobr == 0))
								echo '</td><td class="formField">';
							else
								echo '&nbsp;&nbsp;&nbsp;';

							echo "
							<script type=\"text/javascript\">
                                $(document).ready(function(){
								$(function() {
									$('#".$params['name']."').datepicker({
										changeMonth: true,
										changeYear: true,
										showOn: 'button', 
										buttonImage: 'images/cal.png'
									});
								});
                                });
							</script>
							";
							
							$val = $params['value'];
							
							if(preg_match('/^(\d\d\.\d\d\.\d\d\d\d) (\d\d)\:(\d\d)$/', $val, $matches))
							{
								$val_date = $matches[1];
								$val_hr = $matches[2];
								$val_min = $matches[3];
							}
							else if(preg_match('/^(\d{10})$/', $val, $matches))
							{
								$values = explode(';', date('Y;m;d;H;i;s', $val));
								
								$val_date = $values[2].'.'.$values[1].'.'.$values[0];
								$val_hr = $values[3];
								$val_min = $values[4];
							}
							else
							{
								$val_date = null;
								$val_hr = null;
								$val_min = null;
							}
								
							echo '<input class=textInput '.$name.' '.$id.' size=10 value="'.$val_date.'" '.$readonly.$disabled.'>';
							echo '&nbsp;';
							echo '<select name="'.$params['name'].'_hrs" id="'.$params['name'].'_hrs">';
							echo '<option></option>';
							for($i = 0; $i < 24; $i++)
							{
								if($i < 10)
									$i = '0'.$i;
									
								if($i == $val_hr)
									$selected = ' selected';
								else
									$selected = '';
									
								echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
								$selected = null;
							}
							echo '</select>';
							echo ' : ';
							echo '<select name="'.$params['name'].'_min" id="'.$params['name'].'_min">';
							echo '<option></option>';
							if((!empty($val) || ($params['allminutes'])))
							{
								for($i = 0; $i < 60; $i++)
								{
									if($i < 10)
										$i = '0'.$i;
									
									if($i == $val_min)
										$selected = ' selected';
									else
										$selected = '';

									echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
									$selected = null;
								}
							}
							else
							{
								for($i = 0; $i < 12; $i++)
								{
									$ii = $i*5;
									if($ii < 10)
										$ii = '0'.$ii;
									
									echo '<option value="'.$ii.'"'.$selected.'>'.$ii.'</option>';
									$selected = null;
								}
							}
							echo '</select>';
							
						}
					}
					else if ($params['type'] == 'dateex')
					{
							echo '<span class="formFieldCaption">'.$params['caption'].'</span>';
							
							if($required == '1')
								echo '<span style="color:red"> *</span>';
															
							if($nobr == 0)
								echo '</td><td>';
							else if(($nobr == 1) && ($previous_nobr == 0))
								echo '</td><td>';
							else
								echo '&nbsp;&nbsp;&nbsp;';
								
							if($params['readonly'] == '1')
							{
								echo $params['value'];
								echo '<input type=hidden name='.$params['name'].' value="'.$params['value'].'">';
							}
							else
							{
								echo '<select name='.$params['name'].'_day id='.$params['name'].'_day>';
								if($params['value'] == '')
									echo '<option value="" selected>';
								else
									$selected_value = $params['value']{0}.$params['value']{1};
																	
								for($i = 1; $i < 32; $i++) 
								{ 
									if($i < 10) 
										$j = '0'.$i; 
									else 
										$j = $i; 
								
									if($selected_value == $j)
										$selected_modifier = ' selected';
									else
										$selected_modifier = '';
									
									echo '<option value='.$j.$selected_modifier.'>'.$j.'</option>'; 
								}			
								echo '</select>';
								$selected_value = '';
								echo '<select name='.$params['name'].'_month id='.$params['name'].'_month>';
								if($params['value'] == '')
									echo '<option value="" selected>';
								else
									$selected_value = $params['value']{3}.$params['value']{4};								
								for($i = 1; $i < 13; $i++)
								{ 
									if($i < 10) 
										$j = '0'.$i; 
									else 
										$j = $i; 

									if($selected_value == $j)
										$selected_modifier = ' selected';
									else
										$selected_modifier = '';
									
									echo '<option value='.$j.$selected_modifier.'>'.Utils::GetRuMonth($j).'</option>';
								}
								echo '</select>';
								$selected_value = '';
								echo '<select name='.$params['name'].'_year id='.$params['name'].'_year>';
								if($params['value'] == '')
									echo '<option value="" selected>';
								else
									$selected_value = $params['value']{6}.$params['value']{7}.$params['value']{8}.$params['value']{9};
								for($i = 2008; $i > 1945; $i--)
								{
									if($selected_value == $i)
										$selected_modifier = ' selected';
									else
										$selected_modifier = '';							
							 
									echo '<option value='.$i.$selected_modifier.'>'.$i.'</option>'; 
								}
								echo '</select>';
								$selected_value = '';
							}			
					}
					else  if ($params['type'] == 'submit')
					{
                        if(!defined("DISABLE_BUTTON_HIGHLIGHT"))
                        {
						    $onmouseover = 'onmouseover="this.style.backgroundColor=\'#cccccc\'"';
						    $onmouseout = 'onmouseout="this.style.backgroundColor=\'#eeeeee\'"';
                        }
						
						if($params['cancel'] == '1')
							$cancel_button = ' <input class="submitButton" type="submit" name="btnCancel" value="'.$this->cancelButtonCaption.'" '.$onmouseover.' '.$onmouseout.'>';
                        else
                            $cancel_button = '';
                        
						echo '<span class="formFieldCaption">'.$params['caption'].'</span>';
						
						if($nobr == 0)
							echo '</td><td class="formField">';
						else if(($nobr == 1) && ($previous_nobr == 0))
							echo '</td><td class="formField">';
						
						echo '<input class="submitButton" '.$onmouseover.' '.$onmouseout.' '.$type.' '.$name.' '.$id.' '.$size.' '.$value.$readonly.$disabled.' '.$params['inline'].'>'.$cancel_button;				
					}
					else  if ($params['type'] == 'button')
					{	
						if($params['icon'] != '')
							$icon = '<img src=images/'.$params['icon'].'> ';
						else
							$icon = '';

						echo '<span class="formFieldCaption">'.$params['caption'].'</span>';

						if($nobr == 0)
							echo '</td><td class="formField">';
						else if(($nobr == 1) && ($previous_nobr == 0))
							echo '</td><td class="formField">';
						else
							echo '&nbsp;&nbsp;&nbsp;';						
						
						echo $icon.'<input class=submitButton type=button '.$name.' '.$id.' '.$size.' '.$value.$readonly.$disabled.' '.$params['inline'].'>';				
					}					
					else  if ($params['type'] == 'label')
					{
						echo '<span class="formFieldCaption">'.$params['caption'].'</span>';

						if($nobr == 0)
							echo '</td><td class="formField">';
						else if(($nobr == 1) && ($previous_nobr == 0))
							echo '</td><td class="formField">';
						else
							echo '&nbsp;&nbsp;&nbsp;';
						
						echo '<span id="'.$params['name'].'"';
						if(!empty($params['color']))
							echo ' style="color:'.$params['color'].'"';
						if(!empty($params['style']))
							echo ' style="'.$params['style'].'"';
						echo '>'.'<span class="formFieldValue">'.$params['value'].'</span>'.'</span>';
                        echo $params['postfix'];

						$was_label = true;
					}
					else  if ($params['type'] == 'section')
					{
						if($params['hidden'] != '1')
							echo '<table width="100%" style="background-color:silver"><tr><td>'.$params['caption'].'</td></tr></table>';
					}
					else  if ($params['type'] == 'subsection')
					{
						if($params['hidden'] != '1')
							echo '<table width="100%" class="subsectionBox"><tr><td class="subsectionBox">'.$params['caption'].'</td></tr></table>';
					}
                    else  if ($params['type'] == 'line')
					{
                        echo '<hr style="height:1px; width:100%;  color:white; background-color:#E0E0E0; border: 0px; margin-left: 0">';
					}
					else  if ($params['type'] == 'select_url')
					{
						echo '<span class="formFieldCaption">'.$params['caption'].'</span>';

						if($nobr == 0)
							echo '</td><td class="formField">';
						else if(($nobr == 1) && ($previous_nobr == 0))
							echo '</td><td class="formField">';
						else
							echo '&nbsp;&nbsp;&nbsp;';

						if(!empty($params['target']))
							$target = ' target='.$params['target'];
						else
							$target = '';
							
						if(!empty($params['public_value']))
							echo $params['public_value'].' &nbsp; ';

						if(
							(strpos($params['url'], 'http://') === false) &&
							(strpos($params['url'], 'mailto:') === false) &&
							(strpos($params['url'], 'ftp://') === false) &&
                            (strpos($params['url'], 'javascript:') === false) &&
							(strpos($params['url'], '.php') === false) 
						  )	
							$params['url'] = 'http://'.$params['url'];
							 
						echo '<a href="'.$params['url'].'"'.$target.'>'.$params['url_caption'].'</a>';
                        echo ' ' . $params['postfix'] . ' ';
						echo '<input type=hidden name='.$params['name'].' value="'.$params['value'].'">';				
					}
					else  if ($params['type'] == 'ajax_url')
					{
						echo '<span class="formFieldCaption">'.$params['caption'].'</span>';

						if($nobr == 0)
							echo '</td><td class="formField">';
						else if(($nobr == 1) && ($previous_nobr == 0))
							echo '</td><td class="formField">';
						else
							echo '&nbsp;&nbsp;&nbsp;';
													
						echo $params['public_value'].' &nbsp; <a href='.$params['url'].' onclick="';

						echo ' return checkAjaxURL'.$params['name'].'(\''.$params['url'].'\');">'.$params['url_caption'].'</a>';
						echo '<input type=hidden name='.$params['name'].' value="'.$params['value'].'">';
						echo '<script>
						function checkAjaxURL'.$params['name'].'(url)
						{
						';
						
						if($params['confirm'] != '')
							echo ' var answer = confirm("'.$params['confirm'].'"); 
							if(!answer) return false;';
						
						if($params['checkhide'] != '')
						{
							$elements = explode(',', $params['checkhide']);
							for($i = 0; $i < count($elements); $i++)
							{
								echo "document.getElementById('".trim($elements[$i]).'tr'."').style.display='none';";
							}							
						}						
						
						echo '
							$.ajax({
   								type: "POST",
   								url: url,
   								data: "ajax=1",
   										success: function(msg){

   										}
   							});

							return false;
						}
						
						</script>';				
					}
					else if ($params['type'] == 'null')
					{
						if($nobr == 0)
							echo '</td><td class="formField">';
						else if(($nobr == 1) && ($previous_nobr == 0))
							echo '</td><td class="formField">';
						else
							echo '&nbsp;';
					}
					else if ( ($params['type'] == 'text') || ($params['type'] == 'password') )
					{
						$params['value'] = Utils::EscapeString($params['value']);
						
						echo '<span class="formFieldCaption">'.$params['caption'].'</span>';
						if(($required == '1') && ($params['readonly'] != '1'))
							echo '<span style="color:red"> *</span>';

						if($nobr == 0)
							echo '</td><td class="formField">';
						else if(($nobr == 1) && ($previous_nobr == 0))
							echo '</td><td class="formField">';
						else
							echo '&nbsp;';
							
						if(($params['size'] != '') && ($params['size'] < 10))
							$maxlength = 'maxlength="'.$params['size'].'"';
						else if($params['maxlength'] != '')
                            $maxlength = 'maxlength="'.$params['maxlength'].'"';
                        else
							$maxlength = '';

                        if(!empty($params['tooltip']))
                        {
                            $tooltip = ' title="'.$params['tooltip'].'" ';
                        }
                        else
                            $tooltip = '';

						if($params['readonly'] == '1')
						{
							echo '<span class="formFieldValue">'.$params['prefix'].' '.$params['value'].' '.$params['postfix'].'</span>';
							echo '<input type=hidden name='.$params['name'].' value="'.$params['value'].'">';
                            echo $params['postfix'];
						}
						else
                        {
                            echo $params['prefix'].'<input class="textInput';
                            if(!empty($params['class']))
                                echo ' '.$params['class'];
                            echo '" '.$type.' '.$name.' '.$id.' '.$size.' '.$maxlength.' value="'.$params['value'].'" '.$readonly.$disabled.' '.$params['inline'].' '.$tooltip.'>'.$params['postfix'];
                        }

						if(!empty($params['autocomplete']))
						{
							echo '<script src="scripts/jquery.autocomplete.js"></script>';
							echo '<script>
    $(document).ready(function(){
								$("#'.$params['id'].'").autocomplete("'.$params['autocomplete'].'", { cacheLength: 0, delay: 200});
								});
							</script>';
						}

                        if(!empty($params['tooltip']))
                        {
                            //echo ' &nbsp;&nbsp;<a id="'.$params['id'].'_tooltip" href="#" '.$tooltip.' style="text-decoration:none;">?</a>';
                            echo '&nbsp;<a id="'.$params['id'].'_tooltip" href="#" '.$tooltip.' style="text-decoration:none;"><img src="images/question.png" style="border:0px;"></a>';

                            echo '<script>
    $(document).ready(function(){
                                $("#'.$params['id'].'_tooltip").tooltip({
                                    track: true,
                                    delay: 0,
                                    showURL: false,
                                    fade: 200

                                });
                                });
                            </script>';
                        }

						if($params['makebr'] == '1')
							echo '<br><br>';
					}
					else  if ($params['type'] == 'file')
					{
						if($params['readonly'] != '1')
						{
							echo '<span class="formFieldCaption">'.$params['caption'].'</span>';
							if($required == '1')
								echo '<span style="color:red"> *</span>';

							if($nobr == 0)
								echo '</td><td class="formField">';
							else if(($nobr == 1) && ($previous_nobr == 0))
								echo '</td><td class="formField">';
							else
								echo '&nbsp;';

							echo '<input class="textInput';
                            if(!empty($params['class']))
                                echo ' '.$params['class'];
                            echo '" type='.$params['type'].' name="'.$params['name'].'" value="'.$params['value'].'">';
						}
					}
					else  if ($params['type'] == 'image')
					{
						if($params['hidden'] != '1')
						{
							echo '<span class="formFieldCaption">'.$params['caption'].'</span>';

							if($nobr == 0)
								echo '</td><td class="formField">';
							else if(($nobr == 1) && ($previous_nobr == 0))
								echo '</td><td class="formField">';
							else
								echo '&nbsp;';
								
							if(!empty($params['ajax_url']))
							{
								echo '<a href="'.$params['ajax_url'].'" onclick="';

								echo 'return checkAjaxURL'.$params['name'].'(\''.$params['url'].'\');"><img src="'.$params['src'].'" title="'.$params['title'].'" style="border-width:0px; vertical-align:middle"></a>';
								echo '<input type=hidden name='.$params['name'].' value="'.$params['value'].'">';
								echo '<script>
								function checkAjaxURL'.$params['name'].'(url)
								{
								';
								
								if($params['confirm'] != '')
									echo ' var answer = confirm("'.$params['confirm'].'"); 
									if(!answer) return false;';
						
								if($params['checkhide'] != '')
								{
									$elements = explode(',', $params['checkhide']);
									for($i = 0; $i < count($elements); $i++)
									{
										echo "document.getElementById('".trim($elements[$i]).'tr'."').style.display='none';";
									}							
								}						
						
								echo '
									$.ajax({
   										type: "POST",
   										url: url,
   										data: "ajax=1",
   												success: function(msg){

   												}
   									});

									return false;
								}
						
								</script>';
							}
							else
							{
								if(!empty($params['target']))
									$target = ' target="'.$params['target'].'"';
								else
									$target = '';								

                                echo $params['prefix'];

								if($params['url'] != '')
									echo '<a id="'.$params['name'].'_a" title="'.$params['title'].'" href="'.$params['url'].'"'.$target.'>';
								echo '<img id="'.$params['name'].'" src="'.$params['src'].'" title="'.$params['title'].'"';
                                if(empty($params['inline']))
                                {
                                    echo ' style="border-width:0px; vertical-align:middle;';
								    if($params['width'] != '')
									    echo 'width:'.$params['width'];
                                }
                                else
                                {
                                    echo '" '.$params['inline'];
                                }
                                echo '">';
								if($params['url'] != '')
									echo '</a>';
                                echo $params['postfix'];
								echo ' '.$params['value'];

                                if($params['box'] == 1)
                                {
                                    echo "
                                    <script>
                                        $(document).ready(function(){
                                            $('#".$params['name']."_a').fancybox({
				                                    'titleShow' : true,
				                                    'type' : 'image'
			                                    });
                                        });
                                    </script>
                                    ";
                                }
							}
						}
					}
					else if ($params['type'] == 'color')
					{
						echo '<span class="formFieldCaption">'.$params['caption'].'</span>';
						if($required == '1')
							echo '<span style="color:red"> *</span>';

						if($nobr == 0)
							echo '</td><td class="formField">';
						else if(($nobr == 1) && ($previous_nobr == 0))
							echo '</td><td class="formField">';
						else
							echo '&nbsp;';
							
						if($params['readonly'] == '1')
						{
							echo $params['prefix'].' '.$params['value'].' '.$params['postfix'];
							echo '<input type=hidden name='.$params['name'].' value="'.$params['value'].'">';
						}
						else
						{
							if(!isset($color_script_included))
							{
								$color_script_included = true;
								echo '<script type="text/javascript" src="scripts/iColorPicker.js"></script>';
							}						

							echo $params['prefix'].'<input class="textInput iColorPicker" '.$name.' '.$id.' size="10" '.$value.$readonly.$disabled.'>'.$params['postfix'];
						}

						if($params['makebr'] == '1')
							echo '<br><br>';
					}
					
					if (!$this->_is_standard_element($params['type']))
					{
						if(method_exists($params['type'].'FormComponent', 'End'))
							call_user_func(array($params['type'].'FormComponent', 'End'), $params);
					}
					else
					{						
						if($nobr == 0)
						{
							echo '</td></tr>';
						}
						else
						{
							if(($was_label) && ($params['type'] != '_label'))
							{
								$was_label = false;
								echo '&nbsp;&nbsp;';
							}
							else if(!$was_label)
								echo '&nbsp;&nbsp;';							
						}
						
						$previous_nobr = $nobr;
					}
				}
				else
				{
					echo '<input type=hidden id='.$params['name'].' name='.$params['name'].' value="'.$params['value'].'">';
				}				
			}

			echo "</table>";
            echo "</form>";

            if($this->validate)
            {
				?>
                  <script>
                  function validateForm<?php echo $this->id; ?>(){
				    $('#errField').remove();
				    $('#<?php echo $this->id; ?> input, #<?php echo $this->id; ?> textarea, #<?php echo $this->id; ?> select').removeClass('redBorder');
				<?php
                for($i = 0; $i < count($this->_required_fields); $i++)
                {
                    if($this->_is_standard_element($this->_required_fields_compnames[$i]))
                    {
                        ?>
                            if($('#<?php echo $this->id; ?> #<?php echo $this->_required_fields[$i]; ?>').val() == '')
                            {
                                if($('#<?php echo $this->id; ?> #<?php echo $this->_required_fields[$i]; ?>').is(':visible'))
                                {
                                    $.scrollTo('#<?php echo $this->id; ?> #<?php echo $this->_required_fields[$i]; ?>', 400, {offset: {top:-70, left:0}});
                                    $('#<?php echo $this->id; ?> #<?php echo $this->_required_fields[$i]; ?>').addClass('redBorder').focus();
                                    var err_txt = '<div class="errField" id="errField">\
                                    <?php
                                        if($this->GetElementAttribute($this->_required_fields[$i], 'required_caption') != '')
                                            echo $this->GetElementAttribute($this->_required_fields[$i], 'required_caption');
                                        else
                                            echo 'Заполните обязательное поле '.$this->_required_fields_names[$i];
                                    ?>\
                                    </div>';
                                    $('body').append(err_txt);
                                    var w = $('#<?php echo $this->id; ?> #<?php echo $this->_required_fields[$i]; ?>').offset().left;
                                    var h = $('#<?php echo $this->id; ?> #<?php echo $this->_required_fields[$i]; ?>').offset().top;
                                    h += 30;
                                    $('#errField').css({left:w+'px',top:h+'px'});
                                    $('#<?php echo $this->id; ?> #<?php echo $this->_required_fields[$i]; ?>').bind('change',function(){
                                        if($(this).val()!=''){
                                            $('#errField').remove();
                                            $(this).removeClass('redBorder');
                                        }
                                    });

                                    return false;
                                }
                            }
                        <?php
                    }
                    else
                    {
                        $params['name'] = $this->_required_fields[$i];
                        call_user_func(array($this->_required_fields_compnames[$i].'FormComponent', 'RequireComponent'), $params);
                    }
                }

                for($i = 0; $i < count($this->_validations); $i++)
                {
                    echo $this->_validations[$i];
                }

                foreach($this->_fields as $field => $params)
                {
                    if(!empty($params['mask']))
                    {
                        if(empty($params['id']))
                            $params['id'] = $params['name'];

                        if($params['mask'] == Form::Digits)
                        {
                            $params['mask'] = '\d+';
                            $params['mask_description'] = 'Допускаются только цифры.';
                        }
                        else if ($params['mask'] == Form::Float)
                        {
                            $params['mask'] = '[\d.]+';
                            $params['mask_description'] = 'Допускается только число с точкой.';
                        }
                        else if ($params['mask'] == Form::Email)
                        {
                            $params['mask'] = '[a-zA-Z0-9\._\-]+@(([a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,6})|([0-9\.\-а-яА-Я]+\.рф))';
                            $params['mask_description'] = 'Проверьте правильность ввода адреса электронной почты!';
                        }
                        ?>
                            if($('#<?php echo $this->id; ?> #<?php echo $params['id']; ?>').val() != '')
                            {
                                if(!$('#<?php echo $this->id; ?> #<?php echo $params['id']; ?>').val().match(/^<?php echo $params['mask']; ?>$/))
                                {
                                    $.scrollTo('#<?php echo $this->id; ?> #<?php echo $params['id']; ?>', 400, {offset: {top:-70, left:0}});
                                    $('#<?php echo $this->id; ?> #<?php echo $params['id']; ?>').addClass('redBorder').focus();
                                    var err_txt = '<div class="errField" id="errField"><?php echo $params['mask_description']; ?></div>';
                                    $('body').append(err_txt);
                                    var w = $('#<?php echo $this->id; ?> #<?php echo $params['id']; ?>').offset().left;
                                    var h = $('#<?php echo $this->id; ?> #<?php echo $params['id']; ?>').offset().top;
                                    h += 30;
                                    $('#errField').css({left:w+'px',top:h+'px'});
                                    $('#<?php echo $this->id; ?> #<?php echo $params['id']; ?>').bind('keypress',function(){
                                        if($(this).val()!=''){
                                            $('#errField').remove();
                                            $(this).removeClass('redBorder');
                                        }
                                    });

                                    return false;
                                }
                            }

                    <?php
                    }

                    if (!$this->_is_standard_element($params['type']))
                        if(method_exists($params['type'].'FormComponent', 'ValidateComponent'))
                            call_user_func(array($params['type'].'FormComponent', 'ValidateComponent'), $params);
                }
                echo 'return true; } </script>';
            }

			foreach($this->_fields as $field => $params)
			{
				if (!$this->_is_standard_element($params['type']))
				{
					if(method_exists($params['type'].'FormComponent', 'EndForm'))
						call_user_func(array($params['type'].'FormComponent', 'EndForm'), $params);
				}
			}	
			
		}
	
	}

