<?php

	class Mailer
	{
		protected $ObjectType = 'MailerObject';
		
		protected $From;
		protected $Message;
        protected $Html = false;
		
		public function Mailer($from = '')
		{
			$this->From = $from;
		}
		
		public function Send($to, $subject = '', $message = '')
		{
			$headers = 'From: '.$this->From."\r\n";
            $header .= "Content-Type: text/plain; charset=UTF-8\r\n";
			
			if(!empty($message))
				$this->Message = $message;

			require_once "libmail.php";

			$m = new Mail();
			$m->autoCheck(false);
			$m->From($this->From);
		    $m->Subject($subject);
		    $m->Body($message, 'UTF-8');
			$m->To($to);
			$m->Send();

			//@mail($to, $subject, $this->Message, $headers);
		}

        public function SendHtml($to, $subject = '', $message = '')
        {
            if(!empty($message))
                $this->Message = $message;

            require_once "libmail.php";

            $m = new Mail();
            $m->autoCheck(false);
            $m->contentType = 'text/html';            
            $m->From($this->From);
            $m->Subject($subject);
            $m->Body($message, 'UTF-8');
            $m->To($to);
            $m->Send();
        }
	}