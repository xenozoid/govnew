<?php

	class DocumentList extends SqlObject
	{
		protected $Page;
		protected $Table;
		protected $Mode;
	
		protected $_sort_type;
		protected $_sorted_field;
	
		function _parse_request()
		{	
			$this->_sort_type = $_REQUEST['st'];
			$this->_sorted_field = $_REQUEST['sf'];
			
			if($_REQUEST['ajax'] == 1)
			{
				$this->_onAjax();
			}
					
			if($_REQUEST['act'] == 'del')
			{
				$this->_onDelete();
			}
			
			if($_REQUEST['act'] == 'new')
			{
				$this->_onNew();
			}
			
			if($_REQUEST['act'] == 'edit')
			{
				$this->_onEdit();
			}
		}
				
		protected function _onNew()
		{
		
		}

		protected function _onEdit()
		{
		
		}
		
		protected function _onDelete()
		{
		
		}

		protected function _onAjax()
		{
		
		}
		
		protected function Show()
		{
		
		}
	}
