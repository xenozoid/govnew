<?php

//ini_set("display_errors", 1);
//error_reporting(E_ALL^E_NOTICE);

require_once "framework_loader.php";
require_once "classes.php";

$response = array();

$auth = new Auth();
$auth->CheckCredentials();
if(!$auth->IsEnabled())
{
    $response['result'] = 'error';
    $response['error'] = 'not authorized';
    echo json_encode($response);
    exit;
}

$response['result'] = 'ok';

switch($_REQUEST['act'])
{
    case 'auth':
        $acl = array();

        if(!ACL::Check(OBJECT_ORDERS, 'allaccess'))
        {
            $ds = new DataSource("select department_id, name from user_department
                    join departments on department_id = departments.id
                    where user_id = ".$_SESSION['user_id']);
            while($data = $ds->GetString())
            {
                $depts[] = $data['department_id'];
            }
            if($depts)
                $acl['departments'] = $depts;
        }

        $uds = new DataSource("select region_id, name from user_region
                join regions on region_id = regions.id
                where user_id = " . $_SESSION['user_id']);
        if($uds->RowCount)
        {
            while($udata = $uds->GetString())
            {
                $regions[] = $udata['region_id'];
            }
            $acl['regions'] = $regions;
        }

        $response['acl'] = $acl;
        $response['name'] = $_SESSION['user_name'];
        break;

    case 'projects':
        $query = new Query("select");
        $query->AddTable("projects");
        $query->AddField("id");
        $query->AddField("name");
        if(!ACL::Check(OBJECT_PROJECTS, 'allaccess'))
        {
            $query->Where->Add("id in (select project_id from project_role where role_id = ".(int)$_SESSION['user_priv'].")");
        }
        $ds = new DataSource($query);
        while($data = $ds->GetString())
            $projects[$data['id']] = $data['name'];
        $response['projects'] = $projects;
        break;

    case 'jobs':
        $ds = new DataSource("select * from jobs");
        while($data = $ds->GetString())
            $jobs[$data['id']] = $data['name'];
        $response['jobs'] = $jobs;
        break;

    case 'departments':
        $query = new Query("select");
        $query->AddTable("departments");
        $query->AddField("id");
        $query->AddField("name");

        if(!ACL::Check(OBJECT_ORDERS, 'allaccess'))
        {
            if(ACL::Check(OBJECT_ORDERS, 'allaccessdept'))
                $query->Where->Add("id in (select department_id from user_department where user_id = ".$_SESSION['user_id'].")");
        }

        $ds = new DataSource($query);
        while($data = $ds->GetString())
            $depts[$data['id']] = $data['name'];
        $response['departments'] = $depts;
        break;

    case 'regions':
        $query = new Query("select");
        $query->AddTable("regions");
        $query->AddField("id");
        $query->AddField("name");

        $uds = new DataSource("select * from user_region where user_id = " . $_SESSION['user_id']);
        if($uds->RowCount)
        {
            $regions = array();
            while($udata = $uds->GetString())
                $regions[] = $udata['region_id'];
            $query->Where->Add("id in (" . implode(',', $regions) . ")");
        }

        $ds = new DataSource($query);
        while($data = $ds->GetString())
            $regions[$data['id']] = $data['name'];
        $response['regions'] = $regions;
        break;

    case 'list':

        $query = new Query("select");
        $query->AddTable("orders
			left outer join users authors on orders.author_id = authors.id
			left outer join departments on orders.department_id = departments.id
			left outer join regions on orders.region_id = regions.id
			left outer join users operators on orders.operator_id = operators.id
			");
        $query->Fields = array("orders.id", "orders.created", "orders.name",
            "orders.source", "orders.type", "orders.answer", "orders.comment", "orders.alert",
            "orders.status", "result", "(select COUNT(*) from comments where module = 'orders' and owner_id = orders.id and high = 1) gubcomment",
            "(select COUNT(*) from comments where module = 'orders' and owner_id = orders.id) comment_count",
            "days", "operator_id", "recall"
            , "departments.shortname department", "regions.name region"
        );

        if(!ACL::Check(OBJECT_ORDERS, 'allaccess'))
        {
            if(ACL::Check(OBJECT_ORDERS, 'allaccessdept'))
                $query->Where->Add("orders.department_id in (select department_id from user_department where user_id = ".$_SESSION['user_id'].")
                    or orders.municipality_id in (select department_id from user_department where user_id = ".$_SESSION['user_id'].")
                    ");
            else
                $query->Where->Add("orders.expert_id = ".$_SESSION['user_id']);
        }

        if(!ACL::Check(OBJECT_PROJECTS, 'allaccess'))
        {
            $query->Where->Add("orders.project_id in (select project_id from project_role where role_id = ".(int)$_SESSION['user_priv'].")");
        }

        $uds = new DataSource("select * from user_region where user_id = " . $_SESSION['user_id']);
        if($uds->RowCount)
        {
            $regions = array();
            while($udata = $uds->GetString())
                $regions[] = $udata['region_id'];
            $query->Where->Add("orders.region_id in (" . implode(',', $regions) . ")");
        }

        $query->Order = "orders.id desc";

        if((int)$_REQUEST['project_id'])
            $query->Where->Add("orders.project_id = " . (int)$_REQUEST['project_id']);

        if((int)$_REQUEST['department_id'])
            $query->Where->Add("orders.department_id = " . (int)$_REQUEST['department_id']);

        if((int)$_REQUEST['region_id'])
            $query->Where->Add("orders.region_id = " . (int)$_REQUEST['region_id']);

        if((int)$_REQUEST['job_id'])
            $query->Where->Add("orders.job_id = " . (int)$_REQUEST['job_id']);

        if(!(int)$_REQUEST['rowlimit'])
            $_REQUEST['rowlimit'] = 10;

        if(!(int)$_REQUEST['page'])
            $_REQUEST['page'] = 1;

        if($_REQUEST['search'])
            $query->Where->Add("orders.name like '%" . mysql_real_escape_string($_REQUEST['search']) . "%'");

        $query->Limit = ($_REQUEST['page'] - 1) * $_REQUEST['rowlimit'] . ', ' . $_REQUEST['rowlimit'];

        $orders = array();
        $ds = new DataSource($query);
        while($data = $ds->GetString())
        {
            $order = array();
            $order['id'] = $data['id'];
            $order['created'] = $data['created'];
            $order['name'] = $data['name'];
            $order['answer'] = $data['answer'];
            $order['department'] = $data['department'];
            $order['days'] = $data['days'];
            $order['region'] = $data['region'];
            $type = new TypeList();
            $order['type'] = $type->Get($data['type']);
            $order['gubcomment'] = $data['gubcomment'];
            $status = new StatusList();
            $order['status'] = $status->Get($data['status']);

            $orders[] = $order;
        }

        $response['total'] = (int)$ds->found_rows_total();
        $response['orders'] = $orders;

        break;

    case 'view':
        if(!(int)$_REQUEST['id'])
        {
            $response['result'] = 'error';
            $response['error'] = 'no id specified';
            echo json_encode($response);
            exit;
        }

        $sql = new SqlObject();
        $data = $sql->getline("select
        id,
        created,
        lastname,
        firstname,
        middlename,
        sex,
        age,
        job_id,
        city,
        region_id,
        address,
        phone,
        phone2,
        email,
        name,
        question,
        answer,
        comment,
        gubcomment,
        department_id,
        type,
        status,
        days

        from orders where id = " . $_REQUEST['id']);

        if(!(int)$data['id'])
        {
            $response['result'] = 'error';
            $response['error'] = 'invalid id specified';
            echo json_encode($response);
            exit;
        }

        $pd = array();
        $pds = new DataSource("select pd from role_pd where role_id = " . $_SESSION['user_priv']);
        while($pdata = $pds->GetString())
            $pd[] = $pdata['pd'];
//        if(in_array('lastname', $pd) === false)
//        {
//            unset($data['lastname']);
//        }
//        if(in_array('firstname', $pd) === false)
//        {
//            unset($data['firstname']);
//        }
//        if(in_array('middlename', $pd) === false)
//        {
//            unset($data['middlename']);
//        }
//        if(in_array('sex', $pd) === false)
//        {
//            unset($data['sex']);
//        }
//        if(in_array('age', $pd) === false)
//        {
//            unset($data['age']);
//        }
//        if(in_array('occupation', $pd) === false)
//        {
//            unset($data['job_id']);
//        }
//        if(in_array('region', $pd) === false)
//        {
//            unset($data['region_id']);
//        }
//        if(in_array('city', $pd) === false)
//        {
//            unset($data['city']);
//        }
//        if(in_array('address', $pd) === false)
//        {
//            unset($data['address']);
//        }
//        if(in_array('phone', $pd) === false)
//        {
//            unset($data['phone']);
//            unset($data['num']);
//        }
//        if(in_array('phone2', $pd) === false)
//        {
//            unset($data['phone2']);
//        }
//        if(in_array('email', $pd) === false)
//        {
//            unset($data['email']);
//        }

        $result = array();
        $result['Дата/время'] = DT::getDateTime($data['created']);
        if(in_array('lastname', $pd) !== false)
            $result['Фамилия'] = $data['lastname'];
        if(in_array('firstname', $pd) !== false)
            $result['Имя'] = $data['firstname'];
        if(in_array('middlename', $pd) !== false)
            $result['Отчество'] = $data['middlename'];
        if(in_array('sex', $pd) !== false)
        {
            $sex = new SexTypeList();
            $result['Пол'] = $sex->Get($data['sex']);
        }
        if(in_array('age', $pd) !== false)
            $result['Возраст'] = $data['age'];
        if(in_array('occupation', $pd) !== false)
        {
            $job = $sql->getline("select name from jobs where id = " . (int)$data['job_id']);
            $result['Род занятий'] = $job['name'];
        }
        if(in_array('city', $pd) !== false)
            $result['Населенный пункт'] = $data['city'];
        if(in_array('region', $pd) !== false)
        {
            $region = $sql->getline("select name from regions where id = " . (int)$data['region_id']);
            $result['Район'] = $region['name'];
        }
        if(in_array('address', $pd) !== false)
            $result['Адрес'] = $data['address'];
        if(in_array('phone', $pd) !== false)
            $result['Телефон'] = $data['phone'];
        if(in_array('phone2', $pd) !== false)
            $result['Дополнительный телефон'] = $data['phone2'];
        if(in_array('email', $pd) !== false)
            $result['Email'] = $data['email'];
        $result['Заголовок'] = $data['name'];
        $result['Вопрос'] = $data['question'];
        $result['Ответ'] = $data['answer'];
        $result['Комментарий'] = $data['comment'];
        $result['Комментарий губернатора'] = $data['gubcomment'];
        $department = $sql->getline("select name from departments where id = " . (int)$data['department_id']);
        $result['Ответственный'] = $department['name'];
        $type = new TypeList();
        $result['Тип'] = $type->Get($data['type']);
        $status = new StatusList();
        $result['Статус'] = $status->Get($data['status']);

        $comments = array();
        $query = new Query("select");
        $query->AddTable("comments left outer join users on comments.author_id = users.id");
        $query->Fields = array("comments.id", "users.name contact", "comments.created", "comments.comment", "comments.high");
        $query->Where->Add("owner_id = ".$_REQUEST['id']." and module = 'orders'");
        $query->Order = 'comments.created desc';
        $ds = new DataSource($query);
        while($data = $ds->GetString())
        {
            $comments[] = $data;
        }

        $resposne['comments'] = $comments;
        $response['data'] = $result;

        break;

    case 'social_position':
        $result = array();
        $query = new Query("select");
        $query->AddTable("orders join jobs on orders.job_id = jobs.id");
        applyFilter($query);
        $query->AddField("jobs.name");
        $query->AddField("COUNT(*) cnt");
        $query->Group = "job_id";
        $query->Order = 'cnt desc';
        $ds = new DataSource($query);
        //$ds = new DataSource("select jobs.name, COUNT(*) cnt from orders join jobs on orders.job_id = jobs.id group by job_id order by cnt desc");
        while($data = $ds->GetString())
        {
            $res = array($data['name'], (int)$data['cnt']);
            $result[] = $res;
        }

        $response['data'] = $result;

        break;

    case 'qthemes':
        $result = array();
        $query = new Query("select");
        $query->AddTable("orders join departments on orders.department_id = departments.id");
        applyFilter($query);
        $query->AddField("departments.name");
        $query->AddField("COUNT(*) cnt");
        $query->Group = "department_id";
        $query->Order = 'cnt desc';
        $ds = new DataSource($query);
        //$ds = new DataSource("select departments.name, COUNT(*) cnt from orders join departments on orders.department_id = departments.id group by department_id order by cnt desc");
        while($data = $ds->GetString())
        {
            $res = array($data['name'], (int)$data['cnt']);
            $result[] = $res;
        }

        $response['data'] = $result;

        break;

    case 'qdistrict':
        $result = array();
        $query = new Query("select");
        $query->AddTable("orders join regions on orders.region_id = regions.id");
        applyFilter($query);
        $query->AddField("regions.name");
        $query->AddField("COUNT(*) cnt");
        $query->Group = "region_id";
        $query->Order = 'cnt desc';
        $ds = new DataSource($query);
        //$ds = new DataSource("select regions.name, COUNT(*) cnt from orders join regions on orders.region_id = regions.id group by region_id order by cnt desc");
        while($data = $ds->GetString())
        {
            $res = array($data['name'], (int)$data['cnt']);
            $result[] = $res;
        }

        $response['data'] = $result;

        break;

    case 'sexage':
        $result = array();
        $menarr_data[]=0-sexage_curr(0, 19, '1');
        $menarr_data[]=0-sexage_curr(20, 24, '1');
        $menarr_data[]=0-sexage_curr(25, 29, '1');
        $menarr_data[]=0-sexage_curr(30, 34, '1');
        $menarr_data[]=0-sexage_curr(35, 39, '1');
        $menarr_data[]=0-sexage_curr(40, 44, '1');
        $menarr_data[]=0-sexage_curr(45, 49, '1');
        $menarr_data[]=0-sexage_curr(50, 54, '1');
        $menarr_data[]=0-sexage_curr(55, 59, '1');
        $menarr_data[]=0-sexage_curr(60, 64, '1');
        $menarr_data[]=0-sexage_curr(65, 69, '1');
        $menarr_data[]=0-sexage_curr(70, 74, '1');
        $menarr_data[]=0-sexage_curr(75, 79, '1');
        $menarr_data[]=0-sexage_curr(80, 84, '1');
        $menarr_data[]=0-sexage_curr(85, 89, '1');
        $menarr_data[]=0-sexage_curr(90, 200, '1');

        $wmenarr_data[]=sexage_curr(0, 19, '2');
        $wmenarr_data[]=sexage_curr(20, 24, '2');
        $wmenarr_data[]=sexage_curr(25, 29, '2');
        $wmenarr_data[]=sexage_curr(30, 34, '2');
        $wmenarr_data[]=sexage_curr(35, 39, '2');
        $wmenarr_data[]=sexage_curr(40, 44, '2');
        $wmenarr_data[]=sexage_curr(45, 49, '2');
        $wmenarr_data[]=sexage_curr(50, 54, '2');
        $wmenarr_data[]=sexage_curr(55, 59, '2');
        $wmenarr_data[]=sexage_curr(60, 64, '2');
        $wmenarr_data[]=sexage_curr(65, 69, '2');
        $wmenarr_data[]=sexage_curr(70, 74, '2');
        $wmenarr_data[]=sexage_curr(75, 79, '2');
        $wmenarr_data[]=sexage_curr(80, 84, '2');
        $wmenarr_data[]=sexage_curr(85, 89, '2');
        $wmenarr_data[]=sexage_curr(90, 200, '2');

        $arr_d[] = array('name'=>'Мужчины', 'data'=>$menarr_data);
        $arr_d[] = array('name'=>'Женщины', 'data'=>$wmenarr_data);
        $arr['dat']=$arr_d;

        $men_max = max($menarr_data);
        $wmen_max = max($wmenarr_data);
        if ($men_max>$wmen_max){
            $maxv = $men_max;
        } else {
            $maxv = $wmen_max;
        }
        $maxv+=($maxv)*0.1;
        $arr['lrange']=(double)(0-$maxv);
        $arr['rrange']=(double)($maxv);

        $response['data'] = $arr;

        break;

    case 'overall':

//        if((int)$_REQUEST['department_id'])
//            $aWhere = " and department_id = " . (int)$_REQUEST['department_id'];

        $query = new Query("select");
        $query->AddTable("orders");
        applyFilter($query);
        $query->Where->Add("status != 20 and created > 1309464000 and days > 0");
        if((int)$_REQUEST['department_id'])
            $query->Where->Add("department_id = " . (int)$_REQUEST['department_id']);
        $query->AddField("count(id) co");
        $ds = new DataSource($query);
        $row_wrk = $ds->GetString();

//        $bquery_wrk = 'SELECT count(id) as co FROM `orders` where status not like \'20\' and created>1309464000 and days>0'.$aWhere;

        $query = new Query("select");
        $query->AddTable("orders");
        applyFilter($query);
        $query->Where->Add("status = 20 and created > 1309464000 and days > 0");
        if((int)$_REQUEST['department_id'])
            $query->Where->Add("department_id = " . (int)$_REQUEST['department_id']);
        $query->AddField("count(id) co");
        $ds = new DataSource($query);
        $row_done = $ds->GetString();

//        $bquery_done = 'SELECT count(id) as co FROM `orders` where status=\'20\' and created>1309464000 and days>0'.$aWhere;

        $query = new Query("select");
        $query->AddTable("orders");
        applyFilter($query);
        $query->Where->Add("status != 20 and created > 1309464000 and days < 0");
        if((int)$_REQUEST['department_id'])
            $query->Where->Add("department_id = " . (int)$_REQUEST['department_id']);
        $query->AddField("count(id) co");
        $ds = new DataSource($query);
        $row_wrk_overtimed = $ds->GetString();

        //$bquery_wrk_overtimed = 'SELECT count(id) as co FROM `orders` where status not like \'20\' and created>1309464000 and days<0'.$aWhere;

        $query = new Query("select");
        $query->AddTable("orders");
        applyFilter($query);
        $query->Where->Add("status = 20 and created > 1309464000 and days < 0");
        if((int)$_REQUEST['department_id'])
            $query->Where->Add("department_id = " . (int)$_REQUEST['department_id']);
        $query->AddField("count(id) co");
        $ds = new DataSource($query);
        $row_done_overtimed = $ds->GetString();

        //$bquery_done_overtimed = 'SELECT count(id) as co FROM `orders` where status=\'20\' and created>1309464000 and days<0'.$aWhere;

        $sql = new SqlObject();

        //$row_wrk = $sql->getline($bquery_wrk);
        //$row_done = $sql->getline($bquery_done);
        //$row_wrk_overtimed = $sql->getline($bquery_wrk_overtimed);
        //$row_done_overtimed = $sql->getline($bquery_done_overtimed);
        $ovrall_quant = (double)$row_wrk['co']+(double)$row_wrk_overtimed['co']+(double)$row_done['co']+(double)$row_done_overtimed['co'];

        if($row_wrk['co']){
            $out_arr[] = array('name'=>'В работе '.$row_wrk['co'], 'y'=>(double)number_format(($row_wrk['co']/$ovrall_quant)*100,0,',',''), 'color'=>'#4572A7');
        }
        if($row_wrk_overtimed['co']){
            $out_arr[] = array('name'=>'В работе, срок превышен '.$row_wrk_overtimed['co'], 'y'=>(double)number_format(($row_wrk_overtimed['co']/$ovrall_quant)*100,0,',',''), 'color'=>'#AA4643');
        }
        if($row_done['co']){
            $out_arr[] = array('name'=>'Обработано в срок '.$row_done['co'], 'y'=>(double)number_format(($row_done['co']/$ovrall_quant)*100,0,',',''), 'color'=>'#89A54E');
        }
        if($row_done_overtimed['co']){
            $out_arr[] = array('name'=>'Обработано c превышением сроков '.$row_done_overtimed['co'], 'y'=>(double)number_format(($row_done_overtimed['co']/$ovrall_quant)*100,0,',',''), 'color'=>'#DB843D');
        }

        $response['data'] = $out_arr;

        break;

    case 'last_query':
//        $sql = new SqlObject();
        $query = new Query("select");
        $query->AddTable("orders join regions on orders.region_id = regions.id");
        applyFilter($query);
        $query->AddField("regions.name region");
        $query->AddField("orders.name title");
        $query->Order = "orders.id desc";
        $query->Limit = 1;
        $ds = new DataSource($query);
        $data = $ds->GetString();
        //$data = $sql->getline("select regions.name region, orders.name title from orders join regions on orders.region_id = regions.id order by orders.id desc limit 1");
        $response['data'] = array($data['region'], $data['title']);
        break;

    case 'stats':
        //$sql = new SqlObject();
        $result = array();
        $query = new Query("select");
        $query->AddTable("orders");
        applyFilter($query);
        $query->AddField("COUNT(*) cnt");
        $query->Where->Add("created > " . DT::getUnixTime(DT::getDate(DT::Now)));
        $ds = new DataSource($query);
        //$data = $sql->getline("select COUNT(*) cnt from orders where created > " . DT::getUnixTime(DT::getDate(DT::Now)));
        $data = $ds->GetString();
        $result[] = array('За сегодня', (int)$data['cnt']);

        $query = new Query("select");
        $query->AddTable("orders");
        applyFilter($query);
        $query->AddField("COUNT(*) cnt");
        $query->Where->Add("created > " . (DT::getUnixTime(DT::getDate(DT::Now)) - 24*60*60) . " and created < " . DT::getUnixTime(DT::getDate(DT::Now)));
        $ds = new DataSource($query);
        $data = $ds->GetString();
        //$data = $sql->getline("select COUNT(*) cnt from orders where created > " . (DT::getUnixTime(DT::getDate(DT::Now)) - 24*60*60) . " and created < " . DT::getUnixTime(DT::getDate(DT::Now)));
        $result[] = array('Вчера', (int)$data['cnt']);

        $query = new Query("select");
        $query->AddTable("orders");
        applyFilter($query);
        $query->AddField("COUNT(*) cnt");
        $query->Where->Add("created > " . (DT::getUnixTime(DT::getDate(DT::Now)) - 7*24*60*60));
        $ds = new DataSource($query);
        $data = $ds->GetString();
        //$data = $sql->getline("select COUNT(*) cnt from orders where created > " . (DT::getUnixTime(DT::getDate(DT::Now)) - 7*24*60*60));
        $result[] = array('За 7 дней', (int)$data['cnt']);

        $query = new Query("select");
        $query->AddTable("orders");
        applyFilter($query);
        $query->AddField("COUNT(*) cnt");
        $query->Where->Add("created > " . (DT::getUnixTime(DT::getDate(DT::Now)) - 30*24*60*60));
        $ds = new DataSource($query);
        $data = $ds->GetString();
        //$data = $sql->getline("select COUNT(*) cnt from orders where created > " . (DT::getUnixTime(DT::getDate(DT::Now)) - 30*24*60*60));
        $result[] = array('За 30 дней', (int)$data['cnt']);

        $query = new Query("select");
        $query->AddTable("orders");
        applyFilter($query);
        $query->AddField("COUNT(*) cnt");
        $query->Where->Add("created > " . (DT::getUnixTime(DT::getDate(DT::Now)) - 365*24*60*60));
        $ds = new DataSource($query);
        $data = $ds->GetString();
        //$data = $sql->getline("select COUNT(*) cnt from orders where created > " . (DT::getUnixTime(DT::getDate(DT::Now)) - 365*24*60*60));
        $result[] = array('За год', (int)$data['cnt']);

        $query = new Query("select");
        $query->AddTable("orders");
        applyFilter($query);
        $query->AddField("COUNT(*) cnt");
        //$query->Where->Add("created > " . (DT::getUnixTime(DT::getDate(DT::Now)) - 365*24*60*60));
        $ds = new DataSource($query);
        $data = $ds->GetString();
        //$data = $sql->getline("select COUNT(*) cnt from orders");
        $result[] = array('Всего', (int)$data['cnt']);

        $response['data'] = $result;
        break;
}

echo json_encode($response);

function sexage_curr($a1, $a2, $sex)
{
    //$sql = new SqlObject();
    $query = new Query("select");
    $query->AddField("count(id) cnt");
    $query->AddTable("orders");
    $query->Where->Add("(age between ".$a1." and ".$a2.") and sex= " . $sex);
    applyFilter($query);

    //$data = $sql->getline('SELECT count(id) cnt FROM orders where (age between '.$a1.' and '.$a2.') and sex=\''.$sex.'\'');

    $ds = new DataSource($query);
    $data = $ds->GetString();

    //echo $query->Get() . '<br><br>';

    return (double)$data['cnt'];
}

function applyFilter($query)
{
    if(!ACL::Check(OBJECT_ORDERS, 'allaccess'))
    {
        if(ACL::Check(OBJECT_ORDERS, 'allaccessdept'))
            $query->Where->Add("orders.department_id in (select department_id from user_department where user_id = ".$_SESSION['user_id'].")
                    or orders.municipality_id in (select department_id from user_department where user_id = ".$_SESSION['user_id'].")
                    ");
        else
            $query->Where->Add("orders.expert_id = ".$_SESSION['user_id']);
    }

    if(!ACL::Check(OBJECT_PROJECTS, 'allaccess'))
    {
        $query->Where->Add("orders.project_id in (select project_id from project_role where role_id = ".(int)$_SESSION['user_priv'].")");
    }

    $uds = new DataSource("select * from user_region where user_id = " . $_SESSION['user_id']);
    if($uds->RowCount)
    {
        $regions = array();
        while($udata = $uds->GetString())
            $regions[] = $udata['region_id'];
        $query->Where->Add("orders.region_id in (" . implode(',', $regions) . ")");
    }

    return $query;
}