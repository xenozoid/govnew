<?php

$path = '/var/www/vhosts/gov29/';

require_once $path."config.php";
require_once $path."framework/ACL.php";
require_once $path."framework/Debug.php";
require_once $path."framework/DT.php";
require_once $path."framework/SqlObject.php";
require_once $path."framework/DataSource.php";
require_once $path."framework/Query.php";
require_once $path."framework/Where.php";
require_once $path."framework/Log.php";
require_once $path."framework/ValueList.php";
require_once $path."framework/Registry.php";
require_once $path."framework/SelectList.php";
require_once $path."framework/PHPMailer.php";
require_once $path."framework/class.smtp.php";
require_once $path."classes.php";

ini_set('display_errors', true);
error_reporting(E_ALL^E_NOTICE);

$all_depts = array();

$ds = new DataSource("select id from departments");
while($data = $ds->GetString())
    $all_depts[] = $data['id'];

$ds = new DataSource("select distinct user_id id, priv, email
from user_department
join users on user_department.user_id = users.id
where (email != '' and email is not null)
");
while($user = $ds->GetString())
{
    $user_depts = array();

    if(ACL::Check(OBJECT_ORDERS, 'reportalldaily', $user['priv']))
    {
        $user_depts = $all_depts;
    }
    else if(ACL::Check(OBJECT_ORDERS, 'reportdaily', $user['priv']))
    {
        $depts = new DataSource("select distinct department_id id from user_department where user_id = ".$user['id']);
        while($dept = $depts->GetString())
        {
            $user_depts[] = $dept['id'];
        }
    }

    if($user_depts)
    {
        $report = OrderUtils::getOrdersReport(0, $user_depts);
        if($report['count'])
            Email::send($user['email'], 'Отчет о состоянии обработки запросов граждан на Прямую линию Правительства Архангельской области по состоянию на '.date('d.m.Y').'г.', $report['content']);
    }
}

