<?php
  if(!class_exists('AGI_AsteriskManager'))
  {
    require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'asm.php');
  }

  class AGI
  {
   var $request;
   var $config;
   var $asmanager;
   var $in = NULL;
   var $out = NULL;
   var $socket = NULL;
   var $audio = NULL;
   function AGI($config=NULL, $optconfig=array(), $socket=NULL)
    {
      if(!is_null($config) && file_exists($config))
        $this->config = parse_ini_file($config, true);
      elseif(file_exists(DEFAULT_PHPAGI_CONFIG))
        $this->config = parse_ini_file(DEFAULT_PHPAGI_CONFIG, true);
      foreach($optconfig as $var=>$val)
        $this->config['phpagi'][$var] = $val;
      if(!isset($this->config['phpagi']['error_handler'])) $this->config['phpagi']['error_handler'] = true;
      if(!isset($this->config['phpagi']['debug'])) $this->config['phpagi']['debug'] = false;
      if(!isset($this->config['phpagi']['admin'])) $this->config['phpagi']['admin'] = NULL;
      if(!isset($this->config['phpagi']['tempdir'])) $this->config['phpagi']['tempdir'] = AST_TMP_DIR;
      ob_implicit_flush(true);
      if(is_null($socket))
      {
        $this->in  = defined('STDIN')  ? STDIN  : fopen('php://stdin',  'r');
        $this->out = defined('STDOUT') ? STDOUT : fopen('php://stdout', 'w');
      }
      else
        $this->socket = $socket;
      if($this->config['phpagi']['error_handler'] == true)
      {
        set_error_handler('phpagi_error_handler');
        global $phpagi_error_handler_email;
        $phpagi_error_handler_email = $this->config['phpagi']['admin'];
        error_reporting(E_ALL);
      }
      $this->make_folder($this->config['phpagi']['tempdir']);
      $str = is_null($this->socket) ? fgets($this->in) : socket_read($this->socket, 4096, PHP_NORMAL_READ);
      while($str != "\n")
      {
        $this->request[substr($str, 0, strpos($str, ':'))] = trim(substr($str, strpos($str, ':') + 1));
        $str = is_null($this->socket) ? fgets($this->in) : socket_read($this->socket, 4096, PHP_NORMAL_READ);
      }
      if($this->request['agi_enhanced'] == '1.0')
      {
        if(file_exists('/proc/' . getmypid() . '/fd/3'))
        {
          $this->audio = fopen('/proc/' . getmypid() . '/fd/3', 'r');
        }
        elseif(file_exists('/dev/fd/3'))
        {
          $this->audio = fopen('/dev/fd/3', 'r');
        }
        else
          $this->conlog('Unable to open audio stream');

        if($this->audio) stream_set_blocking($this->audio, 0);
      }
      $this->conlog('PHPAGI internal configuration:');
      $this->conlog(print_r($this->config, true));
    }

    function exec($application, $options)
    {
      if(is_array($options)) $options = join('|', $options);
      return $this->evaluate("EXEC $application $options");
    }

  
    function noop()
    {
      return $this->evaluate('NOOP');
    }

    function verbose($message, $level=1)
    {
      foreach(explode("\n", str_replace("\r\n", "\n", print_r($message, true))) as $msg)
      {
        $ret = $this->evaluate("VERBOSE \"$msg\" $level");
      }
      return $ret;
    }

 
    function exec_absolutetimeout($seconds=0)
    {
      return $this->exec('AbsoluteTimeout', $seconds);
    }
    function exec_agi($command, $args)
    {
      return $this->exec("AGI $command", $args);
    }



    function &new_AsteriskManager()
    {
      $this->asm = new AGI_AsteriskManager(NULL, $this->config);
      $this->asm->pagi =& $this;
      $this->config =& $this->asm->config;
      return $this->asm;
    }

    function evaluate($command)
    {
      $broken = array('code'=>500, 'result'=>-1, 'data'=>'');
      if(is_null($this->socket))
      {
        if(!@fwrite($this->out, trim($command) . "\n")) return $broken;
        fflush($this->out);
      }
      elseif(socket_write($this->socket, trim($command) . "\n") === false) return $broken;
      $count = 0;
      do
      {
        $str = is_null($this->socket) ? @fgets($this->in, 4096) : @socket_read($this->socket, 4096, PHP_NORMAL_READ);
      } while($str == '' && $count++ < 5);

      if($count >= 5)
      {
        return $broken;
      }

      $ret['code'] = substr($str, 0, 3);
      $str = trim(substr($str, 3));

      if($str{0} == '-') 
      {
        $count = 0;
        $str = substr($str, 1) . "\n";

        $line = is_null($this->socket) ? @fgets($this->in, 4096) : @socket_read($this->socket, 4096, PHP_NORMAL_READ);
        while(substr($line, 0, 3) != $ret['code'] && $count < 5)
        {
          $str .= $line;
          $line = is_null($this->socket) ? @fgets($this->in, 4096) : @socket_read($this->socket, 4096, PHP_NORMAL_READ);
          $count = (trim($line) == '') ? $count + 1 : 0;
        }
        if($count >= 5)
        {
          return $broken;
        }
      }

      $ret['result'] = NULL;
      $ret['data'] = '';
      if($ret['code'] != AGIRES_OK)
      {
        $ret['data'] = $str;
        $this->conlog(print_r($ret, true));
      }
      else
      {
        $parse = explode(' ', trim($str));
        $in_token = false;
        foreach($parse as $token)
        {
          if($in_token) 
          {
	    $tmp = trim($token);
	    $tmp = $tmp{0} == '(' ? substr($tmp,1):$tmp;
	    $tmp = substr($tmp,-1) == ')' ? substr($tmp,0,strlen($tmp)-1):$tmp;
	    $ret['data'] .= ' ' . trim($tmp);
            if($token{strlen($token)-1} == ')') $in_token = false;
          }
          elseif($token{0} == '(')
          {
            if($token{strlen($token)-1} != ')') $in_token = true;
	    $tmp = trim(substr($token,1));
	    $tmp = $in_token ? $tmp : substr($tmp,0,strlen($tmp)-1);
	    $ret['data'] .= ' ' . trim($tmp);
          }
          elseif(strpos($token, '='))
          {
            $token = explode('=', $token);
            $ret[$token[0]] = $token[1];
          }
          elseif($token != '')
            $ret['data'] .= ' ' . $token;
        }
        $ret['data'] = trim($ret['data']);
      }

      if($ret['result'] < 0)
        $this->conlog("$command returned {$ret['result']}");
      return $ret;
    }

    function conlog($str, $vbl=1)
    {
      static $busy = false;

      if($this->config['phpagi']['debug'] != false)
      {
        if(!$busy) 
        {
          $busy = true;
          $this->verbose($str, $vbl);
          $busy = false;
        }
      }
    }

    function which($cmd, $checkpath=NULL)
    {
      global $_ENV;
      $chpath = is_null($checkpath) ? $_ENV['PATH'] : $checkpath;

      foreach(explode(PATH_SEPERATOR, $chpath) as $path)
        if(!function_exists('is_executable') || is_executable($path . DIRECTORY_SEPERATOR . $cmd))
          return $path . DIRECTORY_SEPERATOR . $cmd;

      if(is_null($checkpath))
      {
        if(substr(strtoupper(PHP_OS, 0, 3)) != 'WIN')
          return $this->which($cmd, '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:'.
                                    '/usr/X11R6/bin:/usr/local/apache/bin:/usr/local/mysql/bin');
      }
      return false;
    }

    function make_folder($folder, $perms=0755)
    {
      $f = explode(DIRECTORY_SEPARATOR, $folder);
      $base = '';
      for($i = 0; $i < count($f); $i++)
      {
        $base .= $f[$i];
        if($f[$i] != '' && !file_exists($base))
          mkdir($base, $perms);
        $base .= DIRECTORY_SEPARATOR;
      }
    }	
  }

  function phpagi_error_handler($level, $message, $file, $line, $context)
  {
    if(ini_get('error_reporting') == 0) return; // this happens with an @

    @syslog(LOG_WARNING, $file . '[' . $line . ']: ' . $message);

    global $phpagi_error_handler_email;
    if(function_exists('mail') && !is_null($phpagi_error_handler_email)) 
    {
      switch($level)
      {
        case E_WARNING:
        case E_USER_WARNING:
          $level = "Warning";
          break;
        case E_NOTICE:
        case E_USER_NOTICE:
          $level = "Notice";
          break;
        case E_USER_ERROR:
          $level = "Error";
          break;
      }

      $basefile = basename($file);
      $subject = "$basefile/$line/$level: $message";
      $message = "$level: $message in $file on line $line\n\n";

      if(function_exists('mysql_errno') && strpos(' '.strtolower($message), 'mysql'))
        $message .= 'MySQL error ' . mysql_errno() . ": " . mysql_error() . "\n\n";

      if(function_exists('socket_create'))
      {
        $addr = NULL;
        $port = 80;
        $socket = @socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        @socket_connect($socket, '64.0.0.0', $port);
        @socket_getsockname($socket, $addr, $port);
        @socket_close($socket);
        $message .= "\n\nIP Address: $addr\n";
      }

      $message .= "\n\nContext:\n" . print_r($context, true);
      $message .= "\n\nGLOBALS:\n" . print_r($GLOBALS, true);
      $message .= "\n\nBacktrace:\n" . print_r(debug_backtrace(), true);

      if(file_exists($file))
      {
        $message .= "\n\n$file:\n";
        $code = @file($file);
        for($i = max(0, $line - 10); $i < min($line + 10, count($code)); $i++)
          $message .= ($i + 1)."\t$code[$i]";
      }

      $ret = '';
      for($i = 0; $i < strlen($message); $i++)
      {
        $c = ord($message{$i});
        if($c == 10 || $c == 13 || $c == 9)
          $ret .= $message{$i};
        elseif($c < 16)
          $ret .= '\x0' . dechex($c);
        elseif($c < 32 || $c > 127)
          $ret .= '\x' . dechex($c);
        else
          $ret .= $message{$i};
      }
      $message = $ret;

      static $mailcount = 0;
      if($mailcount < 5)
        @mail($phpagi_error_handler_email, $subject, $message);
      $mailcount++;
    }
  }
  $phpagi_error_handler_email = NULL;
?>
