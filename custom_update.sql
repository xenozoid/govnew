ALTER TABLE projects ADD active INT NOT NULL DEFAULT 1;
ALTER TABLE projects ADD department_id INT;
ALTER TABLE projects ADD job_id INT;
ALTER TABLE projects ADD service VARCHAR(255);

INSERT INTO object_types (id, name, code, group_id, editable, weight, module, icon) VALUES
(100,	'Организации',	'OBJECT_ORGANIZATIONS',	1,	0,	0,	'organizations',	NULL),
(101,	'Услуги',	'OBJECT_SERVICES',	1,	0,	0,	'services',	NULL);