<?php
	require("framework_loader.php");
	require("classes.php");

	$object = new PageProducer('Восстановление пароля', false);
	$object->Begin();
	
	$sql = new SqlObject();
	
	if(!empty($_REQUEST['user_data']))
	{
		if(strpos($_REQUEST['user_data'], '@'))
		{
			$data = $sql->getline("select id from users where email = '".$_REQUEST['user_data']."'");
            $id = $data['id'];
		}
		else
		{
			$data = $sql->getline("select id from users where login = '".$_REQUEST['user_data']."'");
            $id = $data['id'];
		}

		if($id)
		{
            $data = $sql->getline("select * from users where id = ".$id);
            $role = $sql->getline("select name from roles where id = ".$data['priv']);
            if($data['email'])
            {
                $content = 'Добрый день,<br><br>Вам был предоставлен доступ в систему обработки запросов граждан на Прямую линию Правительства Архангельской области<br>';
                if((DT::getDate($data['active_from'])) || (DT::getDate($data['active_to'])))
                {
                    $content .= '<strong><u>на период ';
                    if(DT::getDate($data['active_from']))
                        $content .= ' с '.DT::getDate($data['active_from']);
                    if(DT::getDate($data['active_to']))
                        $content .= ' по '.DT::getDate($data['active_to']);
                    $content .= '</u></strong>';
                }
                $content .= 'Адрес: <a href="http://gov29.firecall.net">http://gov29.firecall.net</a><br>';
                $content .= 'Имя: '.$data['login'].'<br>';
                $content .= 'Пароль: '.$data['password'].'<br>';
                $content .= 'Роль: '.$role['name'].'<br>';

                $ds = new DataSource("select departments.name
                from user_department join departments
                on user_department.department_id = departments.id
                where user_id = ".$_SESSION['user_id']);
                while($dept = $ds->GetString())
                {
                    $depts[] = $dept['name'];
                }
                if($depts)
                    $content .= 'Доступные разделы: '.implode(', ', $depts).'<br>';

                $content .= '<br><br>--<br>Центр обработки вызовов Прямой линии Правительства Архангельской области';

                Email::send($data['email'], 'Реквизиты доступа с системе обработки запросов граждан на Прямую линию Правительства Архангельской области', $content);
            }

			$recovered = true;
		}
	}
	
	if($recovered)
	{
		?>
Ваш пароль восстановлен и отправлен по email.<br><br><br>
<a href="index.php">войти в систему</a> 
		<?php 
	}
	else
	{
		$form = new Form('Восстановление пароля', array(
			array('caption' => 'Логин или email', 'type' => 'text', 'name' => 'user_data', 'size' => '50'), 	
			array('type' => 'submit', 'name' => 'submitBtn', 'value' => 'Восстановить')
		));
		$form->Show();
	}	
	
	$object->End();
