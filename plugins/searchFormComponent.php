<?php

	class searchFormComponent extends FormComponent
	{	
		public function Begin($params)
		{
			echo '<tr id="'.$params['name'].'tr"><td class="formField"><span class="formFieldCaption">'.$params['caption'].'</span>';

            if($params['required'])
            {
                echo '<span style="color:red"> *</span>';
            }

            echo '</td>';
		}

        private static function showReadOnly($params)
        {
            echo '<td class="formField">';

            if(!empty($params['value']))
            {
                for($i = 0; $i < count($params['keys']); $i++)
                {
                    if($params['value'] == $params['values'][$i])
                    {
                        echo $params['prefix'].$params['keys'][$i].$params['postfix'];
                        break;
                    }
                }
            }

            echo '</td>';
        }
		
		public function Show($params)
		{
            if($params['readonly'])
            {
                searchFormComponent::showReadOnly($params);
                return;
            }
            
			?>
			<td>
            <?php
            ?>
			<script>

                function open<?php echo ucfirst($params['name']); ?>Dialog()
                {
                    $('#errField').remove();
                    $('#<?php echo $params['name']; ?>-item').removeClass('redBorder');
                    $('#<?php echo $params['name']; ?>-window').show();
                    $('#<?php echo $params['name']; ?>-name')[0].focus();
                }

                function select<?php echo ucfirst($params['name']); ?>Item()
                {
                    var id = $('#<?php echo $params['name']; ?>-list').val();
                    var text = $('#<?php echo $params['name']; ?>-list :selected').text();
                    $('#<?php echo $params['name']; ?>-item').html('<a href="index.php?module=<?php echo $params['module']; ?>&act=view&id=' + id + '" target="_blank">' + shortString(text) + '</a>');
                    $('#<?php echo $params['name']; ?>').val(id);
                    $('#<?php echo $params['name']; ?>-window').hide();
                }

                $(document).ready(function(){
                    $('#<?php echo $params['name']; ?>-list').bind('dblclick', select<?php echo ucfirst($params['name']); ?>Item);
                    $('#btn<?php echo ucfirst($params['name']); ?>DialogOk').bind('click', select<?php echo ucfirst($params['name']); ?>Item);
                    $('#btn<?php echo ucfirst($params['name']); ?>DialogClose').bind('click', function(){$('#<?php echo $params['name']; ?>-window').hide();});
                    $('#<?php echo $params['name']; ?>-name').bind('keyup',
                        {
                            sourceField: '<?php echo $params['name']; ?>-name',
                            targetField: '<?php echo $params['name']; ?>-list',
                            url : 'index.php?module=%<?php echo $params['name']; ?>-module%&act=ajax&name=%id%',
                            emptyOption: '',
                            emptyOptionSuccess: ''
                        },
                        changeHandler);

<?php
    if($params['value'])
    {
        ?>
                    $.ajax({
                        type: 'post',
                        url:  'index.php?module=<?php echo $params['module']; ?>&act=ajax&id=<?php echo $params['value']; ?>',
                        success: function(value)
                        {
                            $('#<?php echo $params['name']; ?>-item').html('<a href="index.php?module=<?php echo $params['module']; ?>&act=view&id=<?php echo $params['value']; ?>" target="_blank">' + value + '</a>');
                        }
                    });
        <?php
    }
?>
                });
			</script>

            <div id="<?php echo $params['name']; ?>-window" class="searchDialog">
                <table>
                    <tr>
                        <td>
                            <input type="text" id="<?php echo $params['name']; ?>-name" class="textInput" style="width:240px;">
                            <input type="button" id="btn<?php echo ucfirst($params['name']); ?>DialogOk" class="submitButton" style="width:70px;" value="OK">
                            <input type="button" id="btn<?php echo ucfirst($params['name']); ?>DialogClose" class="submitButton" style="width:70px;" value="Закрыть">
                       </td>
                    </tr>
                    <tr>
                        <td>
                            <select id="<?php echo $params['name']; ?>-list" size="20" style="width:400px;">
                                <option style="color:silver; font-weight:normal;">Введите значение для выбора из списка</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <?php

            ?>
            <span id="<?php echo $params['name']; ?>-item" class="emptySearchItem"><a href="javascript:open<?php echo ucfirst($params['name']); ?>Dialog()">выберите значение...</a></span>
            <img src="images/magnifier-medium.png" title="Найти в списке" alt="Найти в списке" style="vertical-align:middle;" onclick="open<?php echo ucfirst($params['name']); ?>Dialog()">
            <input type="hidden" name="<?php echo $params['name']; ?>" id="<?php echo $params['name']; ?>" value="<?php echo $params['value']; ?>">
            <input type="hidden" name="<?php echo $params['name']; ?>-module" id="<?php echo $params['name']; ?>-module" value="<?php echo $params['module']; ?>">
			<?php
		}
		
		public function RequireComponent($params)
		{
			?>
                if($('#<?php echo $params['name']; ?>').is(':visible'))
                if($('#<?php echo $params['name']; ?>').val() == '' || $('#<?php echo $params['name']; ?>').val() == 0)
                {
                    $.scrollTo('#<?php echo $params['name']; ?>-item', 400, {offset: {top:-70, left:0}});
                    $('#<?php echo $params['name']; ?>-item').addClass('redBorder');
                    var err_txt = '<div class="errField" id="errField">\
                    Пожалуйста, выберите значение из списка!\
                    </div>';
                    $('body').append(err_txt);
                    var w = $('#<?php echo $params['name']; ?>-item').offset().left;
                    var h = $('#<?php echo $params['name']; ?>-item').offset().top;
                    h += 30;
                    $('#errField').css({left:w+'px',top:h+'px'});

                    return false;
                }
			<?php
		}
		
		public function End($params)
		{
			echo '</td></tr>';
		}
	}