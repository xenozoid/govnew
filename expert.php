<?php

require_once "framework_loader.php";
require_once "classes.php";

if($_REQUEST['act'] == 'thanks')
{
    $page = new PageProducer("Благодарим за оставленный ответ!", false);
    $page->ShowMenu = false;
    $page->Begin();

?>
        Благодарим за оставленный ответ!
<?php
    $page->End();
    exit;
}

if($_REQUEST['act'] == 'close')
{
    $page = new PageProducer("Благодарим за ознакомление с вопросом!", false);
    $page->ShowMenu = false;
    $page->Begin();

?>
        Благодарим за ознакомление с вопросом!
<?php
    $page->End();
    exit;
}

$code = (int)$_REQUEST['code'];

$sql = new SqlObject();
$data = $sql->getline("select id, status, expert_id from orders where code = '".$code."'");
if($data['id'])
{
    if($data['status'] == 10)
    {
        $user = $sql->getline("select * from users where id = ".$data['expert_id']);
        $_REQUEST['login'] = $user['login'];
        $_REQUEST['password'] = $user['password'];
        $_REQUEST['loggedin'] = 1;
        $auth = new Auth();
        $auth->CheckCredentials();
        $_SESSION['expert_id'] = $user['id'];
        header('Location: /index.php?module=orders&act=edit&id='.$data['id']);
    }
    else
    {
        $page = new PageProducer("Этот вопрос уже не находится на стадии делегирования!", false);
        $page->ShowMenu = false;
        $page->Begin();
?>
            Этот вопрос уже не находится на стадии делегирования!
    <?php
        $page->End();
    }
}
else
{
        $page = new PageProducer("Неправильная ссылка!", false);
        $page->ShowMenu = false;
        $page->Begin();
?>
            Неправильная ссылка!
<?php

        $page->End();
}
