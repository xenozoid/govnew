/* Author: Andrew KLADOV
 * Description: Ami request for ingoing call info, parse returned data.
 * Date: 19.10.2012
 * DEV_ID: GAU_2012
 *                 |-->BRANCH: ASTERISK
 *                                  |-->BRANCH: INGOING
 */
$(document).ready(              
  function(){
      
    jQuery.fx.off = true  
      
    setInterval(function(){
    //  $(document).everyTime(4000, "amiAsk", function(){
    console.log('interval')      
        
        $.ajax({
        url: 'request.php',
        type: "POST",
        data: {act: 'get_incoming',ext: $("#exten").val()},
        async: true,
        dataType: "json",
        success: function(msg){
           // if (msg!=null){
           if (msg.hasOwnProperty('state')){
          //     console.log('go to switch');
                switch(msg.state){
                    case 'Ringing':
                        $("#state").html('Входящий звонок по линии: '+msg.did);
                        $("#state").slideDown('fast');
                       localStorage.setItem('pstate', 'Ringing');
                        break;
                    case 'Up':
                        console.log('Up!')
                        $("#state").html('Абонент на линии: '+msg.callerid);
                        console.log('state set up')
                        $("#state").slideDown('fast');
                        console.log('slide down complete')
                        var pstate = localStorage.getItem('pstate');
                        if (typeof pstate == "undefined") {
                                localStorage.setItem('pstate', 'Idle');
                                pstate = 'Idle'
                            }
                        if ((pstate=='Ringing' || pstate=='Idle') && (localStorage.getItem('windowId')!=msg.channel)){
                            console.log('OPENING WINDOW');
                            window.open("./index.php?module=orders&act=new&num="+msg.callerid);
                            localStorage.setItem('windowId', msg.channel);
                        }
                        localStorage.setItem('pstate', 'Up');
                        break;
                }

            } else {
                $("#state").hide();
                $("#state").html(''); 
                localStorage.setItem('pstate', 'Idle');
            }
                console.log(localStorage.getItem('pstate'));
               console.log(localStorage.getItem('windowId'));
        }
    })
  }, 4000);
   //   });
  }
);
