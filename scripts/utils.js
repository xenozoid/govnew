function changeHandler(event)
{
    if(!event.data)
        return;

    var source = event.data.sourceField;
    if(!window[source+'EventCounter'])
        window[source+'EventCounter'] = 0;
    window[source+'EventCounter']++;
    var target = event.data.targetField;
    var url = event.data.url;
    url = url.replace('%id%', $('#' + source).val());
    if(matches = url.match(/%([\w\_\-\d]+)%/))
    {
        for(var i = 1; i < matches.length; i++)
        {
            url = url.replace(/%([\w\_\-\d]+)%/g, $('#' + matches[i]).val());
        }
    }
    var emptyOption = event.data.emptyOption;
    var emptyOptionSuccess = event.data.emptyOptionSuccess;

    $.ajax({
        type: 'post',
        url: encodeURI(url),
        data: 'ajax=1',
        counter: window[source+'EventCounter'],
        success: function(msg)
        {
            if(this.counter < window[source+'EventCounter'])
            {
                return;
            }

            $('#' + target + ' option').remove();
            if(emptyOption)
            {
                $('#' + target).append('<option>' + emptyOption + '</option>');
            }

            if(msg == '') return;
            
            var success = false;
            var data = eval('(' + msg + ')');
            for(var i = 0; i < data.length; i++)
            {
                var option_data = data[i];
                if((!success) && emptyOptionSuccess && emptyOptionSuccess.length)
                {
                    $('#' + target + ' option').remove();
                    $('#' + target).append('<option>' + emptyOptionSuccess + '</option>');
                    success = true;
                }

                $('#' + target).append('<option value="' + option_data.id + '">' + option_data.value + '</option>');
            }

            if(event && event.data && event.data.onSuccess && typeof event.data.onSuccess == 'function')
                event.data.onSuccess();
        }
    });
}

function isValidINN(i)
{
    if ( i.match(/\D/) ) return false;

    var inn = i.match(/(\d)/g);

    if ( inn.length == 10 )
    {
        return inn[9] == String(((
            2*inn[0] + 4*inn[1] + 10*inn[2] +
            3*inn[3] + 5*inn[4] +  9*inn[5] +
            4*inn[6] + 6*inn[7] +  8*inn[8]
        ) % 11) % 10);
    }
    else if ( inn.length == 12 )
    {
        return inn[10] == String(((
             7*inn[0] + 2*inn[1] + 4*inn[2] +
            10*inn[3] + 3*inn[4] + 5*inn[5] +
             9*inn[6] + 4*inn[7] + 6*inn[8] +
             8*inn[9]
        ) % 11) % 10) && inn[11] == String(((
            3*inn[0] +  7*inn[1] + 2*inn[2] +
            4*inn[3] + 10*inn[4] + 3*inn[5] +
            5*inn[6] +  9*inn[7] + 4*inn[8] +
            6*inn[9] +  8*inn[10]
        ) % 11) % 10);
    }

    return false;
}

function isValidOGRN(i)
{
    if ( i.match(/\D/) ) return false;

    var ogrn = i;

    if(ogrn.length != 13 && ogrn.length != 15)
        return false;

    var test = ogrn.substring(0, ogrn.length-1);
    var test2 = parseInt(parseInt(test) / (ogrn.length-2));
    var test3 = test2 * (ogrn.length-2);
    var test4 = parseInt(test) - test3 + '';
    if(test4.charAt(test4.length-1) == ogrn.charAt(ogrn.length-1))
        return true;

    return false;
}

function zeroPrefix(value)
{
    if(parseInt(value) < 10)
        return '0' + value;

    return value;
}

function shortString(string, size)
{
    if(!size)
        size = 50;

    if(string.length <= size)
        return string;
    else
        return string.substring(0, size) + '...';
}