/* Author: Andrew KLADOV
 * Description: Ami request for ingoing call info, parse returned data.
 * Date: 19.10.2012
 * DEV_ID: GAU_2012
 *                 |-->BRANCH: ASTERISK
 *                                  |-->BRANCH: INGOING
 */
$(document).ready(
  function(){
      $("#state").everyTime(1000, "amiAsk", function(){
          $.ajax({
        url: 'request.php',
        type: "POST",
        data: {act: 'get_incoming',ext: $("#exten").val()},
        async: false,
        dataType: "json",
        success: function(msg){
            if (msg!=null){
                switch(msg.state){
                    case 'Ringing':
                        $("#state").html('Входящий звонок по линии: '+msg.did);
                        $("#state").slideDown('fast');
                       // $("#pstate").val('Ringing');
                       localStorage.setItem('pstate', 'Ringing');
                        break;
                    case 'Up':
                        $("#state").html('Абонент на линии: '+msg.callerid);
                        $("#state").slideDown('fast');
                       
                        //if ($("#pstate").val()=='Ringing' || $("#pstate").val()=='Idle'){
                        var pstate = localStorage.getItem('pstate');
                        if (typeof pstate == "undefined") {
                                localStorage.setItem('pstate', 'Idle');
                            }
                        if ((pstate=='Ringing' || pstate=='Idle') && (localStorage.getItem('windowId')!=msg.channel)){
                        //    console.log('OPENING WINDOW');
                            window.open("./index.php?module=orders&act=new&num="+msg.callerid);
                            localStorage.setItem('windowId', msg.channel);
                        }
                        localStorage.setItem('pstate', 'Up');
                        //$("#pstate").val('Up');
                        break;
                }
            } else {
                $("#state").slideUp('fast', function(){
                                    $("#state").html('');
                                    localStorage.setItem('pstate', 'Idle');
                });
            }
        }
    })
    
      });
  }
);
