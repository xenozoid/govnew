ALTER TABLE departments ADD archive INT NOT NULL DEFAULT 0;

--

CREATE TABLE topics (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255),
  PRIMARY KEY (id)
);

INSERT INTO topics (name) VALUES
('Жилищно-коммунальная сфера')
,('Экономика, хозяйственная деятельность')
,('Государство, общество, политика')
,('Социальная, здравоохранение, образование')
,('Оборона, безопасность, законность')
;

ALTER TABLE orders ADD topic_id INT;

INSERT INTO object_types (name, code, group_id, editable, weight, module, icon) VALUES
('Тематика', 'OBJECT_TOPICS', 2, 0, 0, 'topics', NULL);
