<?php

echo '<html><head><meta charset="utf-8"></head><body>';

chdir('/var/www/vhosts/gov29');

require_once "./config.php";
require_once "./framework/Debug.php";
require_once "./framework/DT.php";
require_once "./framework/SqlObject.php";
require_once "./framework/Query.php";
require_once "./framework/Where.php";
require_once "./framework/Log.php";

ini_set('display_errors', true);
error_reporting(E_ALL^E_NOTICE);

$server = '{imap.gmail.com:993/imap/ssl}INBOX';
$login = 'gkh@firecall.ru';
$password = 'firegkh42';

$inbox = imap_open($server, $login, $password);
if(!$inbox)
    echo 'ERROR opening mailbox!';
else
    echo "CONNECTION successful!\n";
$emails = imap_search($inbox, 'UNSEEN');
if($emails)
{
    rsort($emails);
    foreach($emails as $email)
    {
        $header = imap_headerinfo($inbox, $email);
        $from = $header->fromaddress;
        $subject = $header->subject;
        if(preg_match('/TID#(\d+)/', $subject, $matches))
        {
            $external_id = $matches[1];
            echo "External ID ".$external_id."\n";
        }
        $lines = imap_body($inbox, $email);
        $lines = str_replace("\r\n", "\n", $lines);
        $lines = explode("\n", $lines);
        $result = array();
        $questionMode = false;
        foreach($lines as $line)
        {
            $line = iconv('windows-1251', 'utf8', $line);

            if(preg_match('/^Тип: (.*)/', $line, $matches))
            {
                $result['department'] = $matches[1];
                $sql = new SqlObject();
                $dept = $sql->getline("select id from departments where name = '".$result['department']."'");
                if($dept['id'])
                    $result['department_id'] = $dept['id'];

                $questionMode = false;
            }
            else if(preg_match('/^Заголовок: (.*)/', $line, $matches))
            {
                $result['name'] = $matches[1];

                $questionMode = false;
            }
            else if(preg_match('/^Вопрос: (.*)/', $line, $matches))
            {
                $result['question'] = $matches[1];
                $questionMode = true;
            }
            else if(preg_match('/^Фамилия: (.*)/', $line, $matches))
            {
                $result['lastname'] = $matches[1];
                $questionMode = false;
            }
            else if(preg_match('/^Имя: (.*)/', $line, $matches))
            {
                $result['firstname'] = $matches[1];
                $questionMode = false;
            }
            else if(preg_match('/^Отчество: (.*)/', $line, $matches))
            {
                $result['middlename'] = $matches[1];
                $questionMode = false;
            }
            else if(preg_match('/^Email: (.*)/', $line, $matches))
            {
                $result['email'] = $matches[1];
                $questionMode = false;
            }
            else if(preg_match('/^Телефон: (.*)/', $line, $matches))
            {
                $result['phone'] = $matches[1];
                $questionMode = false;
            }
            else if(preg_match('/^Район: (.*)/', $line, $matches))
            {
                $matches[1] = str_replace('г. ', '', $matches[1]);
                $matches[1] = str_replace('р-н ', '', $matches[1]);
                $result['region'] = mb_substr($matches[1], 0, 4, 'UTF-8');
                echo 'SEARCHING FOR: ' . $result['region'] . '!';
                $sql = new SqlObject();
                $q = "select id from regions where name like '".$result['region']."%'";
                $region = $sql->getline($q);
                echo 'SQL:' . $q . '!';
                if($region['id'])
                {
                    $result['region_id'] = $region['id'];
                    echo 'REGION FOUND!';
                }

                $questionMode = false;
            }
            else if(preg_match('/^Город: (.*)/', $line, $matches))
            {
                $result['city'] = $matches[1];
                $questionMode = false;

//                $sql = new SqlObject();
//                $region = $sql->getline("select region_id from cities where name = '".$result['city']."'");
//                if($region['region_id'])
//                    $result['region_id'] = $region['region_id'];

            }
            else if(preg_match('/^Адрес: (.*)/', $line, $matches))
            {
                $result['address'] = $matches[1];
                $questionMode = false;
            }
            else if(preg_match('/^День рождения: (.*)/', $line, $matches))
            {
                if($matches[1])
                    $result['age'] = (int)floor((time() - DT::getUnixTime($matches[1]))/(365*24*60*60));
                $questionMode = false;
            }
            else if(preg_match('/^Пол: (.*)/', $line, $matches))
            {
                if($matches[1] == 'F')
                    $result['sex'] = 2;
                else
                    $result['sex'] = 1;

                $questionMode = false;
            }
            else if($questionMode)
            {
                $result['question'] .= $line;
            }
        }

        print_r($result);

        if((int)$result['region_id'])
        {
            $query = new Query("insert");
            $query->AddTable("orders");
            $query->AddField("created", 'inline:UNIX_TIMESTAMP()');
            $query->AddField("source", 2);
            $query->AddField("project_id", 3);
            $query->AddField("lastname", $result['lastname']);
            $query->AddField("firstname", $result['firstname']);
            $query->AddField("middlename", $result['middlename']);
            $query->AddField("sex", $result['sex']);
            $query->AddField("age", $result['age']);
            $query->AddField("city", $result['city']);
            $query->AddField("region_id", (int)$result['region_id']);
            $query->AddField("address", $result['address']);
            $result['phone'] = preg_replace('/\D/', '', $result['phone']);
            if(preg_match('/(.{10})$/', $result['phone'], $matches))
                $result['phone'] = $matches[1];
            $query->AddField("phone", $result['phone']);
            $query->AddField("email", $result['email']);
            $query->AddField("name", $result['name']);
            $query->AddField("question", $result['question']);
            $query->AddField("type", 2);
            $query->AddField("status", 5);
            $query->AddField("department_id", $result['department_id']);
            $query->AddField("external_id", $external_id);
            $query->AddField("external_email", $from);
            $query->Execute();
            $id = $query->last_insert_id();

            echo "ID: ".$id."\n";

            Log::Add(OBJECT_ORDERS, 'orders', 'Создание вопроса', $id);
            if($result['department_id'])
            {
                Log::Add(OBJECT_ORDERS, 'orders', 'Назначение ответственного', $id, '', $result['department']);
            }
        }
    }
}
else
    echo 'ERROR listing inbox!';
