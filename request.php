<?
/* Author: Andrew KLADOV
 * Description: Ami request for ingoing call info, parse returned data.
 * Date: 19.10.2012
 * DEV_ID: GAU_2012
 *                 |-->BRANCH: ASTERISK
 *                                  |-->BRANCH: INGOING
 */

require_once 'asm.php';
require_once "framework_loader.php";
require_once "classes.php";

function getProjectPhones()
{
    $result = array();
    $ds = new DataSource("select phone from projects where phone is not null and phone != ''");
    while($data = $ds->GetString())
        $result[] = $data['phone'];
    return $result;
}

function getActiveUserPhones()
{
    $result = array();
    $ds = new DataSource("select internal_num from users where internal_active = 1 and internal_num is not null and internal_num != '' and last_activity >= " . (time() - 15 * 60));
    while($data = $ds->GetString())
        $result[] = $data['internal_num'];
    return $result;
}



$watch_exten = getActiveUserPhones();
$monitored_dids = getProjectPhones();
$ami_connect_params = array('ami_host' => Registry::Get('ats_host', '192.168.1.3'),
    'ami_user' => Registry::Get('ats_user', 'admin'),
    'ami_secret' => Registry::Get('ats_password', 'elastix456'));



$astman = new AGI_AsteriskManager();
$astman->connect($ami_connect_params['ami_host'], $ami_connect_params['ami_user'], $ami_connect_params['ami_secret']);


switch ($_POST['act']) {
    case 'get_incoming':
        $target_exten = $_POST['ext'];
        $ans=$astman->Command('show channels');
        $extensions = findout_ext_lines($ans['data'], $watch_exten);
        $extensions = findoutStates($astman, $extensions);
        if (count($extensions)>0){
            echo json_encode($extensions[$target_exten]);
        } else {
            $callers['numb']=0;
            echo json_encode($callers);
        }
        break;
    case 'dialout':
        @session_start();
        switch($_SESSION['user_data']['outgoing_phone'])
        {
            case 1:
                $outgoing_number = $_SESSION['user_data']['phone'];
                break;

            case 2:
                $outgoing_number = $_SESSION['user_data']['cellphone'];
                break;

            default:
                $outgoing_number = $_SESSION['user_data']['internal_num'];
        }

        $dcontext = $_REQUEST['context'];
        $channel = $_SESSION['user_data']['channel'];  
        $user_phone = $outgoing_number; 
        $ans = $astman->Originate($channel.'/'.$user_phone,'8'.$_POST['bnumber'], $dcontext, 1);
        var_dump($ans['data']);
        break;
    default:
        break;
}

$astman->Logoff();
function findout_ext_lines ($ans, $watch_exten = array()) {
    $extStates = array();
    $ans_exp = explode("\r\n", $ans);
    $ans_data = explode("\n",$ans_exp[1]);
    unset($ans_data[0]);
    for ($i=count($ans_data)-2;$i<=count($ans_data); $i++){
        unset($ans_data[$i]);
    }
    array_pop($ans_data);
    foreach ($ans_data as $key => $value) {
        $rs = explode(' ', $ans_data[$key]);
        foreach ($rs as $fkey=>$fval){
            foreach ($watch_exten as $wExten) {
                if (preg_match('(SIP/'.$wExten.'-\w+)', $fval, $matches)){
                    $extStates[$wExten] = array('channel'=>$matches[0]);
                }
                if (preg_match('(IAX2/.+\s+'.$wExten.')', $fval, $matches)){
                    $extStates[$wExten] = array('channel'=>$matches[0]);
                }
            }

        }
    }
    return $extStates;
}

function findoutStates($astman, $extensions = array()){
    foreach ($extensions as $key => $value) {
        $ans = $astman->Command('show channel '.$value['channel']);
        if (preg_match("\n\r(State:\s+\w+)", $ans['data'], $matches)){
            $extensions[$key]['state'] = substr($matches[0], 7);
        }
        
        if (preg_match("\n\r(UniqueID:\s+\w+)", $ans['data'], $matches)) {
            $extensions[$key]['uid'] = substr($matches[0], 10);
        }
        
        
        if ($extensions[$key]['state']=='Up'){
            if (preg_match("#(?<!\])\bBRIDGEPEER=[^\s\[<]+#i", $ans['data'], $matches)){
                $extensions[$key]['bridged'] = substr($matches[0],11);
                $callerid = $astman->Command('show channel '.$extensions[$key]['bridged']);
                if (preg_match("\n\r(Caller\sID:\s\d+)",$callerid['data'],$matches)){
                    $extensions[$key]['callerid'] = substr($matches[0],11);
                    unset($extensions[$key]['bridged']);
                }
            }
        }
        if (preg_match("\n\r(FROM_DID=\d+)", $ans['data'], $matches)){
            $did = substr($matches[0], 9);
            $extensions[$key]['did'] = $did;
        }
        if (preg_match("\n\r(Application: AppDial)", $ans['data'], $appm)){
            $app = substr($appm[0], 13);
            $extensions[$key]['app']=$app;
        }

        if (($extensions[$key]['app'] != 'AppDial') | !isset($extensions[$key]['app'])){
            unset($extensions[$key]);
        }
    }
    return $extensions;
}
